<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Masuk - Prakarsa Putra</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/adminlte.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/toastr/toastr.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/style.css">
    <!-- Perpage CSS -->
    <?php if (isset($style['css'])) : ?>
        <link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/custom/<?= $style['css'] ?>">
    <?php endif ?>

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="card">
            <div class="card-body login-card-body">
                <div class="login-logo">
                    <a href="/"><b>PRAKARSA</b> PUTRA</a>
                </div>
                <hr class="mt-0 w-25">
                <!-- /.login-logo -->
                <p class="login-box-msg">SELAMAT DATANG! Silahkan masuk</p>

                <form action="<?= base_url('auth/login') ?>" method="post">
                    <div class="input-group mb-3">
                        <input type="text" name="username" value="<?= set_value('username') ?>" class="form-control <?= form_error('username') ? 'is-invalid' : '' ?>" id="username" placeholder="Username .." autofocus required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <div id="username" class="invalid-feedback">
                            <?= form_error('username') ?>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" value="<?= set_value('password') ?>" class="form-control <?= form_error('password') ? 'is-invalid' : '' ?>" id="password" placeholder="Password .." required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div id="password" class="invalid-feedback">
                            <?= form_error('password') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8 d-flex align-items-center">
                            <a href="forgot-password.html">Lupa Password?</a>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?= base_url('assets') ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets') ?>/dist/js/adminlte.js"></script>
    <!-- Toastr -->
    <script src="<?= base_url('assets') ?>/plugins/toastr/toastr.min.js"></script>
    <?php if ($this->session->flashdata('message_login_error')) : ?>
        <script>
            toastr.error("<?= $this->session->flashdata('message_login_error') ?>", "Gagal Masuk", {
                "progressBar": true,
                "positionClass": "toast-top-right",
                "timeOut": "5000",
            });
        </script>
    <?php endif ?>

    <!-- Perpage JS -->
    <?php if (isset($style['js'])) : ?>
        <script src="<?= base_url('assets') ?>/dist/js/custom/<?= $style['js'] ?>"></script>
    <?php endif ?>
</body>

</html>