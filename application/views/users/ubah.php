<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('customers') ?>">Data Customers</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <form action="<?= base_url('customers/aksi_ubah') ?>" method="POST">
                            <input type="hidden" name="id" value="<?= $customer->id ?>">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-sm-10">
                                        <label for="nama">Nama</label>
                                        <input type="text" name="nama" value="<?= set_value('nama', $customer->nama) ?>" class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" id="nama" placeholder="Nama Customers .." autofocus required>
                                        <div id="nama" class="invalid-feedback">
                                            <?= form_error('nama') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="telefon">Telefon</label>
                                        <input type="text" name="telefon" value="<?= set_value('telefon', $customer->telefon) ?>" class="form-control <?= form_error('telefon') ? 'is-invalid' : '' ?>" id="telefon" placeholder="Nomor Telefon ..">
                                        <div id="telefon" class="invalid-feedback">
                                            <?= form_error('telefon') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" value="<?= set_value('email', $customer->email) ?>" class="form-control <?= form_error('email') ? 'is-invalid' : '' ?>" id="email" placeholder="Alamat Email ..">
                                        <div id="email" class="invalid-feedback">
                                            <?= form_error('email') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row mb-0">
                                    <div class="form-group col-md-12">
                                        <label for="alamat">Alamat</label>
                                        <textarea name="alamat" class="form-control <?= form_error('alamat') ? 'is-invalid' : '' ?>" id="alamat" rows="2" placeholder="Alamat .."><?= set_value('alamat', $customer->alamat) ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer text-right">
                                <a href="<?= base_url('customers') ?>" class="btn btn-md btn-success float-left">
                                    <i class="fas fa-arrow-left mr-2"></i>
                                    Kembali
                                </a>
                                <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button>
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->