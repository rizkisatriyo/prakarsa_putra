<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('users') ?>">Data Pengguna</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<form action="<?= base_url('users/aksi_tambah') ?>" method="POST">
							<div class="card-body">
								<div class="row">
									<div class="col-md-7">
										<div class="form-row">
											<div class="form-group col-sm-10">
												<label for="nama">Nama Lengkap</label>
												<input type="text" name="name" value="<?= set_value('name') ?>" class="form-control <?= form_error('name') ? 'is-invalid' : '' ?>" id="nama" placeholder="Nama Lengkap .." autofocus required>
												<div id="nama" class="invalid-feedback">
													<?= form_error('name') ?>
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="telefon">Telefon</label>
												<input type="text" name="phone" value="<?= set_value('phone') ?>" class="form-control <?= form_error('phone') ? 'is-invalid' : '' ?>" id="telefon" placeholder="Nomor Telefon ..">
												<div id="telefon" class="invalid-feedback">
													<?= form_error('phone') ?>
												</div>
											</div>
											<div class="form-group col-md-6">
												<label for="email">Email</label>
												<input type="email" name="email" value="<?= set_value('email') ?>" class="form-control <?= form_error('email') ? 'is-invalid' : '' ?>" id="email" placeholder="Alamat Email ..">
												<div id="email" class="invalid-feedback">
													<?= form_error('email') ?>
												</div>
											</div>
										</div>
										<div class="form-row mb-0">
											<div class="form-group col-md-12">
												<label for="alamat">Alamat</label>
												<textarea name="address" class="form-control <?= form_error('address') ? 'is-invalid' : '' ?>" id="alamat" rows="2" placeholder="Alamat .."><?= set_value('address') ?></textarea>
											</div>
										</div>
									</div>
									<div class="col offset-md-1">
										<div class="form-row">
											<div class="form-group col-sm-9">
												<label for="username">Username</label>
												<input type="text" name="username" value="<?= set_value('username') ?>" class="form-control <?= form_error('username') ? 'is-invalid' : '' ?>" id="username" placeholder="Username .." required>
												<div id="username" class="invalid-feedback">
													<?= form_error('username') ?>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="password">Password</label>
											<input type="password" name="password" value="<?= set_value('password') ?>" class="form-control <?= form_error('password') ? 'is-invalid' : '' ?>" id="password" placeholder="Masukkan password .." required>
											<div id="password" class="invalid-feedback">
												<?= form_error('password') ?>
											</div>
										</div>
										<div class="form-group">
											<label for="password_confirmation">Konfirmasi password</label>
											<input type="password" name="password_confirmation" value="<?= set_value('password_confirmation') ?>" class="form-control <?= form_error('password_confirmation') ? 'is-invalid' : '' ?>" id="password_confirmation" placeholder="Masukkan ulang password .." required>
											<div id="password_confirmation" class="invalid-feedback">
												<?= form_error('password_confirmation') ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer text-right">
								<button type="button" onclick="history.back()" class="btn btn-md btn-success float-left">
									<i class="fas fa-arrow-left mr-2"></i>
									Kembali
								</button>
								<button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button>
								<button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Simpan</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->