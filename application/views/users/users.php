<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    $current_user = $this->auth_model->current_user();
                    if ($current_user->role == 'Administrator') :
                    ?>
                        <a href="<?= base_url('users/tambah') ?>" class="btn btn-md btn-primary">
                            <i class="fas fa-plus-circle mr-2"></i>
                            Tambah Pengguna
                        </a>
                    <?php else : ?>
                        <h1 class="mb-0"><?= $title ?></h1>
                    <?php endif ?>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="users" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Avatar</th>
                                        <th>Nama</th>
                                        <th>Telefon</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <!-- <th>Sisa Piutang</th>
                                        <th>Piutang/Deposit</th> -->
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($users as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <?php
                                            $current_user = $this->auth_model->current_user();
                                            $avatar = $current_user->avatar ? base_url('assets/dist/upload/img/avatar/' . $current_user->avatar) : get_gravatar($current_user->email)
                                            ?>
                                            <td class="text-center">
                                                <a href="<?= base_url('users/detail/') . $item['id'] ?>">
                                                    <img src="<?= $avatar ?>" class="user-image img-circle elevation-2" alt="User Image" style="width: 35px; height: 35px; object-fit: cover;">
                                                </a>
                                            </td>
                                            <td><?= $item['name'] ?></td>
                                            <td class="text-center"><?= $item['phone'] ?></td>
                                            <td><?= $item['email'] ?></td>
                                            <td class="text-center">
                                                <?php if ($item['role'] == 'Administrator') : ?>
                                                    <span class="badge badge-danger"><?= $item['role'] ?></span>
                                                <?php else : ?>
                                                    <span class="badge badge-warning"><?= $item['role'] ?></span>
                                                <?php endif ?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($item['status'] == 1) : ?>
                                                    <span class="badge badge-success">Aktif</span>
                                                <?php else : ?>
                                                    <span class="badge badge-danger">Nonaktif</span>
                                                <?php endif ?>
                                            </td>
                                            <!-- <td class="<?= $item['sisa_piutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['sisa_piutang'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="<?= $item['piutang_deposit'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['piutang_deposit'], 0, ',', '.') ?>,-</span>
                                            </td> -->
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('users/detail/') . $item['id'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                    <?php
                                                    if ($current_user->role == 'Administrator') :
                                                    ?>
                                                        <a href="<?= base_url('users/ubah/') . $item['id'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                                        <a href="<?= base_url('users/hapus/') . $item['id'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a>
                                                    <?php endif ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->