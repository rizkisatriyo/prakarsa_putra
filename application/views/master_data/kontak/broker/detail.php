<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="mb-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('broker') ?>">Broker</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <?php
                            $avatar = $broker->avatar ?
                                base_url('assets/dist/upload/img/avatar/broker/' . $broker->avatar)
                                : get_gravatar($broker->email)
                            ?>
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="<?= $avatar ?>" alt="User profile picture" style="object-fit: cover;">
                            </div>

                            <h3 class="profile-username text-center"><?= html_escape($broker->nama) ?></h3>

                            <p class="text-muted text-center"><?= html_escape($broker->email) ?></p>

                            <button type="button" onclick="history.back()" class="btn btn-success btn-block">
                                <i class="fas fa-arrow-left mr-2"></i><b>Kembali</b>
                            </button>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#about" data-toggle="tab">Detail Broker</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="about">
                                    <strong><i class="fas fa-edit mr-1"></i> Nama</strong>
                                    <p class="text-muted"><?= $broker->nama ?></p>
                                    <hr>

                                    <strong><i class="fas fa-edit mr-1"></i> Singkatan</strong>
                                    <p class="text-muted"><?= $broker->singkatan ?></p>
                                    <hr>

                                    <?php
                                    $current_user = $this->auth_model->current_user();
                                    if ($current_user->role == 'Administrator') :
                                    ?>

                                        <strong><i class="fas fa-phone fa-flip-horizontal mr-1"></i> No. Telefon</strong>
                                        <p class="text-muted"><?= $broker->telefon ?></p>
                                        <hr>

                                        <strong><i class="fas fa-envelope mr-1"></i> Email</strong>
                                        <p class="text-muted"><?= $broker->email ?></p>
                                        <hr>

                                    <?php endif ?>

                                    <strong><i class="fas fa-file-alt mr-1"></i> Alamat</strong>
                                    <p class="text-muted"><?= nl2br($broker->alamat) ?></p>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->