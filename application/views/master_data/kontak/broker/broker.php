<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    $current_user = $this->auth_model->current_user();
                    if ($current_user->role == 'Administrator') :
                    ?>
                        <a href="<?= base_url('broker/tambah') ?>" class="btn btn-md btn-primary">
                            <i class="fas fa-plus-circle mr-2"></i>
                            Tambah Broker
                        </a>
                    <?php else : ?>
                        <h1 class="mb-0"><?= $title ?></h1>
                    <?php endif ?>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="broker" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Avatar</th>
                                        <th>Nama</th>
                                        <th>Singkatan</th>
                                        <?php
                                        $current_user = $this->auth_model->current_user();
                                        if ($current_user->role == 'Administrator') :
                                        ?>
                                            <th>Telefon</th>
                                            <th>Email</th>
                                        <?php endif ?>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($broker as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <?php $avatar = $item['avatar'] ? base_url('assets/dist/upload/img/avatar/broker/' . $item['avatar']) : get_gravatar($item['email']) ?>
                                            <td class="text-center">
                                                <a href="<?= base_url('broker/detail/') . $item['id'] ?>">
                                                    <img src="<?= $avatar ?>" class="user-image img-circle elevation-2" alt="User Image" style="width: 35px; height: 35px; object-fit: cover;">
                                                </a>
                                            </td>
                                            <td><?= $item['nama'] ?></td>
                                            <td><?= $item['singkatan'] ?></td>
                                            <?php
                                            $current_user = $this->auth_model->current_user();
                                            if ($current_user->role == 'Administrator') :
                                            ?>
                                                <td class="text-center"><?= $item['telefon'] ?></td>
                                                <td><?= $item['email'] ?></td>
                                            <?php endif ?>
                                            <td><?= $item['alamat'] ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('broker/detail/') . $item['id'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                    <?php
                                                    $current_user = $this->auth_model->current_user();
                                                    if ($current_user->role == 'Administrator') :
                                                    ?>
                                                        <a href="<?= base_url('broker/ubah/') . $item['id'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                                        <a href="<?= base_url('broker/hapus/') . $item['id'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a>
                                                    <?php endif ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->