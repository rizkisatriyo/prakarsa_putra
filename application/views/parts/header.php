<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?> - Prakarsa Putra</title>

	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bootstrap 4 -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/summernote/summernote-bs4.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/toastr/toastr.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/daterangepicker/daterangepicker.css">
	<!-- jQuery -->
	<script src="<?= base_url('assets') ?>/plugins/jquery/jquery.min.js"></script>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets') ?>/dist/favicon.ico" type="image/x-icon">
	<!-- Style -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/style.css">
	<!-- Perpage CSS -->
	<?php if (isset($style['css'])) : ?>
		<link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/custom/<?= $style['css'] ?>">
	<?php endif ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link nav-toggle" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
				</li>
				<!-- <li class="nav-item d-none d-sm-inline-block">
					<a href="index3.html" class="nav-link">Home</a>
				</li>
				<li class="nav-item d-none d-sm-inline-block">
					<a href="#" class="nav-link">Contact</a>
				</li> -->
			</ul>

			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Navbar Search -->
				<!-- <li class="nav-item">
					<a class="nav-link" data-widget="navbar-search" href="#" role="button">
						<i class="fas fa-search"></i>
					</a>
					<div class="navbar-search-block">
						<form class="form-inline">
							<div class="input-group input-group-sm">
								<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
								<div class="input-group-append">
									<button class="btn btn-navbar" type="submit">
										<i class="fas fa-search"></i>
									</button>
									<button class="btn btn-navbar" type="button" data-widget="navbar-search">
										<i class="fas fa-times"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</li> -->

				<!-- Notifications Dropdown Menu -->
				<!-- <li class="nav-item dropdown">
					<a class="nav-link" data-toggle="dropdown" href="#">
						<i class="far fa-bell"></i>
						<span class="badge badge-warning navbar-badge">15</span>
					</a>
					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
						<span class="dropdown-item dropdown-header">15 Notifications</span>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item">
							<i class="fas fa-envelope mr-2"></i> 4 new messages
							<span class="float-right text-muted text-sm">3 mins</span>
						</a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item">
							<i class="fas fa-users mr-2"></i> 8 friend requests
							<span class="float-right text-muted text-sm">12 hours</span>
						</a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item">
							<i class="fas fa-file mr-2"></i> 3 new reports
							<span class="float-right text-muted text-sm">2 days</span>
						</a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
					</div>
				</li> -->
				<!-- <li class="nav-item">
					<a class="nav-link" data-widget="fullscreen" href="#" role="button">
						<i class="fas fa-expand-arrows-alt"></i>
					</a>
				</li> -->
				<li class="nav-item dropdown user user-menu">
					<?php
					$current_user = $this->auth_model->current_user();
					$avatar = $current_user->avatar ? base_url('assets/dist/upload/img/avatar/' . $current_user->avatar) : get_gravatar($current_user->email)
					?>
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
						<img src="<?= $avatar ?>" class="user-image img-circle elevation-2" alt="User Image">
						<span class="hidden-xs"><?= $current_user->name ?></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
						<!-- User image -->
						<li class="user-header bg-primary">
							<img src="<?= $avatar ?>" class="img-circle elevation-2" alt="User Image">
							<p>
								<?= $current_user->name ?>
								<small><?= $current_user->email ?></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="float-left">
								<a href="<?= base_url('profile') ?>" class="btn btn-default btn-flat">Profil</a>
							</div>
							<div class="float-right">
								<a href="<?= base_url('auth/logout') ?>" class="btn btn-default btn-flat">Keluar</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="<?= base_url() ?>" class="brand-link">
				<img src="<?= base_url('assets') ?>/dist/img/logo.png" alt="PPB Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
				<span class="brand-text font-weight-light"><b>PRAKARSA</b> PUTRA</span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar user panel (optional) -->
				<!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
					<div class="image">
						<img src="<?= base_url('assets') ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
					</div>
					<div class="info">
						<a href="<?= base_url('profile') ?>" class="d-block">Administrator</a>
					</div>
				</div> -->

				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<li class="nav-item">
							<a href="<?= base_url() ?>" class="nav-link <?= ($this->uri->segment(1) == 'dashboard') || ($this->uri->segment(1) == '') ? 'active' : '' ?>">
								<i class="nav-icon fas fa-chart-pie"></i>
								<p>Dashboard</p>
							</a>
						</li>
						<li class="nav-header">MASTER DATA</li>
						<li class="nav-item">
							<a href="<?= base_url('kandang') ?>" class="nav-link <?= $this->uri->segment(1) == 'kandang' ? 'active' : ''; ?>">
								<i class="nav-icon fas fa-warehouse"></i>
								<p>Master Kandang</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'suppliers') || ($this->uri->segment(1) == 'customers') || ($this->uri->segment(1) == 'broker') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-address-card"></i>
								<p>
									Master Kontak
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('suppliers') ?>" class="nav-link <?= $this->uri->segment(1) == 'suppliers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-industry ml-3"></i>
										<p>Suppliers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('customers') ?>" class="nav-link <?= $this->uri->segment(1) == 'customers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-users ml-3"></i>
										<p>Customers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('broker') ?>" class="nav-link <?= $this->uri->segment(1) == 'broker' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-house-user ml-3"></i>
										<p>Brokers</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-header">TRANSAKSI</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'pembelian') || ($this->uri->segment(1) == 'pembayaran_pembelian') || ($this->uri->segment(1) == 'penerimaan_ayam') || ($this->uri->segment(1) == 'susut_pembelian') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-truck fa-flip-horizontal"></i>
								<p>
									Data Pembelian
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('pembelian') ?>" class="nav-link <?= $this->uri->segment(1) == 'pembelian' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-shopping-cart fa-flip-horizontal ml-3"></i>
										<p>Pembelian</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('pembelian_broker') ?>" class="nav-link <?= $this->uri->segment(1) == 'pembelian_broker' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-shopping-cart fa-flip-horizontal ml-3"></i>
										<p>Pembelian Broker</p>
									</a>
								</li>

								<li class="nav-item">
									<a href="<?= base_url('pembayaran_pembelian') ?>" class="nav-link <?= $this->uri->segment(1) == 'pembayaran_pembelian' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-money-bill fa-flip-horizontal ml-3"></i>
										<p>Pembayaran</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('penerimaan_ayam') ?>" class="nav-link <?= $this->uri->segment(1) == 'penerimaan_ayam' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-hand-holding fa-flip-horizontal ml-3"></i>
										<p>Penerimaan</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('susut_pembelian') ?>" class="nav-link <?= $this->uri->segment(1) == 'susut_pembelian' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-crop ml-3"></i>
										<p>Rugi / Susut</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'penjualan') || ($this->uri->segment(1) == 'penjualan_broker') || ($this->uri->segment(1) == 'pembayaran_penjualan') || ($this->uri->segment(1) == 'pembayaran_penjualan_broker') || ($this->uri->segment(1) == 'penjualan_checker') || ($this->uri->segment(1) == 'susut_penjualan') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fab fa-shopify"></i>
								<p>
									Data Penjualan
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item <?= ($this->uri->segment(1) == 'penjualan') || ($this->uri->segment(1) == 'penjualan_broker') ? 'menu-open' : ''; ?>">
									<a href="#" class="nav-link">
										<i class="nav-icon fas fa-shopping-cart ml-3"></i>
										<p>
											Penjualan
											<i class="right fas fa-angle-right"></i>
										</p>
									</a>
									<ul class="nav nav-treeview <?= ($this->uri->segment(1) == 'penjualan') || ($this->uri->segment(1) == 'penjualan_broker') ? 'menu-open' : ''; ?>" style="display: none;">
										<li class="nav-item">
											<a href="<?= base_url('penjualan') ?>" class="nav-link <?= $this->uri->segment(1) == 'penjualan' ? 'active' : ''; ?>">
												<i class="nav-icon fas fa-users ml-5"></i>
												<p>RPA</p>
											</a>
										</li>
										<li class="nav-item">
											<a href="<?= base_url('penjualan_broker') ?>" class="nav-link <?= $this->uri->segment(1) == 'penjualan_broker' ? 'active' : ''; ?>">
												<i class="nav-icon fas fa-house-user ml-5"></i>
												<p>BROKER</p>
											</a>
										</li>
									</ul>
								</li>
								<li class="nav-item <?= ($this->uri->segment(1) == 'pembayaran_penjualan') || ($this->uri->segment(1) == 'pembayaran_penjualan_broker') ? 'menu-open' : ''; ?>">
									<a href="#" class="nav-link">
										<i class="nav-icon fas fa-money-bill ml-3"></i>
										<p>
											Pembayaran
											<i class="right fas fa-angle-right"></i>
										</p>
									</a>
									<ul class="nav nav-treeview <?= ($this->uri->segment(1) == 'pembayaran_penjualan') || ($this->uri->segment(1) == 'pembayaran_penjualan_broker') ? 'menu-open' : ''; ?>" style="display: none;">
										<li class="nav-item">
											<a href="<?= base_url('pembayaran_penjualan') ?>" class="nav-link <?= $this->uri->segment(1) == 'pembayaran_penjualan' ? 'active' : ''; ?>">
												<i class="nav-icon fas fa-users ml-5"></i>
												<p>RPA</p>
											</a>
										</li>
										<li class="nav-item">
											<a href="<?= base_url('pembayaran_penjualan_broker') ?>" class="nav-link <?= $this->uri->segment(1) == 'pembayaran_penjualan_broker' ? 'active' : ''; ?>">
												<i class="nav-icon fas fa-house-user ml-5"></i>
												<p>BROKER</p>
											</a>
										</li>
									</ul>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('penjualan_checker') ?>" class="nav-link <?= $this->uri->segment(1) == 'penjualan_checker' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-truck-loading ml-3"></i>
										<p>Checker Penjualan</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('susut_penjualan') ?>" class="nav-link <?= $this->uri->segment(1) == 'susut_penjualan' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-crop ml-3"></i>
										<p>Rugi / Susut</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'hutang_suppliers') || ($this->uri->segment(1) == 'piutang_customers') || ($this->uri->segment(1) == 'piutang_brokers') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-money-check-alt"></i>
								<p>
									Hutang & Piutang
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('hutang_suppliers') ?>" class="nav-link <?= $this->uri->segment(1) == 'hutang_suppliers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-industry ml-3"></i>
										<p>Suppliers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('piutang_customers') ?>" class="nav-link <?= $this->uri->segment(1) == 'piutang_customers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-users ml-3"></i>
										<p>Customers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('piutang_brokers') ?>" class="nav-link <?= $this->uri->segment(1) == 'piutang_brokers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-house-user ml-3"></i>
										<p>Brokers</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'history_suppliers') || ($this->uri->segment(1) == 'history_customers') || ($this->uri->segment(1) == 'history_brokers') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-history"></i>
								<p>
									History Pembayaran
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('history_suppliers') ?>" class="nav-link <?= $this->uri->segment(1) == 'history_suppliers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-industry ml-3"></i>
										<p>Suppliers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('history_customers') ?>" class="nav-link <?= $this->uri->segment(1) == 'history_customers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-users ml-3"></i>
										<p>Customers</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('history_brokers') ?>" class="nav-link <?= $this->uri->segment(1) == 'history_brokers' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-house-user ml-3"></i>
										<p>Brokers</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('saldo_awal') ?>" class="nav-link <?= ($this->uri->segment(1) == 'saldo_awal') ? 'active' : ''; ?>">
								<i class="nav-icon fas fa-money-check"></i>
								<p>Saldo Awal</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'uang_masuk') || ($this->uri->segment(1) == 'pengeluaran') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-file-invoice-dollar"></i>
								<p>
									In Out
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('uang_masuk') ?>" class="nav-link <?= $this->uri->segment(1) == 'uang_masuk' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-arrow-left ml-3"></i>
										<p>Uang Masuk</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('pengeluaran') ?>" class="nav-link <?= $this->uri->segment(1) == 'pengeluaran' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-arrow-right ml-3"></i>
										<p>Pengeluaran</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'pengeluaran_akun') || ($this->uri->segment(1) == 'pengeluaran_akun_detail') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-file-invoice"></i>
								<p>
									Akun Pengeluaran
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('pengeluaran_akun') ?>" class="nav-link <?= $this->uri->segment(1) == 'pengeluaran_akun' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-chart-bar fa-rotate-90 ml-3"></i>
										<p>Kategori Akun</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('pengeluaran_akun_detail') ?>" class="nav-link <?= $this->uri->segment(1) == 'pengeluaran_akun_detail' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-file-invoice ml-3"></i>
										<p>Akun Pengeluaran</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-header">INVENTORI</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'stok_masuk') || ($this->uri->segment(1) == 'stok_keluar') || ($this->uri->segment(1) == 'stok_opname') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-boxes"></i>
								<p>
									Stok Kandang
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= base_url('stok_masuk') ?>" class="nav-link <?= $this->uri->segment(1) == 'stok_masuk' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-arrow-left ml-3"></i>
										<p>Stok Masuk</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('stok_keluar') ?>" class="nav-link <?= $this->uri->segment(1) == 'stok_keluar' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-arrow-right ml-3"></i>
										<p>Stok Keluar</p>
									</a>
								</li>
								<!-- <li class="nav-item">
									<a href="<?= base_url('stok_opname') ?>" class="nav-link <?= $this->uri->segment(1) == 'stok_opname' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-pallet ml-3"></i>
										<p>Stok Opname</p>
									</a>
								</li> -->
							</ul>
						</li>
						<li class="nav-header">LAPORAN</li>
						<li class="nav-item <?= ($this->uri->segment(1) == 'laporan_pembelian') || ($this->uri->segment(1) == 'laporan_penerimaan') || ($this->uri->segment(1) == 'laporan_penjualan') || ($this->uri->segment(1) == 'laporan_rekap') || ($this->uri->segment(1) == 'laporan_hr') || ($this->uri->segment(1) == 'laporan_inout') ? 'menu-open' : ''; ?>">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-book-open"></i>
								<p>
									Laporan
									<i class="fas fa-angle-right right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<!-- <li class="nav-item">
									<a href="<?= base_url('laporan_pembelian') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_pembelian' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-truck fa-flip-horizontal ml-3"></i>
										<p>Laporan Pembelian</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('laporan_penerimaan') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_penerimaan' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-hand-holding fa-flip-horizontal ml-3"></i>
										<p>Laporan Penerimaan</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('laporan_pembayaran') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_pembayaran' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-hand-holding-usd fa-flip-horizontal ml-3"></i>
										<p>Laporan Pembayaran</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('laporan_penjualan') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_penjualan' ? 'active' : ''; ?>">
										<i class="nav-icon fab fa-shopify ml-3"></i>
										<p>Laporan Penjualan</p>
									</a>
								</li> -->
								<!-- <li class="nav-item">
									<a href="<?= base_url('laporan_stok') ?>" class="nav-link">
										<i class="nav-icon fas fa-boxes ml-3"></i>
										<p>Laporan Stok</p>
									</a>
								</li> -->
								<li class="nav-item">
									<a href="<?= base_url('laporan_rekap') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_rekap' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-receipt fa-flip-horizontal ml-3"></i>
										<p>REKAP</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('laporan_hr') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_hr' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-receipt fa-flip-horizontal ml-3"></i>
										<p>Laporan HR</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= base_url('laporan_inout') ?>" class="nav-link <?= $this->uri->segment(1) == 'laporan_inout' ? 'active' : ''; ?>">
										<i class="nav-icon fas fa-receipt fa-flip-horizontal ml-3"></i>
										<p>Laporan In-Out</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-header">LAINNYA</li>
						<li class="nav-item">
							<a href="<?= base_url('users') ?>" class="nav-link <?= $this->uri->segment(1) == 'users' ? 'active' : ''; ?>">
								<i class="nav-icon fas fa-users"></i>
								<p>Data Pengguna</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('profile') ?>" class="nav-link <?= $this->uri->segment(1) == 'profile' ? 'active' : ''; ?>">
								<i class="nav-icon fas fa-user-circle"></i>
								<p>Profil Saya</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('auth/logout') ?>" class="nav-link">
								<i class="nav-icon fas fa-sign-out-alt fa-flip-horizontal"></i>
								<p>Keluar</p>
							</a>
						</li>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>