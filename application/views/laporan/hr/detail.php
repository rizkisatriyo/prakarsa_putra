<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<form action="<?= base_url('laporan_hr/find') ?>" method="GET">
								<div class="form-row">
									<div class="form-group col-md-4 mb-md-0">
										<input type="date" name="tanggal_hr" value="<?= $tanggal_hr ?>" class="form-control" id="tanggal_hr">
										<div id="tanggal_hr" class="invalid-feedback">
											<?= form_error('tanggal_hr') ?>
										</div>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-block">Cari</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PEMBELIAN -->
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Pembelian</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-6 d-flex align-items-center">
									<b>No. Pembelian</b> : #<?= $pembelian->no_transaksi ?>
								</div>
								<div class="col-sm-6 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($pembelian->tgl_beli)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Supplier</th>
												<th>Ekor</th>
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($pembelian_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="2" class="text-right">J U M L A H</th>
												<th class="text-right"><?= number_format($pembelian->ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($pembelian->kg, 2, ',', '.') ?></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<div class="col-md-6 pb-3">
					<div class="card h-100">
						<div class="card-header">
							<h3 class="card-title"># Penyisihan untuk Broker</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-6 d-flex align-items-center">
									<b>No. Penjualan</b> : #<?= $penjualan_broker->no_transaksi ?>
								</div>
								<div class="col-sm-6 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan_broker->tgl_jual)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="pembelian_detail" class="table table-sm table-bordered table-hover h-100">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Broker</th>
												<th>Suppliers</th>
												<!-- <th>Ekor</th> -->
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($penjualan_broker_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<ul class="pl-4 mb-0">
															<?php
															$this->db->join('suppliers', 'suppliers.id = penjualan_broker_detail_supplier.id_supplier', 'left');
															$supps = $this->db->get_where('penjualan_broker_detail_supplier', ['no_trxdetail' => $item['no_trxdetail']])->result_array();
															foreach ($supps as $supp) :
															?>
																<li><?= $supp['nama'] ?></li>
															<?php endforeach ?>
														</ul>
													</td>
													<!-- <td class="text-right">-</td> -->
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<!-- <th class="text-right"><?= $penjualan_broker->total_ekor ? number_format($penjualan_broker->total_ekor, 0, ',', '.') : '-' ?></th> -->
												<th class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- CHECKER PENERIMAAN -->
			<div class="row">
				<div class="col">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Checker Penerimaan</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-6 d-flex align-items-center">
									<b>No. Checker</b> : #<?= $checker_penerimaan->no_checker ?>
								</div>
								<div class="col-sm-6 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penerimaan->tgl_diterima)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<table class="table table-bordered table-hover table-sm">
										<tbody>
											<tr>
												<?php
												$total_belanja_ekor = $pembelian->ekor;
												$total_belanja_kg = $pembelian->kg - $penjualan_broker->total_kg;
												?>
												<th class="bg-light align-middle text-center" style="width: 150px;">Belanja</th>
												<td class="text-center align-middle"><?= $total_belanja_ekor ? number_format($total_belanja_ekor, 0, ',', '.') : '-' ?></td>
												<td class="text-center align-middle"><?= $total_belanja_kg ? number_format($total_belanja_kg, 2, ',', '.') : '-' ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row d-flex justify-content-center">
								<?php foreach ($kandang as $item) : ?>
									<?php if (array_search($item['id'], array_column($checker_penerimaan_detail, 'id_kandang')) !== FALSE) : ?>
										<div class="col-3">
											<table class="table table-bordered table-hover table-sm table-checker" id="table-<?= $item['id'] ?>" data-id_kandang="<?= $item['id'] ?>">
												<thead class="thead-light">
													<tr>
														<th colspan="2" class="text-center"><?= $item['nama'] ?></th>
													</tr>
												</thead>
												<tbody>
													<?php
													$total_ekor = 0;
													$total_kg = 0;
													foreach ($checker_penerimaan_detail as $item_checker) :
													?>
														<?php
														if ($item_checker['id_kandang'] == $item['id']) :
															$total_ekor += $item_checker['ekor'];
															$total_kg += $item_checker['kg'];
														?>
															<tr>
																<td class="text-right"><?= number_format($item_checker['ekor'], 0, ",", '.') ?></td>
																<td class="text-right"><?= number_format($item_checker['kg'], 2, ",", '.') ?></td>
															</tr>
														<?php endif ?>
													<?php endforeach ?>
												</tbody>
												<tfoot>
													<tr class="bg-light">
														<th class="text-right"><?= number_format($total_ekor, 0, ",", '.') ?></th>
														<th class="text-right"><?= number_format($total_kg, 2, ",", '.') ?></th>
													</tr>
												</tfoot>
											</table>
										</div>
									<?php endif; ?>
								<?php endforeach ?>
							</div>
							<div class="row">
								<div class="col-md-8 offset-md-2">
									<table class="table table-sm table-bordered mt-2">
										<tbody>
											<tr>
												<th class="bg-light align-middle text-right">Total Ekor</th>
												<td class="text-center font-weight-bold"><?= number_format($penerimaan->bongkar_ekor, 0, ",", '.') ?></td>
												<th class="bg-light align-middle text-right">Total Kg</th>
												<td class="text-center font-weight-bold"><?= number_format($penerimaan->bongkar_kg, 2, ",", '.') ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>

			<!-- PENERIMAAN & TOTAL & SUSUT -->
			<div class="row">
				<div class="col">
					<div class="row">

						<!-- PENERIMAAN & TOTAL -->
						<div class="col-md-4">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title"># Data Penerimaan</h3>
									<span class="badge badge-warning text-light float-right"><?= date('d M Y', strtotime($penerimaan->tgl_diterima)) ?></span>
								</div>
								<!-- /.card-header -->
								<div class="card-body py-0">
									<div class="row my-2">
										<div class="col d-flex align-items-center">
											<b>No. Penerimaan</b> : #<?= $penerimaan->no_transaksi ?>
										</div>
										<!-- <div class="col-sm-6 d-flex align-items-center">
											<b>Tanggal</b> : <?= date('d M Y', strtotime($penerimaan->tgl_diterima)) ?>
										</div> -->
									</div>
									<div class="row">
										<div class="col-12">
											<table id="penerimaan_detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<th>Keterangan</th>
														<th>Ekor</th>
														<th>Kg</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th class="bg-light" style="width: 150px;">Bongkar</th>
														<td class="text-right"><?= number_format($penerimaan->bongkar_ekor, 0, ',', '.') ?></td>
														<td class="text-right"><?= number_format($penerimaan->bongkar_kg, 2, ',', '.') ?></td>
													</tr>
													<tr>
														<th class="bg-light" style="width: 150px;">Kirim</th>
														<td class="text-right"><?= $penerimaan->kirim_ekor ? number_format($penerimaan->kirim_ekor, 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= $penerimaan->kirim_kg ? number_format($penerimaan->kirim_kg, 2, ',', '.') : '-' ?></td>
													</tr>
													<tr>
														<th class="bg-light" style="width: 150px;">Mati</th>
														<td class="text-right"><?= $penerimaan->mati_ekor ? number_format($penerimaan->mati_ekor, 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= $penerimaan->mati_kg ? number_format($penerimaan->mati_kg, 2, ',', '.') : '-' ?></td>
													</tr>
												</tbody>
											</table>

											<table class="table table-bordered table-hover table-sm">
												<!-- <thead class="thead-light">
													<tr class="text-center">
														<th>Keterangan</th>
														<th>Ekor</th>
														<th>Kg</th>
													</tr>
												</thead> -->
												<tbody>
													<tr>
														<th class="bg-light" style="width: 150px;">Sisa Kandang</th>
														<td class="text-right"><?= number_format($sisa_kandang->ekor, 0, ',', '.') ?></td>
														<td class="text-right"><?= number_format($sisa_kandang->kg, 2, ',', '.') ?></td>
													</tr>
													<tr>
														<?php
														$total_stok_ekor = $sisa_kandang->ekor + $penerimaan->bongkar_ekor;
														$total_stok_kg = $sisa_kandang->kg + $penerimaan->bongkar_kg;
														?>
														<th class="bg-light" style="width: 150px;">Total Stok</th>
														<td class="text-right"><?= $total_stok_ekor ? number_format($total_stok_ekor, 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= $total_stok_kg ? number_format($total_stok_kg, 2, ',', '.') : '-' ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->

						<!-- SUSUT -->
						<div class="col-md-4 d-flex align-items-center">
							<div class="card" style="flex-grow: 1;">
								<div class="card-header">
									<h3 class="card-title"># Susut</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body py-0">
									<div class="row mt-3">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<!-- <th colspan="4" class="text-center">Penyusutan</th> -->
														<th>Ekor</th>
														<!-- <th>Persentase Ekor</th> -->
														<th>Kg</th>
														<th>Persentase Kg</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="text-center font-weight-bold"><?= $susut_penerimaan->ekor ? number_format($susut_penerimaan->ekor, 0, ',', '.') : '-' ?></td>
														<!-- <td class="text-center font-weight-bold"><?= $susut_penerimaan->presentase_ekor ? number_format($susut_penerimaan->presentase_ekor, 2, ',', '.') . '%' : '-' ?></td> -->
														<td class="text-center font-weight-bold"><?= $susut_penerimaan->kg ? number_format($susut_penerimaan->kg, 2, ',', '.') : '-' ?></td>
														<td class="text-center font-weight-bold"><?= $susut_penerimaan->presentase_kg ? number_format($susut_penerimaan->presentase_kg, 2, ',', '.') . '%' : '-' ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->

						<!-- PARAF SUPIR -->
						<div class="col-md-4 pb-3">
							<div class="card h-100">
								<div class="card-header">
									<h3 class="card-title"># Paraf Supir</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body text-center pb-0">
									<b>PT. PRAKARSA PUTRA BROILER</b>
									<br>
									<br>
									<br>
									<br>
									<br>
									<b><u> SARIP </u></b>
									<br>
									<p>Supir</p>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->
					</div>
				</div>
			</div>
			<!-- /.row -->

			<!-- CHECKER PENJUALAN -->
			<div class="row">
				<div class="col">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Checker Penjualan</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-6 d-flex align-items-center">
									<b>No. Checker</b> : #<?= $checker_penjualan->no_checker ?>
								</div>
								<div class="col-sm-6 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($checker_penjualan->tgl_check)) ?>
								</div>
							</div>
							<div class="row d-flex justify-content-center">
								<?php foreach ($kandang as $item) : ?>
									<?php if (array_search($item['id'], array_column($checker_penjualan_detail, 'id_kandang')) !== FALSE) : ?>
										<div class="col-3">
											<table class="table table-bordered table-hover table-sm table-checker" id="table-<?= $item['id'] ?>" data-id_kandang="<?= $item['id'] ?>">
												<thead class="thead-light">
													<tr>
														<th colspan="3" class="text-center"><?= $item['nama'] ?></th>
													</tr>
												</thead>
												<tbody>
													<?php
													$total_ekor = 0;
													$total_kg = 0;
													foreach ($checker_penjualan_detail as $item_checker) :
													?>
														<?php
														if ($item_checker['id_kandang'] == $item['id']) :
															$total_ekor += $item_checker['ekor'];
															$total_kg += $item_checker['kg'];
														?>
															<tr>
																<td class="text-left"><?= $item_checker['nama'] ?></td>
																<td class="text-right"><?= number_format($item_checker['ekor'], 0, ",", '.') ?></td>
																<td class="text-right"><?= number_format($item_checker['kg'], 2, ",", '.') ?></td>
															</tr>
														<?php endif ?>
													<?php endforeach ?>
												</tbody>
												<tfoot>
													<tr class="bg-light">
														<th class="text-right">TOTAL</th>
														<th class="text-right"><?= number_format($total_ekor, 0, ",", '.') ?></th>
														<th class="text-right"><?= number_format($total_kg, 2, ",", '.') ?></th>
													</tr>
												</tfoot>
											</table>
										</div>
									<?php endif; ?>
								<?php endforeach ?>
							</div>
							<div class="row">
								<div class="col-md-8 offset-md-2">
									<table class="table table-sm table-bordered mt-2">
										<tbody>
											<tr>
												<th class="bg-light align-middle text-right">Total Ekor</th>
												<td class="text-center font-weight-bold"><?= number_format($checker_penjualan->total_ekor, 0, ",", '.') ?></td>
												<th class="bg-light align-middle text-right">Total Kg</th>
												<td class="text-center font-weight-bold"><?= number_format($checker_penjualan->total_kg, 2, ",", '.') ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>

			<!-- PENJUALAN & SUSUT -->
			<div class="row">
				<div class="col">
					<div class="row">

						<!-- PENJUALAN & TOTAL -->
						<div class="col-md-4">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title"># Data Penjualan</h3>
									<span class="badge badge-warning text-light float-right"><?= date('d M Y', strtotime($checker_penjualan->tgl_check)) ?></span>
								</div>
								<!-- /.card-header -->
								<div class="card-body py-0">
									<div class="row my-2">
										<div class="col d-flex align-items-center">
											<b>No. Penjualan</b> : #<?= $checker_penjualan->no_transaksi_penjualan ?>
										</div>
										<!-- <div class="col-sm-6 d-flex align-items-center">
								<b>Tanggal</b> : <?= date('d M Y', strtotime($checker_penjualan->tgl_diterima)) ?>
							</div> -->
									</div>
									<div class="row">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<th>Keterangan</th>
														<th>Ekor</th>
														<th>Kg</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th class="bg-light" style="width: 150px;">Total Penjualan</th>
														<td class="text-right"><?= number_format($checker_penjualan->total_ekor, 0, ',', '.') ?></td>
														<td class="text-right"><?= number_format($checker_penjualan->total_kg, 2, ',', '.') ?></td>
													</tr>
													<tr>
														<th class="bg-light" style="width: 150px;">Sisa</th>
														<td class="text-right"><?= $checker_penjualan->sisa_ekor ? number_format($checker_penjualan->sisa_ekor, 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= $checker_penjualan->sisa_kg ? number_format($checker_penjualan->sisa_kg, 2, ',', '.') : '-' ?></td>
													</tr>
													<tr>
														<th class="bg-light" style="width: 150px;">Mati</th>
														<td class="text-right"><?= $checker_penjualan->mati_ekor ? number_format($checker_penjualan->mati_ekor, 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= $checker_penjualan->mati_kg ? number_format($checker_penjualan->mati_kg, 2, ',', '.') : '-' ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->

						<!-- SUSUT -->
						<div class="col-md-4 d-flex align-items-center">
							<div class="card" style="flex-grow: 1;">
								<div class="card-header">
									<h3 class="card-title"># Susut</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body py-0">
									<div class="row mt-3">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<!-- <th colspan="4" class="text-center">Penyusutan</th> -->
														<th>Ekor</th>
														<!-- <th>Persentase Ekor</th> -->
														<th>Kg</th>
														<th>Persentase Kg</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="text-center font-weight-bold"><?= $susut_penjualan->ekor ? number_format($susut_penjualan->ekor, 0, ',', '.') : '-' ?></td>
														<!-- <td class="text-center font-weight-bold"><?= $susut_penjualan->presentase_ekor ? number_format($susut_penjualan->presentase_ekor, 2, ',', '.') . '%' : '-' ?></td> -->
														<td class="text-center font-weight-bold"><?= $susut_penjualan->kg ? number_format($susut_penjualan->kg, 2, ',', '.') : '-' ?></td>
														<td class="text-center font-weight-bold"><?= $susut_penjualan->presentase_kg ? number_format($susut_penjualan->presentase_kg, 2, ',', '.') . '%' : '-' ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->

						<!-- PARAF PENIMBANG -->
						<div class="col-md-4 pb-3">
							<div class="card h-100">
								<div class="card-header">
									<h3 class="card-title"># Paraf Penimbang</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body text-center pb-0">
									<b>PT. PRAKARSA PUTRA BROILER</b>
									<br>
									<br>
									<br>
									<br>
									<br>
									<b><u> DEDI </u></b>
									<br>
									<p>Penimbang</p>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->
					</div>
				</div>
			</div>
			<!-- /.row -->

			<!-- TOTAL SUSUT -->
			<div class="row">
				<div class="col">
					<div class="row d-flex justify-content-center">
						<div class="col-md-6">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title"># Total Susut</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body py-0">
									<div class="row mt-3">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<!-- <th colspan="4" class="text-center">Penyusutan</th> -->
														<th>Ekor</th>
														<!-- <th>Persentase Ekor</th> -->
														<th>Kg</th>
														<th>Persentase Kg</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$total_susut_ekor = $penerimaan->mati_ekor + $susut_penerimaan->ekor + $checker_penjualan->mati_ekor + $susut_penjualan->ekor;
													// $total_persentase_susut_ekor = $susut_penerimaan->presentase_ekor + $susut_penjualan->presentase_ekor;
													$total_persentase_susut_ekor = $total_susut_ekor / ($sisa_kandang->ekor + $pembelian->ekor) * 100;
													$total_susut_kg = $susut_penerimaan->kg + $susut_penjualan->kg;
													// $total_persentase_susut_kg = $susut_penerimaan->presentase_kg + $susut_penjualan->presentase_kg;
													$total_persentase_susut_kg = $total_susut_kg / ($sisa_kandang->kg + $pembelian->kg - $penjualan_broker->total_kg) * 100;
													?>
													<tr>
														<td class="text-center font-weight-bold"><?= $total_susut_ekor ? number_format($total_susut_ekor, 0, ',', '.') : '-' ?></td>
														<!-- <td class="text-center font-weight-bold"><?= $total_persentase_susut_ekor ? number_format($total_persentase_susut_ekor, 2, ',', '.') . '%' : '-' ?></td> -->
														<td class="text-center font-weight-bold"><?= $total_susut_kg ? number_format($total_susut_kg, 2, ',', '.') : '-' ?></td>
														<td class="text-center font-weight-bold"><?= $total_persentase_susut_kg ? number_format($total_persentase_susut_kg, 2, ',', '.') . '%' : '-' ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->
					</div>
				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->