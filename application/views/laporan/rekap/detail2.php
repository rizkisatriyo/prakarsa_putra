<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<form action="<?= base_url('laporan_rekap/find') ?>" method="GET">
								<div class="form-row">
									<div class="form-group col-md-4 mb-md-0">
										<input type="date" name="tanggal_rekap" value="<?= $tanggal_rekap ?>" class="form-control" id="tanggal_rekap">
										<div id="tanggal_rekap" class="invalid-feedback">
											<?= form_error('tanggal_rekap') ?>
										</div>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-block">Cari</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PEMBELIAN -->
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Pembelian</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Pembelian</b> : #<?= $pembelian->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($pembelian->tgl_beli)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Supplier</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($pembelian_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['hrg_ayam'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['subtotal'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">S I S A</th>
												<th class="text-right"><?= number_format($sisa->ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($sisa->kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa->jumlah, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th class="text-right"><?= number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($pembelian->total_hutang + $sisa->jumlah) / ($pembelian->kg + $sisa->kg), 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang + $sisa->jumlah, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PENJUALAN RPA -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Penjualan RPA</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Penjualan</b> : #<?= $penjualan->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan->tgl_jual)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="penjualan_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Customer</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<!-- <th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th> -->
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($penjualan_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= $item['kg'] ? number_format($item['kg'], 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($item['harga']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['harga'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($item['jumlah']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['jumlah'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<!-- <td class="text-right <?= $item['bayar'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['bayar']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['sisa_piutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['sisa_piutang']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['sisa_piutang'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['piutang_deposit'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['piutang_deposit']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['piutang_deposit'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td> -->
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<!-- <th class="<?= $penjualan->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th> -->
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PENJUALAN BROKER -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Penjualan BROKER</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Penjualan</b> : #<?= $penjualan_broker->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan_broker->tgl_jual)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="penjualan_broker_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Broker</th>
												<!-- <th>Ekor</th> -->
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<!-- <th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th> -->
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($penjualan_broker_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<!-- <td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td> -->
													<td class="text-right"><?= $item['kg'] ? number_format($item['kg'], 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($item['harga']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['harga'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($item['jumlah']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['jumlah'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<!-- <td class="text-right <?= $item['bayar'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['bayar']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['sisa_piutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['sisa_piutang']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['sisa_piutang'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['piutang_deposit'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['piutang_deposit']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['piutang_deposit'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td> -->
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<!-- <th class="text-right"><?= $penjualan_broker->total_ekor ? number_format($penjualan_broker->total_ekor, 0, ',', '.') : '-'; ?></th> -->
												<th class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<!-- <th class="<?= $penjualan_broker->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan_broker->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan_broker->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th> -->
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- SEMUA PENJUALAN -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Semua Penjualan</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="semua_penjualan" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Keterangan</th>
												<th>No. Transaksi</th>
												<!-- <th>Broker / Customer</th> -->
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<!-- <th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th> -->
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td class="text-left">Penjualan RPA</td>
												<td class="text-center">#<?= $penjualan->no_transaksi ?></td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah, 0, ',', '.') ?>,-</span>
												</td>
												<!-- <td class="<?= $penjualan->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_bayar, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td> -->
											</tr>
											<tr>
												<td>2</td>
												<td class="text-left">Penjualan Broker</td>
												<td class="text-center">#<?= $penjualan_broker->no_transaksi ?></td>
												<td class="text-right">-</td>
												<td class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</td>
												<!-- <td class="<?= $penjualan_broker->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_bayar, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan_broker->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan_broker->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td> -->
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($penjualan->total_kg + $penjualan_broker->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($penjualan->jumlah + $penjualan_broker->jumlah) / ($penjualan->total_kg + $penjualan_broker->total_kg), 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah + $penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<!-- <th class="<?= $penjualan->total_bayar + $penjualan_broker->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_bayar + $penjualan_broker->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan->total_sisa_piutang + $penjualan_broker->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_sisa_piutang + $penjualan_broker->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $penjualan->total_piutang_deposit + $penjualan_broker->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_piutang_deposit + $penjualan_broker->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th> -->
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- SISA STOK -->
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Stok</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<!-- <th>#</th> -->
												<th>Keterangan</th>
												<!-- <th>Supplier</th> -->
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<!-- <td class="text-center" width="50"><?= $i++ ?></td> -->
												<td class="text-center">Sisa Stok</td>
												<!-- <td><?= $sisa_stok->nama ?></td> -->
												<td class="text-right"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->harga_stok, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->jumlah_stok, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- RUGI SUSUT & PIUTANG -->
			<div class="row">
				<!-- RUGI SUSUT -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Rugi / Susut</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Ekor</th>
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left">Belanja</td>
												<td class="text-right"><?= number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') ?></td>
												<td class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Penjualan RPA</td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Penjualan Broker</td>
												<td class="text-right">-</td>
												<td class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Sisa Stok</td>
												<td class="text-right"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<?php
											$total_ekor = ($pembelian->ekor + $sisa->ekor) - $penjualan->total_ekor - $sisa_stok->ekor;
											$total_kg = ($pembelian->kg + $sisa->kg) - $penjualan->total_kg - $penjualan_broker->total_kg - $sisa_stok->kg;
											$persentase_ekor = $total_ekor / ($pembelian->ekor + $sisa->ekor) * 100;
											$persentase_kg = $total_kg / ($pembelian->kg + $sisa->kg) * 100;
											?>
											<tr>
												<th class="text-right">TOTAL</th>
												<th class="text-right"><?= $total_ekor ? number_format($total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($total_kg, 2, ',', '.') ?></th>
											</tr>
											<tr>
												<th class="text-right">PERSENTASE</th>
												<th class="text-right"><?= $persentase_ekor ? number_format($persentase_ekor, 2, ',', '.') : '-'; ?>%</th>
												<th class="text-right"><?= number_format($persentase_kg, 2, ',', '.') ?>%</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<!-- PIUTANG -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Piutang</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Bayar</th>
												<th>Piutang</th>
												<th>Deposit</th>
											</tr>
										</thead>
										<tbody>
											<?php
											// $total_sisa_piutang = $penjualan->total_sisa_piutang + $penjualan_broker->total_sisa_piutang;
											// $total_piutang_deposit = $penjualan->total_piutang_deposit + $penjualan_broker->total_piutang_deposit;
											// $total_bayar_broker = $penjualan_broker->total_sisa_piutang - $penjualan_broker->total_piutang_deposit;
											// $total_bayar_rpa = ($total_sisa_piutang - $total_piutang_deposit) - $total_bayar_broker;
											?>
											<tr>
												<td class="text-left">Penjualan RPA</td>
												<!-- <td class="<?= $total_bayar_rpa < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar_rpa, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td> -->
											</tr>
											<tr>
												<td class="text-left">Penjualan Broker</td>
												<!-- <td class="<?= $total_bayar_broker < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar_broker, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan_broker->total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $penjualan_broker->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td> -->
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">TOTAL PIUTANG</th>
												<!-- <th class="<?= $penjualan->total_piutang_deposit + $penjualan_broker->total_piutang_deposit < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->total_piutang_deposit + $penjualan_broker->total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th> -->
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PEMBAYARAN -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># H U T A N G</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Pembayaran</b> : #<?= $hutang->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($hutang->tgl_bayar)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="pembayaran_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Supplier</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Bayar</th>
												<th>Hutang</th>
												<th>Sisa Hutang</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($pembayaran_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= $item['kg'] ? number_format($item['kg'], 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($item['harga']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['harga'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($item['jumlah']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['jumlah'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['bayar'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['bayar']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['hutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['hutang']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['hutang'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right <?= $item['sisa_hutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
														<?php if ($item['sisa_hutang']) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($item['sisa_hutang'], 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="7" class="text-right">J U M L A H</th>
												<th class="<?= $pembayaran->total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $pembayaran->total_hutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran->total_hutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $pembayaran->total_sisa_hutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran->total_sisa_hutang, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- LABA RUGI & OUTPUT -->
			<div class="row">
				<!-- LABA RUGI -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Laba / Rugi</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left">Belanja</td>
												<td class="text-right"><?= $pembelian->ekor + $sisa->ekor ? number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($pembelian->total_hutang + $sisa->jumlah) / ($pembelian->kg + $sisa->kg), 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang + $sisa->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td class="text-left">Penjualan</td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg + $penjualan_broker->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($penjualan->jumlah + $penjualan_broker->jumlah) / ($penjualan->total_kg + $penjualan_broker->total_kg), 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah + $penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td class="text-left">Sisa</td>
												<td class="text-right"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="4" class="text-right">LABA / RUGI</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format((($penjualan->jumlah + $penjualan_broker->jumlah) + $sisa_stok->jumlah) - ($pembelian->total_hutang + $sisa->jumlah) - 1689024.631, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<!-- OUTPUT -->
				<div class="col-md-4 offset-md-1 d-flex align-items-center">
					<div class="card" style="flex-grow: 1">
						<div class="card-header">
							<h3 class="card-title"># Output</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>OUTPUT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th class="text-lg <?= 1689024.631 < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(1689024.631, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->