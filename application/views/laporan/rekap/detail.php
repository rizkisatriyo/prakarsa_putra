<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<form action="<?= base_url('laporan_rekap/find') ?>" method="GET">
								<div class="form-row">
									<div class="form-group col-md-4 mb-md-0">
										<input type="date" name="tanggal_rekap" value="<?= $tanggal_rekap ?>" class="form-control" id="tanggal_rekap">
										<div id="tanggal_rekap" class="invalid-feedback">
											<?= form_error('tanggal_rekap') ?>
										</div>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-block">Cari</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PEMBELIAN -->
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Pembelian</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<!-- <div class="col-sm-4 d-flex align-items-center">
									<b>No. Pembelian</b> : #<?= $pembelian->no_transaksi ?>
								</div> -->
								<!-- <div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($pembelian->tgl_beli)) ?>
								</div> -->
							</div>
							<div class="row">
								<div class="col-12">
									<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Supplier</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($pembelian_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['hrg_ayam'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['subtotal'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">S I S A</th>
												<th class="text-right"><?= number_format($sisa->ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($sisa->kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa->jumlah, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th class="text-right"><?= number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($pembelian->total_hutang + $sisa->jumlah) / ($pembelian->kg + $sisa->kg), 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang + $sisa->jumlah, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PENJUALAN RPA -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Penjualan RPA</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Penjualan</b> : #<?= $penjualan->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan->tgl_jual)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="penjualan_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<!-- <th>No. Transaksi</th> -->
												<th>Customer</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i = 1;
											$total_bayar = 0;
											$total_sisa_piutang = 0;
											$total_sisa_hutang = 0;
											// hitung subtotal semua penjualan
											$rpa_total_bayar = 0;
											$rpa_total_sisa_piutang = 0;
											$rpa_total_piutang_deposit = 0;

											foreach ($penjualan_pembayaran_detail as $item) : 
												$this->db->select('ekor, kg, penjualan_detail.harga, penjualan_detail.jumlah');
												$this->db->order_by('tgl_jual', 'desc');
												$this->db->join('penjualan', 'penjualan.no_transaksi = penjualan_detail.no_transaksi', 'left');
												$pjn_det = $this->db->get_where('penjualan_detail', ['id_customer' => $item['id_customer'], 'tgl_jual <=' => $tanggal_rekap])->row();

												$this->db->order_by('tgl_bayar', 'desc');
												$this->db->join('penjualan_pembayaran', 'penjualan_pembayaran.no_transaksi = penjualan_pembayaran_detail.no_transaksi', 'left');
												$pbn_det = $this->db->get_where('penjualan_pembayaran_detail', ['id_customer' => $item['id_customer'], 'tgl_bayar <=' => $tanggal_rekap])->row();

												$nama = $item['nama'];
												$ekor = null;
												$kg = null;
												$harga = null;
												$jumlah = null;
												$bayar = null;
												$sisa_piutang = null;
												$piutang_deposit = null;

												if ($pjn_det) {
													$ekor = $pjn_det->ekor;
													$kg = $pjn_det->kg;
													$harga = $pjn_det->harga;
													$jumlah = $pjn_det->jumlah;
												}

												if ($pbn_det) {
													$bayar = $pbn_det->bayar;
													$sisa_piutang = $pbn_det->deposit ? -1 * $pbn_det->deposit : $pbn_det->piutang - $jumlah;
													$piutang_deposit = $jumlah - $bayar + $sisa_piutang;

													$total_bayar += $bayar;
													$total_sisa_piutang += $sisa_piutang;
													$total_piutang_deposit += $piutang_deposit;

													$rpa_total_bayar += $bayar;
													$rpa_total_sisa_piutang += $sisa_piutang;
													$rpa_total_piutang_deposit += $piutang_deposit;
												}
												?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<!-- <td class="text-center">#<?= $item['no_trxdetail'] ?></td> -->
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $ekor ? number_format($ekor, 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= $kg ? number_format($kg, 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($harga) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($harga, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($jumlah) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($jumlah, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($bayar) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($bayar, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($sisa_piutang) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($sisa_piutang, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right text-danger">
														<?php if ($piutang_deposit) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($piutang_deposit, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="2" class="text-right">J U M L A H</th>
												<th class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PENJUALAN BROKER -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Penjualan BROKER</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Penjualan</b> : #<?= $penjualan_broker->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan_broker->tgl_jual)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<table id="penjualan_broker_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<!-- <th>No. Transaksi</th> -->
												<th>Broker</th>
												<th>Supplier</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i = 1; 
											$total_bayar = 0;
											$total_sisa_piutang = 0;
											$total_piutang_deposit = 0;
											foreach ($penjualan_broker_pembayaran_detail as $item) : 
												$this->db->select('no_trxdetail, kg, penjualan_broker_detail.harga, penjualan_broker_detail.jumlah');
												$this->db->order_by('tgl_jual', 'desc');
												$this->db->join('penjualan_broker', 'penjualan_broker.no_transaksi = penjualan_broker_detail.no_transaksi', 'left');
												$pjb_det = $this->db->get_where('penjualan_broker_detail', ['id_broker' => $item['id_broker'], 'tgl_jual <=' => $tanggal_rekap])->row();

												$this->db->order_by('tgl_bayar', 'desc');
												$this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.no_transaksi = penjualan_broker_pembayaran_detail.no_transaksi', 'left');
												$pbb_det = $this->db->get_where('penjualan_broker_pembayaran_detail', ['id_broker' => $item['id_broker'], 'tgl_bayar <=' => $tanggal_rekap])->row();

												$nama = $item['nama'];
												$no_trxdetail = null;
												// $ekor = null;
												$kg = null;
												$harga = null;
												$jumlah = null;
												$bayar = null;
												$sisa_piutang = null;
												$piutang_deposit = null;

												if ($pjb_det) {
													$no_trxdetail = $pjb_det->no_trxdetail;
													// $ekor = $pjb_det->ekor;
													$kg = $pjb_det->kg;
													$harga = $pjb_det->harga;
													$jumlah = $pjb_det->jumlah;
												}

												if ($pbb_det) {
													$bayar = $pbb_det->bayar;
													$sisa_piutang = $pbb_det->deposit ? -1 * $pbb_det->deposit : $pbb_det->piutang - $jumlah;
													$piutang_deposit = $jumlah - $bayar + $sisa_piutang;

													$total_bayar += $bayar;
													$total_sisa_piutang += $sisa_piutang;
													$total_piutang_deposit += $piutang_deposit;
												}
												?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<!-- <td class="text-center">#<?= $item['no_trxdetail'] ?></td> -->
													<td><?= $item['nama'] ?></td>
													<td>
														<ul class="pl-4 mb-0">
															<?php
															$this->db->join('suppliers', 'suppliers.id = penjualan_broker_detail_supplier.id_supplier', 'left');
															$supps = $this->db->get_where('penjualan_broker_detail_supplier', ['no_trxdetail' => $no_trxdetail])->result_array();
															foreach ($supps as $supp) :
															?>
																<li><?= $supp['nama'] ?></li>
															<?php endforeach ?>
														</ul>
													</td>
													<!-- <td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-'; ?></td> -->
													<td class="text-right"><?= $kg ? number_format($kg, 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($harga) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($harga, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($jumlah) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($jumlah, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($bayar) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($bayar, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($sisa_piutang) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($sisa_piutang, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right text-danger">
														<?php if ($piutang_deposit) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($piutang_deposit, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<!-- <th class="text-right"><?= $penjualan_broker->total_ekor ? number_format($penjualan_broker->total_ekor, 0, ',', '.') : '-'; ?></th> -->
												<th class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->harga, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- SEMUA PENJUALAN -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Semua Penjualan</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="semua_penjualan" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Keterangan</th>
												<th>No. Transaksi</th>
												<!-- <th>Broker / Customer</th> -->
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Bayar</th>
												<th>Sisa Piutang</th>
												<th>Piutang/Deposit</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td class="text-left">Penjualan RPA</td>
												<td class="text-center">#<?= $penjualan->no_transaksi ?></td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $rpa_total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_bayar, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $rpa_total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td>2</td>
												<td class="text-left">Penjualan Broker</td>
												<td class="text-center">#<?= $penjualan_broker->no_transaksi ?></td>
												<td class="text-right">-</td>
												<td class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($penjualan->total_kg + $penjualan_broker->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($penjualan->jumlah + $penjualan_broker->jumlah) / ($penjualan->total_kg + $penjualan_broker->total_kg), 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah + $penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $rpa_total_bayar + $total_bayar < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_bayar + $total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th class="<?= $rpa_total_sisa_piutang + $total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_sisa_piutang + $total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_piutang_deposit + $total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- SISA STOK -->
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Data Stok</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<!-- <th>#</th> -->
												<th>Keterangan</th>
												<!-- <th>Supplier</th> -->
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<!-- <td class="text-center" width="50"><?= $i++ ?></td> -->
												<td class="text-center">Sisa Stok</td>
												<!-- <td><?= $sisa_stok->nama ?></td> -->
												<td class="text-center"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-center"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->harga_stok, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->jumlah_stok, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- RUGI SUSUT & PIUTANG -->
			<div class="row">
				<!-- RUGI SUSUT -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Rugi / Susut</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Ekor</th>
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left">Belanja</td>
												<td class="text-right"><?= number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') ?></td>
												<td class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Penjualan RPA</td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Penjualan Broker</td>
												<td class="text-right">-</td>
												<td class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<td class="text-left">Sisa Stok</td>
												<td class="text-right"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<?php
											$total_ekor = ($pembelian->ekor + $sisa->ekor) - $penjualan->total_ekor - $sisa_stok->ekor;
											$total_kg = ($pembelian->kg + $sisa->kg) - $penjualan->total_kg - $penjualan_broker->total_kg - $sisa_stok->kg;
											$persentase_ekor = $total_ekor / ($pembelian->ekor + $sisa->ekor) * 100;
											$persentase_kg = $total_kg / ($pembelian->kg + $sisa->kg) * 100;
											?>
											<tr>
												<th class="text-right">TOTAL</th>
												<th class="text-right"><?= $total_ekor ? number_format($total_ekor, 0, ',', '.') : '-'; ?></th>
												<th class="text-right"><?= number_format($total_kg, 2, ',', '.') ?></th>
											</tr>
											<tr>
												<th class="text-right">PERSENTASE</th>
												<th class="text-right"><?= $persentase_ekor ? number_format($persentase_ekor, 2, ',', '.') : '-'; ?>%</th>
												<th class="text-right"><?= number_format($persentase_kg, 2, ',', '.') ?>%</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<!-- PIUTANG -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Piutang</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Bayar</th>
												<th>Piutang</th>
												<th>Sisa Piutang</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$byr_broker = $total_sisa_piutang - $total_piutang_deposit;
											$byr_rpa = (($rpa_total_sisa_piutang + $total_sisa_piutang) - ($rpa_total_piutang_deposit + $total_piutang_deposit)) - $byr_broker;
											?>
											<tr>
												<td class="text-left">Penjualan RPA</td>
												<td class="<?= $byr_rpa < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($byr_rpa, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $rpa_total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td class="text-left">Penjualan Broker</td>
												<td class="<?= $byr_broker < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($byr_broker, 0, ',', '.') ?>,-</span>
												</td>
												<td class="<?= $total_sisa_piutang < 0 ? 'font-weight-bold text-danger' : ''; ?>">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_sisa_piutang, 0, ',', '.') ?>,-</span>
												</td>
												<td class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">TOTAL PIUTANG</th>
												<th class="font-weight-bold text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa_total_piutang_deposit + $total_piutang_deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- PEMBAYARAN -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># H U T A N G</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col-sm-4 d-flex align-items-center">
									<b>No. Pembayaran</b> : #<?= $hutang->no_transaksi ?>
								</div>
								<div class="col-sm-4 d-flex align-items-center">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($hutang->tgl_bayar)) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">

									<table id="hutang_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<!-- <th>No. Transaksi</th> -->
												<th>Supplier</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Bayar</th>
												<th>Hutang</th>
												<th>Sisa Hutang</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i = 1;
											$total_bayar = 0;
											$total_hutang = 0;
											$total_sisa_hutang = 0;
											foreach ($pembelian_pembayaran_detail as $item) : 
												$this->db->select('pembelian_detail.ekor, pembelian_detail.kg, pembelian_detail.hrg_ayam, pembelian_detail.subtotal, pembelian.tgl_beli');
												$this->db->order_by('tgl_beli', 'desc');
												$this->db->join('pembelian', 'pembelian.no_transaksi = pembelian_detail.no_transaksi', 'left');
												$pbn_det = $this->db->get_where('pembelian_detail', ['id_supplier' => $item['id_supplier'], 'tgl_beli' => $tanggal_kemarin])->row();

												$nama = $item['nama'];
												$ekor = null;
												$kg = null;
												$harga = null;
												$jumlah = null;
												$bayar = null;
												$hutang = null;
												$sisa_hutang = null;

												$ekor = $pbn_det->ekor;
												$kg = $pbn_det->kg;
												$harga = $pbn_det->hrg_ayam;
												$jumlah = $pbn_det->subtotal;

												$this->db->order_by('tgl_bayar', 'desc');
												$this->db->join('pembelian_pembayaran', 'pembelian_pembayaran.no_transaksi = pembelian_pembayaran_detail.no_transaksi', 'left');
												$pbp_det = $this->db->get_where('pembelian_pembayaran_detail', ['id_supplier' => $item['id_supplier'], 'tgl_bayar <' => $tanggal_rekap])->row();

												$this->db->order_by('tgl_bayar', 'desc');
												$this->db->join('pembelian_pembayaran', 'pembelian_pembayaran.no_transaksi = pembelian_pembayaran_detail.no_transaksi', 'left');
												$pbpn_det = $this->db->get_where('pembelian_pembayaran_detail', ['id_supplier' => $item['id_supplier'], 'tgl_bayar' => $tanggal_rekap])->row();
												if ($pbpn_det) {
													$bayar = $pbpn_det->bayar;
												}
												?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<!-- <td class="text-center">#<?= $item['no_trxdetail'] ?></td> -->
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $ekor ? number_format($ekor, 0, ',', '.') : '-'; ?></td>
													<td class="text-right"><?= $kg ? number_format($kg, 2, ',', '.') : '-'; ?></td>
													<td class="text-right">
														<?php if ($harga) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($harga, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($jumlah) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($jumlah, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php if ($bayar) : ?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($bayar, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right">
														<?php
														$hutang = -1 * $pbp_det->hutang;
														if (!$pbp_det->hutang) {
															$hutang = -1 * $pbp_det->deposit;
														}
														if ($hutang) :
														?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($hutang, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
													<td class="text-right text-danger">
														<?php
														$sisa_hutang = $pbn_det->subtotal + $hutang - $bayar;
														if ($sisa_hutang) :
														?>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($sisa_hutang, 0, ',', '.') ?>,-</span>
														<?php else : ?>
															-
														<?php endif ?>
													</td>
												</tr>
											<?php
												$total_ekor += $ekor;
												$total_kg += $kg;
												$total_harga += $harga;
												$total_jumlah += $jumlah;
												$total_bayar += $bayar;
												$total_hutang += $hutang;
												$total_sisa_hutang += $sisa_hutang;
											endforeach;
											?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="6" class="text-right">J U M L A H</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_hutang, 0, ',', '.') ?>,-</span>
												</th>
												<th class="text-danger">
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($total_sisa_hutang, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- LABA RUGI & OUTPUT -->
			<div class="row">
				<!-- LABA RUGI -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Laba / Rugi</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left">Belanja</td>
												<td class="text-right"><?= $pembelian->ekor + $sisa->ekor ? number_format($pembelian->ekor + $sisa->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($pembelian->kg + $sisa->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($pembelian->total_hutang + $sisa->jumlah) / ($pembelian->kg + $sisa->kg), 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang + $sisa->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td class="text-left">Penjualan</td>
												<td class="text-right"><?= $penjualan->total_ekor ? number_format($penjualan->total_ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($penjualan->total_kg + $penjualan_broker->total_kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format(($penjualan->jumlah + $penjualan_broker->jumlah) / ($penjualan->total_kg + $penjualan_broker->total_kg), 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan->jumlah + $penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
											<tr>
												<td class="text-left">Sisa</td>
												<td class="text-right"><?= $sisa_stok->ekor ? number_format($sisa_stok->ekor, 0, ',', '.') : '-'; ?></td>
												<td class="text-right"><?= number_format($sisa_stok->kg, 2, ',', '.') ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->harga, 0, ',', '.') ?>,-</span>
												</td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($sisa_stok->jumlah, 0, ',', '.') ?>,-</span>
												</td>
											</tr>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="4" class="text-right">LABA / RUGI</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format((($penjualan->jumlah + $penjualan_broker->jumlah) + $sisa_stok->jumlah) - ($pembelian->total_hutang + $sisa->jumlah) - 1689024.631, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<!-- OUTPUT -->
				<div class="col-md-4 offset-md-1 d-flex align-items-center">
					<div class="card" style="flex-grow: 1">
						<div class="card-header">
							<h3 class="card-title"># Output</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row mt-3">
								<div class="col-12">
									<table id="sisa_stok" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>OUTPUT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<?php
												$this->db->order_by('tanggal', 'desc');
												$output_pokok = $this->db->get_where('pengeluaran', ['tanggal <=' => $tanggal_rekap])->row('total');
												?>
												<th class="text-lg text-center font-weight-bold text-danger">
													<span class="mr-3">Rp.</span><?= number_format($output_pokok, 0, ',', '.') ?>,-
												</th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->