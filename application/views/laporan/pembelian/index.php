<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="<?= base_url('laporan_pembelian/filter') ?>">
                                <div class="form-row">
                                    <div class="form-group col-md-4 mb-md-0">
                                        <select name="pembelian" id="pembelian" class="form-control select2bs4 <?= form_error('pembelian') ? 'is-invalid' : '' ?>" style="width: 100%;">
                                            <option></option>
                                            <?php foreach ($pembelian as $item) : ?>
                                                <option value="<?= $item['no_transaksi'] ?>" <?php (set_value('no_transaksi') == $item['no_transaksi']) ? 'selected' : '' ?>><?= $item['no_transaksi'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <div id="pembelian" class="invalid-feedback">
                                            <?= form_error('pembelian') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 mb-md-0">
                                        <!-- <label for="status">Status</label> -->
                                        <select name="status" id="status" class="form-control select2bs4 <?= form_error('status') ? 'is-invalid' : '' ?>" style="width: 100%;" data-minimum-results-for-search="Infinity">
                                            <option></option>
                                            <option value="Tertunda">Tertunda</option>
                                            <option value="Tertunda">Selesai</option>
                                        </select>
                                        <div id="status" class="invalid-feedback">
                                            <?= form_error('status') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 mb-md-0">
                                        <!-- <label for="tanggal">Rentang Waktu :</label> -->
                                        <div class="input-group">
                                            <button type="button" class="btn btn-default float-right" id="tanggal">
                                                <i class="far fa-calendar-alt mr-2"></i> Pilih Rentang Waktu
                                                <i class="fas fa-caret-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-block">Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="laporan_pembelian" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No. Transaksi</th>
                                        <!-- <th>Pembelian</th> -->
                                        <!-- <th>customer</th> -->
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Ekor</th>
                                        <th>Kg</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($pembelian as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <!-- <a href="<?= base_url('pembelian/detail/') . $item['no_transaksi'] ?>" class="badge badge-light">#<?= $item['no_transaksi'] ?></a> -->
                                                <button type="button" class="btn badge badge-light">#<?= $item['no_transaksi'] ?></button>
                                            </td>
                                            <!-- <td><?= $item['nama'] ?></td> -->
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tgl_beli'])) ?></td>
                                            <td class="text-center">
                                                <?php if ($item['status'] == 'Tertunda') : ?>
                                                    <span class="badge badge-danger"><?= $item['status'] ?></span>
                                                <?php elseif ($item['status'] == 'Selesai') : ?>
                                                    <span class="badge badge-success"><?= $item['status'] ?></span>
                                                <?php endif ?>
                                            </td>
                                            <td class="text-center"><?= $item['ekor'] ?></td>
                                            <td class="text-center"><?= $item['kg'] ?></td>
                                            <td>
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['total_hutang'], 0, ',', '.') ?>,-</span>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->