<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid pb-3">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<form action="<?= base_url('laporan_inout/find') ?>" method="GET">
								<div class="form-row">
									<div class="form-group col-md-4 mb-md-0">
										<input type="date" name="tanggal_inout" value="<?= $tanggal_inout ?>" class="form-control" id="tanggal_inout">
										<div id="tanggal_inout" class="invalid-feedback">
											<?= form_error('tanggal_inout') ?>
										</div>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-block">Cari</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- UANG MASUK -->
			<div class="row">
				<div class="col-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Uang Masuk</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row">
								<div class="col-12">
									<div class="row my-2">
										<div class="col">
											<b>No. Penjualan</b> : #<?= $uang_masuk_rpa->no_transaksi ?>
										</div>
										<div class="col">
											<b>Tanggal</b> : <?= date('d M Y', strtotime($uang_masuk_rpa->tgl_bayar)) ?>
										</div>
									</div>
									<table id="uang_masuk_rpa_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Keterangan</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($uang_masuk_rpa_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot>
											<tr>
												<th class="bg-light text-right" colspan="2">TOTAL</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($uang_masuk_rpa->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="row my-2">
										<div class="col">
											<b>No. Penjualan</b> : #<?= $uang_masuk_broker->no_transaksi ?>
										</div>
										<div class="col">
											<b>Tanggal</b> : <?= date('d M Y', strtotime($uang_masuk_broker->tgl_bayar)) ?>
										</div>
									</div>
									<table id="uang_masuk_broker_detail" class="table table-sm table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Keterangan</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($uang_masuk_broker_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot>
											<tr>
												<th class="bg-light text-right" colspan="2">TOTAL</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($uang_masuk_broker->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<div class="col-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Pengeluaran</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body py-0">
							<div class="row my-2">
								<div class="col">
									<b>No. Pengeluaran</b> : #<?= $pengeluaran->no_transaksi ?>
								</div>
								<div class="col">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($pengeluaran->tanggal)) ?>
								</div>
							</div>
							<div class="row">
								<?php foreach ($this->db->get('pengeluaran_akun')->result_array() as $pengeluaran_akun) : ?>
									<div class="col-12">
										<h6># <?= $pengeluaran_akun['nama'] ?></h6>
										<table id="pengeluaran_detail" class="table table-sm table-bordered table-hover">
											<thead class="thead-light">
												<tr class="text-center">
													<th>#</th>
													<th>Akun</th>
													<th>Keterangan</th>
													<th>Jumlah</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$i = 1;
												$total_pengeluaran_detail = 0;
												foreach ($this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $pengeluaran_akun['id']])->result_array() as $pengeluaran_akun_detail) :
												?>
													<tr>
														<td class="text-center" width="50"><?= $i++ ?></td>
														<td><?= $pengeluaran_akun_detail['nama'] ?></td>
														<?php
														$this->db->order_by('tanggal', 'desc');
														$this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
														$pengeluaran_detail = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => $pengeluaran_akun_detail['id']])->row();
														if ($pengeluaran_detail) {
															$total_pengeluaran_detail += $pengeluaran_detail->bayar;
														}
														?>
														<td><?= $pengeluaran_detail ? $pengeluaran_detail->keterangan : ''; ?></td>
														<td>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= $pengeluaran_detail ? number_format($pengeluaran_detail->bayar, 0, ',', '.') : 0; ?>,-</span>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="3" class="bg-light text-right">TOTAL</th>
													<th>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($total_pengeluaran_detail, 0, ',', '.') ?>,-</span>
													</th>
												</tr>
											</tfoot>
										</table>
									</div>
								<?php endforeach ?>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row -->

			<div class="row">
				<!-- SUBTOTAL UANG MASUK -->
				<div class="col-6">
					<div class="card">
						<div class="card-body">
							<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
								<tr>
									<th class="text-center bg-light">SUBTOTAL</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($uang_masuk_rpa->total_bayar + $uang_masuk_broker->total_bayar, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- SUBTOTAL PENGELUARAN -->
				<div class="col-6">
					<div class="card">
						<div class="card-body">
							<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
								<tr>
									<th class="text-center bg-light">SUBTOTAL</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->total, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row -->


			<?php
			// $this->db->select('SUM(bayar) AS subtotal');
			// $this->db->order_by('tanggal', 'desc');
			// $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
			// $this->db->join('pengeluaran_akun_detail', 'pengeluaran_akun_detail.id = pengeluaran_detail.id_pengeluaran_akun_detail', 'left');
			// $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_akun_detail.id_pengeluaran_akun', 'left');
			// $sub_kantor = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'pengeluaran_akun.id' => 3])->row('subtotal');

			// $this->db->order_by('tanggal', 'desc');
			// $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
			// $mob_pemeliharaan = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 6])->row('bayar');

			// $this->db->order_by('tanggal', 'desc');
			// $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
			// $prod_plastik = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 11])->row('bayar');

			// $this->db->order_by('tanggal', 'desc');
			// $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
			// $prod_pemeliharaan = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 12])->row('bayar');
			// $output_pokok = ($pengeluaran->total - ($sub_kantor + ($mob_pemeliharaan + $prod_plastik + $prod_pemeliharaan)));
			// $tot_setor_bank = ($uang_masuk_rpa->total_bayar + $uang_masuk_broker->total_bayar) - $output_pokok;

			// $this->db->order_by('tgl_bayar', 'desc');
			// $setoran_rpa = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

			// $this->db->order_by('tgl_bayar', 'desc');
			// $setoran_broker = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

			// $setor_tabungan = $pengeluaran->total - $output_pokok;

			// $setor_modal_rpa = $setoran_rpa - $pengeluaran->total;
			// $setor_modal_total = 53285200;
			// $sisa_setoran = $tot_setor_bank - $setor_tabungan - $setor_modal_total;
			?>
			<div class="row">
				<!-- OUTPUT -->
				<div class="col-6">
					<div class="card">
						<div class="card-body">
							<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
								<tr>
									<th class="bg-light">TOTAL SETOR BANK</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->tot_setor_bank, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SETORAN RPA</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->setoran_rpa, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SETORAN BROKER</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->setoran_broker, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SETOR TABUNGAN</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->setor_tabungan, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SETOR MODAL RPA</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->setor_modal_rpa, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SETOR MODAL TOTAL</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->setor_modal_total, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
								<tr>
									<th class="bg-light">SISA SETORAN</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->sisa_setoran, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- OUTPUT POKOK -->
				<div class="col-6">
					<div class="card">
						<div class="card-body">
							<table id="pembelian_detail" class="table table-sm table-bordered table-hover">
								<tr>
									<th class="text-center bg-light">OUTPUT POKOK</th>
									<th>
										<span class="float-left">Rp.</span>
										<span class="float-right"><?= number_format($pengeluaran->output_pokok, 0, ',', '.') ?>,-</span>
									</th>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row -->
			<div class="row">
				<!-- PARAF AUDITOR -->
				<div class="col-6 offset-6">
					<div class="card h-100">
						<div class="card-header">
							<h3 class="card-title"># Paraf Auditor</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body text-center pb-0">
							<b>PT. PRAKARSA PUTRA BROILER</b>
							<br>
							<br>
							<br>
							<br>
							<br>
							<b><u> SUKANO </u></b>
							<br>
							<p>Auditor</p>
						</div>
						<!-- /.card-body -->
						<!-- /.card -->
					</div>
					<!-- /.col -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->