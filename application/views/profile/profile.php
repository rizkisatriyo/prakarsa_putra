<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="mb-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <?php
                            $avatar = $current_user->avatar ?
                                base_url('assets/dist/upload/img/avatar/' . $current_user->avatar)
                                : get_gravatar($current_user->email)
                            ?>
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="<?= $avatar ?>" alt="User profile picture" style="object-fit: cover;">
                            </div>

                            <h3 class="profile-username text-center"><?= html_escape($current_user->name) ?></h3>

                            <p class="text-muted text-center"><?= html_escape($current_user->email) ?></p>

                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-avatar">
                                <i class="fas fa-user-circle mr-2"></i><b>Ubah Avatar</b>
                            </button>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#about" data-toggle="tab">Tentang Saya</a></li>
                                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Pengaturan</a></li>
                                <li class="nav-item"><a class="nav-link" href="#up_pw" data-toggle="tab">Ubah Password</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="about">
                                    <strong><i class="fas fa-edit mr-1"></i> Nama</strong>
                                    <p class="text-muted"><?= $user->name ?></p>
                                    <hr>

                                    <strong><i class="fas fa-user mr-1"></i> Username</strong>
                                    <p class="text-muted"><?= $user->username ?></p>
                                    <hr>

                                    <strong><i class="fas fa-phone fa-flip-horizontal mr-1"></i> No. Telefon</strong>
                                    <p class="text-muted"><?= $user->phone ?></p>
                                    <hr>

                                    <strong><i class="fas fa-envelope mr-1"></i> Email</strong>
                                    <p class="text-muted"><?= $user->email ?></p>
                                    <hr>

                                    <strong><i class="fas fa-file-alt mr-1"></i> Alamat</strong>
                                    <p class="text-muted"><?= nl2br($user->address) ?></p>
                                </div>

                                <div class="tab-pane" id="settings">
                                    <form action="<?= base_url('profile/update') ?>" method="POST" class="form-horizontal">
                                        <div class="form-group row">
                                            <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" value="<?= set_value('name', $user->name) ?>" class="form-control <?= form_error('name') ? 'is-invalid' : '' ?>" id="inputNama" placeholder="Nama ..">
                                                <div id="inputNama" class="invalid-feedback">
                                                    <?= form_error('name') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputTelefon" class="col-sm-2 col-form-label">Telefon</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="phone" value="<?= $user->phone ?>" class="form-control <?= form_error('phone') ? 'is-invalid' : '' ?>" id="inputTelefon" placeholder="Telefon ..">
                                                <div id="inputTelefon" class="invalid-feedback">
                                                    <?= form_error('name') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" value="<?= $user->email ?>" class="form-control <?= form_error('email') ? 'is-invalid' : '' ?>" id="inputEmail" placeholder="Email ..">
                                                <div id="inputEmail" class="invalid-feedback">
                                                    <?= form_error('name') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" name="address" id="inputAlamat" placeholder="Alamat .."><?= $user->address ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row mb-0 text-right">
                                            <div class="col">
                                                <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i>Reset</button>
                                                <button type="submit" class="btn btn-success"><i class="fas fa-save mr-2"></i>Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="up_pw">
                                    <form action="<?= base_url('profile/update_pw') ?>" method="POST" class="form-horizontal">
                                        <div class="form-group row">
                                            <label for="password" class="col-sm-3 col-form-label">Password baru</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password" value="<?= set_value('password') ?>" class="form-control <?= form_error('password') ? 'is-invalid' : '' ?>" id="password" placeholder="Masukkan password baru ..">
                                                <div id="password" class="invalid-feedback">
                                                    <?= form_error('password') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password_confirmation" class="col-sm-3 col-form-label">Konfirmasi password</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password_confirmation" value="<?= set_value('password_confirmation') ?>" class="form-control <?= form_error('password_confirmation') ? 'is-invalid' : '' ?>" id="password_confirmation" placeholder="Masukkan ulang password ..">
                                                <div id="password_confirmation" class="invalid-feedback">
                                                    <?= form_error('password_confirmation') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-0 text-right">
                                            <div class="col">
                                                <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i>Reset</button>
                                                <button type="submit" class="btn btn-success"><i class="fas fa-save mr-2"></i>Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="modal-avatar">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="<?= base_url('profile/upload_avatar') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Ubah Avatar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="avatar">Pilih Gambar</label>
                            <input type="file" name="avatar" class="form-control-file <?= form_error('avatar') ? 'is-invalid' : '' ?>" id="avatar" accept="image/png, image/jpeg, image/jpg, image/gif" required>
                            <small class="text-danger text-sm"><?= $error ?></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->