<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="stok_keluar" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No Checker</th>
                                        <th>Penjualan</th>
                                        <th>Tanggal</th>
                                        <th>Ekor</th>
                                        <th>Kg</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($stok_keluar as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <a href="<?= base_url('penjualan_checker/detail/') . $item['no_checker'] ?>" class="badge badge-light">#<?= $item['no_checker'] ?></a>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?= base_url('penjualan/detail/') . $item['no_transaksi_penjualan'] ?>" class="badge badge-light">#<?= $item['no_transaksi_penjualan'] ?></a>
                                            </td>
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tgl_check'])) ?></td>
                                            <td class="text-center"><?= $item['total_ekor'] ?></td>
                                            <td class="text-center"><?= $item['total_kg'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->