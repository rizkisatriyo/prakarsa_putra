<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <a href="<?= base_url('saldo_awal/tambah') ?>" class="btn btn-md btn-primary">
                        <i class="fas fa-sync-alt mr-2"></i>
                        Perbarui Saldo
                    </a>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="saldo_awal" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No. Transaksi</th>
                                        <th>Tanggal</th>
                                        <th>Total</th>
                                        <!-- <th>Keterangan</th> -->
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($saldo_awal as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <a href="<?= base_url('saldo_awal/detail/') . $item['no_transaksi'] ?>" class="badge badge-light">#<?= $item['no_transaksi'] ?></a>
                                            </td>
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tanggal'])) ?></td>
                                            <td class="text-right"><?= number_format($item['total'], 2, ',', '.') ?></span></td>
                                            <!-- <td><?= nl2br($saldo_awal['keterangan']) ?></td> -->
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('saldo_awal/detail/') . $item['no_transaksi'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->