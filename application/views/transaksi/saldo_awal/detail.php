<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('saldo_awal') ?>">Saldo Awal</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<?php foreach ($this->db->get('pengeluaran_akun')->result_array() as $pengeluaran_akun) : ?>
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title"># <?= $pengeluaran_akun['nama'] ?></h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<table id="saldo_awal_detail" class="table table-sm table-bordered table-hover">
									<thead class="thead-light">
										<tr class="text-center">
											<th>#</th>
											<th>Akun</th>
											<th>Keterangan</th>
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 1;
										$total_saldo_awal_detail = 0;
										foreach ($this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $pengeluaran_akun['id']])->result_array() as $pengeluaran_akun_detail) :
										?>
											<!-- DIDIEUUUU LANJUTKEN SALDO AWAL DETAIL DI DB MASI KOSONG -->
											<tr>
												<td class="text-center" width="50"><?= $i++ ?></td>
												<td><?= $pengeluaran_akun_detail['nama'] ?></td>
												<?php
												$this->db->order_by('tanggal', 'desc');
												$this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
												$saldo_awal_detail = $this->db->get_where('saldo_awal_detail', ['saldo_awal.no_transaksi' => $no_transaksi, 'id_pengeluaran_akun_detail' => $pengeluaran_akun_detail['id']])->row();
												if ($saldo_awal_detail) {
													$total_saldo_awal_detail += $saldo_awal_detail->bayar;
												}
												?>
												<td><?= $saldo_awal_detail ? $saldo_awal_detail->keterangan : ''; ?></td>
												<td class="text-right"><?= $saldo_awal_detail ? number_format($saldo_awal_detail->bayar, 2, ',', '.') : 0; ?></td>
											</tr>
										<?php endforeach ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="3" class="bg-light text-right">TOTAL</th>
											<th class="text-right"><?= number_format($total_saldo_awal_detail, 2, ',', '.') ?></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
				<?php endforeach ?>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># SUBTOTAL</h3>
						</div>
						<div class="card-body">
							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover">
										<tbody>
											<tr class="text-center">
												<th class="bg-light">J U M L A H</th>
												<th><?= number_format($saldo_awal->total, 2, ',', '.') ?></th>
											</tr>
											<!-- DIDIEUUU !!!!!!!! NEXT NU SALDO PEWMELIHARAAN NA KUMAHA -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->