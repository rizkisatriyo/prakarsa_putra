<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('saldo_awal') ?>">Saldo Awal</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <form method="POST">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-sm-4 col-md-3">
                                        <label for="tanggal">Tanggal</label>
                                        <input type="date" name="tanggal" value="<?= date('Y-m-d') ?>" class="form-control tanggal" id="tanggal" placeholder="Tanggal .." required>
                                        <div id="tanggal" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group col-sm-4 col-md-3">
                                        <label for="sisa_kandang">No. Saldo Awal</label>
                                        <input type="text" name="no_transaksi" value="<?= set_value('no_transaksi', $trans_no) ?>" class="form-control" id="no_transaksi" placeholder="No. Transaksi .." required disabled>
                                        <div id="no_transaksi" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="id_pengeluaran_akun">Jenis Pengeluaran</label>
                                        <select name="id_pengeluaran_akun" id="id_pengeluaran_akun" class="form-control select2bs4 <?= form_error('id_pengeluaran_akun') ? 'is-invalid' : '' ?>" style="width: 100%;">
                                            <option></option>
                                            <?php foreach ($pengeluaran_akun as $item) : ?>
                                                <option value="<?= $item['id'] ?>" <?php (set_value('id') == $item['id']) ? 'selected' : '' ?> data-nama="<?= $item['nama'] ?>"><?= $item['nama'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <div id="id_pengeluaran_akun" class="invalid-feedback">
                                            <?= form_error('id_pengeluaran_akun') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3 col-sm-3">
                                        <label for="id_pengeluaran_akun_detail">Akun Pengeluaran</label>
                                        <select name="id_pengeluaran_akun_detail" id="id_pengeluaran_akun_detail" class="form-control select2bs4 <?= form_error('id_pengeluaran_akun_detail') ? 'is-invalid' : '' ?>" style="width: 100%;">
                                            <option></option>
                                        </select>
                                        <div id="id_pengeluaran_akun_detail" class="invalid-feedback">
                                            <?= form_error('id_pengeluaran_akun_detail') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6">
                                        <label for="keterangan">Keterangan</label>
                                        <textarea name="keterangan" class="form-control <?= form_error('keterangan') ? 'is-invalid' : '' ?>" id="keterangan" rows="1" placeholder="Keterangan .."><?= set_value('keterangan') ?></textarea>
                                        <div id="keterangan" class="invalid-feedback">
                                            <?= form_error('keterangan') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6">
                                        <label for="jml_bayar">Jumlah</label>
                                        <input type="number" name="jml_bayar" value="<?= set_value('jml_bayar') ?>" class="form-control <?= form_error('jml_bayar') ? 'is-invalid' : '' ?>" id="jml_bayar" placeholder="Jumlah ..">
                                        <div id="jml_bayar" class="invalid-feedback">
                                            <?= form_error('jml_bayar') ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 d-flex align-items-end">
                                        <button type="submit" class="btn btn-primary btn-block" id="tambah_detail">
                                            <i class="fas fa-plus-circle mr-2"></i> Tambahkan
                                        </button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col table-responsive">
                                        <table id="saldo_awal_detail" class="table table-bordered table-hover">
                                            <thead class="thead-light">
                                                <tr class="text-center">
                                                    <th>#</th>
                                                    <th>No. Transaksi</th>
                                                    <th>Jenis Pengeluaran</th>
                                                    <th>Akun Pengeluaran</th>
                                                    <th>Keterangan</th>
                                                    <th>Jumlah</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1 ?>
                                                <?php foreach ($saldo_awal_detail as $item) : ?>
                                                    <tr>
                                                        <td class="text-center" width="50"><?= $i++ ?></td>
                                                        <?php $colors = ['', 'primary', 'success', 'warning', 'danger', 'info'] ?>
                                                        <td class="text-center">
                                                            <span class="badge badge-<?= $colors[$item['id_pengeluaran_akun']] ?>"><?= $item['kategori'] ?></span>
                                                        </td>
                                                        <td><?= $item['nama'] ?></td>
                                                        <!-- <td class="text-center">
                                                            <?php if ($item['biaya_variabel'] == 1) : ?>
                                                                <span class="badge badge-pill badge-danger">Variabel</span>
                                                            <?php else : ?>
                                                                <span class="badge badge-pill badge-info">Tetap</span>
                                                            <?php endif ?>
                                                        </td> -->
                                                        <td><?= $item['keterangan'] ?></td>
                                                        <td><input type="number" name="jml_bayar" value="<?= set_value('jml_bayar',$item['jumlah']) ?>" class="form-control <?= form_error('jml_bayar') ? 'is-invalid' : '' ?>" id="jml_bayar" placeholder="Jumlah .."></td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <!-- <a href="<?= base_url('pengeluaran_akun_detail/detail/') . $item['id'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a> -->
                                                                <?php if ($item['set_lock'] != 1) : ?>
                                                                    <a href="<?= base_url('pengeluaran_akun_detail/ubah/') . $item['id'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                                                    <a href="<?= base_url('pengeluaran_akun_detail/hapus/') . $item['id'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a>
                                                                <?php endif ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer text-right">
                                <button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
                                    <i class="fas fa-arrow-left mr-2"></i>
                                    Kembali
                                </button>
                                <!-- <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button> -->
                                <button type="submit" class="btn btn-primary simpan_saldo_awal"><i class="fas fa-save mr-2"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row pb-3">
                <div class="col">
                    <h6>Demo saldo_awal</h6>
                    <button id="demo_26" class="btn btn-primary">26 Februari 2022</button>
                    <button id="demo_27" class="btn btn-primary">27 Februari 2022</button>
                    <button id="demo_28" class="btn btn-primary">28 Februari 2022</button>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->