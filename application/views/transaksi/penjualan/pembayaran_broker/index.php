<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="pembayaran_penjualan_broker" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No. Transaksi</th>
                                        <!-- <th>penjualan</th> -->
                                        <th>Tanggal</th>
                                        <!-- <th>Status</th> -->
                                        <th>Total Piutang</th>
                                        <th>Total Bayar</th>
                                        <th>Total Deposit</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($pembayaran_penjualan_broker as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <a href="<?= base_url('pembayaran_penjualan_broker/detail/') . $item['no_transaksi'] ?>" class="badge badge-light">#<?= $item['no_transaksi'] ?></a>
                                            </td>
                                            <!-- <td class="text-center">
                                                <a href="<?= base_url('penjualan/detail/') . $item['no_transaksi_penjualan'] ?>" class="badge badge-secondary">#<?= $item['no_transaksi_penjualan'] ?></a>
                                            </td> -->
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tgl_bayar'])) ?></td>
                                            <!-- <td class="text-center">
                                                <?php if ($item['status'] == 'Tertunda') : ?>
                                                    <span class="badge badge-danger"><?= $item['status'] ?></span>
                                                <?php elseif ($item['status'] == 'Selesai') : ?>
                                                    <span class="badge badge-success"><?= $item['status'] ?></span>
                                                <?php endif ?>
                                            </td> -->
                                            <!-- <td class="text-center"><?= number_format($item['ekor'], 0, ',', '.') ?></td>
                                            <td class="text-center"><?= number_format($item['kg'], 2, ',', '.') ?></td> -->
                                            <td class="<?= $item['total_piutang'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['total_piutang'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="<?= $item['total_bayar'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['total_bayar'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="<?= $item['total_deposit'] < 0 ? 'font-weight-bold text-danger' : ''; ?>">
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['total_deposit'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('pembayaran_penjualan_broker/detail/') . $item['no_transaksi'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                    <!-- <a href="<?= base_url('pembayaran_penjualan_broker/ubah/') . $item['no_transaksi'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a> -->
                                                    <!-- <a href="<?= base_url('pembayaran_penjualan_broker/hapus/') . $item['no_transaksi'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a> -->
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->