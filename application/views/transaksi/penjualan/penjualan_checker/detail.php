<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('penjualan_checker') ?>">Checker Penjualan</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<!-- <div class="col-sm-4 col-md-3">
									<b>No. Transaksi</b> : #<?= $penjualan_checker->no_transaksi ?>
								</div> -->
								<div class="col-sm-4 col-md-3">
									<b>No. Checker</b> : #<?= $penjualan_checker->no_checker ?>
								</div>
								<div class="col-sm-4 col-md-3">
									<b>Tanggal</b> : <?= date('d M Y', strtotime($penjualan_checker->tgl_check)) ?>
								</div>
								<div class="col-sm-4 col-md-3 offset-md-3">
									<b>Penjualan</b> : #<?= $penjualan_checker->no_transaksi_penjualan ?>
								</div>
							</div>
							<hr>

							<div class="row mt-3">
								<div class="col-lg-4 table-responsive">
									<h6 class="font-weight-bold mt-0"># Detail Penjualan</h6>
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>Customer</th>
												<th>Ekor</th>
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($penjualan_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-' ?></td>
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot>
											<tr class="bg-light">
												<th colspan="2" class="text-right align-middle">Total Keseluruhan</th>
												<th class="text-right"><?= number_format($penjualan->total_ekor, 0, ',', '.') ?></th>
												<th class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></th>
											</tr>
										</tfoot>
									</table>

									<hr>
									<h6 class="font-weight-bold mt-0"># Detail Penerimaan</h6>
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Ekor</th>
												<th>Kg</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th class="bg-light" style="width: 150px;">Total Penjualan</th>
												<td class="text-right"><?= number_format($penjualan_checker->total_ekor, 0, ',', '.') ?></td>
												<td class="text-right"><?= number_format($penjualan_checker->total_kg, 2, ',', '.') ?></td>
											</tr>
											<tr>
												<th class="bg-light" style="width: 150px;">Sisa</th>
												<td class="text-right"><?= $penjualan_checker->sisa_ekor ? number_format($penjualan_checker->sisa_ekor, 0, ',', '.') : '-' ?></td>
												<td class="text-right"><?= $penjualan_checker->sisa_kg ? number_format($penjualan_checker->sisa_kg, 2, ',', '.') : '-' ?></td>
											</tr>
											<tr>
												<th class="bg-light" style="width: 150px;">Mati</th>
												<td class="text-right"><?= $penjualan_checker->mati_ekor ? number_format($penjualan_checker->mati_ekor, 0, ',', '.') : '-' ?></td>
												<td class="text-right"><?= $penjualan_checker->mati_kg ? number_format($penjualan_checker->mati_kg, 2, ',', '.') : '-' ?></td>
											</tr>
										</tbody>
									</table>

									<hr>
									<h6 class="font-weight-bold mt-0"># Detail Penyusutan</h6>
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<!-- <th colspan="4" class="text-center">Penyusutan</th> -->
												<th>Ekor</th>
												<!-- <th>Presentase Ekor</th> -->
												<th>Kg</th>
												<th>Presentase Kg</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center font-weight-bold"><?= $penerimaan_susut->ekor ? number_format($penerimaan_susut->ekor, 0, ',', '.') : '-' ?></td>
												<!-- <td class="text-center font-weight-bold"><?= $penerimaan_susut->presentase_ekor ? number_format($penerimaan_susut->presentase_ekor, 2, ',', '.') . '%' : '-' ?></td> -->
												<td class="text-center font-weight-bold"><?= $penerimaan_susut->kg ? number_format($penerimaan_susut->kg, 2, ',', '.') : '-' ?></td>
												<td class="text-center font-weight-bold"><?= $penerimaan_susut->presentase_kg ? number_format($penerimaan_susut->presentase_kg, 2, ',', '.') . '%' : '-' ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-lg-8">
									<h6 class="font-weight-bold mt-0"># Detail Checker</h6>
									<div class="row d-flex justify-content-center">
										<?php $detail = $this->db->get_where('penjualan_checker_detail', ['no_checker' => $penjualan_checker->no_checker])->result_array(); ?>
										<?php foreach ($kandang as $item) : ?>
											<?php if (array_search($item['id'], array_column($detail, 'id_kandang')) !== FALSE) : ?>
												<div class="col-md-4 col-sm-6">
													<table class="table table-bordered table-hover table-sm table-checker" id="table-<?= $item['id'] ?>" data-id_kandang="<?= $item['id'] ?>">
														<thead class="thead-light">
															<tr>
																<th colspan="3" class="text-center"><?= $item['nama'] ?></th>
															</tr>
														</thead>
														<tbody>
															<?php
															$total_ekor = 0;
															$total_kg = 0;
															foreach ($penjualan_checker_detail as $item_checker) :
															?>
																<?php
																if ($item_checker['id_kandang'] == $item['id']) :
																	$total_ekor += $item_checker['ekor'];
																	$total_kg += $item_checker['kg'];
																?>
																	<tr>
																		<td><?= $item_checker['nama'] ?></td>
																		<td class="text-right"><?= number_format($item_checker['ekor'], 0, ",", '.') ?></td>
																		<td class="text-right"><?= number_format($item_checker['kg'], 2, ",", '.') ?></td>
																	</tr>
																<?php endif ?>
															<?php endforeach ?>
														</tbody>
														<tfoot>
															<tr class="bg-light">
																<th class="text-center">TOTAL</th>
																<th class="text-right"><?= number_format($total_ekor, 0, ",", '.') ?></th>
																<th class="text-right"><?= number_format($total_kg, 2, ",", '.') ?></th>
															</tr>
														</tfoot>
													</table>
												</div>
											<?php endif; ?>
										<?php endforeach ?>
									</div>
									<div class="row d-flex align-items-end">
										<div class="col mt-5">
											<hr>
											<h6 class="font-weight-bold mt-0"># Total Keseluruhan</h6>
											<table class="table table-bordered">
												<tbody>
													<tr>
														<th class="bg-light align-middle text-right">Total Ekor</th>
														<td class="text-center font-weight-bold"><?= number_format($penjualan_checker->total_ekor, 0, ",", '.') ?></td>
														<th class="bg-light align-middle text-right">Total Kg</th>
														<td class="text-center font-weight-bold"><?= number_format($penjualan_checker->total_kg, 2, ",", '.') ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->