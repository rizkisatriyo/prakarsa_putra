<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('penjualan_checekr') ?>">Checker Penjualan</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<form>
							<div class="card-body">
								<div class="form-row">
									<div class="form-group col-sm-4 col-md-3">
										<label for="tgl_check">Tanggal</label>
										<input type="date" name="tgl_check" value="<?= date('Y-m-d') ?>" class="form-control <?= form_error('tgl_check') ? 'is-invalid' : '' ?>" id="tgl_check" placeholder="Tanggal .." required>
										<div id="tgl_check" class="invalid-feedback">
											<?= form_error('tgl_check') ?>
										</div>
									</div>
									<div class="form-group col-sm-4 col-md-3">
										<label for="no_checker">No. Checker</label>
										<input type="text" name="no_checker" value="<?= set_value('no_checker', $no_checker) ?>" class="form-control <?= form_error('no_checker') ? 'is-invalid' : '' ?>" id="no_checker" placeholder="No. Checker .." required>
										<div id="no_checker" class="invalid-feedback">
											<?= form_error('no_checker') ?>
										</div>
									</div>
									<div class="form-group col-sm-4 col-md-3 offset-md-3">
										<label for="no_transaksi_penjualan">Penjualan</label>
										<input type="text" name="no_transaksi_penjualan" value="<?= set_value('no_transaksi_penjualan', $no_transaksi_penjualan) ?>" class="form-control <?= form_error('no_transaksi_penjualan') ? 'is-invalid' : '' ?>" id="no_transaksi_penjualan" placeholder="No. Transaksi .." required disabled>
										<div id="no_transaksi_penjualan" class="invalid-feedback">
											<?= form_error('no_transaksi_penjualan') ?>
										</div>
									</div>
								</div>

								<div class="row mt-3">
									<div class="col-lg-4 table-responsive">
										<h6 class="font-weight-bold mt-0"># Detail Penjualan : #<?= $penjualan->no_transaksi ?></h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<thead class="thead-light">
												<tr class="text-center">
													<th>#</th>
													<th>Customer</th>
													<th>Ekor</th>
													<th>Kg</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 1 ?>
												<?php foreach ($detail as $item) : ?>
													<tr>
														<td class="text-center" width="50"><?= $i++ ?></td>
														<td><?= $item['nama'] ?></td>
														<td class="text-right"><?= $item['ekor'] ? $item['ekor'] : '-' ?></td>
														<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
													</tr>
												<?php endforeach ?>
											</tbody>
											<tfoot>
												<tr class="bg-light">
													<th colspan="2" class="text-right align-middle">Total Keseluruhan</th>
													<th class="text-right"><?= $penjualan->total_ekor ?></th>
													<th class="text-right"><?= number_format($penjualan->total_kg, 2, ',', '.') ?></th>
												</tr>
											</tfoot>
										</table>

										<hr>
										<h6 class="font-weight-bold mt-0"># Detail</h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<tbody>
												<tr>
													<th class="bg-light" style="width: 150px;">Total Stok</th>
													<td><input type="number" name="t_stok_ekor" value="<?= $t_stok->ekor ?>" id="t_stok_ekor" class="form-control form-control-sm text-center" required disabled></td>
													<td><input type="number" name="t_stok_kg" value="<?= $t_stok->kg ?>" id="t_stok_kg" class="form-control form-control-sm text-center" required disabled></td>
												</tr>
												<tr>
													<th class="bg-light" style="width: 150px;">Total Penjualan</th>
													<td><input type="number" name="p_total_ekor" id="p_total_ekor" class="form-control form-control-sm text-center" required disabled></td>
													<td><input type="number" name="p_total_kg" id="p_total_kg" class="form-control form-control-sm text-center" required disabled></td>
												</tr>
												<tr>
													<th class="bg-light" style="width: 150px;">Sisa</th>
													<td><input type="number" name="sisa_ekor" id="sisa_ekor" class="form-control form-control-sm text-center" required></td>
													<td><input type="number" name="sisa_kg" id="sisa_kg" class="form-control form-control-sm text-center" required></td>
													<?php
													$tgl = date('Y-m-d', strtotime('-1 day', strtotime($penjualan->tgl_jual)));
													$this->db->order_by('tgl_beli', 'desc');
													$beli = $this->db->get_where('pembelian', ['tgl_beli <=' => $tgl])->row();
													$sisa = $this->db->get_where('sisa_stok', ['tanggal <=' => $tgl])->row();
													$jml = $beli->total_hutang + $sisa->jumlah;
													$harga = $jml / ($beli->kg + $sisa->kg);
													?>
													<input type="hidden" name="sisa_harga" id="sisa_harga" value="<?= $harga ?>">
													<!-- <input type="hidden" name="sisa_jumlah" value="0"> -->
												</tr>
												<tr>
													<th class="bg-light" style="width: 150px;">Mati</th>
													<td><input type="number" name="mati_ekor" id="mati_ekor" class="form-control form-control-sm text-center"></td>
													<td><input type="number" name="mati_kg" id="mati_kg" class="form-control form-control-sm text-center"></td>
												</tr>
											</tbody>
										</table>

										<hr>
										<h6 class="font-weight-bold mt-0"># Detail Penyusutan</h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<thead class="thead-light">
												<tr>
													<th colspan="4" class="text-center">Penyusutan</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="number" name="susut_ekor" id="susut_ekor" class="form-control form-control-sm text-center font-weight-bold" disabled></td>
													<td><input type="number" name="susut_kg" id="susut_kg" class="form-control form-control-sm text-center font-weight-bold" disabled></td>
													<td><input type="number" name="presentase_susut_kg" id="presentase_susut_kg" class="form-control form-control-sm text-center font-weight-bold" disabled></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-lg-8">
										<h6 class="font-weight-bold mt-0"># Input Checker : <span class="txt_no_checker">#<?= $no_checker ?></span></h6>
										<?php
										$this->db->select('customers.id AS c_id');
										$this->db->select('nama');
										$this->db->join('customers', 'customers.id = penjualan_detail.id_customer');
										$this->db->order_by('nama');
										$cs = $this->db->get_where('penjualan_detail', ['no_transaksi' => $no_transaksi_penjualan])->result_array();
										?>
										<div class="row d-flex justify-content-center">
											<?php foreach ($kandang as $item) : ?>
												<div class="col-md-6 col-sm-6">
													<table class="table table-bordered table-hover table-sm table-checker" id="table-<?= $item['id'] ?>" data-id_kandang="<?= $item['id'] ?>">
														<thead>
															<tr>
																<th colspan="3" class="text-center"><?= $item['nama'] ?></th>
																<input type="hidden" name="id_kandang" value="<?= $item['id'] ?>" id="id_kandang">
															</tr>
														</thead>
														<tbody>
															<tr>
																<td style="width: 35%;">
																	<select name="id_customer" id="id_customer" class="select2bs4 form-control form-control-sm <?= form_error('id_customer') ? 'is-invalid' : '' ?>">
																		<option selected disabled></option>
																		<?php foreach ($cs as $c) : ?>
																			<option class="pr-2" value="<?= $c['c_id'] ?>" <?php (set_value('c_id') == $c['c_id']) ? 'selected' : '' ?> data-nama="<?= $c['nama'] ?>"><?= $c['nama'] ?></option>
																		<?php endforeach ?>
																	</select>
																</td>
																<td><input type="number" name="ekor" class="form-control form-control-sm text-center ekor" id="ekor"></td>
																<td><input type="number" name="kg" class="form-control form-control-sm text-center kg" id="kg"></td>
															</tr>
														</tbody>
														<tfoot>
															<tr>
																<td colspan="3">
																	<button type="button" class="btn btn-primary btn-sm btn-block tambah_baru" data-id_kandang="<?= $item['id'] ?>" data-no_trans="<?= $no_transaksi_penjualan ?>">
																		<i class="fas fa-plus mr-2 fa-sm"></i> Tambah Baru
																	</button>
																</td>
															</tr>
															<!-- <tr>
                                                                <th><input type="number" name="ekor" class="form-control form-control-sm text-center" id="total_ekor-<?= $item['id'] ?>" placeholder="Ekor" disabled></th>
                                                                <th><input type="number" name="kg" class="form-control form-control-sm text-center" id="total_kg-<?= $item['id'] ?>" placeholder="Kg" disabled></th>
                                                            </tr> -->
														</tfoot>
													</table>
												</div>
											<?php endforeach ?>
										</div>
										<div class="row mt-1 d-flex align-items-end">
											<div class="col">
												<hr>
												<h6 class="font-weight-bold mt-0"># Total Keseluruhan</h6>
												<table class="table table-bordered">
													<tbody>
														<tr>
															<th class="bg-light align-middle">Total Ekor</th>
															<td>
																<input type="number" name="total_ekor" id="total_ekor" class="form-control text-center font-weight-bold" required disabled>
																<!-- <div id="total_ekor" class="invalid-feedback">
																	<?= form_error('total_ekor') ?>
																</div> -->
															</td>
															<th class="bg-light align-middle">Total Kg</th>
															<td>
																<input type="number" name="total_kg" id="total_kg" class="form-control text-center font-weight-bold" required disabled>
																<!-- <div id="total_kg" class="invalid-feedback">
																	<?= form_error('total_kg') ?>
																</div> -->
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer text-right">
								<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
									<i class="fas fa-arrow-left mr-2"></i>
									Kembali
								</button>
								<button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button>
								<button type="button" class="btn btn-primary simpan"><i class="fas fa-save mr-2"></i> Simpan</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<div class="row pb-3">
				<div class="col">
					<h6>Demo Checker Penjualan</h6>
					<button id="demo_26" class="btn btn-primary">26 Februari 2022</button>
					<button id="demo_27" class="btn btn-primary">27 Februari 2022</button>
					<button id="demo_28" class="btn btn-primary">28 Februari 2022</button>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->