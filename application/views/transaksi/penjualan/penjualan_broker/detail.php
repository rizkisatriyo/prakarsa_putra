<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('penjualan_broker') ?>">Data Penjualan_broker</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">
					<div class="card">
						<!-- <form action="<?= base_url('penjualan_broker/aksi_tambah') ?>" method="POST"> -->
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6">
									<!-- <div class="row">
										<div class="col-12">
											<div class="btn-group">
												<?php if ($penjualan_broker->status != 'Selesai') : ?>
													<a href="<?= base_url('penjualan_broker_checker/tambah/') . $penjualan_broker->no_transaksi ?>" class="btn btn-outline-primary">Input Checker</a>
												<?php endif ?>
											</div>
										</div>
									</div> -->
									<div class="row mt-2">
										<div class="col-md-7">
											<b>No. Penjualan_broker</b> : #<?= $penjualan_broker->no_transaksi ?>
										</div>
										<div class="col-md-5 text-right">
											<b>Tanggal</b> : <?= date('d/m/Y', strtotime($penjualan_broker->tgl_jual)) ?>
										</div>
									</div>
								</div>
								<!-- <div class="col-sm-6 text-right">
									<?php if ($penjualan_broker->status == 'Tertunda') : ?>
										<h6 class="display-4 text-danger"><?= $penjualan_broker->status ?> <i class="fas fa-clock fa-sm"></i></h6>
									<?php elseif ($penjualan_broker->status == 'Selesai') : ?>
										<h6 class="display-4 text-success"><?= $penjualan_broker->status ?> <i class="fas fa-check fa-sm"></i></h6>
									<?php endif ?>
								</div> -->
							</div>

							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Broker</th>
												<th>Supplier(s)</th>
												<th>Kg</th>
												<th>Harga Ayam</th>
												<th>Subtotal</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<ul class="pl-4 mb-0">
															<?php
															$this->db->join('suppliers', 'suppliers.id = penjualan_broker_detail_supplier.id_supplier', 'left');
															$supps = $this->db->get_where('penjualan_broker_detail_supplier', ['no_trxdetail' => $item['no_trxdetail']])->result_array();
															foreach ($supps as $supp) :
															?>
																<li><?= $supp['nama'] ?></li>
															<?php endforeach ?>
														</ul>
													</td>
													<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['harga'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['jumlah'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="4" class="text-right">Total Keseluruhan</th>
												<!-- <th class="text-right"><?= $penjualan_broker->total_ekor ?></th> -->
												<th class="text-right"><?= number_format($penjualan_broker->total_kg, 2, ',', '.') ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah / $penjualan_broker->total_kg, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($penjualan_broker->jumlah, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->