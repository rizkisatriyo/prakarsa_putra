<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('pembayaran_penjualan') ?>">Data Pembayaran</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<!-- <form action="<?= base_url('pembayaran_penjualan/aksi_tambah') ?>" method="POST"> -->
						<div class="card-body">
							<div class="row">
								<div class="col-sm-8">
									<div class="row">
										<div class="col-12">
											<!-- <div class="btn-group">
												<?php if ($pembayaran_penjualan->status != 'Selesai') : ?>
													<a href="<?= base_url('penerimaan_ayam/tambah/') . $pembayaran_penjualan->no_transaksi ?>" class="btn btn-outline-primary">Input Penerimaan</a>
												<?php endif ?>
											</div> -->
										</div>
									</div>
									<div class="row mt-2">
										<div class="col-md-5">
											<b>No. Pembayaran</b> : #<?= $pembayaran_penjualan->no_transaksi ?>
										</div>
										<!-- <div class="col-md-4">
											<b>No. penjualan</b> : #<?= $pembayaran_penjualan->no_transaksi_penjualan ?>
										</div> -->
										<div class="col-md-4 text-right">
											<b>Tanggal</b> : <?= date('d/m/Y', strtotime($pembayaran_penjualan->tgl_bayar)) ?>
										</div>
									</div>
								</div>
								<!-- <div class="col-sm-4 text-right">
									<?php if ($pembayaran_penjualan->status == 'Tertunda') : ?>
										<h6 class="display-4 text-danger"><?= $pembayaran_penjualan->status ?> <i class="fas fa-clock fa-sm"></i></h6>
									<?php elseif ($pembayaran_penjualan->status == 'Selesai') : ?>
										<h6 class="display-4 text-success"><?= $pembayaran_penjualan->status ?> <i class="fas fa-check fa-sm"></i></h6>
									<?php endif ?>
								</div> -->
							</div>

							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Customer</th>
												<th>Piutang</th>
												<th>Bayar</th>
												<th>Deposit</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['piutang'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['deposit'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran_penjualan->total_piutang, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran_penjualan->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembayaran_penjualan->total_deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->