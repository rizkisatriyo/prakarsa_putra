<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('pengeluaran_akun') ?>">Kategori Pengeluaran</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Kategori</b> : <?= $pengeluaran_akun->nama ?>
                                </div>
                                <div class="col-md-4 text-center">
                                    <b>Items</b> : <?= count($pengeluaran_akun_detail) ?> Akun
                                </div>
                                <div class="col-md-4 text-right">
                                    <b>Keterangan</b> :
                                    <?php if ($pengeluaran_akun->keterangan) : ?>
                                        <?= nl2br($pengeluaran_akun->keterangan) ?>
                                    <?php else : ?>
                                        -
                                    <?php endif ?>
                                </div>
                            </div>
                            <table id="pengeluaran_akun_detail" class="table table-sm table-bordered table-hover mt-3">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Akun Pengeluaran</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($pengeluaran_akun_detail as $item) :
                                    ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td><?= $item['nama'] ?></td>
                                            <td><?= $item['keterangan'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer text-right">
                            <button type="button" onclick="history.back()" class="btn btn-md btn-success float-left">
                                <i class="fas fa-arrow-left mr-2"></i>
                                Kembali
                            </button>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->