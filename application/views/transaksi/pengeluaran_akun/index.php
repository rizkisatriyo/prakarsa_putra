<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <a href="<?= base_url('pengeluaran_akun/tambah') ?>" class="btn btn-md btn-primary">
                        <i class="fas fa-plus-circle mr-2"></i>
                        Tambah Akun
                    </a>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="pengeluaran_akun" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <!-- <th>Sisa Ekor</th>
                                        <th>Sisa Kg</th> -->
                                        <!-- <th>Alamat</th> -->
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($pengeluaran_akun as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td><?= $item['nama'] ?></td>
                                            <!-- <td class="text-center"><?= $item['ekor'] ?></td>
                                            <td class="text-center"><?= $item['kg'] ?></td> -->
                                            <!-- <td><?= $item['alamat'] ?></td> -->
                                            <td><?= $item['keterangan'] ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('pengeluaran_akun/detail/') . $item['id'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                    <?php if ($item['set_lock'] == false) : ?>
                                                        <a href="<?= base_url('pengeluaran_akun/ubah/') . $item['id'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                                        <a href="<?= base_url('pengeluaran_akun/hapus/') . $item['id'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a>
                                                    <?php endif ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->