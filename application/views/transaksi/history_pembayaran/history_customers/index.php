<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="history_customers" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No. Transaksi</th>
                                        <th>Nama</th>
                                        <th>Tanggal</th>
                                        <th>piutang</th>
                                        <th>Bayar</th>
                                        <th>Deposit</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($history_customers as $item) : ?>
                                        <tr>
                                            <td class="align-middle text-center" width="50"><?= $i++ ?></td>
                                            <td class="align-middle text-center">#<?= $item['no_trxdetail'] ?></td>
                                            <td class="align-middle "><?= $item['nama'] ?></td>
                                            <td class="align-middle text-center"><?= date('d M Y', strtotime($item['tgl_bayar'])) ?></td>
                                            <td>
                                                <span class="float-left mt-3">Rp.</span>
                                                <span class="float-right mt-3"><?= number_format($item['piutang'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td>
                                                <span class="float-left mt-3">Rp.</span>
                                                <span class="float-right mt-3"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td>
                                                <span class="float-left mt-3">Rp.</span>
                                                <span class="float-right mt-3"><?= number_format($item['deposit'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <!-- <button type="button" class="btn btn-app bg-success mb-0" data-toggle="modal" data-target="#modal-bayar" data-id="<?= $item['id'] ?>" data-piutang="<?= $item['piutang_customer'] ?>" data-deposit="<?= $item['deposit_customer'] ?>">
                                                    <?php if ($item['piutang_customer']) {
                                                        echo '<span class="badge bg-danger">' . $item['piutang_customer'] . '</span>';
                                                    } ?>
                                                    <i class="fas fa-money-check fa-sm"></i> Bayar
                                                </button> -->
                                                <!-- <a href="<?= base_url('history_customers/detail/') . $item['id'] ?>" class="btn btn-app bg-info mb-0">
                                                    <i class="fas fa-history fa-sm"></i> History Pembayaran
                                                </a> -->
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-bayar">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="<?= base_url('history_customers/bayar') ?>">
                <div class="modal-header">
                    <h4 class="modal-title">Pembayaran piutang</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="id_customer">customer</label>
                            <select name="id_customer" id="id_customer" class="form-control select2bs4 <?= form_error('id_customer') ? 'is-invalid' : '' ?>" style="width: 100%;">
                                <option></option>
                                <?php foreach ($history_customers as $item) : ?>
                                    <option value="<?= $item['id'] ?>" <?php (set_value('id') == $item['id']) ? 'selected' : '' ?> data-nama="<?= $item['nama'] ?>" data-piutang="<?= $item['piutang_customer'] ?>" data-deposit="<?= $item['deposit_customer'] ?>"><?= $item['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <div id="id_customer" class="invalid-feedback">
                                <?= form_error('id_customer') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="piutang_customer">Jumlah piutang</label>
                            <input type="text" name="piutang_customer" value="<?= set_value('piutang_customer') ?>" class="form-control <?= form_error('piutang_customer') ? 'is-invalid' : '' ?>" id="piutang_customer" placeholder="Jumlah piutang .." disabled>
                            <div id="piutang_customer" class="invalid-feedback">
                                <?= form_error('piutang_customer') ?>
                            </div>
                        </div>
                        <div class="form-group col">
                            <label for="deposit_customer">Jumlah Deposit</label>
                            <input type="text" name="deposit_customer" value="<?= set_value('deposit_customer') ?>" class="form-control <?= form_error('deposit_customer') ? 'is-invalid' : '' ?>" id="deposit_customer" placeholder="Jumlah Deposit .." disabled>
                            <div id="deposit_customer" class="invalid-feedback">
                                <?= form_error('deposit_customer') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="jml_bayar">Jumlah Bayar</label>
                            <input type="text" name="jml_bayar" value="<?= set_value('jml_bayar') ?>" class="form-control <?= form_error('jml_bayar') ? 'is-invalid' : '' ?>" id="jml_bayar" placeholder="Jumlah Bayar ..">
                            <div id="jml_bayar" class="invalid-feedback">
                                <?= form_error('jml_bayar') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->