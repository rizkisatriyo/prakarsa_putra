<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('history_suppliers') ?>">History Pembayaran Supplier</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-info alert-dismissible">
								<h5><i class="icon fas fa-info"></i> Informasi Supplier</h5>
								<div class="row">
									<div class="col-md-8">
										<dl class="row mb-0">
											<dt class="col-2 mb-0">Nama</dt>
											<dd class="col-4 mb-0">: <?= $supplier->nama ?></dd>
											<dt class="col-2 mb-0">Telefon</dt>
											<dd class="col-4 mb-0">: <?= $supplier->telefon ?></dd>
										</dl>
										<dl class="row mb-0">
											<dt class="col-2 mb-0">Email</dt>
											<dd class="col-4 mb-0">: <?= $supplier->email ?></dd>
											<dt class="col-2 mb-0">Alamat</dt>
											<dd class="col-4 mb-0">:
												<?= nl2br($supplier->alamat) ?>
											</dd>
										</dl>
									</div>
									<div class="col-md-4">
										<dl class="row mb-0">
											<dt class="col-4 mb-0">Hutang</dt>
											<dd class="col-8 mb-0">: <?= $supplier->hutang_supplier ? '<span class="badge badge-danger">Rp. ' . number_format($supplier->hutang_supplier, 0, ',', '.') . ',-</span>' : '-'; ?></dd>
										</dl>
										<dl class="row mb-0">
											<dt class="col-4 mb-0">Deposit</dt>
											<dd class="col-8 mb-0">: <?= $supplier->deposit_supplier ? '<span class="badge badge-success">Rp. ' . number_format($supplier->deposit_supplier, 0, ',', '.') . ',-</span>' : '-'; ?></dd>
										</dl>
									</div>
								</div>
							</div>

							<div class="row mt-2">
								<div class="col-12">
									<table id="history_suppliers" class="table table-bordered table-hover">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Tanggal</th>
												<th>Ekor</th>
												<th>Kg</th>
												<th>Harga</th>
												<th>Jumlah</th>
												<th>Hutang</th>
												<th>Bayar</th>
												<th>Deposit</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i = 1;
											// $hutang = 0;
											// $bayar = 0;
											// $deposit = 0;
											foreach ($history as $item) :
												// $hutang += (float)$item['hutang'];
												// $bayar += (float)$item['bayar'];
												// $deposit += (float)$item['deposit'];

												$this->db->join('pembelian', 'pembelian.no_transaksi = pembelian_detail.no_transaksi', 'left');
												$pbn = $this->db->get_where('pembelian_detail', ['id_supplier' => $supplier->id, 'pembelian.tgl_beli' => $item['tgl_bayar']])->row();
											?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td class="text-center"><?= date('d M Y', strtotime($item['tgl_bayar'])) ?></td>
													<!-- <td class="text-right"><?= $pbn->ekor ? $pbn->ekor : '-'; ?></td>
													<td class="text-right"><?= number_format($pbn->kg, 2, ',', '.') ?></td> -->
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($pbn->hrg_ayam, 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($pbn->subtotal, 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['hutang'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['deposit'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<!-- <tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($hutang, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($bayar, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($deposit, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot> -->
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->