<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="susut_pembelian" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Penerimaan</th>
                                        <th>Tanggal</th>
                                        <th>Ekor</th>
                                        <!-- <th>Presentase Ekor</th> -->
                                        <th>Kg</th>
                                        <th>Presentase Kg</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($susut_pembelian as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <a href="<?= base_url('penerimaan_ayam/detail/') . $item['no_transaksi_penerimaan'] ?>" class="badge badge-secondary">#<?= $item['no_transaksi_penerimaan'] ?></a>
                                            </td>
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tgl_diterima'])) ?></td>
                                            <td class="text-center"><?= $item['ekor'] ? $item['ekor'] : '-' ?></td>
                                            <!-- <td class="text-center"><?= $item['presentase_ekor'] ? $item['presentase_ekor'] . '%' : '-' ?></td> -->
                                            <td class="text-center"><?= $item['kg'] ? number_format($item['kg'], 2, ',', '.') : '-' ?></td>
                                            <td class="text-center"><?= $item['presentase_kg'] ? $item['presentase_kg'] . '%' : '-' ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('penerimaan_ayam/detail/') . $item['no_transaksi_penerimaan'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye mr-2"></i> Detail</a>
                                                    <!-- <a href="<?= base_url('susut_pembelian/ubah/') . $item['no_transaksi_penerimaan'] ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                                    <a href="<?= base_url('susut_pembelian/hapus/') . $item['no_transaksi_penerimaan'] ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fas fa-trash"></i></a> -->
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->