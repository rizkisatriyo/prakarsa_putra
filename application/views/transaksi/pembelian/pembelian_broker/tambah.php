<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('pembelian_broker') ?>">Pembelian</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<div class="card">
						<form method="POST">
							<div class="card-body">
								<div class="form-row">
									<div class="form-group col-sm-4 col-md-3">
										<label for="tgl_beli">Tanggal Pembelian</label>
										<input type="date" name="tgl_beli" value="<?= date('Y-m-d') ?>" class="form-control tgl_beli" id="tgl_beli" placeholder="Tanggal .." required>
										<div id="tgl_beli" class="invalid-feedback"></div>
									</div>
									<div class="form-group col-sm-4 col-md-3">
										<label for="sisa_kandang">No. Pembelian</label>
										<input type="text" name="no_transaksi" value="<?= set_value('no_transaksi', $trans_no) ?>" class="form-control" id="no_transaksi" placeholder="No. Transaksi .." required disabled>
										<div id="no_transaksi" class="invalid-feedback"></div>
									</div>
									<!-- <div class="form-group col-sm-2 col-md-2 offset-md-2 bg-info p-3 rounded-left">
										<label for="sisa_ekor">Sisa Ekor</label>
										<input type="text" name="sisa_ekor" value="<?= $sisa_stok->ekor ?>" class="form-control text-danger font-weight-bold" id="sisa_ekor" placeholder="Sisa Ekor .." required disabled>
									</div>
									<div class="form-group col-sm-2 col-md-2 bg-info p-3 rounded-right">
										<label for="sisa_kg">Sisa Kg</label>
										<input type="text" name="sisa_kg" value="<?= $sisa_stok->kg ?>" class="form-control text-danger font-weight-bold" id="sisa_kg" placeholder="Sisa Kg .." required disabled>
									</div> -->
								</div>

								<div class="form-row">
									<div class="form-group col-md-4">
										<label for="id_supplier">Supplier</label>
										<select name="id_supplier" id="id_supplier" class="form-control select2bs4 <?= form_error('id_supplier') ? 'is-invalid' : '' ?>" style="width: 100%;">
											<option></option>
											<?php foreach ($suppliers as $item) : ?>
												<option value="<?= $item['id'] ?>" <?php (set_value('id') == $item['id']) ? 'selected' : '' ?> data-nama="<?= $item['nama'] ?>" data-hutang="<?= $item['hutang_supplier'] ?>" data-deposit="<?= $item['deposit_supplier'] ?>"><?= $item['nama'] ?></option>
											<?php endforeach ?>
										</select>
										<div id="id_supplier" class="invalid-feedback">
											<?= form_error('id_supplier') ?>
										</div>
									</div>
									<div class="form-group col-md-2 offset-md-1 d-none hutang_supplier">
										<label for="hutang_supplier">Hutang</label>
										<input type="text" name="hutang_supplier" value="<?= set_value('hutang_supplier') ?>" class="bg-white font-weight-bold text-danger form-control <?= form_error('hutang_supplier') ? 'is-invalid' : '' ?>" id="hutang_supplier" placeholder="Hutang Supplier .." disabled>
										<div id="hutang_supplier" class="invalid-feedback">
											<?= form_error('hutang_supplier') ?>
										</div>
									</div>
									<div class="form-group col-md-2 d-none deposit_supplier">
										<label for="deposit_supplier">Deposit</label>
										<input type="text" name="deposit_supplier" value="<?= set_value('deposit_supplier') ?>" class="bg-white font-weight-bold text-success form-control <?= form_error('deposit_supplier') ? 'is-invalid' : '' ?>" id="deposit_supplier" placeholder="Deposit Supplier .." disabled>
										<div id="deposit_supplier" class="invalid-feedback">
											<?= form_error('deposit_supplier') ?>
										</div>
									</div>
								</div>
								<div class="form-row">
									<!-- <div class="form-group col-md-2 col-sm-3">
										<label for="ekor">Ekor</label>
										<input type="number" name="ekor" value="<?= set_value('ekor') ?>" class="form-control <?= form_error('ekor') ? 'is-invalid' : '' ?>" id="ekor" placeholder="Ekor ..">
										<div id="ekor" class="invalid-feedback">
											<?= form_error('ekor') ?>
										</div>
									</div> -->
									<div class="form-group col-md-2 col-sm-3">
										<label for="kg">Kg</label>
										<input type="number" name="kg" value="<?= set_value('kg') ?>" class="form-control <?= form_error('kg') ? 'is-invalid' : '' ?>" id="kg" placeholder="Kg ..">
										<div id="kg" class="invalid-feedback">
											<?= form_error('kg') ?>
										</div>
									</div>
									<div class="form-group col-md-2 col-sm-6">
										<label for="hrg_ayam">Harga Ayam</label>
										<input type="text" name="hrg_ayam" value="<?= set_value('hrg_ayam') ?>" class="form-control <?= form_error('hrg_ayam') ? 'is-invalid' : '' ?>" id="hrg_ayam" placeholder="Harga Ayam ..">
										<div id="hrg_ayam" class="invalid-feedback">
											<?= form_error('hrg_ayam') ?>
										</div>
									</div>
									<div class="form-group col-md-2">
										<label for="subtotal">Subtotal</label>
										<input type="text" name="subtotal" value="<?= set_value('subtotal') ?>" class="form-control <?= form_error('subtotal') ? 'is-invalid' : '' ?>" id="subtotal" placeholder="Subtotal .." disabled>
										<div id="subtotal" class="invalid-feedback">
											<?= form_error('subtotal') ?>
										</div>
									</div>
									<div class="form-group col-md-2 col-sm-6">
										<label for="jml_bayar">Jumlah Bayar</label>
										<input type="text" name="jml_bayar" value="<?= set_value('jml_bayar') ?>" class="form-control <?= form_error('jml_bayar') ? 'is-invalid' : '' ?>" id="jml_bayar" placeholder="Jumlah Bayar ..">
										<div id="jml_bayar" class="invalid-feedback">
											<?= form_error('jml_bayar') ?>
										</div>
									</div>
									<div class="form-group col-md-2 d-flex align-items-end">
										<button type="submit" class="btn btn-primary btn-block" id="tambah_detail">
											<i class="fas fa-plus-circle mr-2"></i> Tambahkan
										</button>
									</div>
								</div>

								<div class="row">
									<div class="col table-responsive">
										<table id="pembelian_detail" class="table table-bordered table-hover">
											<thead class="thead-light">
												<tr class="text-center">
													<th>#</th>
													<th>No. Transaksi</th>
													<th>Supplier</th>
													<th>Kg</th>
													<th>Harga Ayam</th>
													<th>Subtotal</th>
													<th>Jumlah Bayar</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer text-right">
								<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
									<i class="fas fa-arrow-left mr-2"></i>
									Kembali
								</button>
								<!-- <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button> -->
								<button type="submit" class="btn btn-primary simpan_pembelian"><i class="fas fa-save mr-2"></i> Simpan</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<div class="row pb-3">
				<div class="col">
					<h6>Demo Pembelian</h6>
					<button id="demo_26" class="btn btn-primary">26 Februari 2022</button>
					<button id="demo_27" class="btn btn-primary">27 Februari 2022</button>
					<button id="demo_28" class="btn btn-primary">28 Februari 2022</button>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->