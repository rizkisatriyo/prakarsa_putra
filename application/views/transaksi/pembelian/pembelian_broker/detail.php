<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('pembelian_broker') ?>">Data Pembelian</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<!-- <form action="<?= base_url('pembelian_broker/aksi_tambah') ?>" method="POST"> -->
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-12">
											<div class="btn-group">
												
											</div>
										</div>
									</div>
									<div class="row mt-2">
										<div class="col-md-7">
											<b>No. Pembelian</b> : #<?= $pembelian->no_transaksi ?>
										</div>
										<div class="col-md-5 text-right">
											<b>Tanggal</b> : <?= date('d/m/Y', strtotime($pembelian->tgl_beli)) ?>
										</div>
									</div>
								</div>
								<div class="col-sm-6 text-right">
									<?php if ($pembelian->status == 'Tertunda') : ?>
										<h6 class="display-4 text-danger"><?= $pembelian->status ?> <i class="fas fa-clock fa-sm"></i></h6>
									<?php elseif ($pembelian->status == 'Selesai') : ?>
										<h6 class="display-4 text-success"><?= $pembelian->status ?> <i class="fas fa-check fa-sm"></i></h6>
									<?php endif ?>
								</div>
							</div>

							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Supplier</th>
												<th>Kg</th>
												<th>Harga Ayam</th>
												<th>Subtotal</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td class="text-right"><?= number_format($item['kg']) ?> </td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['hrg_ayam'], 0, ',', '.') ?>,-</span>
													</td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['subtotal'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">Total Pembelian</th>
												<th class="text-right"><?= number_format($pembelian->kg) ?></th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang / $pembelian->kg, 0, ',', '.') ?>,-</span>
												</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pembelian->total_hutang, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
											
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-warning float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
							<?php if ($pembelian->status != 'Selesai') : ?>
								<a href="<?= base_url('Penjualan_broker/inputPenjualanDetail/') . $item['no_transaksi'] ?>" class="btn btn-md btn-success float-Right">Input Penjualan 	<i class="fas fa-arrow-right mr-2"></i></a> 								
							<?php endif ?>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->