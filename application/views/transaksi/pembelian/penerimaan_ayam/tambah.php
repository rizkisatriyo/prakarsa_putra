Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('penerimaan_ayam') ?>">Penerimaan Ayam</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<form>
							<div class="card-body">
								<div class="form-row">
									<div class="form-group col-sm-4 col-md-3">
										<label for="tgl_diterima">Tanggal</label>
										<input type="date" name="tgl_diterima" value="<?= date('Y-m-d') ?>" class="form-control <?= form_error('tgl_diterima') ? 'is-invalid' : '' ?>" id="tgl_diterima" placeholder="Tanggal .." required>
										<div id="tgl_diterima" class="invalid-feedback">
											<?= form_error('tgl_diterima') ?>
										</div>
									</div>
									<div class="form-group col-sm-4 col-md-3">
										<label for="no_transaksi">No. Transaksi</label>
										<input type="text" name="no_transaksi" value="<?= set_value('no_transaksi', $trans_no) ?>" class="form-control <?= form_error('no_transaksi') ? 'is-invalid' : '' ?>" id="no_transaksi" placeholder="No. Transaksi .." required>
										<div id="no_transaksi" class="invalid-feedback">
											<?= form_error('no_transaksi') ?>
										</div>
									</div>
									<div class="form-group col-sm-4 col-md-3">
										<label for="no_transaksi_pembelian">No. Pembelian</label>
										<input type="text" name="no_transaksi_pembelian" value="<?= set_value('no_transaksi_pembelian', $no_transaksi_pembelian) ?>" class="form-control <?= form_error('no_transaksi_pembelian') ? 'is-invalid' : '' ?>" id="no_transaksi_pembelian" placeholder="No. Transaksi .." required disabled>
										<div id="no_transaksi_pembelian" class="invalid-feedback">
											<?= form_error('no_transaksi_pembelian') ?>
										</div>
										<input type="hidden" value="<?= $pembelian->total_hutang ?>" id="total_hutang">
									</div>
									<div class="form-group col-sm-4 col-md-3">
										<label for="no_checker">No. Checker</label>
										<input type="text" name="no_checker" value="<?= set_value('no_checker', $no_checker) ?>" class="form-control <?= form_error('no_checker') ? 'is-invalid' : '' ?>" id="no_checker" placeholder="No. Transaksi .." required disabled>
										<div id="no_checker" class="invalid-feedback">
											<?= form_error('no_checker') ?>
										</div>
									</div>
								</div>

								<div class="row mt-3">
									<div class="col-lg-4 table-responsive">
										<h6 class="font-weight-bold mt-0"># Detail Pembelian : #<?= $pembelian->no_transaksi ?></h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<thead class="thead-light">
												<tr class="text-center">
													<th>#</th>
													<th>Supplier</th>
													<th>Ekor</th>
													<th>Kg</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 1 ?>
												<?php foreach ($detail as $item) : ?>
													<tr>
														<td class="text-center" width="50"><?= $i++ ?></td>
														<td><?= $item['nama'] ?></td>
														<td class="text-right"><?= $item['ekor'] ? number_format($item['ekor'], 0, ',', '.') : '-' ?></td>
														<td class="text-right"><?= number_format($item['kg'], 2, ',', '.') ?></td>
													</tr>
												<?php endforeach ?>
											</tbody>
											<tfoot>
												<tr class="bg-light">
													<th colspan="2" class="text-right align-middle">Total Pembelian</th>
													<th class="text-right"><?= number_format($pembelian->ekor, 0, ',', '.') ?></th>
													<th class="text-right"><?= number_format($pembelian->kg, 2, ',', '.') ?></th>
												</tr>
												<tr class="bg-light">
													<th colspan="2" class="text-right align-middle">Penyusutan</th>
													<th class="text-right">
														<input type="number" name="mati_ekor" id="mati_ekor" class="form-control form-control-sm text-center">
													</th>
													<th class="text-right">
														<input type="hidden" value="<?= $pembelian->kg ?>" id="tb_kg">
														<input type="number" name="broker_kg" id="broker_kg" class="form-control form-control-sm text-right">
													</th>
												</tr>
												<tr class="bg-light">
													<th colspan="2" class="text-right align-middle">Sisa Pembelian</th>
													<th class="text-right"><?= number_format($pembelian->ekor, 0, ',', '.') ?></th>
													<th class="text-right"><input type="number" name="tot_beli_kg" id="tot_beli_kg" class="form-control form-control-sm text-right font-weight-bold" disabled></th>
												</tr>
											</tfoot>
										</table>

										<hr>
										<h6 class="font-weight-bold mt-0"># Detail Penerimaan</h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<tbody>
												<tr>
													<th class="bg-light" style="width: 150px;">Bongkar</th>
													<td>
														<input type="number" name="bongkar_ekor" id="bongkar_ekor" class="form-control form-control-sm text-center" required disabled>
														<!-- <div id="bongkar_ekor" class="invalid-feedback">
															<?= form_error('bongkar_ekor') ?>
														</div> -->
													</td>
													<td>
														<input type="number" name="bongkar_kg" id="bongkar_kg" class="form-control form-control-sm text-center" required disabled>
														<!-- <div id="bongkar_kg" class="invalid-feedback">
															<?= form_error('bongkar_kg') ?>
														</div> -->
													</td>
												</tr>
												<tr>
													<th class="bg-light" style="width: 150px;">Sisa Kandang</th>
													<td><input type="number" name="sisa_ekor" value="<?= $sisa_kandang->ekor ?>" id="sisa_ekor" class="form-control form-control-sm font-weight-bold text-center" disabled></td>
													<td><input type="number" name="sisa_kg" value="<?= $sisa_kandang->kg ?>" id="sisa_kg" class="form-control form-control-sm font-weight-bold text-center" disabled></td>
												</tr>

												<tr>
													<th class="bg-light" style="width: 150px;">Total Stok</th>
													<td><input type="number" name="ts_ekor" id="ts_ekor" class="form-control form-control-sm text-center" disabled></td>
													<td><input type="number" name="ts_kg" id="ts_kg" class="form-control form-control-sm text-center" disabled></td>
													<input type="hidden" name="t_kg" id="t_kg" value="<?= $pembelian->kg + $sisa_kandang->kg ?>">
													<input type="hidden" name="ts_jumlah" id="ts_jumlah" value="<?= $pembelian->total_hutang + $sisa_kandang->jumlah ?>">
												</tr>
												
											</tbody>
										</table>

										<!-- <table id="detail" class="table table-bordered table-hover table-sm">
											<tbody>
												<tr>
													<th class="bg-light" style="width: 150px;">Kirim</th>
													<td><input type="number" name="kirim_ekor" id="kirim_ekor" class="form-control form-control-sm text-center"></td>
													<td><input type="number" name="kirim_kg" id="kirim_kg" class="form-control form-control-sm text-center"></td>
												</tr>
												<tr>
													<th class="bg-light" style="width: 150px;">Mati</th>
													<td><input type="number" name="mati_ekor" id="mati_ekor" class="form-control form-control-sm text-center"></td>
													<td><input type="number" name="mati_kg" id="mati_kg" class="form-control form-control-sm text-center"></td>
												</tr>
											</tbody>
										</table> -->

										<hr>
										<h6 class="font-weight-bold mt-0"># Detail Penyusutan</h6>
										<table id="detail" class="table table-bordered table-hover table-sm">
											<thead class="thead-light">
												<tr>
													<th colspan="4" class="text-center">Penyusutan</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<input type="hidden" name="total_beli_ekor" id="total_beli_ekor" value="<?= $pembelian->ekor ?>" class="form-control form-control-sm text-center">
														<label>Ekor</label>
														<input type="number" name="susut_ekor" id="susut_ekor" class="form-control form-control-sm text-center font-weight-bold" >
													</td> 	
													<!-- <td>
														<input type="number" name="presentase_susut_ekor" id="presentase_susut_ekor" class="form-control form-control-sm text-center font-weight-bold" disabled>
													</td> -->
													<td>
														<label>Kg</label>

														<input type="hidden" name="total_beli_kg" id="total_beli_kg" class="form-control form-control-sm text-center font-weight-bold">
														<input type="number" name="susut_kg" id="susut_kg" class="form-control form-control-sm text-center font-weight-bold">
													</td>
													<td>
														<label>Persentase</label>

														<input type="number" name="presentase_susut_kg" id="presentase_susut_kg" class="form-control form-control-sm text-center font-weight-bold">
													</td>
												</tr>
											</tbody>
											<!-- <tbody>
												<tr>
													<td>
														<input type="hidden" name="total_beli_ekor" id="total_beli_ekor" value="<?= $pembelian->ekor ?>" class="form-control form-control-sm text-center" disabled>
														<input type="number" name="susut_ekor" id="susut_ekor" class="form-control form-control-sm text-center font-weight-bold" disabled>
													</td>
													<td>
														<input type="number" name="presentase_susut_ekor" id="presentase_susut_ekor" class="form-control form-control-sm text-center font-weight-bold" disabled>
													</td>
													<td>
														<input type="hidden" name="total_beli_kg" id="total_beli_kg" value="<?= $pembelian->kg ?>" class="form-control form-control-sm text-center font-weight-bold" disabled>
														<input type="number" name="susut_kg" id="susut_kg" class="form-control form-control-sm text-center font-weight-bold" disabled>
													</td>
													<td>
														<input type="number" name="presentase_susut_kg" id="presentase_susut_kg" class="form-control form-control-sm text-center font-weight-bold" disabled>
													</td>
												</tr>
											</tbody> -->
										</table>
									</div>
									<div class="col-lg-8">
										<!-- <h6 class="font-weight-bold mt-0"># Input Broker</h6>
										<div class="row">
											<div class="col">

												<div class="card card-warning collapsed-card">
													<div class="card-header" data-card-widget="collapse">
														<h3 class="card-title text-dark font-weight-bold">Penjualan kepada Broker?</h3>
														<div class="card-tools">
															<button type="button" class="btn btn-tool"><i class="fas fa-plus"></i>
															</button>
														</div>
													</div>
													<div class="card-body">
														<div class="form-row">
															<div class="form-group col-md-4">
																<label for="id_broker">Broker</label>
																<select name="id_broker" data-placeholder="Pilih Broker" id="pb_id_broker" class="form-control select2bs4 <?= form_error('id_broker') ? 'is-invalid' : '' ?>" style="width: 100%;">
																	<option></option>
																	<?php foreach ($brokers as $item) : ?>
																		<option value="<?= $item['id'] ?>" <?php (set_value('id') == $item['id']) ? 'selected' : '' ?> data-nama="<?= $item['nama'] ?>"><?= $item['nama'] ?></option>
																	<?php endforeach ?>
																</select>
																<div id="pb_id_broker" class="invalid-feedback">
																	<?= form_error('id_broker') ?>
																</div>
															</div>
															<div class="form-group col-md-4">
																<label for="id_supplier">Supplier</label>
																<select name="id_supplier" data-placeholder="Pilih Supplier" multiple="multiple" id="pb_id_supplier" class="form-control select2bs4 <?= form_error('id_supplier') ? 'is-invalid' : '' ?>" style="width: 100%;">
																	<option></option>
																	<?php foreach ($suppliers as $item) : ?>
																		<option value="<?= $item['id'] ?>" <?php (set_value('id') == $item['id']) ? 'selected' : '' ?> data-nama="<?= $item['nama'] ?>"><?= $item['nama'] ?></option>
																	<?php endforeach ?>
																</select>
																<div id="pb_id_supplier" class="invalid-feedback">
																	<?= form_error('id_supplier') ?>
																</div>
															</div>
														</div>
														<div class="form-row">
															<div class="form-group col-md-2 col-sm-6">
																<label for="kg">Kg</label>
																<input type="text" name="kg" value="<?= set_value('kg') ?>" class="form-control <?= form_error('kg') ? 'is-invalid' : '' ?>" id="pb_kg" placeholder="Kg ..">
																<div id="pb_kg" class="invalid-feedback">
																	<?= form_error('kg') ?>
																</div>
															</div>
															<div class="form-group col-md-4 col-sm-6">
																<label for="hrg_ayam">Harga Ayam</label>
																<input type="text" name="hrg_ayam" value="<?= set_value('hrg_ayam') ?>" class="form-control <?= form_error('hrg_ayam') ? 'is-invalid' : '' ?>" id="pb_hrg_ayam" placeholder="Harga ..">
																<div id="pb_hrg_ayam" class="invalid-feedback">
																	<?= form_error('hrg_ayam') ?>
																</div>
															</div>
															<div class="form-group col-md-4">
																<label for="subtotal">Subtotal</label>
																<input type="text" name="subtotal" value="<?= set_value('subtotal') ?>" class="form-control <?= form_error('subtotal') ? 'is-invalid' : '' ?>" id="pb_subtotal" placeholder="Subtotal .." disabled>
																<div id="pb_subtotal" class="invalid-feedback">
																	<?= form_error('subtotal') ?>
																</div>
															</div>
															<div class="form-group col-md-2 d-flex align-items-end">
																<button type="submit" class="btn btn-primary btn-block" id="tambah_detail">
																	Tambah
																</button>
															</div>
														</div>

														<div class="row">
															<div class="col table-responsive">
																<table id="penjualan_broker" class="table table-bordered table-hover table-sm">
																	<thead class="thead-light">
																		<tr class="text-center">
																			<th>#</th>
																			<th>No. Transaksi</th>
																			<th>Broker</th>
																			<th>Kg</th>
																			<th>Harga</th>
																			<th>Jumlah</th>
																		</tr>
																	</thead>
																	<tbody></tbody>
																</table>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
										<hr class="mt-1"> -->
										<h6 class="font-weight-bold mt-0"># Input Checker : <span class="txt_no_checker">#<?= $no_checker ?></span></h6>
										<div class="row d-flex justify-content-center">
										
											<?php foreach ($kandang as $item) : ?>
												<div class="col-md-4 col-sm-6">
													<table class="table table-bordered table-hover table-sm table-checker" id="table-<?= $item['id'] ?>" data-id_kandang="<?= $item['id'] ?>">
														<thead>
															<tr>
																<th colspan="2" class="text-center"><?= $item['nama'] ?></th>
																<input type="hidden" name="id_kandang" value="<?= $item['id'] ?>" id="id_kandang">
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><input type="number" name="ekor" class="form-control form-control-sm text-center ekor"
																		id="ekor-<?= $item['id'] ?>" placeholder="Ekor"></td>
																<td><input type="number" name="kg" class="form-control form-control-sm text-center kg"
																		id="kg-<?= $item['id'] ?>" placeholder="Kg"></td>
															</tr>
														<!-- 		<tr>
																	<td><input type="number" name="ekor" class="form-control form-control-sm text-center ekor"
																			id="ekor-<?= $item['id'] ?>"></td>
																	<td><input type="number" name="kg" class="form-control form-control-sm text-center kg"
																			id="kg-<?= $item['id'] ?>"></td>
																</tr> -->

														</tbody>														<tfoot>
															<tr>
																<td colspan="2">
																	<button type="button" class="btn btn-primary btn-sm btn-block tambah_baru" data-id_kandang="<?= $item['id'] ?>">
																		<i class="fas fa-plus mr-2 fa-sm"></i> Tambah Baru
																	</button>
																</td>
															</tr>
															<tr>
															<!-- <tr>
																<td colspan="2" class="text-center" >Subtotal</td>
															</tr> -->
															<!-- <tr>
																<td  id="subtotal-ekor-<?= $item['id'] ?>" class="text-center">0</td>
																<td  id="subtotal-kg-<?= $item['id'] ?>" class="text-center">0</td>
															</tr> -->
															<script>
																	// Add event listeners to input fields to update subtotal on input change
																	document.getElementById('ekor-<?= $item['id'] ?>').addEventListener('input', updateSubtotal<?= $item['id'] ?>);
																	document.getElementById('kg-<?= $item['id'] ?>').addEventListener('input', updateSubtotal<?= $item['id'] ?>);

																	function updateSubtotal<?= $item['id'] ?>() {
																		// Get the values from input fields
																		var ekorValue = document.getElementById('ekor-<?= $item['id'] ?>').value;
																		var kgValue = document.getElementById('kg-<?= $item['id'] ?>').value;

																		// Calculate subtotal
																		var subtotalEkor = ekorValue * 1; // Assuming a numeric value, change this accordingly
																		var subtotalKg = kgValue * 1; // Assuming a numeric value, change this accordingly

																		// Update subtotal in the corresponding elements
																		document.getElementById('subtotal-ekor-<?= $item['id'] ?>').textContent = subtotalEkor;
																		document.getElementById('subtotal-kg-<?= $item['id'] ?>').textContent = subtotalKg;
																	}
																</script>
																															<!-- <tr>
                                                                <th><input type="number" name="ekor" class="form-control form-control-sm text-center" id="total_ekor-<?= $item['id'] ?>" placeholder="Ekor" disabled></th>
                                                                <th><input type="number" name="kg" class="form-control form-control-sm text-center" id="total_kg-<?= $item['id'] ?>" placeholder="Kg" disabled></th>
                                                            </tr> -->
														</tfoot>
													</table>
												</div>
											<?php endforeach ?>
										</div>
										<div class="row mt-1 d-flex align-items-end">
											<div class="col">
												<hr>
												<h6 class="font-weight-bold mt-0"># Total Keseluruhan</h6>
												<table class="table table-bordered">
													<tbody>
														<tr>
															<th class="bg-light align-middle">Total Ekor</th>
															<td>
																<input type="number" name="total_ekor" id="total_ekor" class="form-control text-center font-weight-bold" required disabled>
																<div id="total_ekor" class="invalid-feedback">
																	<?= form_error('total_ekor') ?>
																</div>
															</td>
															<th class="bg-light align-middle">Total Kg</th>
															<td>
																<input type="number" name="total_kg" id="total_kg" class="form-control text-center font-weight-bold" required disabled>
																
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer text-right">
								<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
									<i class="fas fa-arrow-left mr-2"></i>
									Kembali
								</button>
								<button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button>
								<button type="button" class="btn btn-primary simpan"><i class="fas fa-save mr-2"></i> Simpan</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->

		<div class="row pb-3">
			<div class="col">
				<h6>Demo Penerimaan</h6>
				<button id="demo_26" class="btn btn-primary">26 Februari 2022</button>
				<button id="demo_27" class="btn btn-primary">27 Februari 2022</button>
				<button id="demo_28" class="btn btn-primary">28 Februari 2022</button>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper