<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('pengeluaran') ?>">Pengeluaran</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<?php foreach ($this->db->get('pengeluaran_akun')->result_array() as $pengeluaran_akun) : ?>
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title"># <?= $pengeluaran_akun['nama'] ?></h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<table id="pengeluaran_detail" class="table table-sm table-bordered table-hover">
									<thead class="thead-light">
										<tr class="text-center">
											<th>#</th>
											<th>Akun</th>
											<th>Keterangan</th>
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 1;
										$total_pengeluaran_detail = 0;
										foreach ($this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $pengeluaran_akun['id']])->result_array() as $pengeluaran_akun_detail) :
										?>
											<tr>
												<td class="text-center" width="50"><?= $i++ ?></td>
												<td><?= $pengeluaran_akun_detail['nama'] ?></td>
												<?php
												$this->db->order_by('tanggal', 'desc');
												$this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
												$pengeluaran_detail = $this->db->get_where('pengeluaran_detail', ['pengeluaran.no_transaksi' => $no_transaksi, 'id_pengeluaran_akun_detail' => $pengeluaran_akun_detail['id']])->row();
												if ($pengeluaran_detail) {
													$total_pengeluaran_detail += $pengeluaran_detail->bayar;
												}
												?>
												<td><?= $pengeluaran_detail ? $pengeluaran_detail->keterangan : ''; ?></td>
												<td>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= $pengeluaran_detail ? number_format($pengeluaran_detail->bayar, 0, ',', '.') : 0; ?>,-</span>
												</td>
											</tr>
										<?php endforeach ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="3" class="bg-light text-right">TOTAL</th>
											<th>
												<span class="float-left">Rp.</span>
												<span class="float-right"><?= number_format($total_pengeluaran_detail, 0, ',', '.') ?>,-</span>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
				<?php endforeach ?>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># SUBTOTAL</h3>
						</div>
						<div class="card-body">
							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>Keterangan</th>
												<th>Bayar</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th class="text-right">J U M L A H</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($pengeluaran->total, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer text-right">
							<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
								<i class="fas fa-arrow-left mr-2"></i>
								Kembali
							</button>
						</div>
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->