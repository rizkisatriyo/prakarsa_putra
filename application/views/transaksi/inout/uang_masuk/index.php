<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h3 class="card-title"><?= $title ?></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="uang_masuk" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No. Penjualan RPA</th>
                                        <th>No. Penjualan Broker</th>
                                        <th>Tanggal</th>
                                        <th>Total Bayar</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($uang_masuk as $item) : ?>
                                        <tr>
                                            <td class="text-center" width="50"><?= $i++ ?></td>
                                            <td class="text-center">
                                                <a href="<?= base_url('pembayaran_penjualan/detail/') . $item['no_trans_rpa'] ?>" class="badge badge-primary">#<?= $item['no_trans_rpa'] ?></a>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($item['no_trans_broker']) : ?>
                                                    <a href="<?= base_url('pembayaran_penjualan_broker/detail/') . $item['no_trans_broker'] ?>" class="badge badge-danger">#<?= $item['no_trans_broker'] ?></a>
                                                <?php endif ?>
                                            <td class="text-center"><?= date('d M Y', strtotime($item['tgl_bayar'])) ?></td>
                                            <td>
                                                <span class="float-left">Rp.</span>
                                                <span class="float-right"><?= number_format($item['tot_byr_rpa'] + $item['tot_byr_broker'], 0, ',', '.') ?>,-</span>
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= base_url('uang_masuk/detail/') . $item['tgl_bayar'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->