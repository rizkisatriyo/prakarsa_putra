<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0"><?= $title ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('uang_masuk') ?>">Uang Masuk</a></li>
						<li class="breadcrumb-item active"><?= $title ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"># Pembayaran RPA</h3>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<b>No. Pembayaran</b> : #<?= $rpa->no_transaksi ?>
								</div>
								<div class="col-md-6 text-right">
									<b>Tanggal</b> : <?= date('d/m/Y', strtotime($rpa->tgl_bayar)) ?>
								</div>
							</div>

							<div class="row mt-2">
								<div class="col-12">
									<table id="detail" class="table table-bordered table-hover table-sm">
										<thead class="thead-light">
											<tr class="text-center">
												<th>#</th>
												<th>No. Transaksi</th>
												<th>Customer</th>
												<th>Bayar</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ?>
											<?php foreach ($rpa_detail as $item) : ?>
												<tr>
													<td class="text-center" width="50"><?= $i++ ?></td>
													<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
													<td><?= $item['nama'] ?></td>
													<td>
														<span class="float-left">Rp.</span>
														<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
										<tfoot class="bg-light">
											<tr>
												<th colspan="3" class="text-right">J U M L A H</th>
												<th>
													<span class="float-left">Rp.</span>
													<span class="float-right"><?= number_format($rpa->total_bayar, 0, ',', '.') ?>,-</span>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<div class="col-md-6">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title"># Pembayaran BROKER</h3>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-6">
											<b>No. Pembayaran</b> : #<?= $broker->no_transaksi ?>
										</div>
										<div class="col-md-6 text-right">
											<b>Tanggal</b> : <?= date('d/m/Y', strtotime($broker->tgl_bayar)) ?>
										</div>
									</div>

									<div class="row mt-2">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<th>#</th>
														<th>No. Transaksi</th>
														<th>Customer</th>
														<th>Bayar</th>
													</tr>
												</thead>
												<tbody>
													<?php $i = 1 ?>
													<?php foreach ($broker_detail as $item) : ?>
														<tr>
															<td class="text-center" width="50"><?= $i++ ?></td>
															<td class="text-center">#<?= $item['no_trxdetail'] ?></td>
															<td><?= $item['nama'] ?></td>
															<td>
																<span class="float-left">Rp.</span>
																<span class="float-right"><?= number_format($item['bayar'], 0, ',', '.') ?>,-</span>
															</td>
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="bg-light">
													<tr>
														<th colspan="3" class="text-right">J U M L A H</th>
														<th>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($broker->total_bayar, 0, ',', '.') ?>,-</span>
														</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card -->
						</div>
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title"># SUBTOTAL</h3>
								</div>
								<div class="card-body">
									<div class="row mt-2">
										<div class="col-12">
											<table id="detail" class="table table-bordered table-hover table-sm">
												<thead class="thead-light">
													<tr class="text-center">
														<th>Keterangan</th>
														<th>Bayar</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Pembayaran RPA</td>
														<td>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($rpa->total_bayar, 0, ',', '.') ?>,-</span>
														</td>
													</tr>
													<tr>
														<td>Pembayaran BROKER</td>
														<td>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($broker->total_bayar, 0, ',', '.') ?>,-</span>
														</td>
													</tr>
												</tbody>
												<tfoot class="bg-light">
													<tr>
														<th class="text-right">J U M L A H</th>
														<th>
															<span class="float-left">Rp.</span>
															<span class="float-right"><?= number_format($rpa->total_bayar + $broker->total_bayar, 0, ',', '.') ?>,-</span>
														</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
								<div class="card-footer text-right">
									<button type="button" class="btn btn-md btn-success float-left" onclick="history.back()">
										<i class="fas fa-arrow-left mr-2"></i>
										Kembali
									</button>
								</div>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->