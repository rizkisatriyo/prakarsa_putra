<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('pengeluaran_akun_detail') ?>">Akun Pengeluaran</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <form action="<?= base_url('pengeluaran_akun_detail/aksi_tambah') ?>" method="POST">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-sm-8">
                                        <label for="nama">Nama</label>
                                        <input type="text" name="nama" value="<?= set_value('nama') ?>" class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" id="nama" placeholder="Nama Akun .." autofocus required>
                                        <div id="nama" class="invalid-feedback">
                                            <?= form_error('nama') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-8">
                                        <label for="id_pengeluaran_akun">Kategori</label>
                                        <select name="id_pengeluaran_akun" id="id_pengeluaran_akun" class="form-control select2bs4 <?= form_error('id_pengeluaran_akun') ? 'is-invalid' : '' ?>" style="width: 100%;" required>
                                            <option></option>
                                            <?php foreach ($pengeluaran_akun as $item) : ?>
                                                <option value="<?= $item['id'] ?>" <?= (set_value('id_pengeluaran_akun') == $item['id']) ? 'selected' : '' ?>><?= $item['nama'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <div id="id_pengeluaran_akun" class="invalid-feedback">
                                            <?= form_error('id_pengeluaran_akun') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row mb-0">
                                    <div class="form-group col-md-6">
                                        <label for="keterangan">Keterangan</label>
                                        <textarea name="keterangan" class="form-control <?= form_error('keterangan') ? 'is-invalid' : '' ?>" id="keterangan" rows="2" placeholder="Keterangan .."><?= set_value('keterangan') ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer text-right">
                                <button type="button" onclick="history.back()" class="btn btn-md btn-success float-left">
                                    <i class="fas fa-arrow-left mr-2"></i>
                                    Kembali
                                </button>
                                <button type="reset" class="btn btn-secondary"><i class="fas fa-sync-alt mr-2"></i> Reset</button>
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->