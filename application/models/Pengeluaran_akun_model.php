<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_akun_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        return $this->db->get('pengeluaran_akun')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('pengeluaran_akun', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->insert('pengeluaran_akun', $data);
    }

    public function ubah($id)
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->update('pengeluaran_akun', $data, ['id' => $id]);
    }

    public function hapus($id)
    {
        return $this->db->delete('pengeluaran_akun', ['id' => $id]);
    }
}
