<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('name');
        return $this->db->get('user')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('user', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'username' => $this->input->post('username'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'status' => 1,
            'role' => 'Operator'
        ];

        $this->db->insert('user', $data);
    }

    // public function ubah($id)
    // {
    //     $data = [
    //         'name' => $this->input->post('name'),
    //         'phone' => $this->input->post('phone'),
    //         'email' => $this->input->post('email'),
    //         'address' => $this->input->post('address'),
    //     ];

    //     $this->db->update('user', $data, ['id' => $id]);
    // }

    // public function hapus($id)
    // {
    //     return $this->db->delete('user', ['id' => $id]);
    // }
}
