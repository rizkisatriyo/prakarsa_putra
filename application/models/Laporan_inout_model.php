<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_inout_model extends CI_Model
{
    public function find_uang_masuk_rpa($tgl)
    {
        $this->db->order_by('tgl_bayar', 'desc');
        return $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tgl])->row();
    }

    public function find_uang_masuk_rpa_detail($tgl)
    {
        $this->db->order_by('penjualan_pembayaran.tgl_bayar', 'desc');
        $this->db->join('customers', 'customers.id = penjualan_pembayaran_detail.id_customer', 'left');
        $this->db->join('penjualan_pembayaran', 'penjualan_pembayaran.no_transaksi = penjualan_pembayaran_detail.no_transaksi', 'left');
        return $this->db->get_where('penjualan_pembayaran_detail', ['penjualan_pembayaran.tgl_bayar <=' => $tgl])->result_array();
    }

    public function find_uang_masuk_broker($tgl)
    {
        $this->db->order_by('tgl_bayar', 'desc');
        return $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tgl])->row();
    }

    public function find_uang_masuk_broker_detail($tgl)
    {
        $this->db->order_by('penjualan_broker_pembayaran.tgl_bayar', 'desc');
        $this->db->join('broker', 'broker.id = penjualan_broker_pembayaran_detail.id_broker', 'left');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.no_transaksi = penjualan_broker_pembayaran_detail.no_transaksi', 'left');
        return $this->db->get_where('penjualan_broker_pembayaran_detail', ['penjualan_broker_pembayaran.tgl_bayar <=' => $tgl])->result_array();
    }

    public function find_pengeluaran($tgl)
    {
        $this->db->order_by('tanggal', 'desc');
        return $this->db->get_where('pengeluaran', ['tanggal <=' => $tgl])->row();
    }

    public function find_pengeluaran_detail($tgl)
    {
        $this->db->order_by('pengeluaran.tanggal', 'desc');
        $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_detail.id_pengeluaran_akun_detail', 'left');
        $this->db->join('pengeluaran', 'pengeluaran.id = pengeluaran_detail.no_transaksi', 'left');
        return $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tgl])->result_array();
    }
    // public function all()
    // {
    //     return $this->db->get('pembelian')->result_array();
    // }

    public function find_pembelian($tgl_pembelian)
    {
        return $this->db->get_where('pembelian', ['tgl_beli' => $tgl_pembelian])->row();
    }

    public function find_pembelian_detail($no_transaksi)
    {
        $this->db->join('suppliers', 'suppliers.id = pembelian_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find_penjualan($tgl_penjualan)
    {
        return $this->db->get_where('penjualan', ['tgl_jual' => $tgl_penjualan])->row();
    }

    public function find_penjualan_detail($no_transaksi)
    {
        $this->db->join('customers', 'customers.id = penjualan_detail.id_customer', 'left');
        return $this->db->get_where('penjualan_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find_penjualan_broker($tgl_penjualan_broker)
    {
        return $this->db->get_where('penjualan_broker', ['tgl_jual' => $tgl_penjualan_broker])->row();
    }

    public function find_penjualan_broker_detail($no_transaksi)
    {
        $this->db->join('broker', 'broker.id = penjualan_broker_detail.id_broker', 'left');
        return $this->db->get_where('penjualan_broker_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find_sisa_stok($tanggal_penjualan)
    {
        $this->db->select('*');
        $this->db->select('sisa_stok.harga AS harga_stok');
        $this->db->select('sisa_stok.jumlah AS jumlah_stok');
        // $this->db->join('penjualan', 'penjualan.no_transaksi = sisa_stok.no_transaksi_penjualan', 'left');
        return $this->db->get_where('sisa_stok', ['tanggal' => $tanggal_penjualan])->row();
    }

    public function find_hutang($tgl)
    {
        return $this->db->get_where('pembelian_pembayaran', ['tgl_bayar' => $tgl])->row();
    }

    public function find_hutang_detail($no_transaksi)
    {
        $this->db->join('suppliers', 'suppliers.id = pembelian_pembayaran_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }
}
