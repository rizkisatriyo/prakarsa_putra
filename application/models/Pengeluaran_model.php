<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tanggal', 'desc');
        return $this->db->get('pengeluaran')->result_array();
    }

    public function generate_trans_no()
    {
        return "PLR" . date('Ymd');
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('pengeluaran', ['no_transaksi' => $no_transaksi])->row();
    }

    public function find_detail($no_transaksi)
    {
        $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
        return $this->db->get_where('pengeluaran_detail', ['pengeluaran.no_transaksi' => $no_transaksi])->result_array();
    }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'tanggal' => $this->input->post('tanggal'),
            'total' => round($this->input->post('total')),
            'tot_setor_bank' => $this->input->post('tot_setor_bank'),
            'setoran_rpa' => $this->input->post('setoran_rpa'),
            'setoran_broker' => $this->input->post('setoran_broker'),
            'setor_tabungan' => $this->input->post('setor_tabungan'),
            'setor_modal_rpa' => $this->input->post('setor_modal_rpa'),
            'setor_modal_total' => $this->input->post('setor_modal_total'),
            'sisa_setoran' => $this->input->post('sisa_setoran'),
            'output_pokok' => $this->input->post('output_pokok'),
        ];

        $this->db->insert('pengeluaran', $data);
    }

    public function tambah_detail()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_transaksi' => $data[$i]['no_transaksi'],
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_pengeluaran_akun_detail' => $data[$i]['id_pengeluaran_akun_detail'],
                'keterangan' => $data[$i]['keterangan'],
                'bayar' => $data[$i]['jml_bayar'],
            ];

            $this->db->insert('pengeluaran_detail', $dt);
        }
    }
}
