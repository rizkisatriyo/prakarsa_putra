<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kandang_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        return $this->db->get('kandang')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('kandang', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->insert('kandang', $data);
    }

    public function ubah($id)
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->update('kandang', $data, ['id' => $id]);
    }

    public function hapus($id)
    {
        return $this->db->delete('kandang', ['id' => $id]);
    }
}
