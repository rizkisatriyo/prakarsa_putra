<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_customers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        // $this->db->where('piutang_customer !=',0);
        // $this->db->or_where('deposit_customer !=',0);
        return $this->db->get('customers')->result_array();
    }
}
