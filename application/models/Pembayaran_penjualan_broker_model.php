<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_penjualan_broker_model extends CI_Model
{
    public function all()
    {
        // $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_bayar', 'desc');
        return $this->db->get('penjualan_broker_pembayaran')->result_array();
    }

    public function generate_trans_no()
    {
        return "PJB" . date('Ymd');
    }

    public function detail($no_transaksi)
    {
        // $this->db->order_by('nama');
        $this->db->join('broker', 'broker.id = penjualan_broker_pembayaran_detail.id_broker', 'left');
        return $this->db->get_where('penjualan_broker_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('penjualan_broker_pembayaran', ['no_transaksi' => $no_transaksi])->row();
    }

    // DARI FORM TAMBAH penjualan_broker

    public function tambah()
    {
        $data = [
            'no_transaksi' => "PJB" . date('Ymd', strtotime($this->input->post('tgl_bayar'))),
            'tgl_bayar' => $this->input->post('tgl_bayar'),
            'total_piutang' => $this->input->post('total_piutang'),
            'total_bayar' => $this->input->post('total_bayar'),
            'total_deposit' => $this->input->post('total_deposit'),
        ];

        $this->db->insert('penjualan_broker_pembayaran', $data);
    }

    // tanpa loop brokers
    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');

    //     $i = 1;
    //     foreach ($data as $item) {
    //         $piutang = (float) $item['piutang'];
    //         $bayar = (float) $item['jml_bayar'];
    //         $deposit = (float) $item['deposit'];

    //         $dt = [
    //             'no_transaksi' => "PJB" . date('Ymd', strtotime($item['tgl_bayar'])),
    //             'no_trxdetail' => "PJB" . date('Ymd', strtotime($item['tgl_bayar'])) . sprintf("%03d", $i++),
    //             'id_broker' => $item['id_broker'],
    //             'piutang' => $piutang,
    //             'bayar' => $bayar,
    //             'deposit' => $deposit,
    //         ];

    //         $this->db->insert('penjualan_broker_pembayaran_detail', $dt);

    //         $piutang_broker = $piutang - $deposit - $bayar;
    //         if ($piutang_broker < 0) {
    //             $this->db->update('broker', ['piutang_broker' => 0, 'deposit_broker' => abs($piutang_broker)], ['id' => $item['id_broker']]);
    //         } else {
    //             $this->db->update('broker', ['piutang_broker' => $piutang_broker, 'deposit_broker' => 0], ['id' => $item['id_broker']]);
    //         }
    //     }
    // }

    // dgn loop brokers
    public function tambah_detail()
    {
        $data = $this->input->post('data');
        $bs = $this->db->get('broker')->result_array();

        $i = 1;
        $tgl = $data[0]['tgl_bayar'];
        foreach ($bs as $b) {
            $dt = [
                'no_transaksi' => "PJB" . date('Ymd', strtotime($tgl)),
                'no_trxdetail' => "PJB" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
                'id_broker' => $b['id'],
                'piutang' => (float) $b['piutang_broker'],
                'deposit' => (float) $b['deposit_broker'],
            ];

            foreach ($data as $item) {
                if ($item['id_broker'] == $b['id']) {
                    $piutang = (float) $item['piutang'];
                    $bayar = (float) $item['jml_bayar'];
                    $deposit = (float) $item['deposit'];

                    $dt['piutang'] = $piutang;
                    $dt['bayar'] = $bayar;
                    $dt['deposit'] = $deposit;

                    $piutang_broker = $piutang - $deposit - $bayar;
                    if ($piutang_broker < 0) {
                        $this->db->update('broker', ['piutang_broker' => 0, 'deposit_broker' => abs($piutang_broker)], ['id' => $item['id_broker']]);
                    } else {
                        $this->db->update('broker', ['piutang_broker' => $piutang_broker, 'deposit_broker' => 0], ['id' => $item['id_broker']]);
                    }
                }
            }

            $this->db->insert('penjualan_broker_pembayaran_detail', $dt);
        }
    }

    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');
    //     $bs = $this->db->get('broker')->result_array();

    //     $i = 1;
    //     $tgl = $data[0]['tgl_bayar'];
    //     foreach ($bs as $c) {
    //         $dt = [
    //             'no_transaksi' => "PJB" . date('Ymd', strtotime($tgl)),
    //             'no_trxdetail' => "PJB" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
    //             'id_broker' => $c['id'],
    //             'piutang' => (float) $c['piutang_broker'],
    //             'deposit' => (float) $c['deposit_broker'],
    //         ];

    //         foreach ($data as $item) {
    //             if ($item['id_broker'] == $c['id']) {
    //                 $dt['piutang'] = (float) $item['piutang'];
    //                 $dt['bayar'] = (float) $item['jml_bayar'];
    //                 $dt['deposit'] = (float) $item['deposit'];

    //                 if ($item['deposit'] && !$item['jml_bayar']) {
    //                     $deposit_broker = $item['deposit'] - $item['tagihan'];
    //                     if ($deposit_broker < 0) {
    //                         $this->db->update('broker', ['deposit_broker' => 0], ['id' => $dt['id_broker']]);

    //                         $this->db->set('piutang_broker', "piutang_broker+" . abs($deposit_broker), false);
    //                         $this->db->where('id', $dt['id_broker']);
    //                         $this->db->update('broker');
    //                     } else {
    //                         $this->db->update('broker', ['deposit_broker' => $deposit_broker], ['id' => $dt['id_broker']]);
    //                     }
    //                 }
    //                 if ($item['deposit'] && $item['jml_bayar']) {
    //                     $deposit_broker = $item['deposit'] - $item['tagihan'] + $item['jml_bayar'];
    //                     if ($deposit_broker < 0) {
    //                         $this->db->update('broker', ['deposit_broker' => 0], ['id' => $dt['id_broker']]);

    //                         $this->db->set('piutang_broker', "piutang_broker+" . abs($deposit_broker), false);
    //                         $this->db->where('id', $dt['id_broker']);
    //                         $this->db->update('broker');
    //                     } else {
    //                         $this->db->update('broker', ['deposit_broker' => $deposit_broker], ['id' => $dt['id_broker']]);
    //                     }
    //                 }

    //                 if ($item['piutang_broker'] && !$item['jml_bayar']) {
    //                     $piutang_broker = $item['piutang_broker'] + $item['tagihan'];
    //                     if ($piutang_broker < 0) {
    //                         $this->db->update('broker', ['piutang_broker' => 0], ['id' => $dt['id_broker']]);

    //                         $this->db->set('deposit_broker', "deposit_broker+" . abs($piutang_broker), false);
    //                         $this->db->where('id', $dt['id_broker']);
    //                         $this->db->update('broker');
    //                     } else {
    //                         $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //                     }
    //                 }
    //                 if ($item['piutang_broker'] && $item['jml_bayar']) {
    //                     $piutang_broker = $item['piutang_broker'] + $item['tagihan'] - $item['jml_bayar'];
    //                     $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //                     if ($piutang_broker < 0) {
    //                         $this->db->update('broker', ['piutang_broker' => 0], ['id' => $dt['id_broker']]);

    //                         $this->db->set('deposit_broker', "deposit_broker+" . abs($piutang_broker), false);
    //                         $this->db->where('id', $dt['id_broker']);
    //                         $this->db->update('broker');
    //                     } else {
    //                         $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //                     }
    //                 }
    //             }
    //         }

    //         $this->db->insert('penjualan_broker_pembayaran_detail', $dt);
    //     }

    //     // $i = 1;
    //     // foreach ($data as $item) {
    //     //     $dt = [
    //     //         'no_transaksi' => "PJB" . date('Ymd', strtotime($item['tgl_bayar'])),
    //     //         'no_trxdetail' => "PJB" . date('Ymd', strtotime($item['tgl_bayar'])) . sprintf("%03d", $i++),
    //     //         'id_broker' => $item['id_broker'],
    //     //         'piutang' => (float) $item['piutang'],
    //     //         'bayar' => (float) $item['jml_bayar'],
    //     //         'deposit' => (float) $item['deposit'],
    //     //     ];

    //     //     if (!in_array($item['id_broker'], array_column($bs, 'id'))) {
    //     //         $c = $this->db->get_where('broker', ['id' => $item['id']])->row();
    //     //         $dt['piutang'] = $c->piutang_broker;
    //     //         $dt['deposit'] = $c->deposit_broker;
    //     //     }

    //     //     $this->db->insert('penjualan_broker_pembayaran_detail', $dt);

    //     //     if ($dt['deposit'] && !$dt['bayar']) {
    //     //         $deposit_broker = $dt['deposit'] - $item['tagihan'];
    //     //         if ($deposit_broker < 0) {
    //     //             $this->db->update('broker', ['deposit_broker' => 0], ['id' => $dt['id_broker']]);

    //     //             $this->db->set('piutang_broker', "piutang_broker+" . abs($deposit_broker), false);
    //     //             $this->db->where('id', $dt['id_broker']);
    //     //             $this->db->update('broker');
    //     //         } else {
    //     //             $this->db->update('broker', ['deposit_broker' => $deposit_broker], ['id' => $dt['id_broker']]);
    //     //         }
    //     //     }

    //     //     if ($dt['deposit'] && $dt['bayar']) {
    //     //         $deposit_broker = $dt['deposit'] - $item['tagihan'] + $dt['bayar'];
    //     //         if ($deposit_broker < 0) {
    //     //             $this->db->update('broker', ['deposit_broker' => 0], ['id' => $dt['id_broker']]);

    //     //             $this->db->set('piutang_broker', "piutang_broker+" . abs($deposit_broker), false);
    //     //             $this->db->where('id', $dt['id_broker']);
    //     //             $this->db->update('broker');
    //     //         } else {
    //     //             $this->db->update('broker', ['deposit_broker' => $deposit_broker], ['id' => $dt['id_broker']]);
    //     //         }
    //     //     }

    //     //     if ($item['piutang_broker'] && !$dt['bayar']) {
    //     //         $piutang_broker = $item['piutang_broker'] + $item['tagihan'];
    //     //         if ($piutang_broker < 0) {
    //     //             $this->db->update('broker', ['piutang_broker' => 0], ['id' => $dt['id_broker']]);

    //     //             $this->db->set('deposit_broker', "deposit_broker+" . abs($piutang_broker), false);
    //     //             $this->db->where('id', $dt['id_broker']);
    //     //             $this->db->update('broker');
    //     //         } else {
    //     //             $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //     //         }
    //     //     }
    //     //     if ($item['piutang_broker'] && $dt['bayar']) {
    //     //         $piutang_broker = $item['piutang_broker'] + $item['tagihan'] - $dt['bayar'];
    //     //         $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //     //         if ($piutang_broker < 0) {
    //     //             $this->db->update('broker', ['piutang_broker' => 0], ['id' => $dt['id_broker']]);

    //     //             $this->db->set('deposit_broker', "deposit_broker+" . abs($piutang_broker), false);
    //     //             $this->db->where('id', $dt['id_broker']);
    //     //             $this->db->update('broker');
    //     //         } else {
    //     //             $this->db->update('broker', ['piutang_broker' => $piutang_broker], ['id' => $dt['id_broker']]);
    //     //         }
    //     //     }
    //     // }


    // }

    public function bayar()
    {
        $no_trans = $this->input->post('no_transaksi');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $id_broker = $this->input->post('id_broker');
        $piutang_broker = (float) $this->input->post('piutang_broker');
        $deposit_broker = (float) $this->input->post('deposit_broker');
        $jml_bayar = (float) $this->input->post('jml_bayar');

        $p_brok = $piutang_broker - $deposit_broker - $jml_bayar;
        $d_brok = $deposit_broker;
        if ($p_brok < 0) {
            $d_brok = abs($p_brok);
            $p_brok = 0;
        }

        $ckpbr = $this->db->get_where('penjualan_broker_pembayaran', ['no_transaksi' => $no_trans])->row();
        if ($ckpbr) {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_piutang' => $ckpbr->total_piutang + $piutang_broker,
                'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
                'total_deposit' => $ckpbr->total_deposit + $deposit_broker,
            ];

            $this->db->update('penjualan_broker_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

            $this->db->order_by('no_trxdetail', 'desc');
            $maxTrx = $this->db->get_where('penjualan_broker_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
            $num = substr($maxTrx->no_trxdetail, 11);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PJB" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
                'id_broker' => $id_broker,
                'piutang' => $piutang_broker,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_broker,
            ];

            $cktrxb = $this->db->get_where('penjualan_broker_pembayaran_detail', ['no_transaksi' => $no_trans, 'id_broker' => $id_broker])->row();
            if ($cktrxb) {
                $data['no_trxdetail'] = $cktrxb->no_trxdetail;
                $this->db->update('penjualan_broker_pembayaran_detail', $data, ['no_transaksi' => $no_trans, 'id_broker' => $id_broker]);
            } else {
                $this->db->insert('penjualan_broker_pembayaran_detail', $data);
            }

            $dt_brok = [
                'piutang_broker' => $p_brok,
                'deposit_broker' => $d_brok,
            ];

            $this->db->update('broker', $dt_brok, ['id' => $id_broker]);
        } else {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_piutang' => $piutang_broker,
                'total_bayar' => $jml_bayar,
                'total_deposit' => $deposit_broker,
            ];

            $this->db->insert('penjualan_broker_pembayaran', $dt_master);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PJB" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
                'id_broker' => $id_broker,
                'piutang' => $piutang_broker,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_broker,
            ];

            $this->db->insert('penjualan_broker_pembayaran_detail', $data);

            $dt_brok = [
                'piutang_broker' => $p_brok,
                'deposit_broker' => $d_brok,
            ];

            $this->db->update('broker', $dt_brok, ['id' => $id_broker]);
        }
    }
}
