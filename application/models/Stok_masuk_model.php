<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_masuk_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_checker', 'desc');
        $this->db->order_by('no_transaksi_penerimaan', 'desc');
        $this->db->order_by('penerimaan.tgl_diterima', 'desc');
        
        $this->db->join('penerimaan', 'penerimaan.no_transaksi = penerimaan_checker.no_transaksi_penerimaan', 'left');
        
        return $this->db->get('penerimaan_checker')->result_array();
        // $this->db->order_by('tanggal_masuk', 'desc');
        // $this->db->join('kandang', 'kandang.id = stok_masuk.id_kandang');
        // return $this->db->get('stok_masuk')->result_array();
    }
}
