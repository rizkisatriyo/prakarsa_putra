<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_checker_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_checker', 'desc');
        $this->db->order_by('tgl_check', 'desc');
        return $this->db->get('penjualan_checker')->result_array();
    }

    public function generate_trans_no()
    {
        return "TRM" . date('Ymd');
    }

    public function generate_checker_no()
    {
        return "CKJ" . date('Ymd');
    }

    public function tambah()
    {
        // INSERT penjualan_checker
        $data_penjualan_checker = [
            'no_checker' => $this->input->post('no_checker'),
            'tgl_check' => $this->input->post('tgl_check'),
            'no_transaksi_penjualan' => $this->input->post('no_transaksi_penjualan'),
            'total_ekor' => $this->input->post('total_ekor'),
            'total_kg' => $this->input->post('total_kg'),
            'sisa_ekor' => $this->input->post('sisa_ekor'),
            'sisa_kg' => $this->input->post('sisa_kg'),
            'mati_ekor' => $this->input->post('mati_ekor'),
            'mati_kg' => $this->input->post('mati_kg'),
        ];
        $this->db->insert('penjualan_checker', $data_penjualan_checker);

        // UPDATE STATUS PENJUALAN
        $this->db->set('status', 'Selesai');
        $this->db->where('no_transaksi', $this->input->post('no_transaksi_penjualan'));
        $this->db->update('penjualan');

        // INSERT penjualan_checker CHECKER
        // $data_penjualan_checker = [
        //     'no_checker' => $this->input->post('no_checker'),
        //     'no_transaksi_penjualan' => $this->input->post('no_transaksi'),
        //     'total_ekor' => $this->input->post('total_ekor'),
        //     'total_kg' => $this->input->post('total_kg'),
        // ];
        // $this->db->insert('penjualan_checker', $data_penjualan_checker);

        // INSERT DETAIL CHECKER
        $detail_checker = $this->input->post('detail_checker');
        $this->db->insert_batch('penjualan_checker_detail', $detail_checker);

        // INSERT SUSUT
        $data_penjualan_susut = [
            'no_checker' => $this->input->post('no_checker'),
            'ekor' => $this->input->post('ekor'),
            'presentase_ekor' => $this->input->post('presentase_ekor'),
            'kg' => $this->input->post('kg'),
            'presentase_kg' => $this->input->post('presentase_kg'),
        ];
        $this->db->insert('penjualan_susut', $data_penjualan_susut);
    }

    public function find($no_checker)
    {
        return $this->db->get_where('penjualan_checker', ['no_checker' => $no_checker])->row();
    }

    // public function find_checker($no_checker)
    // {
    //     return $this->db->get_where('penjualan_checker', ['no_checker' => $no_checker])->row();
    // }

    public function find_penjualan_checker_detail($no_checker)
    {
        $this->db->join('customers', 'customers.id = penjualan_checker_detail.id_customer', 'left');
        return $this->db->get_where('penjualan_checker_detail', ['no_checker' => $no_checker])->result_array();
    }
}
