<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_beli', 'desc');
        return $this->db->get('pembelian')->result_array();
    }

    public function generate_trans_no()
    {
        return "PBN" . date('Ymd');
    }

    public function detail($no_transaksi)
    {
        $this->db->order_by('nama');
        $this->db->join('suppliers', 'suppliers.id = pembelian_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('pembelian', ['no_transaksi' => $no_transaksi])->row();
    }

    public function sisa_kandang()
    {
        return $this->db->get_where('stok', [])->row();
        return $this->db->get_where('stok', [])->row();

    }
    public function find_sisa_kandang($tanggal)
    {
        return $this->db->get_where('stok', [])->row();
        return $this->db->get_where('sisa_stok', ['tanggal' => $tanggal])->row();

    }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'tgl_beli' => $this->input->post('tgl_beli'),
            'ekor' => $this->input->post('ekor'),
            'kg' => $this->input->post('kg'),
            'harga' => $this->input->post('harga'),
            'total_hutang' => $this->input->post('total_hutang')
        ];

        $this->db->insert('pembelian', $data);
    }

    public function tambah_detail()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_transaksi' => $data[$i]['no_transaksi'],
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_supplier' => $data[$i]['id_supplier'],
                'hrg_ayam' => $data[$i]['hrg_ayam'],
                'ekor' => $data[$i]['ekor'],
                'kg' => $data[$i]['kg'],
                'subtotal' => $data[$i]['subtotal'],
            ];

            $this->db->insert('pembelian_detail', $dt);
        }
    }
}
