<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_pembelian_model extends CI_Model
{
    public function all()
    {
        // $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_bayar', 'desc');
        return $this->db->get('pembelian_pembayaran')->result_array();
    }

    public function generate_trans_no()
    {
        return "PBP" . date('Ymd');
    }

    public function detail($no_transaksi)
    {
        // $this->db->order_by('nama');
        $this->db->join('suppliers', 'suppliers.id = pembelian_pembayaran_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('pembelian_pembayaran', ['no_transaksi' => $no_transaksi])->row();
    }

    // DARI FORM TAMBAH PEMBELIAN

    public function tambah()
    {
        $data = [
            'no_transaksi' => "PBP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))),
            'tgl_bayar' => $this->input->post('tgl_bayar'),
            'total_hutang' => $this->input->post('total_hutang'),
            'total_bayar' => $this->input->post('total_bayar'),
            'total_deposit' => $this->input->post('total_deposit'),
        ];

        $this->db->insert('pembelian_pembayaran', $data);
    }

    // tanpa loop suppliers
    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');

    //     $i = 1;
    //     foreach ($data as $item) {
    //         $hutang = (float) $item['hutang'];
    //         $bayar = (float) $item['jml_bayar'];
    //         $deposit = (float) $item['deposit'];

    //         $dt = [
    //             'no_transaksi' => "PBP" . date('Ymd', strtotime($item['tgl_bayar'])),
    //             'no_trxdetail' => "PBP" . date('Ymd', strtotime($item['tgl_bayar'])) . sprintf("%03d", $i++),
    //             'id_supplier' => $item['id_supplier'],
    //             'hutang' => $hutang,
    //             'bayar' => $bayar,
    //             'deposit' => $deposit,
    //         ];

    //         $this->db->insert('pembelian_pembayaran_detail', $dt);

    //         $hutang_supplier = $hutang - $bayar - $deposit;
    //         if ($hutang_supplier < 0) {
    //             $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_supplier)], ['id' => $item['id_supplier']]);
    //         } else {
    //             $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier, 'deposit_supplier' => 0], ['id' => $item['id_supplier']]);
    //         }
    //     }
    // }

    // dgn loop suppliers
    public function tambah_detail()
    {
        $data = $this->input->post('data');
        $ss = $this->db->get('suppliers')->result_array();

        $i = 1;
        $tgl = $data[0]['tgl_bayar'];
        foreach ($ss as $s) {
            $dt = [
                'no_transaksi' => "PBP" . date('Ymd', strtotime($tgl)),
                'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
                'id_supplier' => $s['id'],
                'hutang' => (float) $s['hutang_supplier'],
                'deposit' => (float) $s['deposit_supplier'],
            ];

            foreach ($data as $item) {
                if ($item['id_supplier'] == $s['id']) {
                    $hutang = (float) $item['hutang'];
                    $bayar = (float) $item['jml_bayar'];
                    $deposit = (float) $item['deposit'];

                    $dt['hutang'] = $hutang;
                    $dt['bayar'] = $bayar;
                    $dt['deposit'] = $deposit;

                    $hutang_supplier = $hutang - $deposit - $bayar;
                    if ($hutang_supplier < 0) {
                        $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_supplier)], ['id' => $item['id_supplier']]);
                    } else {
                        $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier, 'deposit_supplier' => 0], ['id' => $item['id_supplier']]);
                    }
                }
            }

            $this->db->insert('pembelian_pembayaran_detail', $dt);
        }
    }

    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');
    //     $ss = $this->db->get('suppliers')->result_array();

    //     $i = 1;
    //     $tgl = $data[0]['tgl_bayar'];
    //     foreach ($ss as $s) {
    //         $dt = [
    //             'no_transaksi' => "PBP" . date('Ymd', strtotime($tgl)),
    //             'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
    //             'id_supplier' => $c['id'],
    //             'hutang' => (float) $c['hutang_supplier'],
    //             'deposit' => (float) $c['deposit_supplier'],
    //         ];

    //         foreach ($data as $item) {
    //             if ($item['id_supplier'] == $c['id']) {
    //                 $dt['hutang'] = (float) $item['hutang'];
    //                 $dt['bayar'] = (float) $item['jml_bayar'];
    //                 $dt['deposit'] = (float) $item['deposit'];

    //                 if ($item['deposit'] && !$item['jml_bayar']) {
    //                     $deposit_supplier = $item['deposit'] - $item['tagihan'];
    //                     if ($deposit_supplier < 0) {
    //                         $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($deposit_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     } else {
    //                         $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
    //                     }
    //                 }
    //                 if ($item['deposit'] && $item['jml_bayar']) {
    //                     $deposit_supplier = $item['deposit'] - $item['tagihan'] + $item['jml_bayar'];
    //                     if ($deposit_supplier < 0) {
    //                         $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($deposit_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     } else {
    //                         $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
    //                     }
    //                 }
    //                 if (!$item['deposit'] && !$item['jml_bayar']) {
    //                     $hutang_supplier = $item['tagihan'];
    //                     // if ($hutang_supplier < 0) {
    //                     //     $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($hutang_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     // } else {
    //                         // $this->db->update('suppliers', ['hutang_supplier+' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                     // }
    //                 }
    //                 if (!$item['deposit'] && $item['jml_bayar']) {
    //                     $hutang_supplier = $item['hutang'] + $item['tagihan'] - $item['jml_bayar'];
    //                     if ($hutang_supplier < 0) {
    //                         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     } else {
    //                         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                     }
    //                 }

    //                 if ($item['hutang_supplier'] && !$item['jml_bayar']) {
    //                     $hutang_supplier = $item['hutang_supplier'] + $item['tagihan'];
    //                     if ($hutang_supplier < 0) {
    //                         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     } else {
    //                         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                     }
    //                 }
    //                 if ($item['hutang_supplier'] && $item['jml_bayar']) {
    //                     $hutang_supplier = $item['hutang_supplier'] + $item['tagihan'] - $item['jml_bayar'];
    //                     $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                     if ($hutang_supplier < 0) {
    //                         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
    //                         $this->db->where('id', $dt['id_supplier']);
    //                         $this->db->update('suppliers');
    //                     } else {
    //                         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                     }
    //                 }
    //                 // if ($item['tagihan'] > $dt['jml_bayar'] && $dt['deposit'] == 0 ) {
    //                 //     $hutang_supplier = $item['tagihan'] - $item['jml_bayar'];
    //                 //     $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                 //     if ($hutang_supplier < 0) {
    //                 //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                 //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
    //                 //         $this->db->where('id', $dt['id_supplier']);
    //                 //         $this->db->update('suppliers');
    //                 //     } else {
    //                 //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                 //     }
    //                 // }
    //                 // if ($item['tagihan'] < $dt['jml_bayar'] && $dt['hutang'] <= 0 ) {
    //                 //     $hutang_supplier = $item['jml_bayar']-$item['tagihan'];
    //                 //     $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                 //     if ($dt['hutang_supplier'] < 0) {
    //                 //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

    //                 //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
    //                 //         $this->db->where('id', $dt['id_supplier']);
    //                 //         $this->db->update('suppliers');
    //                 //     } else {
    //                 //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
    //                 //     }
    //                 // }
    //             }
    //         }

    //         $this->db->insert('pembelian_pembayaran_detail', $dt);
    //     }
    // }
    

    public function bayar()
    {
        $no_trans = $this->input->post('no_transaksi');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $id_supplier = $this->input->post('id_supplier');
        $hutang_supplier = (float) $this->input->post('hutang_supplier');
        $deposit_supplier = (float) $this->input->post('deposit_supplier');
        $jml_bayar = (float) $this->input->post('jml_bayar');

        $h_supp = $hutang_supplier - $deposit_supplier - $jml_bayar;
        $d_supp = $deposit_supplier;
        if ($h_supp < 0) {
            $d_supp = abs($h_supp);
            $h_supp = 0;
        }

        $ckpbr = $this->db->get_where('pembelian_pembayaran', ['no_transaksi' => $no_trans])->row();
        if ($ckpbr) {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_hutang' => $ckpbr->total_hutang + $hutang_supplier,
                'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
                'total_deposit' => $ckpbr->total_deposit + $deposit_supplier,
            ];

            $this->db->update('pembelian_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

            $this->db->order_by('no_trxdetail', 'desc');
            $maxTrx = $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
            $num = substr($maxTrx->no_trxdetail, 11);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PBP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
                'id_supplier' => $id_supplier,
                'hutang' => $hutang_supplier,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_supplier,
            ];

            $cktrxb = $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_trans, 'id_supplier' => $id_supplier])->row();
            if ($cktrxb) {
                $data['no_trxdetail'] = $cktrxb->no_trxdetail;
                $this->db->update('pembelian_pembayaran_detail', $data, ['no_transaksi' => $no_trans, 'id_supplier' => $id_supplier]);
            } else {
                $this->db->insert('pembelian_pembayaran_detail', $data);
            }

            $dt_brok = [
                'hutang_supplier' => $h_supp,
                'deposit_supplier' => $d_supp,
            ];

            $this->db->update('suppliers', $dt_brok, ['id' => $id_supplier]);
        } else {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_hutang' => $hutang_supplier,
                'total_bayar' => $jml_bayar,
                'total_deposit' => $deposit_supplier,
            ];

            $this->db->insert('pembelian_pembayaran', $dt_master);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
                'id_supplier' => $id_supplier,
                'hutang' => $hutang_supplier,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_supplier,
            ];

            $this->db->insert('pembelian_pembayaran_detail', $data);

            $dt_brok = [
                'hutang_supplier' => $h_supp,
                'deposit_supplier' => $d_supp,
            ];

            $this->db->update('suppliers', $dt_brok, ['id' => $id_supplier]);
        }
    }

    // public function bayar()
    // {
    //     $no_trans = $this->input->post('no_transaksi');
    //     $tgl_bayar = $this->input->post('tgl_bayar');
    //     $id_supplier = $this->input->post('id_supplier');
    //     $hutang_supplier = $this->input->post('hutang_supplier');
    //     $deposit_supplier = $this->input->post('deposit_supplier');
    //     $jml_bayar = $this->input->post('jml_bayar');

    //     $h_supp = (float) $hutang_supplier - (float) $jml_bayar;

    //     $d_supp = $deposit_supplier;
    //     if ($h_supp < 0) {
    //         $d_supp = abs($h_supp);
    //         $h_supp = 0;
    //     }

    //     $ckpbr = $this->db->get_where('pembelian_pembayaran', ['no_transaksi' => $no_trans])->row();
    //     if ($ckpbr) {
    //         $dt_master = [
    //             'no_transaksi' => $no_trans,
    //             'tgl_bayar' => $tgl_bayar,
    //             'total_hutang' => $ckpbr->total_hutang + $hutang_supplier,
    //             'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
    //             'total_deposit' => $ckpbr->total_deposit + $deposit_supplier,
    //         ];

    //         $this->db->update('pembelian_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

    //         $this->db->order_by('no_trxdetail', 'desc');
    //         $maxTrx = $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
    //         $num = substr($maxTrx->no_trxdetail, 11);

    //         $data = [
    //             'no_transaksi' => $no_trans,
    //             'no_trxdetail' => "PBP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
    //             'id_supplier' => $id_supplier,
    //             'hutang' => (float) $hutang_supplier,
    //             'bayar' => (float) $jml_bayar,
    //             'deposit' => (float) $deposit_supplier,
    //         ];

    //         $this->db->insert('pembelian_pembayaran_detail', $data);

    //         $dt_supp = [
    //             'hutang_supplier' => (float) $h_supp,
    //             'deposit_supplier' => (float) $d_supp,
    //         ];

    //         $this->db->update('suppliers', $dt_supp, ['id' => $id_supplier]);
    //     } else {
    //         $dt_master = [
    //             'no_transaksi' => $no_trans,
    //             'tgl_bayar' => $tgl_bayar,
    //             'total_hutang' => $hutang_supplier,
    //             'total_bayar' => $jml_bayar,
    //             'total_deposit' => $deposit_supplier,
    //         ];

    //         $this->db->insert('pembelian_pembayaran', $dt_master);

    //         $data = [
    //             'no_transaksi' => $no_trans,
    //             'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
    //             'id_supplier' => $id_supplier,
    //             'hutang' => (float) $hutang_supplier,
    //             'bayar' => (float) $jml_bayar,
    //             'deposit' => (float) $deposit_supplier,
    //         ];

    //         $this->db->insert('pembelian_pembayaran_detail', $data);

    //         $dt_supp = [
    //             'hutang_supplier' => (float) $h_supp,
    //             'deposit_supplier' => (float) $d_supp,
    //         ];

    //         $this->db->update('suppliers', $dt_supp, ['id' => $id_supplier]);
    //     }
    // }
}
// <?php
// defined('BASEPATH') or exit('No direct script access allowed');

// class Pembayaran_pembelian_model extends CI_Model
// {
//     public function all()
//     {
//         // $this->db->order_by('status');
//         $this->db->order_by('no_transaksi', 'desc');
//         $this->db->order_by('tgl_bayar', 'desc');
//         return $this->db->get('pembelian_pembayaran')->result_array();
//     }

//     public function generate_trans_no()
//     {
//         return "PBP" . date('Ymd');
//     }

//     public function detail($no_transaksi)
//     {
//         // $this->db->order_by('nama');
//         $this->db->join('suppliers', 'suppliers.id = pembelian_pembayaran_detail.id_supplier', 'left');
//         return $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
//     }

//     public function find($no_transaksi)
//     {
//         return $this->db->get_where('pembelian_pembayaran', ['no_transaksi' => $no_transaksi])->row();
//     }

//     // DARI FORM TAMBAH PEMBELIAN

//     public function tambah()
//     {
//         $data = [
//             'no_transaksi' => "PBP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))),
//             'tgl_bayar' => $this->input->post('tgl_bayar'),
//             'total_hutang' => $this->input->post('total_hutang'),
//             'total_bayar' => $this->input->post('total_bayar'),
//             'total_deposit' => $this->input->post('total_deposit'),
//         ];

//         $this->db->insert('pembelian_pembayaran', $data);
//     }

//     public function tambah_detail()
//     {

//         $data = $this->input->post('data');
//         $ss = $this->db->get('suppliers')->result_array();

//         $i = 1;
//         $tgl = $data[0]['tgl_bayar'];
//         foreach ($ss as $c) {
//             $dt = [
//                 'no_transaksi' => "PBP" . date('Ymd', strtotime($tgl)),
//                 'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
//                 'id_supplier' => $c['id'],
//                 'hutang' => (float) $c['hutang_supplier'],
//                 'deposit' => (float) $c['deposit_supplier'],
//             ];

//             foreach ($data as $item) {
//                 if ($item['id_supplier'] == $c['id']) {
//                     $dt['hutang'] = (float) $item['hutang'];
//                     $dt['bayar'] = (float) $item['jml_bayar'];
//                     $dt['deposit'] = (float) $item['deposit'];

//                     // if ($item['jml_bayar']) {       // jika bayar
//                     //     if ($item['hutang']) {      // jika ada hutang
//                     //         $hutang_supplier = $item['hutang'] + $item['tagihan'] - $item['jml_bayar'];
//                     //         if ($hutang_supplier < 0) {
//                     //             $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_supplier)], ['id' => $dt['id_supplier']]);
//                     //         } else {
//                     //             $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //         }
//                     //     } else {                      // jika gada hutang
//                     //         $hutang_supplier = $item['tagihan'] - $item['jml_bayar'];
//                     //         if ($hutang_supplier < 0) {
//                     //             $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_supplier)], ['id' => $dt['id_supplier']]);
//                     //         } else {
//                     //             $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //         }
//                     //     }
//                     // }

//                     if ($c['hutang_supplier']) {
//                         echo "<h1>ADA HUTANG : </h1>";
//                         echo $c['hutang_supplier'];
//                         $hutang_supplier = (float)$item['hutang'] - (float)$item['jml_bayar'];
//                         if ($hutang_supplier < 0) {
//                             echo "<h1>HUTANG JADI DEPOSIT</h1>";
//                             $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_supplier)], ['id' => $dt['id_supplier']]);
//                         } else {
//                             echo "<h1>HUTANG BERKURANG/BERTAMBAH</h1>";
//                             $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                         }
//                     } elseif ($c['deposit_supplier']) {
//                         echo "<h1>ADA DEPOSIT : </h1>";
//                         echo $c['deposit_supplier'];
//                         $deposit_supplier = (float)$item['deposit'] - (float)$item['tagihan'] + (float)$item['jml_bayar'];
//                         if ($deposit_supplier < 0) {
//                             echo "<h1>DEPOSIT JADI HUTANG</h1>";
//                             $this->db->update('suppliers', ['deposit_supplier' => 0, 'hutang_supplier' => abs($deposit_supplier)], ['id' => $dt['id_supplier']]);
//                         } else {
//                             echo "<h1>DEPOSIT BERKURANG/BERTAMBAH</h1>";
//                             $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
//                         }
//                     } else {
//                         echo "<h1>GADA HUTANG/DEPOSIT</h1>";
//                         $hutang_deposit = (float)$item['tagihan'] - (float)$item['jml_bayar'];
//                         if ($hutang_deposit < 0) {
//                             echo "<h1>JADI DEPOSIT</h1>";
//                             $this->db->update('suppliers', ['hutang_supplier' => 0, 'deposit_supplier' => abs($hutang_deposit)], ['id' => $dt['id_supplier']]);
//                         } elseif ($hutang_deposit > 0) {
//                             echo "<h1>JADI HUTANG</h1>";
//                             $this->db->update('suppliers', ['deposit_supplier' => 0, 'hutang_supplier' => $hutang_deposit], ['id' => $dt['id_supplier']]);
//                         }
//                     }

//                     // if ($item['deposit'] && !$item['jml_bayar']) {
//                     //     $deposit_supplier = $item['deposit'] - $item['tagihan'];
//                     //     if ($deposit_supplier < 0) {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($deposit_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                     // if ($item['deposit'] && $item['jml_bayar']) {
//                     //     $deposit_supplier = $item['deposit'] - $item['tagihan'] + $item['jml_bayar'];
//                     //     if ($deposit_supplier < 0) {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($deposit_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                     // if (!$item['deposit'] && !$item['jml_bayar']) {
//                     //     $hutang_supplier = $item['tagihan'];
//                     //     // if ($hutang_supplier < 0) {
//                     //     //     $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($hutang_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     // } else {
//                     //         // $this->db->update('suppliers', ['hutang_supplier+' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     // }
//                     // }
//                     // if (!$item['deposit'] && $item['jml_bayar']) {
//                     //     $hutang_supplier = $item['hutang'] + $item['tagihan'] - $item['jml_bayar'];
//                     //     if ($hutang_supplier < 0) {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }

//                     // if ($item['hutang'] && !$item['jml_bayar']) {
//                     //     $hutang_supplier = $item['hutang'] + $item['tagihan'];
//                     //     if ($hutang_supplier < 0) {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                     // if ($item['hutang'] && $item['jml_bayar']) {
//                     //     $hutang_supplier = $item['hutang'] + $item['tagihan'] - $item['jml_bayar'];
//                     //     $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     if ($hutang_supplier < 0) {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                     // if (!$item['hutang'] && !$item['jml_bayar']) {
//                     //     $deposit_supplier -= $item['tagihan'];
//                     //     if ($deposit_supplier < 0) {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('hutang_supplier', "hutang_supplier+" . abs($deposit_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['deposit_supplier' => $deposit_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                     // if (!$item['hutang'] && $item['jml_bayar']) {
//                     //     $hutang_supplier = $item['hutang'] + $item['tagihan'] - $item['jml_bayar'];
//                     //     if ($hutang_supplier < 0) {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => 0], ['id' => $dt['id_supplier']]);

//                     //         $this->db->set('deposit_supplier', "deposit_supplier+" . abs($hutang_supplier), false);
//                     //         $this->db->where('id', $dt['id_supplier']);
//                     //         $this->db->update('suppliers');
//                     //     } else {
//                     //         $this->db->update('suppliers', ['hutang_supplier' => $hutang_supplier], ['id' => $dt['id_supplier']]);
//                     //     }
//                     // }
//                 }
//             }

//             $this->db->insert('pembelian_pembayaran_detail', $dt);
//         }
//     }

//     public function bayar()
//     {
//         $no_trans = $this->input->post('no_transaksi');
//         $tgl_bayar = $this->input->post('tgl_bayar');
//         $id_supplier = $this->input->post('id_supplier');
//         $hutang_supplier = $this->input->post('hutang_supplier');
//         $deposit_supplier = $this->input->post('deposit_supplier');
//         $jml_bayar = $this->input->post('jml_bayar');

//         $h_supp = (float) $hutang_supplier - (float) $jml_bayar;

//         $d_supp = $deposit_supplier;
//         if ($h_supp < 0) {
//             $d_supp = abs($h_supp);
//             $h_supp = 0;
//         }

//         $ckpbr = $this->db->get_where('pembelian_pembayaran', ['no_transaksi' => $no_trans])->row();
//         if ($ckpbr) {
//             $dt_master = [
//                 'no_transaksi' => $no_trans,
//                 'tgl_bayar' => $tgl_bayar,
//                 'total_hutang' => $ckpbr->total_hutang + $hutang_supplier,
//                 'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
//                 'total_deposit' => $ckpbr->total_deposit + $deposit_supplier,
//             ];

//             $this->db->update('pembelian_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

//             $this->db->order_by('no_trxdetail', 'desc');
//             $maxTrx = $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
//             $num = substr($maxTrx->no_trxdetail, 11);

//             $data = [
//                 'no_transaksi' => $no_trans,
//                 'no_trxdetail' => "PBP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
//                 'id_supplier' => $id_supplier,
//                 'hutang' => (float) $hutang_supplier,
//                 'bayar' => (float) $jml_bayar,
//                 'deposit' => (float) $deposit_supplier,
//             ];

//             $this->db->insert('pembelian_pembayaran_detail', $data);

//             $dt_supp = [
//                 'hutang_supplier' => (float) $h_supp,
//                 'deposit_supplier' => (float) $d_supp,
//             ];

//             $this->db->update('suppliers', $dt_supp, ['id' => $id_supplier]);
//         } else {
//             $dt_master = [
//                 'no_transaksi' => $no_trans,
//                 'tgl_bayar' => $tgl_bayar,
//                 'total_hutang' => $hutang_supplier,
//                 'total_bayar' => $jml_bayar,
//                 'total_deposit' => $deposit_supplier,
//             ];

//             $this->db->insert('pembelian_pembayaran', $dt_master);

//             $data = [
//                 'no_transaksi' => $no_trans,
//                 'no_trxdetail' => "PBP" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
//                 'id_supplier' => $id_supplier,
//                 'hutang' => (float) $hutang_supplier,
//                 'bayar' => (float) $jml_bayar,
//                 'deposit' => (float) $deposit_supplier,
//             ];

//             $this->db->insert('pembelian_pembayaran_detail', $data);

//             $dt_supp = [
//                 'hutang_supplier' => (float) $h_supp,
//                 'deposit_supplier' => (float) $d_supp,
//             ];

//             $this->db->update('suppliers', $dt_supp, ['id' => $id_supplier]);
//         }
//     }
// }
