<?php

class Profile_model extends CI_Model
{
    private $_table = "user";

    public function find()
    {
        return $this->db->get_where('user', ['id' => $this->auth_model->current_user()->id])->row();
    }

    public function profile_rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[255]'
            ],
            [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'max_length[255]'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'max_length[255]'
            ],
        ];
    }

    public function password_rules()
    {
        return [
            [
                'field' => 'password',
                'label' => 'New Password',
                'rules' => 'required'
            ],
            [
                'field' => 'password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'required|matches[password]'
            ],
        ];
    }

    public function update($data)
    {
        if (!$data['id']) {
            return;
        }
        return $this->db->update($this->_table, $data, ['id' => $data['id']]);
    }
}
