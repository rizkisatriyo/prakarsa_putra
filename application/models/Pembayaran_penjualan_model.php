<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_penjualan_model extends CI_Model
{
    public function all()
    {
        // $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_bayar', 'desc');
        return $this->db->get('penjualan_pembayaran')->result_array();
    }

    public function generate_trans_no()
    {
        return "PJP" . date('Ymd');
    }

    public function detail($no_transaksi)
    {
        // $this->db->order_by('nama');
        $this->db->join('customers', 'customers.id = penjualan_pembayaran_detail.id_customer', 'left');
        return $this->db->get_where('penjualan_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('penjualan_pembayaran', ['no_transaksi' => $no_transaksi])->row();
    }

    // DARI FORM TAMBAH penjualan

    public function tambah()
    {
        $data = [
            'no_transaksi' => "PJP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))),
            'tgl_bayar' => $this->input->post('tgl_bayar'),
            'total_piutang' => $this->input->post('total_piutang'),
            'total_bayar' => $this->input->post('total_bayar'),
            'total_deposit' => $this->input->post('total_deposit'),
        ];

        $this->db->insert('penjualan_pembayaran', $data);
    }

    // tanpa loop customers!!!
    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');

    //     $i = 1;
    //     foreach ($data as $item) {
    //         $piutang = (float) $item['piutang'];
    //         $bayar = (float) $item['jml_bayar'];
    //         $deposit = (float) $item['deposit'];

    //         $dt = [
    //             'no_transaksi' => "PJP" . date('Ymd', strtotime($item['tgl_bayar'])),
    //             'no_trxdetail' => "PJP" . date('Ymd', strtotime($item['tgl_bayar'])) . sprintf("%03d", $i++),
    //             'id_customer' => $item['id_customer'],
    //             'piutang' => $piutang,
    //             'bayar' => $bayar,
    //             'deposit' => $deposit,
    //         ];

    //         $this->db->insert('penjualan_pembayaran_detail', $dt);

    //         $piutang_customer = $piutang - $deposit - $bayar;
    //         if ($piutang_customer < 0) {
    //             $this->db->update('customers', ['piutang_customer' => 0, 'deposit_customer' => abs($piutang_customer)], ['id' => $item['id_customer']]);
    //         } else {
    //             $this->db->update('customers', ['piutang_customer' => $piutang_customer, 'deposit_customer' => 0], ['id' => $item['id_customer']]);
    //         }
    //     }
    // }

    // dgn loop customers
    public function tambah_detail()
    {
        $data = $this->input->post('data');
        $cs = $this->db->get('customers')->result_array();

        $i = 1;
        $tgl = $data[0]['tgl_bayar'];
        foreach ($cs as $c) {
            $dt = [
                'no_transaksi' => "PJP" . date('Ymd', strtotime($tgl)),
                'no_trxdetail' => "PJP" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
                'id_customer' => $c['id'],
                'piutang' => (float) $c['piutang_customer'],
                'deposit' => (float) $c['deposit_customer'],
            ];

            foreach ($data as $item) {
                if ($item['id_customer'] == $c['id']) {
                    $piutang = (float) $item['piutang'];
                    $bayar = (float) $item['jml_bayar'];
                    $deposit = (float) $item['deposit'];

                    $dt['piutang'] = $piutang;
                    $dt['bayar'] = $bayar;
                    $dt['deposit'] = $deposit;

                    $piutang_customer = $piutang - $deposit - $bayar;
                    if ($piutang_customer < 0) {
                        $this->db->update('customers', ['piutang_customer' => 0, 'deposit_customer' => abs($piutang_customer)], ['id' => $item['id_customer']]);
                    } else {
                        $this->db->update('customers', ['piutang_customer' => $piutang_customer, 'deposit_customer' => 0], ['id' => $item['id_customer']]);
                    }
                }
            }

            $this->db->insert('penjualan_pembayaran_detail', $dt);
        }
    }

    // public function tambah_detail()
    // {
    //     $data = $this->input->post('data');
    //     $cs = $this->db->get('customers')->result_array();


    //     echo "<pre>";
    //     print_r ($data);
    //     echo "</pre>";
    //     echo "<pre>";
    //     print_r ($cs);
    //     echo "</pre>";


    //     $i = 1;
    //     $tgl = $data[0]['tgl_bayar'];
    //     foreach ($cs as $c) {
    //         $dt = [
    //             'no_transaksi' => "PJP" . date('Ymd', strtotime($tgl)),
    //             'no_trxdetail' => "PJP" . date('Ymd', strtotime($tgl)) . sprintf("%03d", $i++),
    //             'id_customer' => $c['id'],
    //             'piutang' => (float) $c['piutang_customer'],
    //             'deposit' => (float) $c['deposit_customer'],
    //         ];

    //         foreach ($data as $item) {
    //             if ($item['id_customer'] == $c['id']) {
    //                 $dt['piutang'] = (float) $item['piutang'];
    //                 $dt['bayar'] = (float) $item['jml_bayar'];
    //                 $dt['deposit'] = (float) $item['deposit'];

    //                 if ($item['deposit'] && !$item['jml_bayar']) {
    //                     echo "<h1>ADA DEPOSIT & TIDAK BAYAR</h1>";
    //                     $deposit_customer = $item['deposit'] - $item['tagihan'];
    //                     if ($deposit_customer < 0) {
    //                         $this->db->update('customers', ['deposit_customer' => 0], ['id' => $dt['id_customer']]);

    //                         $this->db->set('piutang_customer', "piutang_customer+" . abs($deposit_customer), false);
    //                         $this->db->where('id', $dt['id_customer']);
    //                         $this->db->update('customers');
    //                     } else {
    //                         $this->db->update('customers', ['deposit_customer' => $deposit_customer], ['id' => $dt['id_customer']]);
    //                     }
    //                 }
    //                 if ($item['deposit'] && $item['jml_bayar']) {
    //                     echo "<h1>ADA DEPOSIT & BAYAR</h1>";
    //                     $deposit_customer = $item['deposit'] - $item['tagihan'] + $item['jml_bayar'];
    //                     if ($deposit_customer < 0) {
    //                         $this->db->update('customers', ['deposit_customer' => 0], ['id' => $dt['id_customer']]);

    //                         $this->db->set('piutang_customer', "piutang_customer+" . abs($deposit_customer), false);
    //                         $this->db->where('id', $dt['id_customer']);
    //                         $this->db->update('customers');
    //                     } else {
    //                         $this->db->update('customers', ['deposit_customer' => $deposit_customer], ['id' => $dt['id_customer']]);
    //                     }
    //                 }

    //                 if ($item['piutang_customer'] && !$item['jml_bayar']) {
    //                     echo "<h1>ADA PIUTANG & TIDAK BAYAR</h1>";
    //                     $piutang_customer = $item['piutang_customer'] + $item['tagihan'];
    //                     if ($piutang_customer < 0) {
    //                         $this->db->update('customers', ['piutang_customer' => 0], ['id' => $dt['id_customer']]);

    //                         $this->db->set('deposit_customer', "deposit_customer+" . abs($piutang_customer), false);
    //                         $this->db->where('id', $dt['id_customer']);
    //                         $this->db->update('customers');
    //                     } else {
    //                         $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //                     }
    //                 }
    //                 if ($item['piutang_customer'] && $item['jml_bayar']) {
    //                     echo "<h1>ADA PIUTANG & BAYAR</h1>";
    //                     $piutang_customer = $item['piutang_customer'] + $item['tagihan'] - $item['jml_bayar'];
    //                     $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //                     if ($piutang_customer < 0) {
    //                         $this->db->update('customers', ['piutang_customer' => 0], ['id' => $dt['id_customer']]);

    //                         $this->db->set('deposit_customer', "deposit_customer+" . abs($piutang_customer), false);
    //                         $this->db->where('id', $dt['id_customer']);
    //                         $this->db->update('customers');
    //                     } else {
    //                         $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //                     }
    //                 }
    //             }
    //         }

    //         $this->db->insert('penjualan_pembayaran_detail', $dt);
    //     }

    //     // $i = 1;
    //     // foreach ($data as $item) {
    //     //     $dt = [
    //     //         'no_transaksi' => "PJP" . date('Ymd', strtotime($item['tgl_bayar'])),
    //     //         'no_trxdetail' => "PJP" . date('Ymd', strtotime($item['tgl_bayar'])) . sprintf("%03d", $i++),
    //     //         'id_customer' => $item['id_customer'],
    //     //         'piutang' => (float) $item['piutang'],
    //     //         'bayar' => (float) $item['jml_bayar'],
    //     //         'deposit' => (float) $item['deposit'],
    //     //     ];

    //     //     if (!in_array($item['id_customer'], array_column($cs, 'id'))) {
    //     //         $c = $this->db->get_where('customers', ['id' => $item['id']])->row();
    //     //         $dt['piutang'] = $c->piutang_customer;
    //     //         $dt['deposit'] = $c->deposit_customer;
    //     //     }

    //     //     $this->db->insert('penjualan_pembayaran_detail', $dt);

    //     //     if ($dt['deposit'] && !$dt['bayar']) {
    //     //         $deposit_customer = $dt['deposit'] - $item['tagihan'];
    //     //         if ($deposit_customer < 0) {
    //     //             $this->db->update('customers', ['deposit_customer' => 0], ['id' => $dt['id_customer']]);

    //     //             $this->db->set('piutang_customer', "piutang_customer+" . abs($deposit_customer), false);
    //     //             $this->db->where('id', $dt['id_customer']);
    //     //             $this->db->update('customers');
    //     //         } else {
    //     //             $this->db->update('customers', ['deposit_customer' => $deposit_customer], ['id' => $dt['id_customer']]);
    //     //         }
    //     //     }

    //     //     if ($dt['deposit'] && $dt['bayar']) {
    //     //         $deposit_customer = $dt['deposit'] - $item['tagihan'] + $dt['bayar'];
    //     //         if ($deposit_customer < 0) {
    //     //             $this->db->update('customers', ['deposit_customer' => 0], ['id' => $dt['id_customer']]);

    //     //             $this->db->set('piutang_customer', "piutang_customer+" . abs($deposit_customer), false);
    //     //             $this->db->where('id', $dt['id_customer']);
    //     //             $this->db->update('customers');
    //     //         } else {
    //     //             $this->db->update('customers', ['deposit_customer' => $deposit_customer], ['id' => $dt['id_customer']]);
    //     //         }
    //     //     }

    //     //     if ($item['piutang_customer'] && !$dt['bayar']) {
    //     //         $piutang_customer = $item['piutang_customer'] + $item['tagihan'];
    //     //         if ($piutang_customer < 0) {
    //     //             $this->db->update('customers', ['piutang_customer' => 0], ['id' => $dt['id_customer']]);

    //     //             $this->db->set('deposit_customer', "deposit_customer+" . abs($piutang_customer), false);
    //     //             $this->db->where('id', $dt['id_customer']);
    //     //             $this->db->update('customers');
    //     //         } else {
    //     //             $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //     //         }
    //     //     }
    //     //     if ($item['piutang_customer'] && $dt['bayar']) {
    //     //         $piutang_customer = $item['piutang_customer'] + $item['tagihan'] - $dt['bayar'];
    //     //         $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //     //         if ($piutang_customer < 0) {
    //     //             $this->db->update('customers', ['piutang_customer' => 0], ['id' => $dt['id_customer']]);

    //     //             $this->db->set('deposit_customer', "deposit_customer+" . abs($piutang_customer), false);
    //     //             $this->db->where('id', $dt['id_customer']);
    //     //             $this->db->update('customers');
    //     //         } else {
    //     //             $this->db->update('customers', ['piutang_customer' => $piutang_customer], ['id' => $dt['id_customer']]);
    //     //         }
    //     //     }
    //     // }


    // }

    public function bayar()
    {
        $no_trans = $this->input->post('no_transaksi');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $id_customer = $this->input->post('id_customer');
        $piutang_customer = (float) $this->input->post('piutang_customer');
        $deposit_customer = (float) $this->input->post('deposit_customer');
        $jml_bayar = (float) $this->input->post('jml_bayar');

        $p_cust = $piutang_customer - $deposit_customer - $jml_bayar;
        $d_cust = $deposit_customer;
        if ($p_cust < 0) {
            $d_cust = abs($p_cust);
            $p_cust = 0;
        }

        $ckpbr = $this->db->get_where('penjualan_pembayaran', ['no_transaksi' => $no_trans])->row();
        if ($ckpbr) {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_piutang' => $ckpbr->total_piutang + $piutang_customer,
                'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
                'total_deposit' => $ckpbr->total_deposit + $deposit_customer,
            ];

            $this->db->update('penjualan_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

            $this->db->order_by('no_trxdetail', 'desc');
            $maxTrx = $this->db->get_where('penjualan_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
            $num = substr($maxTrx->no_trxdetail, 11);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PJP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
                'id_customer' => $id_customer,
                'piutang' => $piutang_customer,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_customer,
            ];

            $cktrxb = $this->db->get_where('penjualan_pembayaran_detail', ['no_transaksi' => $no_trans, 'id_customer' => $id_customer])->row();
            if ($cktrxb) {
                $data['no_trxdetail'] = $cktrxb->no_trxdetail;
                $this->db->update('penjualan_pembayaran_detail', $data, ['no_transaksi' => $no_trans, 'id_customer' => $id_customer]);
            } else {
                $this->db->insert('penjualan_pembayaran_detail', $data);
            }

            $dt_brok = [
                'piutang_customer' => $p_cust,
                'deposit_customer' => $d_cust,
            ];

            $this->db->update('customers', $dt_brok, ['id' => $id_customer]);
        } else {
            $dt_master = [
                'no_transaksi' => $no_trans,
                'tgl_bayar' => $tgl_bayar,
                'total_piutang' => $piutang_customer,
                'total_bayar' => $jml_bayar,
                'total_deposit' => $deposit_customer,
            ];

            $this->db->insert('penjualan_pembayaran', $dt_master);

            $data = [
                'no_transaksi' => $no_trans,
                'no_trxdetail' => "PJP" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
                'id_customer' => $id_customer,
                'piutang' => $piutang_customer,
                'bayar' => $jml_bayar,
                'deposit' => $deposit_customer,
            ];

            $this->db->insert('penjualan_pembayaran_detail', $data);

            $dt_brok = [
                'piutang_customer' => $p_cust,
                'deposit_customer' => $d_cust,
            ];

            $this->db->update('customers', $dt_brok, ['id' => $id_customer]);
        }
    }

    // public function bayar()
    // {
    //     $no_trans = $this->input->post('no_transaksi');
    //     $tgl_bayar = $this->input->post('tgl_bayar');
    //     $id_customer = $this->input->post('id_customer');
    //     $piutang_customer = $this->input->post('piutang_customer');
    //     $deposit_customer = $this->input->post('deposit_customer');
    //     $jml_bayar = $this->input->post('jml_bayar');

    //     $h_cust = (float) $piutang_customer - (float) $jml_bayar;

    //     $d_cust = $deposit_customer;
    //     if ($h_cust < 0) {
    //         $d_cust = abs($h_cust);
    //         $h_cust = 0;
    //     }

    //     $ckpbr = $this->db->get_where('penjualan_pembayaran', ['no_transaksi' => $no_trans])->row();
    //     if ($ckpbr) {
    //         $dt_master = [
    //             'no_transaksi' => $no_trans,
    //             'tgl_bayar' => $tgl_bayar,
    //             'total_piutang' => $ckpbr->total_piutang + $piutang_customer,
    //             'total_bayar' => $ckpbr->total_bayar + $jml_bayar,
    //             'total_deposit' => $ckpbr->total_deposit + $deposit_customer,
    //         ];

    //         $this->db->update('penjualan_pembayaran', $dt_master, ['no_transaksi' => $no_trans]);

    //         $this->db->order_by('no_trxdetail', 'desc');
    //         $maxTrx = $this->db->get_where('penjualan_pembayaran_detail', ['no_transaksi' => $no_trans])->row();
    //         $num = substr($maxTrx->no_trxdetail, 11);

    //         $data = [
    //             'no_transaksi' => $no_trans,
    //             'no_trxdetail' => "PJP" . date('Ymd', strtotime($this->input->post('tgl_bayar'))) . sprintf("%03d", (int) $num + 1),
    //             'id_customer' => $id_customer,
    //             'piutang' => (float) $piutang_customer,
    //             'bayar' => (float) $jml_bayar,
    //             'deposit' => (float) $deposit_customer,
    //         ];

    //         $cktrxb = $this->db->get_where('penjualan_pembayaran_detail', ['no_transaksi' => $no_trans, 'id_customer' => $id_customer])->row();
    //         if ($cktrxb) {
    //             $data['no_trxdetail'] = $cktrxb->no_trxdetail;
    //             $this->db->update('penjualan_pembayaran_detail', $data, ['no_transaksi' => $no_trans, 'id_customer' => $id_customer]);
    //         } else {
    //             $this->db->insert('penjualan_pembayaran_detail', $data);
    //         }

    //         $dt_cust = [
    //             'piutang_customer' => (float) $h_cust,
    //             'deposit_customer' => (float) $d_cust,
    //         ];

    //         $this->db->update('customers', $dt_cust, ['id' => $id_customer]);
    //     } else {
    //         $dt_master = [
    //             'no_transaksi' => $no_trans,
    //             'tgl_bayar' => $tgl_bayar,
    //             'total_piutang' => $piutang_customer,
    //             'total_bayar' => $jml_bayar,
    //             'total_deposit' => $deposit_customer,
    //         ];

    //         $this->db->insert('penjualan_pembayaran', $dt_master);

    //         $data = [
    //             'no_transaksi' => $no_trans,
    //             'no_trxdetail' => "PJP" . date('Ymd', strtotime($tgl_bayar)) . sprintf("%03d", (int) 1),
    //             'id_customer' => $id_customer,
    //             'piutang' => (float) $piutang_customer,
    //             'bayar' => (float) $jml_bayar,
    //             'deposit' => (float) $deposit_customer,
    //         ];

    //         $this->db->insert('penjualan_pembayaran_detail', $data);

    //         $dt_cust = [
    //             'piutang_customer' => (float) $h_cust,
    //             'deposit_customer' => (float) $d_cust,
    //         ];

    //         $this->db->update('customers', $dt_cust, ['id' => $id_customer]);
    //     }
    // }
}
