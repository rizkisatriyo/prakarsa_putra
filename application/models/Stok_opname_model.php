<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_opname_model extends CI_Model
{
    public function all()
    {
        // $this->db->order_by('status');
        $this->db->order_by('tanggal', 'desc');
        $this->db->join('kandang', 'kandang.id = stok_opname.id_kandang');
        return $this->db->get('stok_opname')->result_array();
    }

    // public function find($no_transaksi)
    // {
    //     return $this->db->get_where('stok_opname', ['no_transaksi_penerimaan' => $no_transaksi])->row();
    // }
}
