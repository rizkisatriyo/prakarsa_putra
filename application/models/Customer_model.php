<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        return $this->db->get('customers')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('customers', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'telefon' => $this->input->post('telefon'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
        ];

        $this->db->insert('customers', $data);
    }

    public function ubah($id)
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'telefon' => $this->input->post('telefon'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
        ];

        $this->db->update('customers', $data, ['id' => $id]);
    }

    public function hapus($id)
    {
        return $this->db->delete('customers', ['id' => $id]);
    }
}
