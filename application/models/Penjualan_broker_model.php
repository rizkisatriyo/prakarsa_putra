<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_broker_model extends CI_Model
{
    public function all()
    {
        // $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_jual', 'desc');
        return $this->db->get('penjualan_broker')->result_array();
    }
    

    public function generate_trans_no()
    {
        return "PJB" . date('Ymd');
    }

    public function detail($no_transaksi)
    {
        // $this->db->order_by('nama');
        $this->db->join('broker', 'broker.id = penjualan_broker_detail.id_broker', 'left');
        return $this->db->get_where('penjualan_broker_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('penjualan_broker', ['no_transaksi' => $no_transaksi])->row();
    }

    public function find_tersedia_kandang($tanggal_jual)
    {
        return $this->db->get_where('tersedia_stok', ['tanggal' => $tanggal_jual])->row();
    }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'tgl_jual' => $this->input->post('tgl_jual'),
            // 'total_ekor' => $this->input->post('ekor'),
            'total_kg' => $this->input->post('kg'),
            'harga' => $this->input->post('harga'),
            'jumlah' => $this->input->post('total_piutang')
        ];

        $this->db->insert('penjualan_broker', $data);
    }

    // public function tambah_detail()
    // {
    //     $data = [
    //         'no_transaksi' => $this->input->post('no_transaksi'),
    //         'id_broker' => $this->input->post('id_broker'),
    //         'hrg_ayam' => $this->input->post('hrg_ayam'),
    //         'ekor' => $this->input->post('ekor'),
    //         'kg' => $this->input->post('kg'),
    //         'subtotal' => $this->input->post('subtotal'),
    //     ];

    //     $this->db->insert('penjualan_broker_detail', $data);
    // }

    public function tambah_detail()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_transaksi' => $data[$i]['no_transaksi'],
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_broker' => $data[$i]['id_broker'],
                'harga' => $data[$i]['hrg_ayam'],
                // 'ekor' => $data[$i]['ekor'],
                'kg' => $data[$i]['kg'],
                'jumlah' => $data[$i]['subtotal'],
            ];

            $this->db->insert('penjualan_broker_detail', $dt);
        }
    }

    public function tambah_detail_supplier()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_supplier' => $data[$i]['id_supplier'],
            ];

            $this->db->insert('penjualan_broker_detail_supplier', $dt);
        }
    }
}
