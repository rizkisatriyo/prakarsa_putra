<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Susut_pembelian_model extends CI_Model
{
    public function all()
    {
        $this->db->join('penerimaan', 'penerimaan.no_transaksi = penerimaan_susut.no_transaksi_penerimaan', 'left');
        $this->db->order_by('no_transaksi_penerimaan', 'desc');
        // $this->db->order_by('tgl_diterima', 'desc');
        return $this->db->get('penerimaan_susut')->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('penerimaan_susut', ['no_transaksi_penerimaan' => $no_transaksi])->row();
    }
}
