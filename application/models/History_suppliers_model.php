<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_suppliers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('pembelian_pembayaran', 'pembelian_pembayaran.no_transaksi = pembelian_pembayaran_detail.no_transaksi', 'left');
        $this->db->join('suppliers', 'suppliers.id = pembelian_pembayaran_detail.id_supplier', 'left');
        return $this->db->get('pembelian_pembayaran_detail')->result_array();
    }

    public function find($id)
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('pembelian_pembayaran', 'pembelian_pembayaran.no_transaksi = pembelian_pembayaran_detail.no_transaksi', 'left');
        $hasil = $this->db->get_where('pembelian_pembayaran_detail', ['id_supplier' => $id])->result_array();
        print_r($hasil);
        return $hasil;

    }
}
