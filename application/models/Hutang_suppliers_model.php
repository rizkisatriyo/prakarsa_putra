<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang_suppliers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        // $this->db->where('hutang_supplier !=',0);
        // $this->db->or_where('deposit_supplier !=',0);
        return $this->db->get('suppliers')->result_array();
    }
}
