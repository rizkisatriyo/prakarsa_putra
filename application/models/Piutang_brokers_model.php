<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_brokers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        // $this->db->where('piutang_broker !=',0);
        // $this->db->or_where('deposit_broker !=',0);
        return $this->db->get('broker')->result_array();
    }
}
