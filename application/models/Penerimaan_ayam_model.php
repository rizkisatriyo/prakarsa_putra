<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan_ayam_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_diterima', 'desc');
        return $this->db->get('penerimaan')->result_array();
    }

    public function generate_trans_no()
    {
        return "TRM" . date('Ymd');
    }

    public function generate_checker_no()
    {
        return "CKB" . date('Ymd');
    }

    public function tambah()
    {
        // INSERT PENERIMAAN
        $data_penerimaan = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'no_transaksi_pembelian' => $this->input->post('no_transaksi_pembelian'),
            'tgl_diterima' => $this->input->post('tgl_diterima'),
            'bongkar_ekor' => $this->input->post('bongkar_ekor'),
            'bongkar_kg' => $this->input->post('bongkar_kg'),
            'broker_kg' => $this->input->post('broker_kg'),
            'kirim_ekor' => $this->input->post('kirim_ekor'),
            'kirim_kg' => $this->input->post('kirim_kg'),
            'mati_ekor' => $this->input->post('mati_ekor'),
            'mati_kg' => $this->input->post('mati_kg'),
        ];
        $this->db->insert('penerimaan', $data_penerimaan);

        // UPDATE STATUS PEMBELIAN
        $this->db->set('status', 'Selesai');
        $this->db->where('no_transaksi', $this->input->post('no_transaksi_pembelian'));
        $this->db->update('pembelian');

        // INSERT PENERIMAAN CHECKER
        $data_penerimaan_checker = [
            'no_checker' => $this->input->post('no_checker'),
            'no_transaksi_penerimaan' => $this->input->post('no_transaksi'),
            'total_ekor' => $this->input->post('total_ekor'),
            'total_kg' => $this->input->post('total_kg'),
        ];
        $this->db->insert('penerimaan_checker', $data_penerimaan_checker);

        // INSERT DETAIL CHECKER
        $detail_checker = $this->input->post('detail_checker');
        $this->db->insert_batch('penerimaan_checker_detail', $detail_checker);

        // INSERT SUSUT
        $data_penerimaan_susut = [
            'no_transaksi_penerimaan' => $this->input->post('no_transaksi_penerimaan'),
            'ekor' => $this->input->post('ekor'),
            // 'presentase_ekor' => $this->input->post('presentase_ekor'),
            'kg' => $this->input->post('kg'),
            'presentase_kg' => $this->input->post('presentase_kg'),
        ];
        $this->db->insert('penerimaan_susut', $data_penerimaan_susut);
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('penerimaan', ['no_transaksi' => $no_transaksi])->row();
    }

    public function find_checker($no_transaksi_penerimaan)
    {
        return $this->db->get_where('penerimaan_checker', ['no_transaksi_penerimaan' => $no_transaksi_penerimaan])->row();
    }

    public function find_penerimaan_checker_detail($no_checker)
    {
        $this->db->join('kandang', 'kandang.id = penerimaan_checker_detail.id_kandang', 'left');
        return $this->db->get_where('penerimaan_checker_detail', ['no_checker' => $no_checker])->result_array();
    }
}
