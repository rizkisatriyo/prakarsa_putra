<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('nama');
        return $this->db->get('suppliers')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('suppliers', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'telefon' => $this->input->post('telefon'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
            // 'no_akun_utang' => $this->input->post('no_akun_utang')
        ];

        $this->db->insert('suppliers', $data);
    }

    public function ubah($id)
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'telefon' => $this->input->post('telefon'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
            // 'no_akun_utang' => $this->input->post('no_akun_utang')
        ];

        $this->db->update('suppliers', $data, ['id' => $id]);
    }

    public function hapus($id)
    {
        return $this->db->delete('suppliers', ['id' => $id]);
    }
}
