<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_broker_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('status');
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tgl_beli', 'desc');
        return $this->db->get('pembelian_broker')->result_array();
    }

    // public function generate_trans_no()
    // {
    //     // Ambil nomor transaksi terakhir dari database
    //     $lastTransNo = $this->db->select('id')->order_by('id', 'DESC')->limit(1)->get('pembelian_broker')->row('id');

    //     // Jika tidak ada nomor transaksi sebelumnya atau bukan numeric, mulai dari 1
    //     $nextTransNo = (is_numeric($lastTransNo) ? $lastTransNo : 0) + 1;

    //     // Gunakan nomor transaksi yang telah diubah untuk transaksi baru
    //     return "PBN" . date('Ymd') . str_pad($nextTransNo, 4, '0', STR_PAD_LEFT);
    // }

    public function generate_trans_no()
    {
        // Ambil nomor transaksi terakhir dan tanggal terakhir dari database
        $lastTransNoData = $this->db
            ->select('id, created_on')
            ->order_by('created_on', 'DESC')
            ->limit(1)
            ->get('pembelian_broker')
            ->row();

        // Jika tidak ada nomor transaksi sebelumnya atau tanggal terakhir berbeda, mulai dari 1
        if (!$lastTransNoData || date('Y-m-d') > $lastTransNoData->created_on) {
            $nextTransNo = 1;
        } else {
            // Jika tanggal sama, lanjutkan nomor transaksi dari nilai terakhir
            $nextTransNo = $lastTransNoData->id + 1;
        }

        // Gunakan nomor transaksi yang telah diubah untuk transaksi baru
        $newTransNo = "PBN" . date('Ymd') . str_pad($nextTransNo, 2, '0', STR_PAD_LEFT);

        return $newTransNo;
    }


    public function detail($no_transaksi)
    {
        $this->db->order_by('nama');
        $this->db->join('suppliers', 'suppliers.id = pembelian_broker_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_broker_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('pembelian_broker', ['no_transaksi' => $no_transaksi])->row();
    }

    public function sisa_kandang()
    {
        return $this->db->get_where('stok', [])->row();
        return $this->db->get_where('stok', [])->row();

    }
    public function find_sisa_kandang($tanggal)
    {
        return $this->db->get_where('stok', [])->row();
        return $this->db->get_where('sisa_stok', ['tanggal' => $tanggal])->row();

    }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'tgl_beli' => $this->input->post('tgl_beli'),
            'kg' => $this->input->post('kg'),
            'harga' => $this->input->post('harga'),
            'total_hutang' => $this->input->post('total_hutang')
        ];
        // print_r($data);
        $this->db->insert('pembelian_broker', $data);
    }

    public function tambah_detail()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_transaksi' => $data[$i]['no_transaksi'],
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_supplier' => $data[$i]['id_supplier'],
                'hrg_ayam' => $data[$i]['hrg_ayam'],
                'kg' => $data[$i]['kg'],
                'subtotal' => $data[$i]['subtotal'],
            ];

            $this->db->insert('pembelian_broker_detail', $dt);
        }
    }
}
