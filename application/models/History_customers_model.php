<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_customers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('penjualan_pembayaran', 'penjualan_pembayaran.no_transaksi = penjualan_pembayaran_detail.no_transaksi', 'left');
        $this->db->join('customers', 'customers.id = penjualan_pembayaran_detail.id_customer', 'left');
        return $this->db->get('penjualan_pembayaran_detail')->result_array();
    }

    public function find($id)
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('penjualan_pembayaran', 'penjualan_pembayaran.no_transaksi = penjualan_pembayaran_detail.no_transaksi', 'left');
        return $this->db->get_where('penjualan_pembayaran_detail', ['id_customer' => $id])->result_array();
    }
}
