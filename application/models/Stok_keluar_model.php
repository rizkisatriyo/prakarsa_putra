<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_keluar_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_checker', 'desc');
        $this->db->order_by('no_transaksi_penjualan', 'desc');
        $this->db->order_by('tgl_check', 'desc');
        return $this->db->get('penjualan_checker')->result_array();
        // $this->db->order_by('tanggal_keluar', 'desc');
        // $this->db->join('kandang', 'kandang.id = stok_keluar.id_kandang');
        // return $this->db->get('stok_keluar')->result_array();
    }
}
