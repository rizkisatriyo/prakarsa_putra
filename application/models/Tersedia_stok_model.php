<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tersedia_stok_model extends CI_Model
{
    public function latest()
    {
        // $this->db->order_by('tanggal', 'desc');
        return $this->db->get('stok')->row();
    }
    // public function latest()
    // {
    //     $this->db->order_by('tanggal', 'desc');
    //     return $this->db->get('tersedia_stok')->row();
    // }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'no_penerimaan_checker' => $this->input->post('no_checker'),
            'tanggal' => $this->input->post('tanggal'),
            'ekor' => $this->input->post('ekor'),
            'kg' => $this->input->post('kg'),
            'harga' => $this->input->post('harga'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('tersedia_stok', $data);
    }
}
