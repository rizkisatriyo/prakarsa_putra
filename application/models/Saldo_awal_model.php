<?php
defined('BASEPATH') or exit('No direct script access allowed');

class saldo_awal_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_transaksi', 'desc');
        $this->db->order_by('tanggal', 'desc');
        return $this->db->get('saldo_awal')->result_array();
    }

    public function generate_trans_no()
    {
        return "SAL" . date('Ymd');
    }

    public function find($no_transaksi)
    {
        return $this->db->get_where('saldo_awal', ['no_transaksi' => $no_transaksi])->row();
    }

    public function find_detail($no_transaksi)
    {
        $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        return $this->db->get_where('saldo_awal_detail', ['saldo_awal.no_transaksi' => $no_transaksi])->result_array();
    }

    public function tambah()
    {
        $data = [
            'no_transaksi' => $this->input->post('no_transaksi'),
            'tanggal' => $this->input->post('tanggal'),
            'total' => round($this->input->post('total')),
        ];

        $this->db->insert('saldo_awal', $data);
    }

    public function tambah_detail()
    {
        $data = $this->input->post('data');

        for ($i = 0; $i < count($data); $i++) {
            $dt = [
                'no_transaksi' => $data[$i]['no_transaksi'],
                'no_trxdetail' => $data[$i]['no_trxdetail'],
                'id_pengeluaran_akun_detail' => $data[$i]['id_pengeluaran_akun_detail'],
                'keterangan' => $data[$i]['keterangan'],
                'bayar' => $data[$i]['jml_bayar'],
            ];

            $this->db->insert('saldo_awal_detail', $dt);
        }
    }
}
