<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_hr_model extends CI_Model
{
    // public function all()
    // {
    //     return $this->db->get('pembelian')->result_array();
    // }

    public function find_pembelian($tgl_pembelian)
    {
        return $this->db->get_where('pembelian', ['tgl_beli' => $tgl_pembelian])->row();
    }

    public function find_pembelian_detail($no_transaksi)
    {
        $this->db->join('suppliers', 'suppliers.id = pembelian_detail.id_supplier', 'left');
        return $this->db->get_where('pembelian_detail', ['no_transaksi' => $no_transaksi])->result_array();
    }

    public function find_penjualan_broker($tgl_penjualan_broker)
    {
        return $this->db->get_where('penjualan_broker', ['tgl_jual' => $tgl_penjualan_broker])->row();
    }

    public function find_penjualan_broker_detail($no_transaksi)
    {
        $this->db->select('*');
        $this->db->select('penjualan_broker_detail.id AS p_id');
        $this->db->join('broker', 'broker.id = penjualan_broker_detail.id_broker', 'left');
        // $this->db->join('penjualan_broker_detail_supplier', 'penjualan_broker_detail.id = penjualan_broker_detail_supplier.id_penjualan_broker_detail', 'left');
        return $this->db->get_where('penjualan_broker_detail', ['no_transaksi' => $no_transaksi, 'kg !=' => 0])->result_array();
    }

    public function find_penerimaan($tgl_penerimaan)
    {
        return $this->db->get_where('penerimaan', ['tgl_diterima' => $tgl_penerimaan])->row();
    }

    // public function find_penerimaan_detail($no_transaksi)
    // {
    //     $this->db->join('suppliers', 'suppliers.id = penerimaan_detail.id_supplier', 'left');
    //     return $this->db->get_where('penerimaan_detail', ['no_transaksi' => $no_transaksi])->result_array();
    // }

    public function find_checker_penerimaan($no_transaksi)
    {
        return $this->db->get_where('penerimaan_checker', ['no_transaksi_penerimaan' => $no_transaksi])->row();
    }

    public function find_checker_penerimaan_detail($no_checker)
    {
        $this->db->join('kandang', 'kandang.id = penerimaan_checker_detail.id_kandang', 'left');
        return $this->db->get_where('penerimaan_checker_detail', ['no_checker' => $no_checker])->result_array();
    }

    public function find_susut_penerimaan($no_transaksi)
    {
        return $this->db->get_where('penerimaan_susut', ['no_transaksi_penerimaan' => $no_transaksi])->row();
    }

    public function find_sisa_kandang($tanggal_kemarin)
    {
        return $this->db->get_where('sisa_stok', ['tanggal' => $tanggal_kemarin])->row();
    }

    // public function find_penjualan($tgl_penjualan)
    // {
    //     return $this->db->get_where('penjualan', ['tgl_jual' => $tgl_penjualan])->row();
    // }

    // public function find_penjualan_detail($no_transaksi)
    // {
    //     $this->db->join('customers', 'customers.id = penjualan_detail.id_customer', 'left');
    //     return $this->db->get_where('penjualan_detail', ['no_transaksi' => $no_transaksi])->result_array();
    // }

    public function find_checker_penjualan($tgl_penjualan)
    {
        return $this->db->get_where('penjualan_checker', ['tgl_check' => $tgl_penjualan])->row();
    }

    public function find_checker_penjualan_detail($no_checker)
    {
        $this->db->join('kandang', 'kandang.id = penjualan_checker_detail.id_kandang', 'left');
        $this->db->join('customers', 'customers.id = penjualan_checker_detail.id_customer', 'left');
        return $this->db->get_where('penjualan_checker_detail', ['no_checker' => $no_checker])->result_array();
    }

    public function find_susut_penjualan($no_checker)
    {
        return $this->db->get_where('penjualan_susut', ['no_checker' => $no_checker])->row();
    }

    // public function find_penjualan_broker($tgl_penjualan_broker)
    // {
    //     return $this->db->get_where('penjualan_broker', ['tgl_jual' => $tgl_penjualan_broker])->row();
    // }

    // public function find_penjualan_broker_detail($no_transaksi)
    // {
    //     $this->db->join('broker', 'broker.id = penjualan_broker_detail.id_broker', 'left');
    //     return $this->db->get_where('penjualan_broker_detail', ['no_transaksi' => $no_transaksi])->result_array();
    // }

    // public function find_sisa_stok($tanggal_penjualan)
    // {

    //     $this->db->select('*');
    //     $this->db->select('sisa_stok.harga AS harga_stok');
    //     $this->db->select('sisa_stok.jumlah AS jumlah_stok');
    //     $this->db->join('penjualan', 'penjualan.no_transaksi = sisa_stok.no_transaksi_penjualan', 'left');
    //     return $this->db->get_where('sisa_stok', ['penjualan.tgl_jual' => $tanggal_penjualan])->row();
    // }

    // public function find_pembayaran($tgl_pembayaran)
    // {
    //     return $this->db->get_where('pembelian_pembayaran', ['tgl_bayar' => $tgl_pembayaran])->row();
    // }

    // public function find_pembayaran_detail($no_transaksi)
    // {
    //     $this->db->join('suppliers', 'suppliers.id = pembelian_pembayaran_detail.id_supplier', 'left');
    //     return $this->db->get_where('pembelian_pembayaran_detail', ['no_transaksi' => $no_transaksi])->result_array();
    // }
}
