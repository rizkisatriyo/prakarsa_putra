<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_akun_detail_model extends CI_Model
{
    public function all()
    {
        $this->db->select('pengeluaran_akun_detail.*');
        $this->db->select('pengeluaran_akun.nama AS kategori');
        $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_akun_detail.id_pengeluaran_akun', 'left');
        return $this->db->get('pengeluaran_akun_detail')->result_array();
    }

    public function find($id)
    {
        return $this->db->get_where('pengeluaran_akun_detail', ['id' => $id])->row();
    }

    public function tambah()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->insert('pengeluaran_akun_detail', $data);
    }

    public function ubah($id)
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->update('pengeluaran_akun_detail', $data, ['id' => $id]);
    }

    public function hapus($id)
    {
        return $this->db->delete('pengeluaran_akun_detail', ['id' => $id]);
    }
}
