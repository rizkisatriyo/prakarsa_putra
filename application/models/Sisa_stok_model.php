<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sisa_stok_model extends CI_Model
{
    public function latest()
    {
        return $this->db->get('stok')->row();
    }
    // public function latest()
    // {
    //     $this->db->order_by('tanggal', 'desc');
    //     return $this->db->get('tersedia_stok')->row();
    // }


    // public function generate_trans_no()
    // {
    //     return "STK" . date('Ymd');
    // }

    public function tambah()
    {
        $data = [
            'no_transaksi' => "STK" . date('Ymd', strtotime($this->input->post('tanggal'))),
            'no_penjualan_checker' => $this->input->post('no_checker'),
            'tanggal' => $this->input->post('tanggal'),
            'ekor' => $this->input->post('ekor'),
            'kg' => $this->input->post('kg'),
            'harga' => $this->input->post('harga'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('sisa_stok', $data);
    }
}
