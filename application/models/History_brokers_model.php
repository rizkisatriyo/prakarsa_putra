<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_brokers_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.no_transaksi = penjualan_broker_pembayaran_detail.no_transaksi', 'left');
        $this->db->join('broker', 'broker.id = penjualan_broker_pembayaran_detail.id_broker', 'left');
        return $this->db->get('penjualan_broker_pembayaran_detail')->result_array();
    }

    public function find($id)
    {
        $this->db->order_by('no_trxdetail', 'desc');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.no_transaksi = penjualan_broker_pembayaran_detail.no_transaksi', 'left');
        return $this->db->get_where('penjualan_broker_pembayaran_detail', ['id_broker' => $id])->result_array();
    }
}
