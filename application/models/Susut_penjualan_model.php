<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Susut_penjualan_model extends CI_Model
{
    public function all()
    {
        $this->db->order_by('no_checker', 'desc');
        // $this->db->order_by('tgl_diterima', 'desc');
        return $this->db->get('penjualan_susut')->result_array();
    }

    public function find($no_checker)
    {
        return $this->db->get_where('penjualan_susut', ['no_checker' => $no_checker])->row();
    }
}
