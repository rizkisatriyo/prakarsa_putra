<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_brokers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'history_brokers.css',
            'js' => 'history_brokers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/history_pembayaran/history_brokers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['history_brokers'] = $this->history_brokers_model->all();

        $data['title'] = 'History Pembayaran broker';
        $this->loadView('index', $data);
    }

    public function detail($id)
    {
        $data['broker'] = $this->broker_model->find($id);
        $data['history'] = $this->history_brokers_model->find($id);

        $data['title'] = 'Detail History';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan history_brokers.php */
