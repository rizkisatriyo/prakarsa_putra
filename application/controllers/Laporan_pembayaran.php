<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_pembayaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_pembayaran.css',
            'js' => 'laporan_pembayaran.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/pembayaran/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pembayaran'] = $this->pembayaran_model->all();
        $data['suppliers'] = $this->supplier_model->all();

        $data['title'] = 'Laporan pembayaran';
        $this->loadView('index', $data);
    }

    public function detail($no_transaksi)
    {
        $data['pembayaran'] = $this->laporan_pembayaran_model->find($no_transaksi);

        $data['title'] = 'Detail pembayaran';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan Laporan_pembayaran.php */
