<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'penjualan.css',
            'js' => 'penjualan.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/penjualan/penjualan/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penjualan'] = $this->penjualan_model->all();

        $data['title'] = 'Data Penjualan';
        $this->loadView('index', $data);
    }

    public function tambah()
    {
        $data['trans_no'] = $this->penjualan_model->generate_trans_no();
        $data['customers'] = $this->customer_model->all();
        $data['tersedia_stok'] = $this->tersedia_stok_model->latest();

        $data['title'] = 'Tambah Penjualan';
        $this->loadView('tambah', $data);
    }

    public function Penjualan_detail()
    {
        $data['detail'] = $this->penjualan_model->detail($this->penjualan_model->generate_trans_no());

        $this->load->view('transaksi/penjualan/penjualan/penjualan_detail', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[penjualan.no_transaksi]');
        $this->form_validation->set_rules('tgl_jual', 'Tanggal Penjualan', 'required|is_unique[penjualan.tgl_jual]');
        // $this->form_validation->set_rules('ekor', 'Ekor', 'integer');
        // $this->form_validation->set_rules('kg', 'Kg', 'decimal');
        // $this->form_validation->set_rules('total_hutang', 'Total Hutang', 'decimal');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_jual' => form_error('tgl_jual')
            ]);
        } else {
            $this->penjualan_model->tambah();
        }
    }

    public function aksi_tambah_detail()
    {
        $this->penjualan_model->tambah_detail();
    }

    public function aksi_tambah_pembayaran()
    {
        $this->pembayaran_penjualan_model->tambah();
    }

    public function aksi_tambah_detail_pembayaran()
    {
        $this->pembayaran_penjualan_model->tambah_detail();
    }

    public function aksi_tambah_sisa_stok()
    {
        $this->sisa_stok_model->tambah();
    }

    // public function cek_customer($id_supp)
    // {
    //     $no_trans = $this->penjualan_model->generate_trans_no();

    //     $this->db->where('no_transaksi', $no_trans);
    //     $this->db->where('id_customer', $id_supp);
    //     $pemesanan_detail = $this->db->get('penjualan_detail')->num_rows();

    //     if ($pemesanan_detail < 1) {
    //         return TRUE;
    //     } else {
    //         $this->form_validation->set_message('cek_customer', 'Tidak dapat memilih {field} yang sama.');
    //         return FALSE;
    //     }
    // }

    // public function tambah_detail()
    // {
    //     $this->form_validation->set_rules('id_customer', 'customer', 'required|callback_cek_customer');
    //     $this->form_validation->set_rules('hrg_ayam', 'Harga Ayam', 'required|numeric');
    //     $this->form_validation->set_rules('jumlah', 'Jumlah Beli', 'required|numeric');
    //     $this->form_validation->set_rules('subtotal', 'Subtotal', 'required|numeric');

    //     if ($this->form_validation->run() == FALSE) {
    //         echo json_encode([
    //             'id_customer' => form_error('id_customer'),
    //             'hrg_ayam' => form_error('hrg_ayam'),
    //             'jumlah' => form_error('jumlah'),
    //             'subtotal' => form_error('subtotal'),
    //         ]);
    //     } else {
    //         $this->penjualan_model->tambah_detail();
    //     }
    // }

    public function detail($no_transaksi)
    {
        $penjualan = $this->penjualan_model->find($no_transaksi);
        $data['penjualan'] = $this->penjualan_model->find($no_transaksi);
        $data['detail'] = $this->penjualan_model->detail($no_transaksi);
        $data['tersedia_kandang'] = $this->penjualan_model->find_tersedia_kandang($penjualan->tgl_jual);

        $data['title'] = 'Detail Penjualan';
        $this->loadView('detail', $data);
    }

    // public function ubah($id)
    // {
    //     $data['penjualan'] = $this->penjualan_model->find($id);
    //     $data['pesanan'] = $this->penjualan_model->find_pesanan($id);

    //     $data['title'] = 'Ubah Pesanan';
    //     $this->loadView('ubah', $data);
    // }

    // public function aksi_ubah()
    // {
    // $id = $this->input->post('id');
    // $nama = $this->input->post('nama');
    // if ($nama == $this->penjualan_model->find($id)->nama) {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required');
    // } else {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[penjualan.nama]');
    // }

    // $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer|greater_than[0]');

    // if ($this->form_validation->run() == FALSE) {
    //     $this->session->set_flashdata('gagal', 'Data gagal diubah !');
    //     $this->ubah($id);
    // } else {
    //     $this->penjualan_model->ubah($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
    //     redirect('penjualan');
    // }
    // }

    // public function hapus($id)
    // {
    //     $this->penjualan_model->hapus($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
    //     redirect('penjualan');
    // }
}
        
    /* End of file  Pemesanan Penjualan.php */
