<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'profile.css',
            'js' => 'profile.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('profile/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function view($error = null)
    {
        $data['user'] = $this->profile_model->find();
        $data['current_user'] = $this->auth_model->current_user();
        $data['error'] = $error;

        $data['title'] = 'Profil Saya';
        $this->loadView('profile', $data);
    }

    public function index()
    {
        $this->view();
    }

    public function upload_avatar()
    {
        $data['current_user'] = $this->auth_model->current_user();

        // the user id contain dot, so we must remove it
        $file_name = str_replace('.', '', $data['current_user']->id);
        $config['upload_path']          = FCPATH . '/assets/dist/upload/img/avatar/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;
        $config['max_size']             = 4096; // 1MB
        $config['max_width']            = 1080;
        $config['max_height']           = 1080;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('avatar')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('gagal', $error);
            return $this->view($error);
        } else {
            // hapus file
            if ($data['current_user']->avatar) {
                $file_name = str_replace('.', '', $data['current_user']->id);
                array_map('unlink', glob(FCPATH . "/assets/dist/upload/img/avatar/$file_name.*"));
            }

            $uploaded_data = $this->upload->data();

            $new_data = [
                'id' => $data['current_user']->id,
                'avatar' => $uploaded_data['file_name'],
            ];

            if ($this->profile_model->update($new_data)) {
                $this->session->set_flashdata('sukses', 'Berhasil merubah avatar!');
                redirect('profile');
            }
        }
    }

    public function update()
    {
        $data['current_user'] = $this->auth_model->current_user();

        $rules = $this->profile_model->profile_rules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('gagal', 'Profil gagal diubah!');
            return $this->view($data);
        }

        $new_data = [
            'id' => $data['current_user']->id,
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
        ];

        if ($this->profile_model->update($new_data)) {
            $this->session->set_flashdata('sukses', 'Profil berhasil diubah!');
            redirect(site_url('profile'));
        }

        $this->view($data);
    }

    public function update_pw()
    {
        $data['current_user'] = $this->auth_model->current_user();

        $rules = $this->profile_model->password_rules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('gagal', 'Password gagal diubah!');
            return $this->view($data);
        }

        $new_password_data = [
            'id' => $data['current_user']->id,
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            // 'password_updated_at' => date("Y-m-d H:i:s"),
        ];

        if ($this->profile_model->update($new_password_data)) {
            $this->session->set_flashdata('sukses', 'Password berhasil diubah!');
            redirect(site_url('profile'));
        }

        $this->view($data);
    }
}
        
    /* End of file  profile.php */
