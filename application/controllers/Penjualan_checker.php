<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_checker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            'css' => 'penjualan_checker.css',
            'js' => 'penjualan_checker.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/penjualan/penjualan_checker/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penjualan_checker'] = $this->penjualan_checker_model->all();

        $data['title'] = 'Checker Penjualan';
        $this->loadView('index', $data);
    }

    public function tambah($no_transaksi_penjualan)
    {
        // $data['trans_no'] = $this->penjualan_checker_model->generate_trans_no();
        $data['no_checker'] = $this->penjualan_checker_model->generate_checker_no();
        $data['no_transaksi_penjualan'] = $no_transaksi_penjualan;
        $data['detail'] = $this->penjualan_model->detail($no_transaksi_penjualan);
        $data['penjualan'] = $this->penjualan_model->find($no_transaksi_penjualan);
        $data['kandang'] = $this->kandang_model->all();
        $data['t_stok'] = $this->tersedia_stok_model->latest();
        // $data['cs'] = $this->penjualan_checker_model->get_customers($no_transaksi_penjualan);

        $data['title'] = 'Input Checker';
        $this->loadView('tambah', $data);
    }

    public function get_customers($no_transaksi_penjualan)
    {
        $this->db->select('customers.id AS c_id');
        $this->db->select('nama');
        $this->db->join('customers', 'customers.id = penjualan_detail.id_customer');
        $this->db->order_by('nama');
        echo json_encode($this->db->get_where('penjualan_detail', ['no_transaksi' => $no_transaksi_penjualan])->result_array());
    }

    public function aksi_tambah()
    {
        // $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[penerimaan.no_transaksi]');
        $this->form_validation->set_rules('no_checker', 'No Checker', 'required|is_unique[penjualan_checker.no_checker]');
        $this->form_validation->set_rules('tgl_check', 'Tanggal Checker', 'required|is_unique[penjualan_checker.tgl_check]');
        $this->form_validation->set_rules('no_transaksi_penjualan', 'No Transaksi Penjualan', 'required');
        // $this->form_validation->set_rules('bongkar_ekor', 'Bongkar Ekor', 'required|integer');
        // $this->form_validation->set_rules('bongkar_kg', 'Bongkar Kg', 'required|decimal');
        $this->form_validation->set_rules('total_ekor', 'Total Ekor', 'required|integer');
        $this->form_validation->set_rules('total_kg', 'Total Kg', 'required|decimal');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                // 'no_transaksi' => form_error('no_transaksi'),
                'no_checker' => form_error('no_checker'),
                'tgl_check' => form_error('tgl_check'),
                'no_transaksi_penjualan' => form_error('no_transaksi_penjualan'),
                // 'bongkar_ekor' => form_error('bongkar_ekor'),
                // 'bongkar_kg' => form_error('bongkar_kg'),
                'total_ekor' => form_error('total_ekor'),
                'total_kg' => form_error('total_kg'),
            ]);
        } else {
            $this->penjualan_checker_model->tambah();
        }
    }

    public function aksi_tambah_sisa_stok()
    {
        $this->sisa_stok_model->tambah();
    }

    public function detail($no_checker)
    {
        $penjualan_checker = $this->penjualan_checker_model->find($no_checker);
        $data['penjualan_checker'] = $penjualan_checker;
        // $penjualan_checker = $this->penjualan_checker_model->find_checker($no_checker);
        // $data['penjualan_checker'] = $penjualan_checker;
        $data['penjualan'] = $this->penjualan_model->find($penjualan_checker->no_transaksi_penjualan);
        $data['penjualan_detail'] = $this->penjualan_model->detail($penjualan_checker->no_transaksi_penjualan);
        $data['kandang'] = $this->kandang_model->all();
        $data['penjualan_checker_detail'] = $this->penjualan_checker_model->find_penjualan_checker_detail($penjualan_checker->no_checker);
        $data['penerimaan_susut'] = $this->susut_penjualan_model->find($no_checker);

        $data['title'] = 'Detail Checker';
        $this->loadView('detail', $data);
    }

    public function ubah_tanggal()
    {
        $this->db->order_by('tanggal', 'desc');
        $q = $this->db->get_where('tersedia_stok', ['tanggal <' => $this->input->post('tgl')])->row();
        echo json_encode($q);
    }
}
        
    /* End of file  Penjualan_checker.php */
