<?php
defined('BASEPATH') or exit('No direct script access allowed');

class saldo_awal extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'saldo_awal.css',
            'js' => 'saldo_awal.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/saldo_awal/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['saldo_awal'] = $this->saldo_awal_model->all();

        $data['title'] = 'Saldo Awal';
        $this->loadView('index', $data);
    }

    public function perbarui_saldo()
    {
        $this->db->order_by('tanggal', 'desc');
        $saldo_awal = $this->db->get('saldo_awal')->row();
        
        $this->db->select('saldo_awal_detail.*');
        $this->db->select('pengeluaran_akun_detail.*');
        $this->db->select('pengeluaran_akun.nama AS kategori');
        $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_akun_detail.id_pengeluaran_akun', 'left');
        $this->db->join('pengeluaran_akun_detail', 'pengeluaran_akun_detail.id = saldo_awal_detail.id_pengeluaran_akun', 'left');
        $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        $saldo_awal_detail = $this->db->get_where('saldo_awal_detail', ['tanggal' => $saldo_awal->tanggal])->result_array();

        $data['saldo_awal'] = $saldo_awal;
        $data['saldo_awal_detail'] = $saldo_awal_detail;

        $data['trans_no'] = $this->saldo_awal_model->generate_trans_no();
        $data['pengeluaran_akun'] = $this->db->get('pengeluaran_akun')->result_array();

        $data['title'] = 'Perbarui Saldo';
        $this->loadView('tambah', $data);
    }

    public function tambah()
    {
        $data['trans_no'] = $this->saldo_awal_model->generate_trans_no();
        $data['pengeluaran_akun'] = $this->db->get('pengeluaran_akun')->result_array();

        $data['title'] = 'Perbarui Saldo';
        $this->loadView('perbarui_saldo', $data);
    }

    public function get_pengeluaran_akun_detail()
    {
        $id_pengeluaran_akun = $this->input->post('id_pengeluaran_akun');
        $pengeluaran_akun_detail = $this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $id_pengeluaran_akun])->result_array();
        echo json_encode($pengeluaran_akun_detail);
    }

    public function get_setoran()
    {
        $tanggal_inout = $this->input->post('tanggal');

        $this->db->order_by('tgl_bayar', 'desc');
        $data['setoran_rpa'] = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        $this->db->order_by('tgl_bayar', 'desc');
        $data['setoran_broker'] = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        echo json_encode($data);

        // $this->db->select('SUM(bayar) AS subtotal');
        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        // $this->db->join('pengeluaran_akun_detail', 'pengeluaran_akun_detail.id = saldo_awal_detail.id_pengeluaran_akun_detail', 'left');
        // $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_akun_detail.id_pengeluaran_akun', 'left');
        // $sub_kantor = $this->db->get_where('saldo_awal_detail', ['saldo_awal.tanggal <=' => $tanggal_inout, 'pengeluaran_akun.id' => 3])->row('subtotal');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        // $mob_pemeliharaan = $this->db->get_where('saldo_awal_detail', ['saldo_awal.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 6])->row('bayar');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        // $prod_plastik = $this->db->get_where('saldo_awal_detail', ['saldo_awal.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 11])->row('bayar');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        // $prod_pemeliharaan = $this->db->get_where('saldo_awal_detail', ['saldo_awal.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 12])->row('bayar');
        // $output_pokok = ($saldo_awal->total - ($sub_kantor + ($mob_pemeliharaan + $prod_plastik + $prod_pemeliharaan)));
        // $tot_setor_bank = ($uang_masuk_rpa->total_bayar + $uang_masuk_broker->total_bayar) - $output_pokok;

        // $this->db->order_by('tgl_bayar', 'desc');
        // $setoran_rpa = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        // $this->db->order_by('tgl_bayar', 'desc');
        // $setoran_broker = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        // // $setor_tabungan = $saldo_awal->total - $output_pokok;

        // // $setor_modal_rpa = $setoran_rpa - $saldo_awal->total;
        // $setor_modal_total = 53285200;
        // $sisa_setoran = $tot_setor_bank - $setor_tabungan - $setor_modal_total;
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[saldo_awal.no_transaksi]');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|is_unique[saldo_awal.tanggal]');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tanggal' => form_error('tanggal'),
            ]);
        } else {
            $this->saldo_awal_model->tambah();
        }
    }

    public function aksi_tambah_detail()
    {
        $this->saldo_awal_model->tambah_detail();
    }

    public function detail($no_transaksi)
    {
        $data['no_transaksi'] = $no_transaksi;
        $data['saldo_awal'] = $this->saldo_awal_model->find($no_transaksi);
        $data['saldo_awal_detail'] = $this->saldo_awal_model->find_detail($no_transaksi);

        $data['title'] = 'Detail Saldo';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file saldo_awal.php */
