<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_akun_detail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'pengeluaran_akun_detail.css',
            'js' => 'pengeluaran_akun_detail.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/pengeluaran_akun_detail/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pengeluaran_akun_detail'] = $this->pengeluaran_akun_detail_model->all();

        $data['title'] = 'Akun Pengeluaran';
        $this->loadView('index', $data);
    }

    public function tambah()
    {
        $data['pengeluaran_akun'] = $this->db->get('pengeluaran_akun')->result_array();

        $data['title'] = 'Tambah Akun';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[pengeluaran_akun_detail.nama]');
        $this->form_validation->set_rules('id_pengeluaran_akun', 'Kategori', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->pengeluaran_akun_detail_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('pengeluaran_akun_detail');
        }
    }

    public function ubah($id)
    {
        $data['pengeluaran_akun'] = $this->db->get('pengeluaran_akun')->result_array();
        $data['pengeluaran_akun_detail'] = $this->pengeluaran_akun_detail_model->find($id);

        $data['title'] = 'Ubah Akun';
        $this->loadView('ubah', $data);
    }

    public function aksi_ubah()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');

        $this->form_validation->set_rules('id_pengeluaran_akun', 'Kategori', 'required');

        if ($nama == $this->pengeluaran_akun_detail_model->find($id)->nama) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
        } else {
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[pengeluaran_akun_detail.nama]');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal diubah !');
            $this->ubah($id);
        } else {
            $this->pengeluaran_akun_detail_model->ubah($id);

            $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
            redirect('pengeluaran_akun_detail');
        }
    }

    public function hapus($id)
    {
        $this->pengeluaran_akun_detail_model->hapus($id);

        $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
        redirect('pengeluaran_akun_detail');
    }
}
        
    /* End of file  pengeluaran_akun_detail.php */
