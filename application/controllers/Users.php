<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            // $this->session->set_userdata('referred_from', '/';
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'users.css',
            'js' => 'users.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('users/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['users'] = $this->user_model->all();

        $data['title'] = 'Data Pengguna';
        $this->loadView('users', $data);
    }

    public function tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $data['title'] = 'Tambah Pengguna';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'numeric');
        $this->form_validation->set_rules('email', 'Email',  'valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->user_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('users');
        }
    }

    public function detail($id)
    {
        $data['user'] = $this->user_model->find($id);

        $data['title'] = 'Detail Pengguna';
        $this->loadView('detail', $data);
    }

    // public function ubah($id)
    // {
    //     $current_user = $this->auth_model->current_user();
    //     if ($current_user->role != 'Administrator') {
    //         return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
    //     };

    //     $data['user'] = $this->user_model->find($id);

    //     $data['title'] = 'Ubah Pengguna';
    //     $this->loadView('ubah', $data);
    // }

    // public function aksi_ubah()
    // {
    //     $current_user = $this->auth_model->current_user();
    //     if ($current_user->role != 'Administrator') {
    //         return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
    //     };

    //     $id = $this->input->post('id');
    //     $name = $this->input->post('name');
    //     if ($name == $this->user_model->find($id)->name) {
    //         $this->form_validation->set_rules('name', 'Name', 'required');
    //     } else {
    //         $this->form_validation->set_rules('name', 'Name', 'required|is_unique[users.name]');
    //     }

    //     $this->form_validation->set_rules('phone', 'phone', 'numeric');
    //     $this->form_validation->set_rules('email', 'email',  'valid_email');
    //     // $this->form_validation->set_rules('no_akun_utang', 'Nomor Akun Utang',  'required');

    //     if ($this->form_validation->run() == FALSE) {
    //         $this->session->set_flashdata('gagal', 'Data gagal diubah !');
    //         $this->ubah($id);
    //     } else {
    //         $this->user_model->ubah($id);

    //         $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
    //         redirect('users');
    //     }
    // }

    // public function hapus($id)
    // {
    //     $current_user = $this->auth_model->current_user();
    //     if ($current_user->role != 'Administrator') {
    //         return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
    //     };

    //     $this->user_model->hapus($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
    //     redirect('users');
    // }
}
        
    /* End of file  users.php */
