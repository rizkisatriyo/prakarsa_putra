<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uang_masuk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'uang_masuk.css',
            'js' => 'uang_masuk.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/inout/uang_masuk/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $this->db->select('penjualan_pembayaran.no_transaksi AS no_trans_rpa');
        $this->db->select('penjualan_broker_pembayaran.no_transaksi AS no_trans_broker');
        $this->db->select('penjualan_pembayaran.tgl_bayar');
        $this->db->select('penjualan_pembayaran.total_bayar AS tot_byr_rpa');
        $this->db->select('penjualan_broker_pembayaran.total_bayar AS tot_byr_broker');
        $this->db->order_by('tgl_bayar', 'desc');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.tgl_bayar = penjualan_pembayaran.tgl_bayar', 'left');
        $uang_masuk = $this->db->get('penjualan_pembayaran')->result_array();
        $data['uang_masuk'] = $uang_masuk;

        $data['title'] = 'Uang Masuk';
        $this->loadView('index', $data);
    }

    public function detail($tgl)
    {
        $rpa = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar' => $tgl])->row();

        $this->db->join('customers', 'customers.id = penjualan_pembayaran_detail.id_customer', 'left');
        $this->db->join('penjualan_pembayaran', 'penjualan_pembayaran.no_transaksi = penjualan_pembayaran_detail.no_transaksi', 'left');
        $rpa_detail = $this->db->get_where('penjualan_pembayaran_detail', ['tgl_bayar' => $tgl, 'bayar !=' => 0])->result_array();

        $broker = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar' => $tgl])->row();

        $this->db->join('broker', 'broker.id = penjualan_broker_pembayaran_detail.id_broker', 'left');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.no_transaksi = penjualan_broker_pembayaran_detail.no_transaksi', 'left');
        $broker_detail = $this->db->get_where('penjualan_broker_pembayaran_detail', ['tgl_bayar' => $tgl, 'bayar !=' => 0])->result_array();

        $this->db->select('penjualan_pembayaran.no_transaksi AS no_trans_rpa');
        $this->db->select('penjualan_broker_pembayaran.no_transaksi AS no_trans_broker');
        $this->db->select('penjualan_pembayaran.tgl_bayar');
        $this->db->select('penjualan_pembayaran.total_bayar AS tot_byr_rpa');
        $this->db->select('penjualan_broker_pembayaran.total_bayar AS tot_byr_broker');
        $this->db->order_by('tgl_bayar', 'desc');
        $this->db->join('penjualan_broker_pembayaran', 'penjualan_broker_pembayaran.tgl_bayar = penjualan_pembayaran.tgl_bayar', 'left');
        $uang_masuk = $this->db->get_where('penjualan_pembayaran', ['penjualan_pembayaran.tgl_bayar' => $tgl])->row();

        $data['rpa'] = $rpa;
        $data['rpa_detail'] = $rpa_detail;
        $data['broker'] = $broker;
        $data['broker_detail'] = $broker_detail;
        $data['uang_masuk'] = $uang_masuk;

        $data['title'] = 'Detail Uang Masuk';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file Uang_masuk.php */
