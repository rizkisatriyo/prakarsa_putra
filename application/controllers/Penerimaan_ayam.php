<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan_ayam extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            'css' => 'penerimaan_ayam.css',
            'js' => 'penerimaan_ayam.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/pembelian/penerimaan_ayam/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penerimaan_ayam'] = $this->penerimaan_ayam_model->all();

        $data['title'] = 'Penerimaan Ayam';
        $this->loadView('index', $data);
    }

    public function tambah($no_transaksi_pembelian)
    {
        $data['trans_no'] = $this->penerimaan_ayam_model->generate_trans_no();
        $data['no_checker'] = $this->penerimaan_ayam_model->generate_checker_no();
        $data['no_transaksi_pembelian'] = $no_transaksi_pembelian;
        $data['detail'] = $this->pembelian_model->detail($no_transaksi_pembelian);
        $data['pembelian'] = $this->pembelian_model->find($no_transaksi_pembelian);
        $data['kandang'] = $this->kandang_model->all();
        $data['brokers'] = $this->broker_model->all();
        $pembelian = $this->pembelian_model->find($no_transaksi_pembelian);
        $sisaku = $this->pembelian_model->sisa_kandang($pembelian->tgl_beli);
        print_r($sisaku);
        $data['sisa_kandang'] = $sisaku;

        $this->db->join('suppliers', 'suppliers.id = pembelian_detail.id_supplier', 'left');
        $data['suppliers'] = $this->db->get_where('pembelian_detail', ['no_transaksi' => $no_transaksi_pembelian])->result_array();

        $data['title'] = 'Tambah Penerimaan';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[penerimaan.no_transaksi]');
        $this->form_validation->set_rules('no_transaksi_pembelian', 'No Transaksi Pembelian', 'required');
        $this->form_validation->set_rules('tgl_diterima', 'Tanggal Penerimaan', 'required|is_unique[penerimaan.tgl_diterima]');
        // $this->form_validation->set_rules('bongkar_ekor', 'Bongkar Ekor', 'required|integer');
        // $this->form_validation->set_rules('bongkar_kg', 'Bongkar Kg', 'required|decimal');
        $this->form_validation->set_rules('no_checker', 'No Checker', 'required|is_unique[penerimaan_checker.no_checker]');
        $this->form_validation->set_rules('total_ekor', 'Total Ekor', 'required|integer');
        $this->form_validation->set_rules('total_kg', 'Total Kg', 'required|decimal');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'no_transaksi_pembelian' => form_error('no_transaksi_pembelian'),
                'tgl_diterima' => form_error('tgl_diterima'),
                // 'bongkar_ekor' => form_error('bongkar_ekor'),
                // 'bongkar_kg' => form_error('bongkar_kg'),
                'no_checker' => form_error('no_checker'),
                'total_ekor' => form_error('total_ekor'),
                'total_kg' => form_error('total_kg'),
            ]);
        } else {
            $this->penerimaan_ayam_model->tambah();
        }
    }

    public function aksi_tambah_tersedia_stok()
    {
        $this->tersedia_stok_model->tambah();
    }

    public function detail($no_transaksi)
    {
        $penerimaan_ayam = $this->penerimaan_ayam_model->find($no_transaksi);
        $data['penerimaan_ayam'] = $penerimaan_ayam;
        $penerimaan_checker = $this->penerimaan_ayam_model->find_checker($no_transaksi);
        $data['penerimaan_checker'] = $penerimaan_checker;
        $data['pembelian'] = $this->pembelian_model->find($penerimaan_ayam->no_transaksi_pembelian);
        $data['pembelian_detail'] = $this->pembelian_model->detail($penerimaan_ayam->no_transaksi_pembelian);
        $data['kandang'] = $this->kandang_model->all();
        $data['penerimaan_checker_detail'] = $this->penerimaan_ayam_model->find_penerimaan_checker_detail($penerimaan_checker->no_checker);
        $data['penerimaan_susut'] = $this->susut_pembelian_model->find($no_transaksi);
        $data['sisa_kandang'] = $this->pembelian_model->sisa_kandang();

        $data['title'] = 'Detail Penerimaan';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  penerimaan_ayam.php */
