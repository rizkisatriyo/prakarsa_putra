<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran_akun extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'pengeluaran_akun.css',
            'js' => 'pengeluaran_akun.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/pengeluaran_akun/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pengeluaran_akun'] = $this->pengeluaran_akun_model->all();

        $data['title'] = 'Kategori Pengeluaran';
        $this->loadView('index', $data);
    }

    public function tambah()
    {
        $data['title'] = 'Tambah Kategori';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[pengeluaran_akun.nama]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->pengeluaran_akun_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('pengeluaran_akun');
        }
    }

    public function detail($id)
    {
        $data['pengeluaran_akun'] = $this->pengeluaran_akun_model->find($id);
        $data['pengeluaran_akun_detail'] = $this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $id])->result_array();

        $data['title'] = 'Detail Kategori';
        $this->loadView('detail', $data);
    }

    public function ubah($id)
    {
        $data['pengeluaran_akun'] = $this->pengeluaran_akun_model->find($id);

        $data['title'] = 'Ubah Kategori';
        $this->loadView('ubah', $data);
    }

    public function aksi_ubah()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        if ($nama == $this->pengeluaran_akun_model->find($id)->nama) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
        } else {
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[pengeluaran_akun.nama]');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal diubah !');
            $this->ubah($id);
        } else {
            $this->pengeluaran_akun_model->ubah($id);

            $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
            redirect('pengeluaran_akun');
        }
    }

    public function hapus($id)
    {
        $this->pengeluaran_akun_model->hapus($id);

        $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
        redirect('pengeluaran_akun');
    }
}
        
    /* End of file  pengeluaran_akun.php */
