<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang_suppliers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'hutang_suppliers.css',
            'js' => 'hutang_suppliers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/hutang_piutang/hutang_suppliers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['trans_no'] = $this->pembayaran_pembelian_model->generate_trans_no();
        $data['hutang_suppliers'] = $this->hutang_suppliers_model->all();

        $data['title'] = 'Hutang Suppliers';
        $this->loadView('index', $data);
    }

    public function bayar()
    {
        $this->form_validation->set_rules('tgl_bayar', 'Tanggal Pembayaran', 'required');
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'required');
        $this->form_validation->set_rules('jml_bayar', 'Jumlah Bayar', 'required|integer');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_bayar' => form_error('tgl_bayar'),
                'id_supplier' => form_error('id_supplier'),
                'jml_bayar' => form_error('jml_bayar'),
            ]);
        } else {
            $this->pembayaran_pembelian_model->bayar();
        }
    }
}
        
    /* End of file  Pemesanan hutang_suppliers.php */
