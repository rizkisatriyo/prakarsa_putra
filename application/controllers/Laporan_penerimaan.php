<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_penerimaan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_penerimaan.css',
            'js' => 'laporan_penerimaan.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/penerimaan/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penerimaan'] = $this->penerimaan_ayam_model->all();
        $data['suppliers'] = $this->supplier_model->all();

        $data['title'] = 'Laporan Penerimaan';
        $this->loadView('index', $data);
    }
}
        
    /* End of file  Pemesanan Laporan_penerimaan.php */
