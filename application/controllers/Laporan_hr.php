<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_hr extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_hr.css',
            'js' => 'laporan_hr.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/hr/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        // $data['hr'] = $this->hr_model->all();
        // $data['suppliers'] = $this->supplier_model->all();

        $data['title'] = 'Laporan HR';
        $this->loadView('index', $data);
    }

    public function find()
    {
        $tanggal_hr = $this->input->get('tanggal_hr');
        $data['tanggal_hr'] = $tanggal_hr;
        $tanggal_pembelian = date('y-m-d', strtotime('-1 day', strtotime($tanggal_hr)));
        $data['tanggal_pembelian'] = $tanggal_pembelian;
        $tanggal_penerimaan = $tanggal_pembelian;
        $data['tanggal_penerimaan'] = $tanggal_penerimaan;
        $tanggal_checker_penerimaan = $tanggal_penerimaan;
        $data['tanggal_checker_penerimaan'] = $tanggal_checker_penerimaan;
        $tanggal_kemarin = $tanggal_checker_penerimaan;
        $data['tanggal_kemarin'] = $tanggal_kemarin;
        // $tanggal_penjualan = $tanggal_hr;
        // $data['tanggal_penjualan'] = $tanggal_penjualan;
        // $tanggal_penjualan_broker = $tanggal_hr;
        // $data['tanggal_penjualan_broker'] = $tanggal_penjualan_broker;

        $pembelian = $this->laporan_hr_model->find_pembelian($tanggal_pembelian);
        $data['pembelian'] = $pembelian;
        $data['pembelian_detail'] = $this->laporan_hr_model->find_pembelian_detail($pembelian->no_transaksi);

        $penjualan_broker = $this->laporan_hr_model->find_penjualan_broker($tanggal_hr);
        $data['penjualan_broker'] = $penjualan_broker;
        $data['penjualan_broker_detail'] = $this->laporan_hr_model->find_penjualan_broker_detail($penjualan_broker->no_transaksi);

        $penerimaan = $this->laporan_hr_model->find_penerimaan($tanggal_penerimaan);
        $data['penerimaan'] = $penerimaan;
        // $data['penerimaan_detail'] = $this->laporan_hr_model->find_penerimaan_detail($penerimaan->no_transaksi);

        $checker_penerimaan = $this->laporan_hr_model->find_checker_penerimaan($penerimaan->no_transaksi);
        $data['checker_penerimaan'] = $checker_penerimaan;
        $data['checker_penerimaan_detail'] = $this->laporan_hr_model->find_checker_penerimaan_detail($checker_penerimaan->no_checker);
        $data['kandang'] = $this->kandang_model->all();

        $data['susut_penerimaan'] = $this->laporan_hr_model->find_susut_penerimaan($penerimaan->no_transaksi);

        $data['sisa_kandang'] = $this->laporan_hr_model->find_sisa_kandang($tanggal_kemarin);

        $checker_penjualan = $this->laporan_hr_model->find_checker_penjualan($tanggal_hr);
        $data['checker_penjualan'] = $checker_penjualan;
        $data['checker_penjualan_detail'] = $this->laporan_hr_model->find_checker_penjualan_detail($checker_penjualan->no_checker);

        // $penjualan = $this->laporan_hr_model->find_penjualan($tanggal_hr);
        // $data['penjualan'] = $penjualan;
        $data['susut_penjualan'] = $this->laporan_hr_model->find_susut_penjualan($checker_penjualan->no_checker);

        // $penjualan = $this->laporan_hr_model->find_penjualan($tanggal_penjualan);
        // $data['penjualan'] = $penjualan;
        // $data['penjualan_detail'] = $this->laporan_hr_model->find_penjualan_detail($penjualan->no_transaksi);

        // $penjualan_broker = $this->laporan_hr_model->find_penjualan_broker($tanggal_penjualan_broker);
        // $data['penjualan_broker'] = $penjualan_broker;
        // $data['penjualan_broker_detail'] = $this->laporan_hr_model->find_penjualan_broker_detail($penjualan_broker->no_transaksi);

        // $sisa_stok = $this->laporan_hr_model->find_sisa_stok($tanggal_penjualan);
        // $data['sisa_stok'] = $sisa_stok;

        // $pembayaran = $this->laporan_hr_model->find_pembayaran($tanggal_hr);
        // $data['pembayaran'] = $pembayaran;
        // $data['pembayaran_detail'] = $this->laporan_hr_model->find_pembayaran_detail($pembayaran->no_transaksi);

        $data['title'] = 'Laporan HR ' . date('d M Y', strtotime($tanggal_hr));
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan laporan_hr.php */
