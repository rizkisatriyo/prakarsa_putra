<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kandang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'kandang.css',
            'js' => 'kandang.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('master_data/kandang/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['kandang'] = $this->kandang_model->all();

        $data['title'] = 'Data Kandang';
        $this->loadView('kandang', $data);
    }

    public function tambah()
    {
        $data['title'] = 'Tambah Kandang';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[kandang.nama]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->kandang_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('kandang');
        }
    }

    public function ubah($id)
    {
        $data['kandang'] = $this->kandang_model->find($id);

        $data['title'] = 'Ubah Kandang';
        $this->loadView('ubah', $data);
    }

    public function aksi_ubah()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        if ($nama == $this->kandang_model->find($id)->nama) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
        } else {
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[kandang.nama]');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal diubah !');
            $this->ubah($id);
        } else {
            $this->kandang_model->ubah($id);

            $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
            redirect('kandang');
        }
    }

    public function hapus($id)
    {
        $this->kandang_model->hapus($id);

        $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
        redirect('kandang');
    }
}
        
    /* End of file  kandang.php */
