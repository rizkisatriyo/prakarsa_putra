<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_brokers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'piutang_brokers.css',
            'js' => 'piutang_brokers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/hutang_piutang/piutang_brokers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['trans_no'] = $this->pembayaran_penjualan_broker_model->generate_trans_no();
        $data['piutang_brokers'] = $this->piutang_brokers_model->all();

        $data['title'] = 'Piutang brokers';
        $this->loadView('index', $data);
    }

    public function bayar()
    {
        $this->form_validation->set_rules('tgl_bayar', 'Tanggal Pembayaran', 'required');
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required');
        $this->form_validation->set_rules('id_broker', 'broker', 'required');
        $this->form_validation->set_rules('jml_bayar', 'Jumlah Bayar', 'required|integer');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_bayar' => form_error('tgl_bayar'),
                'id_broker' => form_error('id_broker'),
                'jml_bayar' => form_error('jml_bayar'),
            ]);
        } else {
            $this->pembayaran_penjualan_broker_model->bayar();
        }
    }
}
        
    /* End of file  Pemesanan piutang_brokers.php */
