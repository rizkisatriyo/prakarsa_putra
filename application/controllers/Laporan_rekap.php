<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_rekap extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_rekap.css',
            'js' => 'laporan_rekap.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/rekap/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['title'] = 'REKAP';
        $this->loadView('index', $data);
    }

    public function find()
    {
        $tanggal_rekap = $this->input->get('tanggal_rekap');
        $tanggal_pembelian = date('y-m-d', strtotime('-1 day', strtotime($tanggal_rekap)));
        $data['tanggal_rekap'] = $tanggal_rekap;
        $data['tanggal_kemarin'] = $tanggal_pembelian;
        $data['tanggal_pembelian'] = $tanggal_pembelian;
        $data['tanggal_penjualan'] = $tanggal_rekap;
        $data['tanggal_penjualan_broker'] = $tanggal_rekap;

        $pembelian = $this->laporan_rekap_model->find_pembelian($tanggal_pembelian);
        $data['pembelian'] = $pembelian;
        $data['pembelian_detail'] = $this->laporan_rekap_model->find_pembelian_detail($pembelian->no_transaksi);

        $pembelian_pembayaran = $this->laporan_rekap_model->find_pembelian_pembayaran($tanggal_pembelian);
        $data['pembelian_pembayaran'] = $pembelian_pembayaran;
        $data['pembelian_pembayaran_detail'] = $this->laporan_rekap_model->find_pembelian_pembayaran_detail($pembelian_pembayaran->no_transaksi);

        $sisa = $this->laporan_hr_model->find_sisa_kandang($tanggal_pembelian);
        $data['sisa'] = $sisa;

        $penjualan = $this->laporan_rekap_model->find_penjualan($tanggal_rekap);
        $data['penjualan'] = $penjualan;
        $data['penjualan_detail'] = $this->laporan_rekap_model->find_penjualan_detail($penjualan->no_transaksi);

        $penjualan_pembayaran = $this->laporan_rekap_model->find_penjualan_pembayaran($tanggal_rekap);
        $data['penjualan_pembayaran'] = $penjualan_pembayaran;
        $data['penjualan_pembayaran_detail'] = $this->laporan_rekap_model->find_penjualan_pembayaran_detail($penjualan_pembayaran->no_transaksi);

        $penjualan_broker = $this->laporan_rekap_model->find_penjualan_broker($tanggal_rekap);
        $data['penjualan_broker'] = $penjualan_broker;
        $data['penjualan_broker_detail'] = $this->laporan_rekap_model->find_penjualan_broker_detail($penjualan_broker->no_transaksi);
        
        $penjualan_broker_pembayaran = $this->laporan_rekap_model->find_penjualan_broker_pembayaran($tanggal_rekap);
        $data['penjualan_broker_pembayaran'] = $penjualan_broker_pembayaran;
        $data['penjualan_broker_pembayaran_detail'] = $this->laporan_rekap_model->find_penjualan_broker_pembayaran_detail($penjualan_broker_pembayaran->no_transaksi);

        $sisa_stok = $this->laporan_rekap_model->find_sisa_stok($tanggal_rekap);
        $data['sisa_stok'] = $sisa_stok;

        $hutang = $this->laporan_rekap_model->find_hutang($tanggal_rekap);
        $data['hutang'] = $hutang;
        $data['hutang_detail'] = $this->laporan_rekap_model->find_hutang_detail($hutang->no_transaksi);

        // if (!$pembelian && !$penjualan) {
        //     $this->session->set_flashdata('gagal', 'Rekap tidak ditemukan !');
        //     redirect('laporan_rekap');
        // } elseif (!$pembelian) {
        //     $this->session->set_flashdata('gagal', 'Rekap pembelian tidak ditemukan !');
        //     redirect('laporan_rekap');
        // } elseif (!$penjualan) {
        //     $this->session->set_flashdata('gagal', 'Rekap penjualan tidak ditemukan !');
        //     redirect('laporan_rekap');
        // } else {
            $data['title'] = 'REKAP ' . date('d M Y', strtotime($tanggal_rekap));
            $this->loadView('detail', $data);
        // }
    }
}
        
    /* End of file  Pemesanan laporan_rekap.php */
