<?php

class Auth extends CI_Controller
{
    public function index()
    {
        show_404();
    }

    public function login()
    {
        $rules = $this->auth_model->rules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            return $this->load->view('auth/login');
        }

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($this->auth_model->login($username, $password)) {
            if ($this->session->userdata('referred_from')) {
                $referred_from = $this->session->userdata('referred_from');
                redirect($referred_from, 'refresh');
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            $this->session->set_flashdata('message_login_error', 'Pastikan Username dan Password sesuai!');
        }

        $this->load->view('auth/login');
    }

    public function logout()
    {
        $this->auth_model->logout();
        redirect(site_url());
    }
}
