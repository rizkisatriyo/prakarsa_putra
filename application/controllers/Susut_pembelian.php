<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Susut_pembelian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'susut_pembelian.css',
            'js' => 'susut_pembelian.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/pembelian/susut_pembelian/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['susut_pembelian'] = $this->susut_pembelian_model->all();

        $data['title'] = 'Susut Pembelian';
        $this->loadView('index', $data);
    }

    public function detail($no_transaksi)
    {
        $data['susut_pembelian'] = $this->susut_pembelian_model->find($no_transaksi);
        // $data['detail'] = $this->susut_pembelian_model->find_detail($no_transaksi);

        $data['title'] = 'Detail Penyusutan';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan Rugi/Susud.php */
