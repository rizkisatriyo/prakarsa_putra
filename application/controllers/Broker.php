<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Broker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'broker.css',
            'js' => 'broker.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('master_data/kontak/broker/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['broker'] = $this->broker_model->all();

        $data['title'] = 'Data Broker';
        $this->loadView('broker', $data);
    }

    public function tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $data['title'] = 'Tambah Broker';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[broker.nama]');
        $this->form_validation->set_rules('singkatan', 'Singkatan', 'is_unique[broker.singkatan]|max_length[10]');
        $this->form_validation->set_rules('telefon', 'Telefon', 'numeric');
        $this->form_validation->set_rules('email', 'Email',  'valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->broker_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('broker');
        }
    }

    public function detail($id)
    {
        $data['broker'] = $this->broker_model->find($id);

        $data['title'] = 'Detail Broker';
        $this->loadView('detail', $data);
    }

    public function ubah($id)
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $data['broker'] = $this->broker_model->find($id);

        $data['title'] = 'Ubah Broker';
        $this->loadView('ubah', $data);
    }

    public function aksi_ubah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $singkatan = $this->input->post('singkatan');
        $fetch_broker = $this->broker_model->find($id);

        if ($nama == $fetch_broker->nama) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
        } else {
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[broker.nama]');
        }
        if ($singkatan == $fetch_broker->singkatan) {
            $this->form_validation->set_rules('singkatan', 'Singkatan', 'max_length[10]');
        } else {
            $this->form_validation->set_rules('singkatan', 'Singkatan', 'is_unique[broker.singkatan]|max_length[10]');
        }

        $this->form_validation->set_rules('telefon', 'telefon', 'numeric');
        $this->form_validation->set_rules('email', 'Email',  'valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal diubah !');
            $this->ubah($id);
        } else {
            $this->broker_model->ubah($id);

            $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
            redirect('broker');
        }
    }

    public function hapus($id)
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $this->broker_model->hapus($id);

        $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
        redirect('broker');
    }
}
        
    /* End of file  Broker.php */
