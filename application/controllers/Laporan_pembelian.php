<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_pembelian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_pembelian.css',
            'js' => 'laporan_pembelian.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/pembelian/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pembelian'] = $this->pembelian_model->all();
        $data['suppliers'] = $this->supplier_model->all();

        $data['title'] = 'Laporan Pembelian';
        $this->loadView('index', $data);
    }

    public function detail($no_transaksi)
    {
        $data['pembelian'] = $this->laporan_pembelian_model->find($no_transaksi);

        $data['title'] = 'Detail Pembelian';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan Laporan_pembelian.php */
