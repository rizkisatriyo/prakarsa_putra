<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_customers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'history_customers.css',
            'js' => 'history_customers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/history_pembayaran/history_customers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['history_customers'] = $this->history_customers_model->all();

        $data['title'] = 'History Pembayaran customer';
        $this->loadView('index', $data);
    }

    public function detail($id)
    {
        $data['customer'] = $this->customer_model->find($id);
        $data['history'] = $this->history_customers_model->find($id);

        $data['title'] = 'Detail History';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan history_customers.php */
