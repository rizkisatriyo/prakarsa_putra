<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_customers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'piutang_customers.css',
            'js' => 'piutang_customers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/hutang_piutang/piutang_customers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['trans_no'] = $this->pembayaran_penjualan_model->generate_trans_no();
        $data['piutang_customers'] = $this->piutang_customers_model->all();

        $data['title'] = 'Piutang Customers';
        $this->loadView('index', $data);
    }

    public function bayar()
    {
        $this->form_validation->set_rules('tgl_bayar', 'Tanggal Pembayaran', 'required');
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required');
        $this->form_validation->set_rules('id_customer', 'customer', 'required');
        $this->form_validation->set_rules('jml_bayar', 'Jumlah Bayar', 'required|integer');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_bayar' => form_error('tgl_bayar'),
                'id_customer' => form_error('id_customer'),
                'jml_bayar' => form_error('jml_bayar'),
            ]);
        } else {
            $this->pembayaran_penjualan_model->bayar();
        }
    }
}
        
    /* End of file  Pemesanan piutang_customers.php */
