<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Susut_penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'susut_penjualan.css',
            'js' => 'susut_penjualan.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/penjualan/susut_penjualan/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['susut_penjualan'] = $this->susut_penjualan_model->all();

        $data['title'] = 'Susut Penjualan';
        $this->loadView('index', $data);
    }

    public function detail($no_checker)
    {
        $data['susut_penjualan'] = $this->susut_penjualan_model->find($no_checker);
        // $data['detail'] = $this->susut_penjualan_model->find_detail($no_checker);

        $data['title'] = 'Detail Penyusutan';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan Rugi/Susud.php */
