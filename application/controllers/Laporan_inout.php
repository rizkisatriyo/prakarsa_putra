<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_inout extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_inout.css',
            'js' => 'laporan_inout.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/inout/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        // $data['inout'] = $this->inout_model->all();
        // $data['suppliers'] = $this->supplier_model->all();

        $data['title'] = 'Laporan In-Out';
        $this->loadView('index', $data);
    }

    public function find()
    {
        $tanggal_inout = $this->input->get('tanggal_inout');
        $data['tanggal_inout'] = $tanggal_inout;
        $data['tgl'] = $tanggal_inout;

        // UANG MASUK
        $data['uang_masuk_rpa'] = $this->laporan_inout_model->find_uang_masuk_rpa($tanggal_inout);
        $data['uang_masuk_rpa_detail'] = $this->laporan_inout_model->find_uang_masuk_rpa_detail($tanggal_inout);
        $data['uang_masuk_broker'] = $this->laporan_inout_model->find_uang_masuk_broker($tanggal_inout);
        $data['uang_masuk_broker_detail'] = $this->laporan_inout_model->find_uang_masuk_broker_detail($tanggal_inout);

        // PENGELUARAN
        $data['pengeluaran'] = $this->laporan_inout_model->find_pengeluaran($tanggal_inout);
        $data['pengeluaran_detail'] = $this->laporan_inout_model->find_pengeluaran_detail($tanggal_inout);

        $data['title'] = 'Laporan In-Out ' . date('d M Y', strtotime($tanggal_inout));
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan laporan_inout.php */
