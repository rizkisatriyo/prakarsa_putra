<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_broker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
        $this->load->model("pembelian_broker_model");
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'penjualan_broker.css',
            'js' => 'penjualan_broker.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/penjualan/penjualan_broker/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penjualan_broker'] = $this->penjualan_broker_model->all();

        $data['title'] = 'Data Penjualan Broker';
        $this->loadView('index', $data);
    }

    public function get_supplier()
    {
        $tgl_beli = date('Y-m-d', strtotime('-1 day', strtotime($this->input->post('tgl_jual'))));
        
        $this->db->order_by('nama');
        $this->db->join('suppliers', 'suppliers.id = pembelian_detail.id_supplier', 'left');
        $this->db->join('pembelian', 'pembelian.no_transaksi = pembelian_detail.no_transaksi', 'left');
        $supps = $this->db->get_where('pembelian_detail', ['pembelian.tgl_beli' => $tgl_beli])->result_array();
        echo json_encode($supps);
    }

    public function tambah()
    {
        $data['trans_no'] = $this->penjualan_broker_model->generate_trans_no();
        $data['brokers'] = $this->broker_model->all();
        $data['tersedia_stok'] = $this->tersedia_stok_model->latest();

        $data['title'] = 'Tambah Penjualan';
        $this->loadView('tambah', $data);
    }

     public function inputPenjualanDetail($no_transaksi)
    {
        $pembelian = $this->pembelian_broker_model->find($no_transaksi);
        $data['trans_no'] = $this->penjualan_broker_model->generate_trans_no();
        $data['brokers'] = $this->broker_model->all();

        $data['pembelian'] = $pembelian;
        $data['detail'] = $this->pembelian_broker_model->detail($no_transaksi);

        $data['title'] = 'Tambah Penjualan Broker';
        $this->loadView('tambah_jual', $data);
    }

    public function Penjualan_broker_detail()
    {
        $data['detail'] = $this->penjualan_broker_model->detail($this->penjualan_broker_model->generate_trans_no());

        $this->load->view('transaksi/penjualan_broker/penjualan_broker/penjualan_broker_detail', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[penjualan_broker.no_transaksi]');
        $this->form_validation->set_rules('tgl_jual', 'Tanggal Penjualan_broker', 'required|is_unique[penjualan_broker.tgl_jual]');
        // $this->form_validation->set_rules('ekor', 'Ekor', 'integer');
        // $this->form_validation->set_rules('kg', 'Kg', 'decimal');
        // $this->form_validation->set_rules('total_hutang', 'Total Hutang', 'decimal');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_jual' => form_error('tgl_jual')
            ]);
        } else {
            $this->penjualan_broker_model->tambah();
        }
    }



    public function aksi_tambah_detail()
    {
        $this->penjualan_broker_model->tambah_detail();
    }

    public function aksi_tambah_detail_supplier()
    {
        $this->penjualan_broker_model->tambah_detail_supplier();
    }

    public function aksi_tambah_pembayaran()
    {
        $this->pembayaran_penjualan_broker_model->tambah();
    }

    public function aksi_tambah_detail_pembayaran()
    {
        $this->pembayaran_penjualan_broker_model->tambah_detail();
    }

    public function aksi_tambah_sisa_stok()
    {
        $this->sisa_stok_model->tambah();
    }

    // public function cek_broker($id_supp)
    // public function cek_broker($id_supp)
    // {
    //     $no_trans = $this->penjualan_broker_model->generate_trans_no();

    //     $this->db->where('no_transaksi', $no_trans);
    //     $this->db->where('id_broker', $id_supp);
    //     $this->db->where('id_broker', $id_supp);
    //     $pemesanan_detail = $this->db->get('penjualan_broker_detail')->num_rows();

    //     if ($pemesanan_detail < 1) {
    //         return TRUE;
    //     } else {
    //         $this->form_validation->set_message('cek_broker', 'Tidak dapat memilih {field} yang sama.');
    //         $this->form_validation->set_message('cek_broker', 'Tidak dapat memilih {field} yang sama.');
    //         return FALSE;
    //     }
    // }

    // public function tambah_detail()
    // {
    //     $this->form_validation->set_rules('id_broker', 'broker', 'required|callback_cek_broker');
    //     $this->form_validation->set_rules('id_broker', 'broker', 'required|callback_cek_broker');
    //     $this->form_validation->set_rules('id_broker', 'broker', 'required|callback_cek_broker');
    //     $this->form_validation->set_rules('id_broker', 'broker', 'required|callback_cek_broker');
    //     $this->form_validation->set_rules('hrg_ayam', 'Harga Ayam', 'required|numeric');
    //     $this->form_validation->set_rules('jumlah', 'Jumlah Beli', 'required|numeric');
    //     $this->form_validation->set_rules('subtotal', 'Subtotal', 'required|numeric');

    //     if ($this->form_validation->run() == FALSE) {
    //         echo json_encode([
    //             'id_broker' => form_error('id_broker'),
    //             'id_broker' => form_error('id_broker'),
    //             'id_broker' => form_error('id_broker'),
    //             'hrg_ayam' => form_error('hrg_ayam'),
    //             'jumlah' => form_error('jumlah'),
    //             'subtotal' => form_error('subtotal'),
    //         ]);
    //     } else {
    //         $this->penjualan_broker_model->tambah_detail();
    //     }
    // }

    public function detail($no_transaksi)
    {
        $penjualan_broker = $this->penjualan_broker_model->find($no_transaksi);
        $data['penjualan_broker'] = $this->penjualan_broker_model->find($no_transaksi);
        $data['detail'] = $this->penjualan_broker_model->detail($no_transaksi);
        $data['tersedia_kandang'] = $this->penjualan_broker_model->find_tersedia_kandang($penjualan_broker->tgl_jual);

        $data['title'] = 'Detail Penjualan';
        $this->loadView('detail', $data);
    }

    // public function ubah($id)
    // {
    //     $data['penjualan_broker'] = $this->penjualan_broker_model->find($id);
    //     $data['pesanan'] = $this->penjualan_broker_model->find_pesanan($id);

    //     $data['title'] = 'Ubah Pesanan';
    //     $this->loadView('ubah', $data);
    // }

    // public function aksi_ubah()
    // {
    // $id = $this->input->post('id');
    // $nama = $this->input->post('nama');
    // if ($nama == $this->penjualan_broker_model->find($id)->nama) {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required');
    // } else {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[penjualan_broker.nama]');
    // }

    // $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer|greater_than[0]');

    // if ($this->form_validation->run() == FALSE) {
    //     $this->session->set_flashdata('gagal', 'Data gagal diubah !');
    //     $this->ubah($id);
    // } else {
    //     $this->penjualan_broker_model->ubah($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
    //     redirect('penjualan_broker');
    // }
    // }

    // public function hapus($id)
    // {
    //     $this->penjualan_broker_model->hapus($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
    //     redirect('penjualan_broker');
    // }
}
        
    /* End of file  Pemesanan Penjualan_broker.php */
