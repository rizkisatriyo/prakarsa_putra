<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'pembayaran_penjualan.css',
            'js' => 'pembayaran_penjualan.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/penjualan/pembayaran/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pembayaran_penjualan'] = $this->pembayaran_penjualan_model->all();

        $data['title'] = 'Data Pembayaran';
        $this->loadView('index', $data);
    }

    // NEXT DETAIL PEMBAYUARANS
    public function detail($no_transaksi)
    {
        $data['pembayaran_penjualan'] = $this->pembayaran_penjualan_model->find($no_transaksi);
        $data['detail'] = $this->pembayaran_penjualan_model->detail($no_transaksi);

        $data['title'] = 'Detail Pembayaran';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan pembayaran_penjualan.php */
