<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_masuk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'stok_masuk.css',
            'js' => 'stok_masuk.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/stok_kandang/stok_masuk/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['stok_masuk'] = $this->stok_masuk_model->all();

        $data['title'] = 'Stok Masuk';
        $this->loadView('index', $data);
    }

    // public function tambah()
    // {
    //     $data['trans_no'] = $this->stok_masuk_model->generate_trans_no();
    //     $data['suppliers'] = $this->supplier_model->all();

    //     $data['title'] = 'Tambah Pesanan';
    //     $this->loadView('tambah', $data);
    // }

    // public function pemesanan_detail()
    // {
    //     $data['pesanan'] = $this->stok_masuk_model->pesanan();

    //     $this->load->view('transaksi/stok_masuk/stok_masuk/pemesanan_detail', $data);
    // }

    // public function cek_supplier($id_supp)
    // {
    //     $no_trans = $this->stok_masuk_model->generate_trans_no();

    //     $this->db->where('no_transaksi', $no_trans);
    //     $this->db->where('id_supplier', $id_supp);
    //     $pemesanan_detail = $this->db->get('stok_masuk_detail')->num_rows();

    //     if ($pemesanan_detail < 1) {
    //         return TRUE;
    //     } else {
    //         $this->form_validation->set_message('cek_supplier', 'Tidak dapat memilih {field} yang sama.');
    //         return FALSE;
    //     }
    // }

    // public function tambah_detail()
    // {
    //     $this->form_validation->set_rules('id_supplier', 'Supplier', 'required|callback_cek_supplier');
    //     $this->form_validation->set_rules('hrg_ayam', 'Harga Ayam', 'required|numeric');
    //     $this->form_validation->set_rules('jumlah', 'Jumlah Beli', 'required|numeric');
    //     $this->form_validation->set_rules('subtotal', 'Subtotal', 'required|numeric');

    //     if ($this->form_validation->run() == FALSE) {
    //         echo json_encode([
    //             'id_supplier' => form_error('id_supplier'),
    //             'hrg_ayam' => form_error('hrg_ayam'),
    //             'jumlah' => form_error('jumlah'),
    //             'subtotal' => form_error('subtotal'),
    //         ]);
    //     } else {
    //         $this->stok_masuk_model->tambah_detail();
    //     }
    // }

    // public function aksi_tambah()
    // {
    // $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[stok_masuk.nama]');
    // $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer|greater_than[0]');

    // if ($this->form_validation->run() == FALSE) {
    //     $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
    //     $this->tambah();
    // } else {
    //     $this->stok_masuk_model->tambah();

    //     $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
    //     redirect('stok_masuk');
    // }
    // }

    public function detail($no_transaksi)
    {
        $data['stok_masuk'] = $this->stok_masuk_model->find($no_transaksi);
        // $data['detail'] = $this->stok_masuk_model->find_detail($no_transaksi);

        $data['title'] = 'Detail Stok Masuk';
        $this->loadView('detail', $data);
    }

    // public function ubah($id)
    // {
    //     $data['stok_masuk'] = $this->stok_masuk_model->find($id);
    //     $data['pesanan'] = $this->stok_masuk_model->find_pesanan($id);

    //     $data['title'] = 'Ubah Pesanan';
    //     $this->loadView('ubah', $data);
    // }

    // public function aksi_ubah()
    // {
    // $id = $this->input->post('id');
    // $nama = $this->input->post('nama');
    // if ($nama == $this->stok_masuk_model->find($id)->nama) {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required');
    // } else {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[stok_masuk.nama]');
    // }

    // $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer|greater_than[0]');

    // if ($this->form_validation->run() == FALSE) {
    //     $this->session->set_flashdata('gagal', 'Data gagal diubah !');
    //     $this->ubah($id);
    // } else {
    //     $this->stok_masuk_model->ubah($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
    //     redirect('stok_masuk');
    // }
    // }

    // public function hapus($id)
    // {
    //     $this->stok_masuk_model->hapus($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
    //     redirect('stok_masuk');
    // }
}
        
    /* End of file  Pemesanan Stok Masuk.php */
