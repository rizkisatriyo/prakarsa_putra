<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_broker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
        $this->load->model('pembelian_broker_model');

    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'pembelian.css',
            'js' => 'pembelian_broker.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/pembelian/pembelian_broker/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['pembelian'] = $this->pembelian_broker_model->all();

        $data['title'] = 'Data Pembelian Broker';
        $this->loadView('index', $data);
    }

    public function tambah()
    {
        $data['trans_no'] = $this->pembelian_broker_model->generate_trans_no();
        $data['suppliers'] = $this->supplier_model->all();
        $data['sisa_stok'] = $this->sisa_stok_model->latest();

        $data['title'] = 'Tambah Pembelian Broker';
        $this->loadView('tambah', $data);
    }

    public function pembelian_detail()
    {
        $data['detail'] = $this->pembelian_broker_model->detail($this->pembelian_broker_model->generate_trans_no());

        $this->load->view('transaksi/pembelian/pembelian/pembelian_detail', $data);
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[pembelian.no_transaksi]');
        $this->form_validation->set_rules('tgl_beli', 'Tanggal Pembelian', 'required');
        // $this->form_validation->set_rules('ekor', 'Ekor', 'integer');
        // $this->form_validation->set_rules('kg', 'Kg', 'decimal');
        // $this->form_validation->set_rules('total_hutang', 'Total Hutang', 'decimal');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tgl_beli' => form_error('tgl_beli')
            ]);
        } else {
            $this->pembelian_broker_model->tambah();
            // $this->pembayaran_pembelian_model->tambah();
        }
    }

    public function aksi_tambah_detail()
    {
        $this->pembelian_broker_model->tambah_detail();
        // $this->pembayaran_pembelian_broker_model->tambah_detail();
    }

    public function aksi_tambah_pembayaran()
    {
        $this->pembayaran_pembelian_broker_model->tambah();
    }

    public function aksi_tambah_detail_pembayaran()
    {
        $this->pembayaran_pembelian_broker_model->tambah_detail();
    }

    public function detail($no_transaksi)
    {
        $pembelian = $this->pembelian_broker_model->find($no_transaksi);
        $data['pembelian'] = $pembelian;
        $data['detail'] = $this->pembelian_broker_model->detail($no_transaksi);
        $data['sisa_kandang'] = $this->pembelian_broker_model->find_sisa_kandang($pembelian->tgl_beli);

        $data['title'] = 'Detail Pembelian Broker';
        $this->loadView('detail', $data);
    }

    // public function ubah($id)
    // {
    //     $data['pembelian'] = $this->pembelian_broker_model->find($id);
    //     $data['pesanan'] = $this->pembelian_broker_model->find_pesanan($id);

    //     $data['title'] = 'Ubah Pesanan';
    //     $this->loadView('ubah', $data);
    // }

    // public function aksi_ubah()
    // {
    // $id = $this->input->post('id');
    // $nama = $this->input->post('nama');
    // if ($nama == $this->pembelian_broker_model->find($id)->nama) {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required');
    // } else {
    //     $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[pembelian.nama]');
    // }

    // $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer|greater_than[0]');

    // if ($this->form_validation->run() == FALSE) {
    //     $this->session->set_flashdata('gagal', 'Data gagal diubah !');
    //     $this->ubah($id);
    // } else {
    //     $this->pembelian_broker_model->ubah($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
    //     redirect('pembelian');
    // }
    // }

    // public function hapus($id)
    // {
    //     $this->pembelian_broker_model->hapus($id);

    //     $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
    //     redirect('pembelian');
    // }
}
        
    /* End of file  Pemesanan Pembelian.php */
