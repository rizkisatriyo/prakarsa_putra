<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'laporan_penjualan.css',
            'js' => 'laporan_penjualan.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('laporan/penjualan/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['penjualan'] = $this->penjualan_model->all();
        $data['customers'] = $this->customer_model->all();

        $data['title'] = 'Laporan Penjualan';
        $this->loadView('index', $data);
    }

    public function detail($no_transaksi)
    {
        $data['penjualan'] = $this->laporan_penjualan_model->find($no_transaksi);
        $data['penjualan'] = $this->laporan_penjualan_model->find($no_transaksi);
        $data['detail'] = $this->laporan_penjualan_model->find_detail($no_transaksi);

        $data['title'] = 'Detail Penjualan';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan Laporan_penjualan.php */
