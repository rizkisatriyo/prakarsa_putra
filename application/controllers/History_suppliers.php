<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_suppliers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'history_suppliers.css',
            'js' => 'history_suppliers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/history_pembayaran/history_suppliers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['history_suppliers'] = $this->history_suppliers_model->all();

        $data['title'] = 'History Pembayaran Supplier';
        $this->loadView('index', $data);
    }

    public function detail($id)
    {
        $data['supplier'] = $this->supplier_model->find($id);
        $data['history'] = $this->history_suppliers_model->find($id);

        $data['title'] = 'Detail History';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file  Pemesanan history_suppliers.php */
