<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeluaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'pengeluaran.css',
            'js' => 'pengeluaran.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/inout/pengeluaran/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $pengeluaran = $this->db->get('pengeluaran')->result_array();
        $data['pengeluaran'] = $pengeluaran;

        $data['title'] = 'Pengeluaran';
        $this->loadView('index', $data);
    }

    public function tambah()
    {
        $data['trans_no'] = $this->pengeluaran_model->generate_trans_no();
        $data['pengeluaran_akun'] = $this->db->get_where('pengeluaran_akun')->result_array();

        $data['title'] = 'Tambah Pengeluaran';
        $this->loadView('tambah', $data);
    }

    public function get_pengeluaran_akun_detail()
    {
        $id_pengeluaran_akun = $this->input->post('id_pengeluaran_akun');
        $pengeluaran_akun_detail = $this->db->get_where('pengeluaran_akun_detail', ['id_pengeluaran_akun' => $id_pengeluaran_akun, 'biaya_variabel' => 0])->result_array();
        echo json_encode($pengeluaran_akun_detail);
    }

    public function get_saldo_awal()
    {
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        // $this->db->join('saldo_awal', 'saldo_awal.no_transaksi = saldo_awal_detail.no_transaksi', 'left');
        $saldo_awal = $this->db->get_where('saldo_awal_detail', ['id_pengeluaran_akun_detail' => $id])->row('bayar');
        if ($saldo_awal) {
            echo $saldo_awal;
        } else {
            echo 0;
        }
    }

    public function get_setoran()
    {
        $tanggal_inout = $this->input->post('tanggal');

        $this->db->order_by('tgl_bayar', 'desc');
        $data['setoran_rpa'] = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        $this->db->order_by('tgl_bayar', 'desc');
        $data['setoran_broker'] = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        echo json_encode($data);

        // $this->db->select('SUM(bayar) AS subtotal');
        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
        // $this->db->join('pengeluaran_akun_detail', 'pengeluaran_akun_detail.id = pengeluaran_detail.id_pengeluaran_akun_detail', 'left');
        // $this->db->join('pengeluaran_akun', 'pengeluaran_akun.id = pengeluaran_akun_detail.id_pengeluaran_akun', 'left');
        // $sub_kantor = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'pengeluaran_akun.id' => 3])->row('subtotal');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
        // $mob_pemeliharaan = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 6])->row('bayar');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
        // $prod_plastik = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 11])->row('bayar');

        // $this->db->order_by('tanggal', 'desc');
        // $this->db->join('pengeluaran', 'pengeluaran.no_transaksi = pengeluaran_detail.no_transaksi', 'left');
        // $prod_pemeliharaan = $this->db->get_where('pengeluaran_detail', ['pengeluaran.tanggal <=' => $tanggal_inout, 'id_pengeluaran_akun_detail' => 12])->row('bayar');
        // $output_pokok = ($pengeluaran->total - ($sub_kantor + ($mob_pemeliharaan + $prod_plastik + $prod_pemeliharaan)));
        // $tot_setor_bank = ($uang_masuk_rpa->total_bayar + $uang_masuk_broker->total_bayar) - $output_pokok;

        // $this->db->order_by('tgl_bayar', 'desc');
        // $setoran_rpa = $this->db->get_where('penjualan_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        // $this->db->order_by('tgl_bayar', 'desc');
        // $setoran_broker = $this->db->get_where('penjualan_broker_pembayaran', ['tgl_bayar <=' => $tanggal_inout])->row('total_bayar');

        // // $setor_tabungan = $pengeluaran->total - $output_pokok;

        // // $setor_modal_rpa = $setoran_rpa - $pengeluaran->total;
        // $setor_modal_total = 53285200;
        // $sisa_setoran = $tot_setor_bank - $setor_tabungan - $setor_modal_total;
    }

    public function aksi_tambah()
    {
        $this->form_validation->set_rules('no_transaksi', 'No Transaksi', 'required|is_unique[pengeluaran.no_transaksi]');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|is_unique[pengeluaran.tanggal]');
        $this->form_validation->set_rules('setor_modal_total', 'SETOR MODAL TOTAL', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode([
                'no_transaksi' => form_error('no_transaksi'),
                'tanggal' => form_error('tanggal'),
                'setor_modal_total' => form_error('setor_modal_total'),
            ]);
        } else {
            $this->pengeluaran_model->tambah();
        }
    }

    public function aksi_tambah_detail()
    {
        $this->pengeluaran_model->tambah_detail();
    }

    public function detail($no_transaksi)
    {
        $data['no_transaksi'] = $no_transaksi;
        $data['pengeluaran'] = $this->pengeluaran_model->find($no_transaksi);
        $data['pengeluaran_detail'] = $this->pengeluaran_model->find_detail($no_transaksi);

        $data['title'] = 'Detail Pengeluaran';
        $this->loadView('detail', $data);
    }
}
        
    /* End of file pengeluaran.php */
