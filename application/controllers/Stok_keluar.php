<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_keluar extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if(!$this->auth_model->current_user()){
            $this->session->set_userdata('referred_from', current_url());
			redirect('auth/login');
		}
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'stok_keluar.css',
            'js' => 'stok_keluar.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('transaksi/stok_kandang/stok_keluar/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['stok_keluar'] = $this->stok_keluar_model->all();

        $data['title'] = 'Stok Keluar';
        $this->loadView('index', $data);
    }
}
        
    /* End of file  Pemesanan Stok Masuk.php */
