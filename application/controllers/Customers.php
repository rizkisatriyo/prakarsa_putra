<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->auth_model->current_user()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('auth/login');
        }
    }

    private function loadView($file, $data)
    {
        $data['style'] = [
            // 'css' => 'customers.css',
            'js' => 'customers.js',
        ];

        $this->load->view('parts/header', $data);
        $this->load->view('master_data/kontak/customers/' . $file, $data);
        $this->load->view('parts/footer', $data);
    }

    public function index()
    {
        $data['customers'] = $this->customer_model->all();

        $data['title'] = 'Data Customers';
        $this->loadView('customers', $data);
    }

    public function tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $data['title'] = 'Tambah Customer';
        $this->loadView('tambah', $data);
    }

    public function aksi_tambah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[customers.nama]');
        $this->form_validation->set_rules('singkatan', 'Singkatan', 'is_unique[customers.singkatan]|max_length[10]');
        $this->form_validation->set_rules('telefon', 'telefon', 'numeric');
        $this->form_validation->set_rules('email', 'Email',  'valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal ditambahkan !');
            $this->tambah();
        } else {
            $this->customer_model->tambah();

            $this->session->set_flashdata('sukses', 'Data berhasil ditambahkan !');
            redirect('customers');
        }
    }

    public function detail($id)
    {
        $data['customer'] = $this->customer_model->find($id);

        $data['title'] = 'Detail Customer';
        $this->loadView('detail', $data);
    }

    public function ubah($id)
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $data['customer'] = $this->customer_model->find($id);

        $data['title'] = 'Ubah Customer';
        $this->loadView('ubah', $data);
    }

    public function aksi_ubah()
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $singkatan = $this->input->post('singkatan');
        $fetch_customers = $this->customer_model->find($id);

        if ($nama == $fetch_customers->nama) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
        } else {
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[customers.nama]');
        }
        if ($singkatan == $fetch_customers->singkatan) {
            $this->form_validation->set_rules('singkatan', 'Singkatan', 'max_length[10]');
        } else {
            $this->form_validation->set_rules('singkatan', 'Singkatan', 'is_unique[customers.singkatan]|max_length[10]');
        }

        $this->form_validation->set_rules('telefon', 'telefon', 'numeric');
        $this->form_validation->set_rules('email', 'Email',  'valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Data gagal diubah !');
            $this->ubah($id);
        } else {
            $this->customer_model->ubah($id);

            $this->session->set_flashdata('sukses', 'Data berhasil diubah !');
            redirect('customers');
        }
    }

    public function hapus($id)
    {
        $current_user = $this->auth_model->current_user();
        if ($current_user->role != 'Administrator') {
            return show_error('Selain Administrator tidak diperkenankan mengakses halaman ini!', 403, '403 Access Forbidden!');
        };

        $this->customer_model->hapus($id);

        $this->session->set_flashdata('sukses', 'Data berhasil dihapus !');
        redirect('customers');
    }
}
        
    /* End of file  Customers.php */
