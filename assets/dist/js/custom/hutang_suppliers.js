$(document).ready(function () {
  $("#hutang_suppliers").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tgl_bayar").change(function (e) {
    e.preventDefault();
    var tgl_bayar = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PBP" + tgl_bayar);
  });

  $("#modal-bayar").on("shown.bs.modal", function (event) {
    $("#jml_bayar").trigger("focus");
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var hutang = button.data("hutang");
    var deposit = button.data("deposit");

    $("#id_supplier").val(id).trigger("change");
    $("#hutang_supplier").val(hutang).trigger("input");
    $("#deposit_supplier").val(deposit).trigger("input");
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
    placeholder: "Pilih Supplier",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });

  $("#hutang_supplier").mask("#.##0", {
    reverse: true,
  });
  $("#deposit_supplier").mask("#.##0", {
    reverse: true,
  });
  $("#jml_bayar").mask("#.##0", {
    reverse: true,
  });

  $("#id_supplier").on("select2:select", function () {
    var hutang = $("#id_supplier option:selected").data("hutang");
    var deposit = $("#id_supplier option:selected").data("deposit");
    $("#hutang_supplier").val(hutang).trigger("input");
    $("#deposit_supplier").val(deposit).trigger("input");
  });

  $("#form_bayar").submit(function (e) {
    e.preventDefault();
    const data = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_bayar: $("#tgl_bayar").val(),
      id_supplier: $("#id_supplier").val(),
      hutang_supplier: $("#hutang_supplier").cleanVal(),
      deposit_supplier: $("#deposit_supplier").cleanVal(),
      jml_bayar: $("#jml_bayar").cleanVal(),
    };

    $.ajax({
      type: "POST",
      url: "hutang_suppliers/bayar",
      data: data,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
          location.reload();
        } else {
          console.log(errors);
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_bayar);
          $("#id_supplier.invalid-feedback")
            .css("display", "block")
            .html(error.id_supplier);
          $("#jml_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.jml_bayar);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          $("#modal-bayar").modal("show");
        }
      },
    });
  });
});
