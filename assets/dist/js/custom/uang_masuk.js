$("#uang_masuk").DataTable({
  // dom:
  //   "<'.row mb-2'<'col-md-4'l><'.col-md-4 text-center'B><'col-md-4'f>>" +
  //   "<'row'<'col-12'tr>>" +
  //   "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
  // language: {
  // search: "Temukan",
  // sLengthMenu: "Tampilkan _MENU_",
  // searchPlaceholder: "Something ..",
  // },
  responsive: true,
  autoWidth: false,
  // buttons: [
  //   {
  //     extend: "excel",
  //     exportOptions: {
  //       columns: [0, 1, 2, 3, 4],
  //     },
  //   },
  //   {
  //     extend: "pdf",
  //     exportOptions: {
  //       columns: [0, 1, 2, 3, 4],
  //     },
  //     customize: function (doc) {
  //       doc.content[1].table.widths = Array(
  //         doc.content[1].table.body[0].length + 1
  //       )
  //         .join("*")
  //         .split("");
  //     },
  //   },
  //   {
  //     extend: "print",
  //     exportOptions: {
  //       columns: [0, 1, 2, 3, 4],
  //     },
  //   },
  // ],
});
