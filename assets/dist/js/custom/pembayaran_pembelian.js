$(document).ready(function () {
  $("#pembayaran_pembelian").DataTable({
    responsive: true,
    autoWidth: false,
  });

  // function get_pesanan() {
  //   $.ajax({
  //     type: "POST",
  //     url: "pemesanan_detail",
  //     cache: false,
  //     success: function (response) {
  //       $(".pemesanan_detail").html(response);
  //     },
  //   });
  // }

  // get_pesanan();

  // $("#tambah_baru").modal();

  // $(".select2bs4").select2({
  //   theme: "bootstrap4",
  //   placeholder: "Pilih Supplier ..",
  //   dropdownParent: $("#form_detail"),
  // });

  // $("#hrg_ayam").mask("###.###.###", { reverse: true });

  // $("#hrg_ayam, #jumlah").keyup(function (e) {
  //   var hrg_ayam = $("#hrg_ayam").cleanVal();
  //   var jumlah = $("#jumlah").val();
  //   var subtotal = 0;

  //   subtotal = hrg_ayam * jumlah;
  //   $("#subtotal")
  //     .mask("###.###.###", { reverse: true })
  //     .val(subtotal)
  //     .trigger("input");
  // });

  // function reset_form() {
  //   $("#id_supplier").val("").trigger("change");
  //   $("#hrg_ayam").val("");
  //   $("#jumlah").val("");
  //   $("#subtotal").val("");
  // }

  // $(".save").click(function (event) {
  //   var formData = {
  //     no_transaksi: $("#no_transaksi").val(),
  //     id_supplier: $("#id_supplier").val(),
  //     hrg_ayam: $("#hrg_ayam").cleanVal(),
  //     jumlah: $("#jumlah").val(),
  //     subtotal: $("#subtotal").cleanVal(),
  //   };

  //   $.ajax({
  //     type: "POST",
  //     url: "tambah_detail",
  //     data: formData,
  //     success: function (errors) {
  //       if ($.isEmptyObject(errors)) {
  //         $("#form_detail").modal("hide");
  //         reset_form();
  //         get_pesanan();
  //         toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
  //           progressBar: true,
  //           positionClass: "toast-top-right",
  //           timeOut: "2500",
  //         });
  //       } else {
  //         error = JSON.parse(errors);
  //         $("#id_supplier.invalid-feedback")
  //           .css("display", "block")
  //           .html(error.id_supplier);
  //         $("#hrg_ayam.invalid-feedback")
  //           .css("display", "block")
  //           .html(error.hrg_ayam);
  //         $("#jumlah.invalid-feedback")
  //           .css("display", "block")
  //           .html(error.jumlah);
  //         $("#subtotal.invalid-feedback")
  //           .css("display", "block")
  //           .html(error.subtotal);

  //         toastr.error("Data Gagal Ditambahkan !", "Gagal", {
  //           progressBar: true,
  //           positionClass: "toast-top-right",
  //           timeOut: "2500",
  //         });
  //       }
  //     },
  //   });

  //   event.preventDefault();
  // });
});
