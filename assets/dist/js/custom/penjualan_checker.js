$(document).ready(function () {
  $("#penjualan_checker").DataTable({
    responsive: true,
    autoWidth: false,
  });

  // $(".select2bs4").select2({
  //   theme: "bootstrap4",
  //   placeholder: "Pilih Customer",
  // });

  $("#tgl_check").change(function (e) {
    e.preventDefault();
    var tgl_check = moment($(this).val()).format("YYYYMMDD");
    // $("#no_transaksi").val("TRM" + tgl_check);
    $("#no_checker").val("CKJ" + tgl_check);
    $(".txt_no_checker").text("#CKJ" + tgl_check);

    $.ajax({
      type: "POST",
      url: "/prakarsa_putra/penjualan_checker/ubah_tanggal",
      data: { tgl: $(this).val() },
      success: function (res) {
        result = JSON.parse(res);
        if (result != null) {
          $("#t_stok_ekor").val(result.ekor);
          $("#t_stok_kg").val(result.kg);
        } else {
          alert("data tidak ditemukan");
        }
      },
    });
  });

  $(".tambah_baru").each(function (index, element) {
    $(element).click(function (e) {
      e.preventDefault();

      var id_kandang = $(element).data("id_kandang");
      var no_trans = $(element).data("no_trans");

      $("#table-" + id_kandang + " tbody").append(
        "<tr>" +
          '<td><select name="id_customer" id="id_customer" class="select2bs4 form-control form-control-sm id_customer"><option></option></select></td>' +
          '<td><input type="number" name="ekor" class="form-control form-control-sm text-center ekor" id="ekor"></td>' +
          '<td><input type="number" name="kg" class="form-control form-control-sm text-center kg" id="kg"></td></tr>'
      );

      $.ajax({
        type: "GET",
        url: "/prakarsa_putra/penjualan_checker/get_customers/" + no_trans,
        success: function (response) {
          opts = JSON.parse(response);
          $(".id_customer").each(function (index, element) {
            for (let i = 0; i < opts.length; i++) {
              $(element).append(new Option(opts[i].nama, opts[i].c_id));
            }
          });
        },
      });
    });
  });

  $("#total_ekor").change(function (e) {
    e.preventDefault();
    $("#p_total_ekor").val($(this).val());
  });

  $("#total_kg").change(function (e) {
    e.preventDefault();
    $("#p_total_kg").val($(this).val());
  });

  $(document).on(
    "keyup",
    ".ekor, #t_stok_ekor, #sisa_ekor, #mati_ekor",
    function () {
      let total_stok_ekor = parseInt($("#t_stok_ekor").val());
      let ekorSum = [...$(".ekor")]
        .map((input) => Number(input.value))
        .reduce((a, b) => a + b, 0);
      $("#total_ekor").val(ekorSum).trigger("change");
      let sisa_ekor = parseInt($("#sisa_ekor").val());
      let mati_ekor = 0;
      if ($("#mati_ekor").val()) mati_ekor = parseInt($("#mati_ekor").val());

      let susut_ekor = total_stok_ekor - ekorSum - sisa_ekor - mati_ekor;
      $("#susut_ekor").val(susut_ekor);
    }
  );

  $(document).on("keyup", ".kg, #t_stok_kg, #sisa_kg, #mati_kg", function () {
    let total_stok_kg = parseFloat($("#t_stok_kg").val());
    let kgSum = [...$(".kg")]
      .map((input) => Number(input.value))
      .reduce((a, b) => a + b, 0);
    $("#total_kg").val(parseFloat(kgSum).toFixed(2)).trigger("change");
    let sisa_kg = parseFloat($("#sisa_kg").val());
    let mati_kg = parseFloat($("#mati_kg").val());

    let susut_kg = total_stok_kg - kgSum - sisa_kg;
    $("#susut_kg").val(susut_kg.toFixed(2));

    var presentase_susut_kg = (susut_kg / total_stok_kg) * 100;
    $("#presentase_susut_kg").val(presentase_susut_kg.toFixed(2));
  });

  $(".simpan").click(function (e) {
    e.preventDefault();

    // var data_penerimaan = {
    //   no_transaksi: $("#no_transaksi").val(),
    //   no_transaksi_penjualan: $("#no_transaksi_penjualan").val(),
    //   tgl_check: $("#tgl_check").val(),
    //   total_ekor: $("#total_ekor").val(),
    //   total_kg: $("#total_kg").val(),
    //   sisa_ekor: $("#sisa_ekor").val(),
    //   sisa_kg: $("#sisa_kg").val(),
    //   mati_ekor: $("#mati_ekor").val(),
    //   mati_kg: $("#mati_kg").val(),
    // };

    // var data_penerimaan_checker = {
    //   no_transaksi_penerimaan: $("#no_transaksi").val(),
    //   total_ekor: $("#total_ekor").val(),
    //   total_kg: $("#total_kg").val(),
    // };

    // var data_penjualan_checker_detail = {
    //   id_penerimaan_checker: $("#id_penerimaan_checker").val(),
    //   id_kandang: $("#id_kandang").val(),
    //   ekor: $("#ekor").val(),
    //   kg: $("#kg").val(),
    // };

    var data_penjualan_checker_detail = [];
    $(".table-checker").each(function (index, element) {
      var id_kandang = $(element).data("id_kandang");
      var row = $(element).find("tbody tr");
      $(row).each(function (i, e) {
        if ($(e).find("#ekor").val() != "" && $(e).find("#kg").val() != "") {
          var detail = {
            no_checker: $("#no_checker").val(),
            id_kandang: id_kandang,
            id_customer: $(e).find("#id_customer").val(),
            ekor: $(e).find("#ekor").val(),
            kg: $(e).find("#kg").val(),
          };

          data_penjualan_checker_detail.push(detail);
        }
      });
    });

    var form_data = {
      no_checker: $("#no_checker").val(),
      no_transaksi_penjualan: $("#no_transaksi_penjualan").val(),
      tgl_check: $("#tgl_check").val(),
      total_ekor: $("#total_ekor").val(),
      total_kg: $("#total_kg").val(),
      sisa_ekor: $("#sisa_ekor").val(),
      sisa_kg: $("#sisa_kg").val(),
      mati_ekor: $("#mati_ekor").val(),
      mati_kg: $("#mati_kg").val(),
      detail_checker: data_penjualan_checker_detail,
      no_transaksi_penjualan: $("#no_transaksi_penjualan").val(),
      ekor: $("#susut_ekor").val(),
      presentase_ekor: $("#presentase_susut_ekor").val(),
      kg: $("#susut_kg").val(),
      presentase_kg: $("#presentase_susut_kg").val(),
    };

    if (
      $("#no_checker").val() != "" &&
      $("#tgl_check").val() != "" &&
      $("#no_transaksi_penjualan").val() != ""
      // $.isEmptyObject(
      //   data_penjualan_checker_detail.filter(
      //     (detail) => detail.id_customer != null
      //   )
      // )
    ) {
      $.ajax({
        type: "POST",
        url: "/prakarsa_putra/penjualan_checker/aksi_tambah",
        data: form_data,
        success: function (errors) {
          if ($.isEmptyObject(errors)) {
            toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
              progressBar: true,
              positionClass: "toast-top-right",
              timeOut: "2500",
            });
            tambah_sisa_stok();
            document.location.href = "/prakarsa_putra/penjualan_checker";
          } else {
            error = JSON.parse(errors);
            if (error.no_checker) {
              $("#no_checker").addClass("is-invalid");
              $("#no_checker.invalid-feedback")
                .css("display", "block")
                .html(error.no_checker);
            }
            if (error.tgl_check) {
              $("#tgl_check").addClass("is-invalid");
              $("#tgl_check.invalid-feedback")
                .css("display", "block")
                .html(error.tgl_check);
            }
            if (error.no_transaksi_penjualan) {
              $("#no_transaksi_penjualan").addClass("is-invalid");
              $("#no_transaksi_penjualan.invalid-feedback")
                .css("display", "block")
                .html(error.no_transaksi_penjualan);
            }
            if (error.total_ekor) {
              $("#total_ekor").addClass("is-invalid");
              $("#total_ekor.invalid-feedback")
                .css("display", "block")
                .html(error.total_ekor);
            }
            if (error.total_kg) {
              $("#total_kg").addClass("is-invalid");
              $("#total_kg.invalid-feedback")
                .css("display", "block")
                .html(error.total_kg);
            }
            toastr.error("Data Gagal Ditambahkan !", "Gagal", {
              progressBar: true,
              positionClass: "toast-top-right",
              timeOut: "2500",
            });
          }
        },
      });
    }
  });

  function tambah_sisa_stok() {
    var tgl_check = moment($("#tgl_check").val()).format("YYYYMMDD");
    const data = {
      no_transaksi: "STK" + tgl_check,
      no_checker: $("#no_checker").val(),
      tanggal: $("#tgl_check").val(),
      ekor: $("#sisa_ekor").val(),
      kg: $("#sisa_kg").val(),
      harga: $("#sisa_harga").val(),
      jumlah:
        parseFloat($("#sisa_kg").val()) * parseFloat($("#sisa_harga").val()),
    };

    $.ajax({
      type: "POST",
      url: "/prakarsa_putra/penjualan_checker/aksi_tambah_sisa_stok",
      data: data,
    });
  }

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tgl_check").val("2022-02-26").trigger("change");
    $("#broker_kg").val(3200);
    $("#sisa_ekor").val(216);
    $("#sisa_kg").val(289.1);
    $("#mati_ekor").val(3).trigger("keyup");
    $("#mati_kg").val(4.2).trigger("keyup");
    $("#total_ekor").val(500).trigger("change");
    $("#total_kg").val(938.3).trigger("change");
    $("#susut_ekor").val(0);
    $("#susut_kg").val(17.2);
    $("#presentase_susut_kg").val(1.38);

    const cks = [
      {
        id_kandang: "2",
        id_customer: "2",
        nama: "P GEMA",
        ekor: "3",
        kg: "3.7",
      },
      {
        id_kandang: "2",
        id_customer: "40",
        nama: "P TOHA",
        ekor: "5",
        kg: "7.5",
      },
      {
        id_kandang: "2",
        id_customer: "18",
        nama: "RAHMAT",
        ekor: "25",
        kg: "48.3",
      },
      {
        id_kandang: "2",
        id_customer: "18",
        nama: "RAHMAT",
        ekor: "9",
        kg: "16.5",
      },
      {
        id_kandang: "2",
        id_customer: "17",
        nama: "P. EDI",
        ekor: "3",
        kg: "6.1",
      },
      { id_kandang: "3", id_customer: "19", nama: "ADE", ekor: "13", kg: "22" },
      {
        id_kandang: "3",
        id_customer: "14",
        nama: "P DEDI",
        ekor: "15",
        kg: "20.5",
      },
      {
        id_kandang: "3",
        id_customer: "2",
        nama: "P GEMA",
        ekor: "20",
        kg: "30.2",
      },
      {
        id_kandang: "3",
        id_customer: "32",
        nama: "M UHA",
        ekor: "2",
        kg: "3.2",
      },
      {
        id_kandang: "3",
        id_customer: "37",
        nama: "M UJANG",
        ekor: "10",
        kg: "13",
      },
      {
        id_kandang: "3",
        id_customer: "22",
        nama: "B AGUNG",
        ekor: "20",
        kg: "20",
      },
      {
        id_kandang: "3",
        id_customer: "21",
        nama: "B KIKI",
        ekor: "2",
        kg: "2.3",
      },
      { id_kandang: "3", id_customer: "6", nama: "KANTOR", ekor: "6", kg: "5" },
      {
        id_kandang: "3",
        id_customer: "34",
        nama: "KHOLIK",
        ekor: "18",
        kg: "30.2",
      },
      {
        id_kandang: "3",
        id_customer: "3",
        nama: "B ANDON",
        ekor: "1",
        kg: "2.5",
      },
      {
        id_kandang: "3",
        id_customer: "15",
        nama: "B NENENG",
        ekor: "5",
        kg: "7",
      },
      {
        id_kandang: "3",
        id_customer: "24",
        nama: "P TARLIG",
        ekor: "5",
        kg: "8.1",
      },
      {
        id_kandang: "3",
        id_customer: "11",
        nama: "ALIF",
        ekor: "13",
        kg: "17",
      },
      {
        id_kandang: "3",
        id_customer: "10",
        nama: "P OYO",
        ekor: "17",
        kg: "30",
      },
      { id_kandang: "3", id_customer: "19", nama: "ADE", ekor: "5", kg: "6.3" },
      { id_kandang: "3", id_customer: "6", nama: "KANTOR", ekor: "2", kg: "3" },
      {
        id_kandang: "3",
        id_customer: "2",
        nama: "P GEMA",
        ekor: "5",
        kg: "7.4",
      },
      {
        id_kandang: "3",
        id_customer: "13",
        nama: "P ASEP",
        ekor: "7",
        kg: "7.6",
      },
      {
        id_kandang: "3",
        id_customer: "2",
        nama: "P GEMA",
        ekor: "5",
        kg: "7.4",
      },
      { id_kandang: "3", id_customer: "29", nama: "A-B", ekor: "4", kg: "4.7" },
      {
        id_kandang: "3",
        id_customer: "9",
        nama: "HARDI",
        ekor: "6",
        kg: "6.1",
      },
      {
        id_kandang: "4",
        id_customer: "1",
        nama: "B ENTUM",
        ekor: "53",
        kg: "96.6",
      },
      {
        id_kandang: "4",
        id_customer: "6",
        nama: "KANTOR",
        ekor: "18",
        kg: "30",
      },
      {
        id_kandang: "4",
        id_customer: "2",
        nama: "P GEMA",
        ekor: "30",
        kg: "45.6",
      },
      {
        id_kandang: "4",
        id_customer: "2",
        nama: "P GEMA ",
        ekor: "10",
        kg: "15.3",
      },
      {
        id_kandang: "4",
        id_customer: "7",
        nama: "P HASAN",
        ekor: "9",
        kg: "15.7",
      },
      { id_kandang: "5", id_customer: "20", nama: "E", ekor: "1", kg: "2" },
      {
        id_kandang: "5",
        id_customer: "3",
        nama: "B ANDON",
        ekor: "153",
        kg: "397.5",
      },
    ];

    cks.forEach((ck) => {
      $("#table-" + ck.id_kandang + " tbody").append(
        "<tr>" +
          '<td><select name="id_customer" id="id_customer" class="select2bs4 form-control form-control-sm id_customer"><option value="' +
          ck.id_customer +
          '">' +
          ck.nama +
          "</option></select></td>" +
          '<td><input type="number" name="ekor" value="' +
          ck.ekor +
          '" class="form-control form-control-sm text-center ekor" id="ekor"></td>' +
          '<td><input type="number" name="kg" value="' +
          ck.kg +
          '" class="form-control form-control-sm text-center kg" id="kg"></td></tr>'
      );

      $("#ekor").trigger("input");
      $("#kg").trigger("input");
    });
  });

  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tgl_check").val("2022-02-27").trigger("change");
    $("#broker_kg").val(2500);
    $("#sisa_ekor").val(51);
    $("#sisa_kg").val(46.5);
    $("#mati_ekor").val(13).trigger("keyup");
    $("#mati_kg").val(15.4).trigger("keyup");
    $("#total_ekor").val(701).trigger("change");
    $("#total_kg").val(1232).trigger("change");
    $("#susut_ekor").val(0);
    $("#susut_kg").val(31.4);
    $("#presentase_susut_kg").val(2.40);

    const cks = [
      {
        id_kandang: "2",
        id_customer: "19",
        nama: "ADE",
        ekor: "15",
        kg: "23.4",
      },
      {
        id_kandang: "2",
        id_customer: "7",
        nama: "P. HASAN",
        ekor: "11",
        kg: "15.8",
      },
      {
        id_kandang: "2",
        id_customer: "37",
        nama: "MG. UJANG",
        ekor: "15",
        kg: "20.8",
      },
      {
        id_kandang: "2",
        id_customer: "34",
        nama: "KOLIK",
        ekor: "16",
        kg: "25",
      },
      {
        id_kandang: "2",
        id_customer: "15",
        nama: "B. NENENG",
        ekor: "8",
        kg: "11.2",
      },
      {
        id_kandang: "2",
        id_customer: "11",
        nama: "ALIF",
        ekor: "13",
        kg: "17.3",
      },
      {
        id_kandang: "2",
        id_customer: "32",
        nama: "MG. UHA",
        ekor: "2",
        kg: "3",
      },
      {
        id_kandang: "2",
        id_customer: "22",
        nama: "B. AGUNG",
        ekor: "15",
        kg: "20.4",
      },
      {
        id_kandang: "2",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "10",
        kg: "14.5",
      },
      {
        id_kandang: "2",
        id_customer: "32",
        nama: "MG. UHA",
        ekor: "3",
        kg: "4.8",
      },
      {
        id_kandang: "2",
        id_customer: "10",
        nama: "P. OYO",
        ekor: "16",
        kg: "30",
      },
      {
        id_kandang: "2",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "10",
        kg: "13.2",
      },
      {
        id_kandang: "2",
        id_customer: "40",
        nama: "P. TOHA",
        ekor: "2",
        kg: "5.3",
      },
      {
        id_kandang: "2",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "10",
        kg: "14.4",
      },
      {
        id_kandang: "2",
        id_customer: "9",
        nama: "HARDI",
        ekor: "3",
        kg: "3.8",
      },
      {
        id_kandang: "2",
        id_customer: "40",
        nama: "TOHA",
        ekor: "9",
        kg: "8.4",
      },
      {
        id_kandang: "2",
        id_customer: "21",
        nama: "B. KIKI",
        ekor: "2",
        kg: "2",
      },
      {
        id_kandang: "2",
        id_customer: "6",
        nama: "KANTOR",
        ekor: "5",
        kg: "3.5",
      },
      {
        id_kandang: "2",
        id_customer: "39",
        nama: "A DEDI",
        ekor: "4",
        kg: "5.2",
      },
      {
        id_kandang: "3",
        id_customer: "13",
        nama: "P. ASEP",
        ekor: "8",
        kg: "9.5",
      },
      {
        id_kandang: "3",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "10",
        kg: "15.6",
      },
      {
        id_kandang: "3",
        id_customer: "34",
        nama: "KOLIK",
        ekor: "5",
        kg: "7.8",
      },
      {
        id_kandang: "3",
        id_customer: "25",
        nama: "OMPONG",
        ekor: "6",
        kg: "10",
      },
      { id_kandang: "3", id_customer: "29", nama: "A-B", ekor: "4", kg: "4.9" },
      {
        id_kandang: "3",
        id_customer: "40",
        nama: "P. TOHA",
        ekor: "1",
        kg: "1.5",
      },
      {
        id_kandang: "3",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "30",
        kg: "50.9",
      },
      {
        id_kandang: "3",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "31",
        kg: "49.1",
      },
      {
        id_kandang: "3",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "36",
        kg: "51.6",
      },
      {
        id_kandang: "3",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "26",
        kg: "32.6",
      },
      {
        id_kandang: "3",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "29",
        kg: "64",
      },
      {
        id_kandang: "3",
        id_customer: "17",
        nama: "P. EDI",
        ekor: "7",
        kg: "10",
      },
      {
        id_kandang: "4",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "30",
        kg: "46.5",
      },
      {
        id_kandang: "4",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "10",
        kg: "16.5",
      },
      {
        id_kandang: "4",
        id_customer: "2",
        nama: "P. GEMA",
        ekor: "20",
        kg: "32.5",
      },
      {
        id_kandang: "4",
        id_customer: "1",
        nama: "B. ENTUM",
        ekor: "53",
        kg: "90",
      },
      {
        id_kandang: "4",
        id_customer: "6",
        nama: "KANTOR",
        ekor: "22",
        kg: "35",
      },
      {
        id_kandang: "4",
        id_customer: "28",
        nama: "CHIKEUN",
        ekor: "50",
        kg: "66.3",
      },
      {
        id_kandang: "5",
        id_customer: "3",
        nama: "ANDON",
        ekor: "39",
        kg: "100",
      },
      {
        id_kandang: "5",
        id_customer: "3",
        nama: "ANDON",
        ekor: "39",
        kg: "100",
      },
      {
        id_kandang: "5",
        id_customer: "3",
        nama: "ANDON",
        ekor: "39",
        kg: "100",
      },
      {
        id_kandang: "5",
        id_customer: "26",
        nama: "CIMOT",
        ekor: "15",
        kg: "42",
      },
      {
        id_kandang: "5",
        id_customer: "2",
        nama: "B. ENTUM",
        ekor: "10",
        kg: "25.7",
      },
      {
        id_kandang: "5",
        id_customer: "25",
        nama: "OMPONG",
        ekor: "1",
        kg: "2.5",
      },
      {
        id_kandang: "5",
        id_customer: "31",
        nama: "MIX CRB",
        ekor: "11",
        kg: "25.5",
      },
    ];
    cks.forEach((ck) => {
      $("#table-" + ck.id_kandang + " tbody").append(
        "<tr>" +
          '<td><select name="id_customer" id="id_customer" class="select2bs4 form-control form-control-sm id_customer"><option value="' +
          ck.id_customer +
          '">' +
          ck.nama +
          "</option></select></td>" +
          '<td><input type="number" name="ekor" value="' +
          ck.ekor +
          '" class="form-control form-control-sm text-center ekor" id="ekor"></td>' +
          '<td><input type="number" name="kg" value="' +
          ck.kg +
          '" class="form-control form-control-sm text-center kg" id="kg"></td></tr>'
      );

      $(".ekor").trigger("keyup");
      $(".kg").trigger("keyup");
    });
  });
});
