$(document).ready(function () {
  $("#pembelian")
    .DataTable({
      responsive: true,
      lengthChange: false,
      autoWidth: false,
      buttons: [
        {
          extend: "excel",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6],
          },
        },
        {
          extend: "pdf",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6],
          },
          customize: function (doc) {
            doc.content[1].table.widths = Array(
              doc.content[1].table.body[0].length + 1
            )
              .join("*")
              .split("");
          },
        },
        {
          extend: "print",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6],
          },
        },
      ],
    })
    .buttons()
    .container()
    .appendTo("#pembelian_wrapper .col-md-6:eq(0)");

  $("#tgl_beli").change(function (e) {
    e.preventDefault();
    var tgl_beli = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PBN" + tgl_beli);
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
    placeholder: "Pilih Supplier",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });
  $("#id_supplier").on("select2:select", function () {
    var hutang = $("#id_supplier option:selected").data("hutang");
    var deposit = $("#id_supplier option:selected").data("deposit");
    $(".hutang_supplier").removeClass("d-none");
    $("#hutang_supplier").val(hutang.toFixed()).trigger("input");
    $(".deposit_supplier").removeClass("d-none");
    $("#deposit_supplier").val(deposit.toFixed()).trigger("input");
  });

  $("#hrg_ayam").mask("#.##0", { reverse: true });
  $("#jml_bayar").mask("#.##0", { reverse: true });
  $("#hutang_supplier").mask("#.##0", { reverse: true });
  $("#deposit_supplier").mask("#.##0", { reverse: true });

  $("#kg, #hrg_ayam").keyup(function (e) {
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam").cleanVal();
    var subtotal = 0;

    subtotal = kg * hrg_ayam;
    $("#subtotal")
      .mask("#.##0", { reverse: true })
      .val(subtotal)
      .trigger("input");

    let jml_bayar = subtotal - $("#deposit_supplier").cleanVal();
    $("#jml_bayar")
      .mask("#.##0", { reverse: true })
      .val(jml_bayar)
      .trigger("input");
    if ($("#deposit_supplier").val() != 0) {
      $("#jml_bayar").addClass("font-weight-bold text-success");
    }
  });

  $("#tambah_detail").click(function (e) {
    e.preventDefault();

    var no = $("#pembelian_detail tbody tr").length + 1;
    var no_trxdetail =
      "PBN" +
      moment($("#tgl_beli").val()).format("YYYYMMDD") +
      no.toString().padStart(3, "0");
    var id_supplier = $("#id_supplier").val();
    var hutang = $("#id_supplier option:selected").data("hutang");
    var deposit = $("#id_supplier option:selected").data("deposit");
    var nama = $("#id_supplier option:selected").text();
    var ekor = $("#ekor").val();
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam");
    var subtotal = $("#subtotal");
    var jml_bayar = $("#jml_bayar");

    if (
      id_supplier != "" &&
      hrg_ayam.val() != "" &&
      kg != "" &&
      subtotal.val() != ""
      // jml_bayar.val() != ""
    ) {
      $("#pembelian_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_supplier">' +
          id_supplier +
          "</span>" +
          '<span class="d-none hutang">' +
          hutang +
          "</span>" +
          '<span class="d-none deposit">' +
          deposit +
          "</span>" +
          nama +
          "</td>" +
          '<td class="text-center">' +
          ekor +
          "</td>" +
          '<td class="text-center">' +
          kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          hrg_ayam.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(hrg_ayam.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          subtotal.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(subtotal.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          jml_bayar.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(jml_bayar.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      reset_form();

      toastr.success("Data berhasil ditambahkan !", "Berhasil", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });

      const ftgl_beli = $("#tgl_beli").val();
      const fno_transaksi = $("#no_transaksi").val();
      $("#tgl_beli").change(function (e) {
        e.preventDefault();
        alert(
          "Mohon tidak mengatur tanggal setelah menginputkan Supplier !\nSilahkan refresh halaman ini untuk mengatur tanggal"
        );
        $("#tgl_beli").val(ftgl_beli);
        $("#no_transaksi").val(fno_transaksi);
      });
    } else {
      toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });
    }
  });

  function reset_form() {
    $("#id_supplier").val("").trigger("change");
    $("#ekor").val("");
    $("#kg").val("");
    $("#hrg_ayam").val("");
    $("#subtotal").val("");
    $("#jml_bayar").val("");
    $(".select2bs4").select2("open");
    $(".hutang_supplier").addClass("d-none");
    $(".deposit_supplier").addClass("d-none");
    $("#jml_bayar").removeClass("font-weight-bold text-success");
  }

  function tambah_pembelian(master_pembelian, detail_pembelian) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah",
      data: master_pembelian,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          // insert detail
          var data_detail = { data: detail_pembelian };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail",
            data: data_detail,
          });

          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          history.back();
        } else {
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_beli.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_beli);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
        }
      },
    });
  }

  function tambah_pembayaran(master_pembayaran, detail_pembayaran) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah_pembayaran",
      data: master_pembayaran,
      success: function () {
        // insert detail
        var data_detail_pembayaran = { data: detail_pembayaran };
        $.ajax({
          type: "POST",
          url: "aksi_tambah_detail_pembayaran",
          data: data_detail_pembayaran,
        });
      },
    });
  }

  // $(".simpan_pembelian").click(function (e) {
  //   e.preventDefault();
  //   var data = [];
  //   var total_ekor = 0;
  //   var total_kg = 0;
  //   var subtotal = 0;
  //   var total_bayar = 0;
  //   var tgl_beli = $("#tgl_beli").val();

  //   $("#pembelian_detail tr").each(function (row, tr) {
  //     if ($(tr).find("td:eq(0)").text() == "") {
  //     } else {
  //       var sub = {
  //         no_transaksi: $("#no_transaksi").val(),
  //         no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
  //         id_supplier: $(tr).find("td:eq(2) .id_supplier").text(),
  //         ekor: $(tr).find("td:eq(3)").text(),
  //         kg: $(tr).find("td:eq(4)").text(),
  //         hrg_ayam: $(tr).find("td:eq(5) .hrg_ayam").text(),
  //         subtotal: $(tr).find("td:eq(6) .subtotal").text(),
  //         jml_bayar: $(tr).find("td:eq(7) .jml_bayar").text(),
  //         tgl_beli: tgl_beli,
  //       };

  //       data.push(sub);

  //       if ($(tr).find("td:eq(3)").text()) {
  //         total_ekor += parseInt($(tr).find("td:eq(3)").text());
  //       }
  //       total_kg += parseFloat($(tr).find("td:eq(4)").text());
  //       subtotal += parseFloat($(tr).find("td:eq(6) .subtotal").text());
  //       if ($(tr).find("td:eq(7)").text()) {
  //         total_bayar += parseFloat($(tr).find("td:eq(7) .jml_bayar").text());
  //       }
  //     }
  //   });

  //   var c = confirm("Apakah anda yakin ?");
  //   if (c == true) {
  //     var data_pembelian = {
  //       no_transaksi: $("#no_transaksi").val(),
  //       tgl_beli: tgl_beli,
  //       ekor: total_ekor,
  //       kg: total_kg,
  //       total_hutang: subtotal,
  //       total_bayar: total_bayar,
  //     };

  //     $.ajax({
  //       type: "POST",
  //       url: "aksi_tambah",
  //       data: data_pembelian,
  //       success: function (errors) {
  //         if ($.isEmptyObject(errors)) {
  //           toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
  //             progressBar: true,
  //             positionClass: "toast-top-right",
  //             timeOut: "2500",
  //           });
  //           history.back();
  //         } else {
  //           error = JSON.parse(errors);
  //           $("#no_transaksi.invalid-feedback")
  //             .css("display", "block")
  //             .html(error.no_transaksi);
  //           $("#tgl_beli.invalid-feedback")
  //             .css("display", "block")
  //             .html(error.tgl_beli);

  //           toastr.error("Data Gagal Ditambahkan !", "Gagal", {
  //             progressBar: true,
  //             positionClass: "toast-top-right",
  //             timeOut: "2500",
  //           });
  //         }
  //       },
  //     });

  //     var data_detail = { data: data };

  //     $.ajax({
  //       type: "POST",
  //       url: "aksi_tambah_detail",
  //       data: data_detail,
  //     });
  //   }
  // });

  $(".simpan_pembelian").click(function (e) {
    e.preventDefault();
    // INSERT PEMBELIAN
    var total_ekor = 0;
    var total_kg = 0;
    var subtotal = 0;
    var tgl_beli = $(".tgl_beli").val();
    var detail_pembelian = [];
    var total_hutang = 0;
    var total_bayar = 0;
    var total_deposit = 0;
    var detail_pembayaran = [];
    $("#pembelian_detail tr").each(function (row, tr) {
      if ($(tr).find("td:eq(1)").text() == "") {
      } else {
        // PEMBELIAN
        // MASTER
        if ($(tr).find("td:eq(3)").text()) {
          total_ekor += parseInt($(tr).find("td:eq(3)").text());
        }
        if ($(tr).find("td:eq(4)").text()) {
          total_kg += parseFloat($(tr).find("td:eq(4)").text());
        }
        subtotal += parseFloat($(tr).find("td:eq(6) .subtotal").text());
        // DETAIL
        var sub = {
          no_transaksi: $("#no_transaksi").val(),
          no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
          id_supplier: $(tr).find("td:eq(2) .id_supplier").text(),
          ekor: $(tr).find("td:eq(3)").text(),
          kg: $(tr).find("td:eq(4)").text(),
          hrg_ayam: $(tr).find("td:eq(5) .hrg_ayam").text(),
          subtotal: $(tr).find("td:eq(6) .subtotal").text(),
        };
        detail_pembelian.push(sub);

        // PEMBAYARAN
        // MASTER
        if ($(tr).find("td:eq(2) .hutang").text()) {
          total_hutang +=
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .hutang").text());
        }
        if ($(tr).find("td:eq(7) .jml_bayar").text()) {
          total_bayar += parseFloat($(tr).find("td:eq(7) .jml_bayar").text());
        }
        if ($(tr).find("td:eq(2) .deposit").text()) {
          total_deposit += parseFloat($(tr).find("td:eq(2) .deposit").text());
        }

        // DETAIL
        var sub_byr = {
          tgl_bayar: tgl_beli,
          id_supplier: $(tr).find("td:eq(2) .id_supplier").text(),
          tagihan: $(tr).find("td:eq(6) .subtotal").text(),
          hutang:
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .hutang").text()),
          hutang_supplier: $(tr).find("td:eq(2) .hutang").text(),
          jml_bayar: $(tr).find("td:eq(7) .jml_bayar").text(),
          deposit: $(tr).find("td:eq(2) .deposit").text(),
        };
        detail_pembayaran.push(sub_byr);
      }
    });

    // INSERT PEMBELIAN
    var master_pembelian = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_beli: tgl_beli,
      ekor: total_ekor,
      kg: total_kg,
      harga: subtotal / total_kg,
      total_hutang: subtotal,
    };

    // INSERT PEMBAYARAN
    var master_pembayaran = {
      tgl_bayar: tgl_beli,
      total_hutang: total_hutang,
      total_bayar: total_bayar,
      total_deposit: total_deposit,
    };

    var c = confirm("Apakah anda yakin ?");
    if (c == true) {
      tambah_pembayaran(master_pembayaran, detail_pembayaran);
      tambah_pembelian(master_pembelian, detail_pembelian);
    }
  });

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tgl_beli").val("2022-02-25").trigger("change");

    const dets = [
      {
        no: 1,
        id_supplier: 1,
        hutang: 0,
        deposit: 0.000109925866127014,
        nama: "CIOMAS",
        ekor: 154,
        kg: 2550,
        hrg_ayam: 18000,
        subtotal: 45900000,
        jml_bayar: 45900000,
      },
      {
        no: 2,
        id_supplier: 18,
        hutang: 0,
        deposit: 0.000114999711513519,
        nama: "K A M I / VINCENT",
        ekor: 304,
        kg: 1550,
        hrg_ayam: 18500,
        subtotal: 28675000,
        jml_bayar: 28675000,
      },
    ];

    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#pembelian_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_supplier">' +
          det.id_supplier +
          "</span>" +
          '<span class="d-none hutang">' +
          det.hutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td class="text-center">' +
          det.ekor +
          "</td>" +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });

  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tgl_beli").val("2022-02-26").trigger("change");

    const dets = [
      {
        no: 1,
        id_supplier: 1,
        hutang: 0,
        deposit: 0,
        nama: "CIOMAS",
        ekor: 154,
        kg: 1000,
        hrg_ayam: 18000,
        subtotal: 18000000,
        jml_bayar: 18000000,
      },
      {
        no: 2,
        id_supplier: 3,
        hutang: 0,
        deposit: 0,
        nama: "NEWHOPE 2",
        ekor: 113,
        kg: 270,
        hrg_ayam: 19500,
        subtotal: 5265000,
        jml_bayar: 5265000,
      },
      {
        no: 3,
        id_supplier: 8,
        hutang: 0,
        deposit: 0,
        nama: "ASPC",
        ekor: 29,
        kg: 64,
        hrg_ayam: 21000,
        subtotal: 1344000,
        jml_bayar: 1344000,
      },
      {
        no: 4,
        id_supplier: 10,
        hutang: 0,
        deposit: 50000,
        nama: "KMN / MIFTAHUDIN",
        ekor: "",
        kg: 700,
        hrg_ayam: 19000,
        subtotal: 13300000,
        jml_bayar: 13250000,
      },
      {
        no: 5,
        id_supplier: 13,
        hutang: 0,
        deposit: 0,
        nama: "SIDO AGUNG",
        ekor: "",
        kg: 1100,
        hrg_ayam: 18300,
        subtotal: 20130000,
        jml_bayar: 20130000,
      },
      {
        no: 6,
        id_supplier: 14,
        hutang: 0,
        deposit: 0,
        nama: "MPU IDM 192",
        ekor: 253,
        kg: 400,
        hrg_ayam: 18500,
        subtotal: 7400000,
        jml_bayar: 7400000,
      },
    ];

    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#pembelian_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_supplier">' +
          det.id_supplier +
          "</span>" +
          '<span class="d-none hutang">' +
          det.hutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td class="text-center">' +
          det.ekor +
          "</td>" +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });

  $("#demo_28").click(function (e) {
    e.preventDefault();

    $("#tgl_beli").val("2022-02-27").trigger("change");

    const dets = [
      {
        no: "1",
        id_supplier: "1",
        hutang: "",
        deposit: "45900000.0001099",
        nama: "CIOMAS",
        ekor: "220",
        kg: "1350",
        hrg_ayam: "18000",
        subtotal: "24300000",
        jml_bayar: "",
      },
      {
        no: "2",
        id_supplier: "2",
        hutang: "",
        deposit: "0.0000100024044513702",
        nama: "NEWHOPE 1",
        ekor: "253",
        kg: "450",
        hrg_ayam: "19500",
        subtotal: "8775000",
        jml_bayar: "8775000",
      },
      {
        no: "3",
        id_supplier: "3",
        hutang: "",
        deposit: "",
        nama: "NEWHOPE 2",
        ekor: "",
        kg: "392",
        hrg_ayam: "18500",
        subtotal: "7252000",
        jml_bayar: "7252000",
      },
      {
        no: "4",
        id_supplier: "17",
        hutang: "",
        deposit: "",
        nama: "BRU MJK 180",
        ekor: "197",
        kg: "1550",
        hrg_ayam: "18500",
        subtotal: "28675000",
        jml_bayar: "",
      },
    ];
    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#pembelian_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_supplier">' +
          det.id_supplier +
          "</span>" +
          '<span class="d-none hutang">' +
          det.hutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td class="text-center">' +
          det.ekor +
          "</td>" +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });
});
