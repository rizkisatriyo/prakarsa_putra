$(document).ready(function () {
  $("#penjualan").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tgl_jual").change(function (e) {
    e.preventDefault();
    var tgl_jual = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PJN" + tgl_jual);
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
    placeholder: "Pilih Customer",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });
  $("#id_customer").on("select2:select", function () {
    var piutang = $("#id_customer option:selected").data("piutang");
    var deposit = $("#id_customer option:selected").data("deposit");
    $(".piutang_customer").removeClass("d-none");
    $("#piutang_customer").val(piutang).trigger("input");
    $(".deposit_customer").removeClass("d-none");
    $("#deposit_customer").val(deposit).trigger("input");
  });

  $("#hrg_ayam").mask("#.##0", { reverse: true });
  $("#jml_bayar").mask("#.##0", { reverse: true });
  $("#piutang_customer").mask("#.##0", { reverse: true });
  $("#deposit_customer").mask("#.##0", { reverse: true });

  $("#kg, #hrg_ayam").keyup(function (e) {
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam").cleanVal();
    var subtotal = 0;

    subtotal = kg * hrg_ayam;
    $("#subtotal")
      .mask("#.##0", { reverse: true })
      .val(subtotal.toFixed())
      .trigger("input");

    let jml_bayar = subtotal - $("#deposit_customer").cleanVal();
    $("#jml_bayar")
      .mask("#.##0", { reverse: true })
      .val(jml_bayar.toFixed())
      .trigger("input");
    if ($("#deposit_customer").val() != 0) {
      $("#jml_bayar").addClass("font-weight-bold text-success");
    }
  });

  $("#tambah_detail").click(function (e) {
    e.preventDefault();

    var no = $("#penjualan_detail tbody tr").length + 1;
    var no_trxdetail =
      "PJN" +
      moment($("#tgl_jual").val()).format("YYYYMMDD") +
      no.toString().padStart(3, "0");
    var id_customer = $("#id_customer").val();
    var piutang = $("#id_customer option:selected").data("piutang");
    var deposit = $("#id_customer option:selected").data("deposit");
    var nama = $("#id_customer option:selected").text();
    var ekor = $("#ekor").val();
    var category = $("#category").val();
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam");
    var subtotal = $("#subtotal");
    var jml_bayar = $("#jml_bayar");

    if (
      id_customer != "" &&
      hrg_ayam.val() != "" &&
      kg != "" &&
      subtotal.val() != ""
      // jml_bayar.val() != ""
    ) {
      $("#penjualan_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_customer">' +
          id_customer +
          "</span>" +
          '<span class="d-none piutang">' +
          piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          deposit +
          "</span>" +
          nama +
          "</td>" +
          '<td class="text-center">' +
          category +
          "</td>" +
          '<td class="text-center">' +
          ekor +
          "</td>" +
          '<td class="text-center">' +
          kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          hrg_ayam.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(hrg_ayam.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          subtotal.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(subtotal.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          jml_bayar.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(jml_bayar.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      reset_form();

      toastr.success("Data berhasil ditambahkan !", "Berhasil", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });

      const ftgl_jual = $("#tgl_jual").val();
      const fno_transaksi = $("#no_transaksi").val();
      $("#tgl_jual").change(function (e) {
        e.preventDefault();
        alert(
          "Mohon tidak mengatur tanggal setelah menginputkan customer !\nSilahkan refresh halaman ini untuk mengatur tanggal"
        );
        $("#tgl_jual").val(ftgl_jual);
        $("#no_transaksi").val(fno_transaksi);
      });
    } else {
      toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });
    }
  });

  function reset_form() {
    $("#id_customer").val("").trigger("change");
    $("#category").val("");
    $("#ekor").val("");
    $("#kg").val("");
    $("#hrg_ayam").val("");
    $("#subtotal").val("");
    $("#jml_bayar").val("");
    $(".select2bs4").select2("open");
    $(".piutang_customer").addClass("d-none");
    $(".deposit_customer").addClass("d-none");
    $("#jml_bayar").removeClass("font-weight-bold text-success");
  }

  function tambah_penjualan(master_penjualan, detail_penjualan) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah",
      data: master_penjualan,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          // insert detail
          var data_detail = { data: detail_penjualan };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail",
            data: data_detail,
          });

          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          history.back();
        } else {
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_jual.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_jual);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
        }
      },
    });
  }

  function tambah_pembayaran(master_pembayaran, detail_pembayaran) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah_pembayaran",
      data: master_pembayaran,
      success: function () {
        // insert detail
        var data_detail_pembayaran = { data: detail_pembayaran };
        $.ajax({
          type: "POST",
          url: "aksi_tambah_detail_pembayaran",
          data: data_detail_pembayaran,
        });
      },
    });
  }

  // $(".simpan_penjualan").click(function (e) {
  //   e.preventDefault();
  //   var data = [];
  //   var total_ekor = 0;
  //   var total_kg = 0;
  //   var subtotal = 0;
  //   var total_bayar = 0;
  //   var tgl_jual = $("#tgl_jual").val();

  //   $("#penjualan_detail tr").each(function (row, tr) {
  //     if ($(tr).find("td:eq(0)").text() == "") {
  //     } else {
  //       var sub = {
  //         no_transaksi: $("#no_transaksi").val(),
  //         no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
  //         id_customer: $(tr).find("td:eq(2) .id_customer").text(),
  //         ekor: $(tr).find("td:eq(3)").text(),
  //         kg: $(tr).find("td:eq(4)").text(),
  //         hrg_ayam: $(tr).find("td:eq(5) .hrg_ayam").text(),
  //         subtotal: $(tr).find("td:eq(6) .subtotal").text(),
  //         jml_bayar: $(tr).find("td:eq(7) .jml_bayar").text(),
  //         tgl_jual: tgl_jual,
  //       };

  //       data.push(sub);

  //       if ($(tr).find("td:eq(3)").text()) {
  //         total_ekor += parseInt($(tr).find("td:eq(3)").text());
  //       }
  //       total_kg += parseFloat($(tr).find("td:eq(4)").text());
  //       subtotal += parseFloat($(tr).find("td:eq(6) .subtotal").text());
  //       if ($(tr).find("td:eq(7)").text()) {
  //         total_bayar += parseFloat($(tr).find("td:eq(7) .jml_bayar").text());
  //       }
  //     }
  //   });

  //   var c = confirm("Apakah anda yakin ?");
  //   if (c == true) {
  //     var data_penjualan = {
  //       no_transaksi: $("#no_transaksi").val(),
  //       tgl_jual: tgl_jual,
  //       ekor: total_ekor,
  //       kg: total_kg,
  //       total_piutang: subtotal,
  //       total_bayar: total_bayar,
  //     };

  //     $.ajax({
  //       type: "POST",
  //       url: "aksi_tambah",
  //       data: data_penjualan,
  //       success: function (errors) {
  //         if ($.isEmptyObject(errors)) {
  //           toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
  //             progressBar: true,
  //             positionClass: "toast-top-right",
  //             timeOut: "2500",
  //           });
  //           history.back();
  //         } else {
  //           error = JSON.parse(errors);
  //           $("#no_transaksi.invalid-feedback")
  //             .css("display", "block")
  //             .html(error.no_transaksi);
  //           $("#tgl_jual.invalid-feedback")
  //             .css("display", "block")
  //             .html(error.tgl_jual);

  //           toastr.error("Data Gagal Ditambahkan !", "Gagal", {
  //             progressBar: true,
  //             positionClass: "toast-top-right",
  //             timeOut: "2500",
  //           });
  //         }
  //       },
  //     });

  //     var data_detail = { data: data };

  //     $.ajax({
  //       type: "POST",
  //       url: "aksi_tambah_detail",
  //       data: data_detail,
  //     });
  //   }
  // });

  $(".simpan_penjualan").click(function (e) {
    e.preventDefault();
    // INSERT PEnjualAN
    var total_ekor = 0;
    var total_kg = 0;
    var subtotal = 0;
    var tgl_jual = $(".tgl_jual").val();
    var detail_penjualan = [];
    // var total_piutang = 0;
    // var total_bayar = 0;
    // var total_deposit = 0;
    var detail_pembayaran = [];
    $("#penjualan_detail tr").each(function (row, tr) {
      if ($(tr).find("td:eq(1)").text() == "") {
      } else {
        // PEnjualAN
        // MASTER
        if ($(tr).find("td:eq(4)").text()) {
          total_ekor += parseInt($(tr).find("td:eq(4)").text());
        }
        if ($(tr).find("td:eq(5)").text()) {
          total_kg += parseFloat($(tr).find("td:eq(5)").text());
        }
        subtotal += parseFloat($(tr).find("td:eq(7) .subtotal").text());
        // DETAIL
        var sub = {
          no_transaksi: $("#no_transaksi").val(),
          no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
          id_customer: $(tr).find("td:eq(2) .id_customer").text(),
          category: $(tr).find("td:eq(3)").text(),
          ekor: $(tr).find("td:eq(4)").text(),
          kg: $(tr).find("td:eq(5)").text(),
          hrg_ayam: $(tr).find("td:eq(6) .hrg_ayam").text(),
          subtotal: $(tr).find("td:eq(7) .subtotal").text(),
        };
        detail_penjualan.push(sub);

        // PEMBAYARAN
        // MASTER
        if ($(tr).find("td:eq(2) .piutang").text()) {
          total_piutang +=
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .piutang").text());
        }
        if ($(tr).find("td:eq(7) .jml_bayar").text()) {
          total_bayar += parseFloat($(tr).find("td:eq(7) .jml_bayar").text());
        }
        if ($(tr).find("td:eq(2) .deposit").text()) {
          total_deposit += parseFloat($(tr).find("td:eq(2) .deposit").text());
        }

        // DETAIL
        var sub_byr = {
          tgl_bayar: tgl_jual,
          id_customer: $(tr).find("td:eq(2) .id_customer").text(),
          tagihan: $(tr).find("td:eq(6) .subtotal").text(),
          piutang:
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .piutang").text()),
          piutang_customer: $(tr).find("td:eq(2) .piutang").text(),
          jml_bayar: $(tr).find("td:eq(7) .jml_bayar").text(),
          deposit: $(tr).find("td:eq(2) .deposit").text(),
        };
        detail_pembayaran.push(sub_byr);
      }
    });

    // INSERT PEnjualAN
    var master_penjualan = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_jual: tgl_jual,
      ekor: total_ekor,
      kg: total_kg,
      harga: subtotal / total_kg,
      total_piutang: subtotal,
    };

    // INSERT PEMBAYARAN
    var master_pembayaran = {
      tgl_bayar: tgl_jual,
      total_piutang: total_piutang,
      total_bayar: total_bayar,
      total_deposit: total_deposit,
    };

    var c = confirm("Apakah anda yakin ?");
    if (c == true) {
      tambah_pembayaran(master_pembayaran, detail_pembayaran);
      tambah_penjualan(master_penjualan, detail_penjualan);
    }
  });

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tgl_jual").val("2022-02-26").trigger("change");

    const dets = [
      {
        no: "1",
        id_customer: "1",
        piutang: 0,
        deposit: "50214.0000126196",
        nama: "B. ENTUM",
        ekor: "53",
        kg: "96.6",
        hrg_ayam: "21600",
        subtotal: "2086560",
        jml_bayar: "2086000",
      },
      {
        no: "2",
        id_customer: "2",
        piutang: "1475390",
        deposit: 0,
        nama: "P. GEMA",
        ekor: "73",
        kg: "109.6",
        hrg_ayam: "22100",
        subtotal: "2422160",
        jml_bayar: "2444000",
      },
      {
        no: "3",
        id_customer: "3",
        piutang: "44462400",
        deposit: 0,
        nama: "B. ANDON (B)",
        ekor: "154",
        kg: "400",
        hrg_ayam: "22400",
        subtotal: "8960000",
        jml_bayar: "8960000",
      },
      {
        no: "4",
        id_customer: "6",
        piutang: 0,
        deposit: "7632490",
        nama: "KANTOR",
        ekor: "26",
        kg: "38",
        hrg_ayam: "22400",
        subtotal: "851200",
        jml_bayar: "",
      },
      {
        no: "5",
        id_customer: "7",
        piutang: "2024710",
        deposit: 0,
        nama: "P. HASAN",
        ekor: "9",
        kg: "15.7",
        hrg_ayam: "22700",
        subtotal: "356390",
        jml_bayar: "",
      },
      {
        no: "6",
        id_customer: "9",
        piutang: "203790",
        deposit: 0,
        nama: "HARDI/BAKAR",
        ekor: "6",
        kg: "6.1",
        hrg_ayam: "22700",
        subtotal: "138470",
        jml_bayar: "139000",
      },
      {
        no: "7",
        id_customer: "10",
        piutang: "3741150",
        deposit: 0,
        nama: "P. OYO",
        ekor: "17",
        kg: "30",
        hrg_ayam: "22700",
        subtotal: "681000",
        jml_bayar: "1338000",
      },
      {
        no: "8",
        id_customer: "11",
        piutang: 0,
        deposit: "20170.0000000001",
        nama: "P. ALIF",
        ekor: "13",
        kg: "17",
        hrg_ayam: "22700",
        subtotal: "385900",
        jml_bayar: "386000",
      },
      {
        no: "9",
        id_customer: "13",
        piutang: "1145290",
        deposit: 0,
        nama: "P. ASEP",
        ekor: "7",
        kg: "7.6",
        hrg_ayam: "23000",
        subtotal: "174800",
        jml_bayar: "171000",
      },
      {
        no: "10",
        id_customer: "14",
        piutang: 0,
        deposit: "1820",
        nama: "P. DEDI",
        ekor: "15",
        kg: "20.5",
        hrg_ayam: "22700",
        subtotal: "465350",
        jml_bayar: "465000",
      },
      {
        no: "11",
        id_customer: "15",
        piutang: "230380",
        deposit: 0,
        nama: "B. NENENG",
        ekor: "5",
        kg: "7",
        hrg_ayam: "22700",
        subtotal: "158900",
        jml_bayar: "159000",
      },
      {
        no: "12",
        id_customer: "17",
        piutang: "1061010",
        deposit: 0,
        nama: "P. EDI",
        ekor: "3",
        kg: "6.1",
        hrg_ayam: "23000",
        subtotal: "140300",
        jml_bayar: "140300",
      },
      {
        no: "13",
        id_customer: "18",
        piutang: 0,
        deposit: "13390",
        nama: "B. RAHMAT",
        ekor: "34",
        kg: "64.8",
        hrg_ayam: "20000",
        subtotal: "1296000",
        jml_bayar: "",
      },
      {
        no: "14",
        id_customer: "19",
        piutang: "673140",
        deposit: 0,
        nama: "ADE MIQDAD",
        ekor: "18",
        kg: "28.3",
        hrg_ayam: "22200",
        subtotal: "628260",
        jml_bayar: "",
      },
      {
        no: "15",
        id_customer: "20",
        piutang: "89730",
        deposit: 0,
        nama: "E",
        ekor: "1",
        kg: "2",
        hrg_ayam: "",
        subtotal: "0",
        jml_bayar: "",
      },
      {
        no: "16",
        id_customer: "21",
        piutang: "130260",
        deposit: 0,
        nama: "B. KIKI",
        ekor: "2",
        kg: "2.3",
        hrg_ayam: "23000",
        subtotal: "52900",
        jml_bayar: "53000",
      },
      {
        no: "17",
        id_customer: "22",
        piutang: "195310",
        deposit: 0,
        nama: "B. AGUNG",
        ekor: "20",
        kg: "20",
        hrg_ayam: "22700",
        subtotal: "454000",
        jml_bayar: "454000",
      },
      {
        no: "18",
        id_customer: "24",
        piutang: "36999.9999999999",
        deposit: 0,
        nama: "P. TARWIN",
        ekor: "5",
        kg: "8.1",
        hrg_ayam: "23000",
        subtotal: "186300",
        jml_bayar: "186000",
      },
      {
        no: "19",
        id_customer: "29",
        piutang: "348940",
        deposit: 0,
        nama: "A-B",
        ekor: "4",
        kg: "4.7",
        hrg_ayam: "23000",
        subtotal: "108100",
        jml_bayar: "108000",
      },
      {
        no: "20",
        id_customer: "32",
        piutang: "103470",
        deposit: 0,
        nama: "M. UHA",
        ekor: "2",
        kg: "3.2",
        hrg_ayam: "22400",
        subtotal: "71680",
        jml_bayar: "85000",
      },
      {
        no: "21",
        id_customer: "34",
        piutang: "6891090",
        deposit: 0,
        nama: "KHOLIK",
        ekor: "18",
        kg: "30.2",
        hrg_ayam: "22700",
        subtotal: "685540",
        jml_bayar: "2500000",
      },
      {
        no: "22",
        id_customer: "37",
        piutang: "57840",
        deposit: 0,
        nama: "M. UJANG",
        ekor: "10",
        kg: "13",
        hrg_ayam: "23000",
        subtotal: "299000",
        jml_bayar: "350000",
      },
      {
        no: "23",
        id_customer: "40",
        piutang: 0,
        deposit: "2070",
        nama: "P. TOHA",
        ekor: "5",
        kg: "7.5",
        hrg_ayam: "22700",
        subtotal: "170250",
        jml_bayar: "170000",
      },
    ];

    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#penjualan_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_customer">' +
          det.id_customer +
          "</span>" +
          '<span class="d-none piutang">' +
          det.piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td class="text-center">' +
          det.ekor +
          "</td>" +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });
  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tgl_jual").val("2022-02-27").trigger("change");

    const dets = [
      {
        no: "1",
        id_customer: "1",
        piutang: "",
        deposit: "49654.0000126198",
        nama: "B. ENTUM",
        ekor: "63",
        kg: "115.7",
        hrg_ayam: "21600",
        subtotal: "2499120",
        jml_bayar: "2499000",
      },
      {
        no: "2",
        id_customer: "2",
        piutang: "1453550",
        deposit: "",
        nama: "P. GEMA",
        ekor: "100",
        kg: "153.2",
        hrg_ayam: "22100",
        subtotal: "3385720",
        jml_bayar: "3400000",
      },
      {
        no: "3",
        id_customer: "3",
        piutang: "44462400",
        deposit: "",
        nama: "B. ANDON (B)",
        ekor: "117",
        kg: "300",
        hrg_ayam: "22400",
        subtotal: "6720000",
        jml_bayar: "4980000",
      },
      {
        no: "4",
        id_customer: "6",
        piutang: "",
        deposit: "6781290",
        nama: "KANTOR",
        ekor: "27",
        kg: "38.5",
        hrg_ayam: "22400",
        subtotal: "862400",
        jml_bayar: "",
      },
      {
        no: "5",
        id_customer: "7",
        piutang: "2381100",
        deposit: "",
        nama: "P. HASAN",
        ekor: "11",
        kg: "15.8",
        hrg_ayam: "22700",
        subtotal: "358660",
        jml_bayar: "",
      },
      {
        no: "6",
        id_customer: "9",
        piutang: "203260",
        deposit: "",
        nama: "HARDI/BAKAR",
        ekor: "3",
        kg: "3.8",
        hrg_ayam: "23000",
        subtotal: "87400",
        jml_bayar: "87500",
      },
      {
        no: "7",
        id_customer: "10",
        piutang: "3084150",
        deposit: "",
        nama: "P. OYO",
        ekor: "16",
        kg: "30",
        hrg_ayam: "22700",
        subtotal: "681000",
        jml_bayar: "681000",
      },
      {
        no: "8",
        id_customer: "11",
        piutang: "",
        deposit: "20270.0000000001",
        nama: "P. ALIF",
        ekor: "13",
        kg: "17.3",
        hrg_ayam: "22700",
        subtotal: "392710",
        jml_bayar: "359000",
      },
      {
        no: "9",
        id_customer: "13",
        piutang: "1149090",
        deposit: "",
        nama: "P. ASEP",
        ekor: "8",
        kg: "9.5",
        hrg_ayam: "23000",
        subtotal: "218500",
        jml_bayar: "170000",
      },
      {
        no: "10",
        id_customer: "15",
        piutang: "230280",
        deposit: "",
        nama: "B. NENENG",
        ekor: "8",
        kg: "11.2",
        hrg_ayam: "22700",
        subtotal: "254240",
        jml_bayar: "254000",
      },
      {
        no: "11",
        id_customer: "17",
        piutang: "1061010",
        deposit: "",
        nama: "P. EDI",
        ekor: "7",
        kg: "10",
        hrg_ayam: "23000",
        subtotal: "230000",
        jml_bayar: "230000",
      },
      {
        no: "12",
        id_customer: "19",
        piutang: "1301400",
        deposit: "",
        nama: "ADE MIQDAD",
        ekor: "15",
        kg: "23.4",
        hrg_ayam: "22200",
        subtotal: "519480",
        jml_bayar: "",
      },
      {
        no: "13",
        id_customer: "21",
        piutang: "130160",
        deposit: "",
        nama: "B. KIKI",
        ekor: "2",
        kg: "2",
        hrg_ayam: "23000",
        subtotal: "46000",
        jml_bayar: "46000",
      },
      {
        no: "14",
        id_customer: "22",
        piutang: "195310",
        deposit: "",
        nama: "B. AGUNG",
        ekor: "15",
        kg: "20.4",
        hrg_ayam: "22700",
        subtotal: "463080",
        jml_bayar: "463000",
      },
      {
        no: "15",
        id_customer: "25",
        piutang: "",
        deposit: "1330",
        nama: "OMPONG",
        ekor: "7",
        kg: "12.5",
        hrg_ayam: "23000",
        subtotal: "287500",
        jml_bayar: "287500",
      },
      {
        no: "16",
        id_customer: "26",
        piutang: "",
        deposit: "5470",
        nama: "CIMOT",
        ekor: "15",
        kg: "42",
        hrg_ayam: "22400",
        subtotal: "940800",
        jml_bayar: "940000",
      },
      {
        no: "17",
        id_customer: "28",
        piutang: "4008290",
        deposit: "",
        nama: "CIKEUN ABANG",
        ekor: "50",
        kg: "66.3",
        hrg_ayam: "22800",
        subtotal: "1511640",
        jml_bayar: "219000",
      },
      {
        no: "18",
        id_customer: "29",
        piutang: "349040",
        deposit: "",
        nama: "A-B",
        ekor: "4",
        kg: "4.9",
        hrg_ayam: "23000",
        subtotal: "112700",
        jml_bayar: "113000",
      },
      {
        no: "19",
        id_customer: "31",
        piutang: "",
        deposit: "1760",
        nama: "MIX",
        ekor: "163",
        kg: "273.7",
        hrg_ayam: "22000",
        subtotal: "6021400",
        jml_bayar: "6021000",
      },
      {
        no: "20",
        id_customer: "32",
        piutang: "90149.9999999999",
        deposit: "",
        nama: "M. UHA",
        ekor: "5",
        kg: "7.8",
        hrg_ayam: "22200",
        subtotal: "173160",
        jml_bayar: "174000",
      },
      {
        no: "21",
        id_customer: "34",
        piutang: "5076630",
        deposit: "",
        nama: "KHOLIK",
        ekor: "21",
        kg: "32.8",
        hrg_ayam: "22700",
        subtotal: "744560",
        jml_bayar: "177500",
      },
      {
        no: "22",
        id_customer: "37",
        piutang: "6840",
        deposit: "",
        nama: "M. UJANG",
        ekor: "15",
        kg: "20.8",
        hrg_ayam: "23000",
        subtotal: "478400",
        jml_bayar: "478000",
      },
      {
        no: "23",
        id_customer: "39",
        piutang: "82000",
        deposit: "",
        nama: "A DEDI",
        ekor: "4",
        kg: "5.2",
        hrg_ayam: "22200",
        subtotal: "115440",
        jml_bayar: "",
      },
      {
        no: "24",
        id_customer: "40",
        piutang: "",
        deposit: "1820",
        nama: "P. TOHA",
        ekor: "12",
        kg: "15.2",
        hrg_ayam: "22700",
        subtotal: "345040",
        jml_bayar: "343000",
      },
    ];
    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#penjualan_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_customer">' +
          det.id_customer +
          "</span>" +
          '<span class="d-none piutang">' +
          det.piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td class="text-center">' +
          det.ekor +
          "</td>" +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });
});
