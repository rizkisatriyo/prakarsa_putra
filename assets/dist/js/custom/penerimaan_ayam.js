$(document).ready(function () {
  $("#penerimaan_ayam").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tgl_diterima").change(function (e) {
    e.preventDefault();
    var tgl_diterima = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("TRM" + tgl_diterima);
    $("#no_checker").val("CKB" + tgl_diterima);
    $(".txt_no_checker").text("#CKB" + tgl_diterima);
  });

  $(".select2bs4#pb_id_broker").select2({
    theme: "bootstrap4",
  });
  $(".select2bs4#pb_id_supplier").select2({
    theme: "bootstrap4",
  });

  $("#broker_kg").keyup(function (e) {
    let tb_kg = $("#tb_kg").val();
    let kg = $(this).val();
    $("#tot_beli_kg")
      // .mask('#.##0,00', {reverse: true})
      .val(tb_kg - kg)
      .trigger("input");
  });

  // $("#pb_kg").mask("###.###.###", { reverse: true });
  // $("#pb_hrg_ayam").mask("###.###.###", { reverse: true });

  // $("#pb_kg, #pb_hrg_ayam").keyup(function (e) {
  //   var kg = parseFloat($("#pb_kg").cleanVal());
  //   var hrg_ayam = parseFloat($("#pb_hrg_ayam").cleanVal());

  //   $("#pb_subtotal")
  //     .mask("###.###.###", { reverse: true })
  //     .val(kg * hrg_ayam)
  //     .trigger("input");
  // });

  // $("#tambah_detail").click(function (e) {
  //   e.preventDefault();

  //   var no = $("#pembelian_detail tbody tr").length + 1;
  //   var no_trxdetail =
  //     "PBN" +
  //     moment($("#tgl_beli").val()).format("YYYYMMDD") +
  //     no.toString().padStart(3, "0");
  //   var id_supplier = $("#id_supplier").val();
  //   var hutang = $("#id_supplier option:selected").data("hutang");
  //   var deposit = $("#id_supplier option:selected").data("deposit");
  //   var nama = $("#id_supplier option:selected").text();
  //   var ekor = $("#ekor").val();
  //   var kg = $("#kg").val();
  //   var hrg_ayam = $("#hrg_ayam");
  //   var subtotal = $("#subtotal");
  //   var jml_bayar = $("#jml_bayar");

  //   if (
  //     id_supplier != "" &&
  //     hrg_ayam.val() != "" &&
  //     kg != "" &&
  //     subtotal.val() != ""
  //     // jml_bayar.val() != ""
  //   ) {
  //     $("#pembelian_detail tbody:last-child").append(
  //       "<tr>" +
  //         '<td class="text-center" width="50">' +
  //         no +
  //         "</td>" +
  //         '<td class="text-center">#' +
  //         no_trxdetail +
  //         "</td>" +
  //         '<td><span class="d-none id_supplier">' +
  //         id_supplier +
  //         "</span>" +
  //         '<span class="d-none hutang">' +
  //         hutang +
  //         "</span>" +
  //         '<span class="d-none deposit">' +
  //         deposit +
  //         "</span>" +
  //         nama +
  //         "</td>" +
  //         '<td class="text-center">' +
  //         ekor +
  //         "</td>" +
  //         '<td class="text-center">' +
  //         kg +
  //         "</td>" +
  //         '<td><span class="d-none hrg_ayam">' +
  //         hrg_ayam.cleanVal() +
  //         '</span><span class="float-left">Rp.</span><span class="float-right">' +
  //         new Intl.NumberFormat("en-ID", {
  //           style: "currency",
  //           currency: "IDR",
  //         })
  //           .format(hrg_ayam.cleanVal())
  //           .replace(/[IDR]/gi, "")
  //           .replace(/(\.+\d{2})/, "")
  //           .trimLeft() +
  //         ",-</span></td>" +
  //         '<td><span class="d-none subtotal">' +
  //         subtotal.cleanVal() +
  //         '</span><span class="float-left">Rp.</span><span class="float-right">' +
  //         new Intl.NumberFormat("en-ID", {
  //           style: "currency",
  //           currency: "IDR",
  //         })
  //           .format(subtotal.cleanVal())
  //           .replace(/[IDR]/gi, "")
  //           .replace(/(\.+\d{2})/, "")
  //           .trimLeft() +
  //         ",-</span></td>" +
  //         '<td><span class="d-none jml_bayar">' +
  //         jml_bayar.cleanVal() +
  //         '</span><span class="float-left">Rp.</span><span class="float-right">' +
  //         new Intl.NumberFormat("en-ID", {
  //           style: "currency",
  //           currency: "IDR",
  //         })
  //           .format(jml_bayar.cleanVal())
  //           .replace(/[IDR]/gi, "")
  //           .replace(/(\.+\d{2})/, "")
  //           .trimLeft() +
  //         ",-</span></td>" +
  //         // '<td class="text-center">' +
  //         // '<div class="btn-group">' +
  //         // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
  //         // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
  //         // "Apakah anda yakin?" +
  //         // ')"><i class="fas fa-trash"></i></button>' +
  //         // "</div>" +
  //         // "</td>" +
  //         "</tr>"
  //     );

  //     reset_form();

  //     toastr.success("Data berhasil ditambahkan !", "Berhasil", {
  //       progressBar: true,
  //       positionClass: "toast-top-right",
  //       timeOut: "5000",
  //     });

  //     const ftgl_beli = $("#tgl_beli").val();
  //     const fno_transaksi = $("#no_transaksi").val();
  //     $("#tgl_beli").change(function (e) {
  //       e.preventDefault();
  //       alert(
  //         "Mohon tidak mengatur tanggal setelah menginputkan Supplier !\nSilahkan refresh halaman ini untuk mengatur tanggal"
  //       );
  //       $("#tgl_beli").val(ftgl_beli);
  //       $("#no_transaksi").val(fno_transaksi);
  //     });
  //   } else {
  //     toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
  //       progressBar: true,
  //       positionClass: "toast-top-right",
  //       timeOut: "5000",
  //     });
  //   }
  // });

  // function reset_form() {
  //   $("#id_supplier").val("").trigger("change");
  //   $("#ekor").val("");
  //   $("#kg").val("");
  //   $("#hrg_ayam").val("");
  //   $("#subtotal").val("");
  //   $("#jml_bayar").val("");
  //   $(".select2bs4").select2("open");
  //   $(".hutang_supplier").addClass("d-none");
  //   $(".deposit_supplier").addClass("d-none");
  //   $("#jml_bayar").removeClass("font-weight-bold text-success");
  // }

  $(".tambah_baru").each(function (index, element) {
    $(element).click(function (e) {
      e.preventDefault();

      var id_kandang = $(element).data("id_kandang");

      $("#table-" + id_kandang + " tbody").append(
        '<tr><td><input type="number" name="ekor" class="form-control form-control-sm text-right ekor" id="ekor"></td><td><input type="number" name="kg" class="form-control form-control-sm text-right kg" id="kg"></td></tr>'
      );
    });
  });

  $("#total_ekor").change(function (e) {
    e.preventDefault();
    $("#bongkar_ekor").val($(this).val());
  });

  $("#total_kg").change(function (e) {
    e.preventDefault();
    $("#bongkar_kg").val($(this).val());
  });

  $(document).on("keyup", ".ekor, #kirim_ekor, #mati_ekor", function () {
    var ekorSum = [...$(".ekor")]
      .map((input) => Number(input.value))
      .reduce((a, b) => a + b, 0);
    $("#total_ekor").val(ekorSum).trigger("change");

    var ekor_beli = parseInt($("#total_beli_ekor").val());
    var kirim_ekor = 0;
    if (parseInt($("#kirim_ekor").val())) {
      kirim_ekor = parseInt($("#kirim_ekor").val());
    }
    var mati_ekor = 0;
    if (parseInt($("#mati_ekor").val())) {
      mati_ekor = parseInt($("#mati_ekor").val());
    }
    var susut_ekor = parseInt(ekor_beli) - ekorSum - kirim_ekor - mati_ekor;
    $("#susut_ekor").val(parseInt(susut_ekor));

    var ts_ekor =
      parseInt($("#bongkar_ekor").val()) + parseInt($("#sisa_ekor").val());
    $("#ts_ekor").val(ts_ekor);
  });

  $(document).on("keyup", ".kg, #kirim_kg, #mati_kg, #broker_kg", function () {
    var kgSum = [...$(".kg")]
      .map((input) => Number(input.value))
      .reduce((a, b) => a + b, 0);
    $("#total_kg").val(parseFloat(kgSum).toFixed(2)).trigger("change");

    var kg_beli = parseFloat($("#tot_beli_kg").val());
    var kirim_kg = 0;
    if (parseFloat($("#kirim_kg").val())) {
      kirim_kg = parseFloat($("#kirim_kg").val());
    }
    var susut_kg = parseFloat(kg_beli) - kgSum;
    $("#susut_kg").val(parseFloat(susut_kg).toFixed(2));

    var presentase_susut_kg = (susut_kg / kg_beli) * 100;
    $("#presentase_susut_kg").val(parseFloat(presentase_susut_kg).toFixed(2));

    var ts_kg =
      parseFloat($("#bongkar_kg").val()) + parseFloat($("#sisa_kg").val());
    $("#ts_kg").val(ts_kg);
  });

  $(".simpan").click(function (e) {
    e.preventDefault();

    // var data_penerimaan = {
    //   no_transaksi: $("#no_transaksi").val(),
    //   no_transaksi_pembelian: $("#no_transaksi_pembelian").val(),
    //   tgl_diterima: $("#tgl_diterima").val(),
    //   bongkar_ekor: $("#bongkar_ekor").val(),
    //   bongkar_kg: $("#bongkar_kg").val(),
    //   kirim_ekor: $("#kirim_ekor").val(),
    //   kirim_kg: $("#kirim_kg").val(),
    //   mati_ekor: $("#mati_ekor").val(),
    //   mati_kg: $("#mati_kg").val(),
    // };

    // var data_penerimaan_checker = {
    //   no_transaksi_penerimaan: $("#no_transaksi").val(),
    //   total_ekor: $("#total_ekor").val(),
    //   total_kg: $("#total_kg").val(),
    // };

    // var data_penerimaan_checker_detail = {
    //   id_penerimaan_checker: $("#id_penerimaan_checker").val(),
    //   id_kandang: $("#id_kandang").val(),
    //   ekor: $("#ekor").val(),
    //   kg: $("#kg").val(),
    // };

    var data_penerimaan_checker_detail = [];
    $(".table-checker").each(function (index, element) {
      var id_kandang = $(element).data("id_kandang");
      var row = $(element).find("tbody tr");
      $(row).each(function (i, e) {
        if ($(e).find("#ekor").val() != "" && $(e).find("#kg").val() != "") {
          var detail = {
            no_checker: $("#no_checker").val(),
            id_kandang: id_kandang,
            ekor: $(e).find("#ekor").val(),
            kg: $(e).find("#kg").val(),
          };

          data_penerimaan_checker_detail.push(detail);
        }
      });
    });

    // var penerimaan_susut = {
    //   ekor: $("#ekor").val(),
    //   presentasi_ekor: $("#presentasi_ekor").val(),
    //   kg: $("#kg").val(),
    //   presentasi_kg: $("#presentasi_kg").val(),
    // };

    var form_data = {
      no_transaksi: $("#no_transaksi").val(),
      no_transaksi_pembelian: $("#no_transaksi_pembelian").val(),
      tgl_diterima: $("#tgl_diterima").val(),
      bongkar_ekor: $("#bongkar_ekor").val(),
      bongkar_kg: $("#bongkar_kg").val(),
      broker_kg: $("#broker_kg").val(),
      kirim_ekor: $("#kirim_ekor").val(),
      kirim_kg: $("#kirim_kg").val(),
      mati_ekor: $("#mati_ekor").val(),
      mati_kg: $("#mati_kg").val(),
      no_checker: $("#no_checker").val(),
      total_ekor: $("#total_ekor").val(),
      total_kg: $("#total_kg").val(),
      detail_checker: data_penerimaan_checker_detail,
      no_transaksi_penerimaan: $("#no_transaksi").val(),
      ekor: $("#susut_ekor").val(),
      // presentase_ekor: $("#presentase_susut_ekor").val(),
      kg: $("#susut_kg").val(),
      presentase_kg: $("#presentase_susut_kg").val(),
    };

    if (
      $("#no_transaksi").val() != "" &&
      $("#no_transaksi_pembelian").val() != "" &&
      $("#tgl_diterima").val() != ""
    ) {
      $.ajax({
        type: "POST",
        url: "/prakarsa_putra/penerimaan_ayam/aksi_tambah",
        data: form_data,
        success: function (errors) {
          if ($.isEmptyObject(errors)) {
            tambah_tersedia_stok();
            toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
              progressBar: true,
              positionClass: "toast-top-right",
              timeOut: "2500",
            });
            document.location.href = "/prakarsa_putra/penerimaan_ayam";
          } else {
            error = JSON.parse(errors);
            if (error.no_transaksi) {
              $("#no_transaksi").addClass("is-invalid");
              $("#no_transaksi.invalid-feedback")
                .css("display", "block")
                .html(error.no_transaksi);
            }
            if (error.no_transaksi_pembelian) {
              $("#no_transaksi_pembelian").addClass("is-invalid");
              $("#no_transaksi_pembelian.invalid-feedback")
                .css("display", "block")
                .html(error.no_transaksi_pembelian);
            }
            if (error.tgl_diterima) {
              $("#tgl_diterima").addClass("is-invalid");
              $("#tgl_diterima.invalid-feedback")
                .css("display", "block")
                .html(error.tgl_diterima);
            }
            if (error.no_checker) {
              $("#no_checker").addClass("is-invalid");
              $("#no_checker.invalid-feedback")
                .css("display", "block")
                .html(error.no_checker);
            }
            if (error.total_ekor) {
              $("#total_ekor").addClass("is-invalid");
              $("#total_ekor.invalid-feedback")
                .css("display", "block")
                .html(error.total_ekor);
            }
            if (error.total_kg) {
              $("#total_kg").addClass("is-invalid");
              $("#total_kg.invalid-feedback")
                .css("display", "block")
                .html(error.total_kg);
            }

            toastr.error("Data Gagal Ditambahkan !", "Gagal", {
              progressBar: true,
              positionClass: "toast-top-right",
              timeOut: "2500",
            });
          }
        },
      });
    }
  });

  function tambah_tersedia_stok() {
    var tgl_diterima = moment($("#tgl_diterima").val()).format("YYYYMMDD");
    const harga =
      parseFloat($("#ts_jumlah").val()) / parseFloat($("#t_kg").val());
    const data = {
      no_transaksi: "STM" + tgl_diterima,
      no_checker: $("#no_checker").val(),
      tanggal: $("#tgl_diterima").val(),
      ekor: $("#ts_ekor").val(),
      kg: $("#ts_kg").val(),
      harga: harga,
      jumlah: $("#ts_jumlah").val(),
    };

    $.ajax({
      type: "POST",
      url: "/prakarsa_putra/penerimaan_ayam/aksi_tambah_tersedia_stok",
      data: data,
    });
  }

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tgl_diterima").val("2022-02-25").trigger("change");
    $("#broker_kg").val(3200).trigger("keyup");

    const cks = [
      { id_kandang: 2, ekor: 184, kg: 294.9 },
      { id_kandang: 4, ekor: 120, kg: 203.2 },
      { id_kandang: 5, ekor: 154, kg: 399.5 },
    ];

    cks.forEach((ck) => {
      $("#table-" + ck.id_kandang + " tbody")
        .find("#ekor")
        .val(ck.ekor)
        .trigger("keyup");
      $("#table-" + ck.id_kandang + " tbody")
        .find("#kg")
        .val(ck.kg)
        .trigger("keyup");
      // $("#table-" + ck.id_kandang + " tbody").append(
      //   '<tr><td><input type="number" name="ekor" value="' +
      //     ck.ekor +
      //     '" class="form-control form-control-sm text-center ekor" id="ekor"></td><td><input type="number" name="kg" value="' +
      //     ck.kg +
      //     '" class="form-control form-control-sm text-center kg" id="kg"></td></tr>'
      // );
    });
  });
  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tgl_diterima").val("2022-02-26").trigger("change");
    $("#broker_kg").val(2500).trigger("keyup");

    const cks = [
      { id_kandang: "3", ekor: "113", kg: "168.4" },
      { id_kandang: "3", ekor: "253", kg: "390.2" },
      { id_kandang: "3", ekor: "29", kg: "64" },
      { id_kandang: "5", ekor: "154", kg: "398.2" },
    ];
    cks.forEach((ck) => {
      $("#table-" + ck.id_kandang + " tbody").append(
        "<tr>" +
          '<td><input type="number" name="ekor" value="' +
          ck.ekor +
          '" class="form-control form-control-sm text-center ekor" id="ekor"></td>' +
          '<td><input type="number" name="kg" value="' +
          ck.kg +
          '" class="form-control form-control-sm text-center kg" id="kg"></td></tr>'
      );

      $(".ekor").trigger("keyup");
      $(".kg").trigger("keyup");
    });
  });
});
