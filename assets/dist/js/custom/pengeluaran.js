$(document).ready(function () {
  $("#pengeluaran").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tanggal").change(function (e) {
    e.preventDefault();
    var tanggal = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PLR" + tanggal);
    get_setoran();
  });

  $("#id_pengeluaran_akun").select2({
    theme: "bootstrap4",
    placeholder: "Jenis Pengeluaran",
  });
  $("#id_pengeluaran_akun_detail").select2({
    theme: "bootstrap4",
    placeholder: "Akun Pengeluaran",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });

  function get_pengeluaran_akun_detail() {
    $.ajax({
      type: "POST",
      url: "get_pengeluaran_akun_detail",
      data: { id_pengeluaran_akun: $("#id_pengeluaran_akun").val() },
      success: function (response) {
        var dt_acs = JSON.parse(response);
        var acs = [];
        dt_acs.forEach((element) => {
          var data = {
            id: element.id,
            text: element.nama,
          };
          acs.push(data);

          // ADD CUSTOM ATTRIBUTE TO OPTION STILL ERRROR!!!!
          // $.ajax({
          //   type: "POST",
          //   url: "get_saldo_awal",
          //   data: { id: element.id, tanggal: $("#tanggal").val() },
          //   success: function (response) {
          //     console.log(response);
          //   },
          // });
        });

        $("#id_pengeluaran_akun_detail").select2({
          theme: "bootstrap4",
          placeholder: "Akun Pengeluaran",
          data: acs,
        });
      },
    });
  }

  $("#id_pengeluaran_akun").change(function (e) {
    e.preventDefault();
    $("#id_pengeluaran_akun_detail").empty();
    get_pengeluaran_akun_detail();
  });

  $(
    "#jml_bayar, #tot_setor_bank, #setoran_rpa, #setoran_broker, #setor_tabungan, #setor_modal_rpa, #setor_modal_total, #sisa_setoran, #output_pokok"
  ).mask("#.##0", { reverse: true });

  $("#tambah_detail").click(function (e) {
    e.preventDefault();
    get_setoran();

    var no = $("#pengeluaran_detail tbody tr").length + 1;
    var no_trxdetail =
      "PLR" +
      moment($("#tanggal").val()).format("YYYYMMDD") +
      no.toString().padStart(3, "0");
    var id_pengeluaran_akun = $("#id_pengeluaran_akun").val();
    var nama = $("#id_pengeluaran_akun option:selected").text();
    var id_pengeluaran_akun_detail = $("#id_pengeluaran_akun_detail").val();
    var nama_detail = $("#id_pengeluaran_akun_detail option:selected").text();
    var keterangan = $("#keterangan").val();
    var jml_bayar = $("#jml_bayar");

    if (
      id_pengeluaran_akun != "" &&
      id_pengeluaran_akun_detail != "" &&
      jml_bayar.val() != ""
    ) {
      $("#pengeluaran_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun">' +
          id_pengeluaran_akun +
          "</span>" +
          nama +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun_detail">' +
          id_pengeluaran_akun_detail +
          "</span>" +
          nama_detail +
          "</td>" +
          "<td>" +
          keterangan +
          "</td>" +
          '<td><span class="d-none jml_bayar">' +
          jml_bayar.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(jml_bayar.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      reset_form();

      toastr.success("Data berhasil ditambahkan !", "Berhasil", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });

      const ftanggal = $("#tanggal").val();
      const fno_transaksi = $("#no_transaksi").val();
      $("#tanggal").change(function (e) {
        e.preventDefault();
        alert(
          "Mohon tidak mengatur tanggal setelah menginputkan Pengeluaran !\nSilahkan refresh halaman ini untuk mengatur tanggal"
        );
        $("#tanggal").val(ftanggal);
        $("#no_transaksi").val(fno_transaksi);
      });
    } else {
      toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });
    }
  });

  function reset_form() {
    $("#id_pengeluaran_akun").val("").trigger("change");
    $("#id_pengeluaran_akun_detail").val("").trigger("change");
    $("#keterangan").val("");
    $("#jml_bayar").val("");
    $(".select2bs4").select2("open");
  }

  function tambah_pengeluaran(master_pengeluaran, detail_pengeluaran) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah",
      data: master_pengeluaran,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          // insert detail
          var data_detail = { data: detail_pengeluaran };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail",
            data: data_detail,
          });

          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          history.back();
        } else {
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tanggal.invalid-feedback")
            .css("display", "block")
            .html(error.tanggal);
          $("#setor_modal_total.invalid-feedback")
            .css("display", "block")
            .html(error.setor_modal_total);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
        }
      },
    });
  }

  $("#setor_modal_total").keyup(function (e) {
    get_setoran();
  });

  function get_setoran() {
    const tanggal = $("#tanggal").val();
    $.ajax({
      type: "POST",
      url: "get_setoran",
      data: { tanggal: tanggal },
      success: function (response) {
        const setoran = JSON.parse(response);
        const setoran_rpa = parseFloat(setoran.setoran_rpa);
        const setoran_broker = parseFloat(setoran.setoran_broker);
        let setor_modal_total = $("#setor_modal_total").cleanVal();

        var subtotal = 0;
        var detail_pengeluaran = [];
        $("#pengeluaran_detail tr").each(function (row, tr) {
          if ($(tr).find("td:eq(1)").text() == "") {
          } else {
            subtotal += parseFloat($(tr).find("td:eq(5) .jml_bayar").text());
            var sub = {
              id_pengeluaran_akun: $(tr)
                .find("td:eq(2) .id_pengeluaran_akun")
                .text(),
              id_pengeluaran_akun_detail: $(tr)
                .find("td:eq(3) .id_pengeluaran_akun_detail")
                .text(),
              jml_bayar: parseFloat($(tr).find("td:eq(5) .jml_bayar").text()),
            };
            detail_pengeluaran.push(sub);
          }
        });

        var sub_kantor = 0;
        var mob_pemeliharaan = 0;
        var prod_plastik = 0;
        var prod_pemeliharaan = 0;
        detail_pengeluaran.forEach((detail) => {
          if (
            detail.id_pengeluaran_akun == "3" &&
            detail.id_pengeluaran_akun_detail != "13"
          ) {
            sub_kantor += parseFloat(detail.jml_bayar);
          }
          if (detail.id_pengeluaran_akun_detail == "6") {
            mob_pemeliharaan += parseFloat(detail.jml_bayar);
          }
          if (detail.id_pengeluaran_akun_detail == "11") {
            prod_plastik += parseFloat(detail.jml_bayar);
          }
          if (detail.id_pengeluaran_akun_detail == "12") {
            prod_pemeliharaan += parseFloat(detail.jml_bayar);
          }
        });

        var output_pokok =
          subtotal -
          (sub_kantor + (mob_pemeliharaan + prod_plastik + prod_pemeliharaan));
        var tot_setor_bank = setoran_rpa + setoran_broker - output_pokok;
        var setor_tabungan = subtotal - output_pokok;
        var setor_modal_rpa = setoran_rpa - subtotal;
        if (!setor_modal_total) {
          setor_modal_total = 0;
        }
        var sisa_setoran = tot_setor_bank - setor_tabungan - setor_modal_total;

        $("#tot_setor_bank").val(tot_setor_bank.toFixed(0)).trigger("input");
        $("#setoran_rpa").val(setoran_rpa.toFixed(0)).trigger("input");
        $("#setoran_broker").val(setoran_broker.toFixed(0)).trigger("input");
        $("#setor_tabungan").val(setor_tabungan.toFixed(0)).trigger("input");
        $("#setor_modal_rpa").val(setor_modal_rpa.toFixed(0)).trigger("input");
        $("#sisa_setoran").val(sisa_setoran.toFixed(0)).trigger("input");
        $("#output_pokok").val(output_pokok.toFixed(0)).trigger("input");
      },
    });
  }
  get_setoran();

  $(".simpan_pengeluaran").click(function (e) {
    e.preventDefault();
    var subtotal = 0;
    var detail_pengeluaran = [];
    $("#pengeluaran_detail tr").each(function (row, tr) {
      if ($(tr).find("td:eq(1)").text() == "") {
      } else {
        // MASTER
        subtotal += parseFloat($(tr).find("td:eq(5) .jml_bayar").text());
        // DETAIL
        var sub = {
          no_transaksi: $("#no_transaksi").val(),
          no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
          id_pengeluaran_akun_detail: $(tr)
            .find("td:eq(3) .id_pengeluaran_akun_detail")
            .text(),
          keterangan: $(tr).find("td:eq(4)").text(),
          jml_bayar: parseFloat($(tr).find("td:eq(5) .jml_bayar").text()),
        };
        detail_pengeluaran.push(sub);
      }
    });

    // INSERT pengeluaran
    var master_pengeluaran = {
      no_transaksi: $("#no_transaksi").val(),
      tanggal: $(".tanggal").val(),
      total: subtotal,
      tot_setor_bank: $("#tot_setor_bank").cleanVal(),
      setoran_rpa: $("#setoran_rpa").cleanVal(),
      setoran_broker: $("#setoran_broker").cleanVal(),
      setor_tabungan: $("#setor_tabungan").cleanVal(),
      setor_modal_rpa: $("#setor_modal_rpa").cleanVal(),
      setor_modal_total: $("#setor_modal_total").cleanVal(),
      sisa_setoran: $("#sisa_setoran").cleanVal(),
      output_pokok: $("#output_pokok").cleanVal(),
    };

    var c = confirm("Apakah anda yakin ?");
    if (c == true) {
      tambah_pengeluaran(master_pengeluaran, detail_pengeluaran);
    }
  });

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tanggal").val("2022-02-26").trigger("change");
    $("#setor_modal_total").val(53285200).trigger("input");

    const dets = [
      {
        no: "1",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "1",
        nama_detail: "SUPIR",
        keterangan: "",
        jml_bayar: "240000",
      },
      {
        no: "2",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "6",
        nama_detail: "PEMELIHARAAN",
        keterangan: "",
        jml_bayar: "195000",
      },
      {
        no: "3",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "7",
        nama_detail: "UPAH KERJA",
        keterangan: "",
        jml_bayar: "280000",
      },
      {
        no: "4",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "8",
        nama_detail: "UANG MAKAN",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "5",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "10",
        nama_detail: "BENSIN",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "6",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "11",
        nama_detail: "PLASTIK",
        keterangan: "",
        jml_bayar: "53670.76",
      },
      {
        no: "7",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "12",
        nama_detail: "PEMELIHARAAN",
        keterangan: "",
        jml_bayar: "29634.6416666667",
      },
      {
        no: "8",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "14",
        nama_detail: "ATK",
        keterangan: "",
        jml_bayar: "10000",
      },
      {
        no: "9",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "15",
        nama_detail: "GAS LPG",
        keterangan: "",
        jml_bayar: "81866.675",
      },
      {
        no: "10",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "16",
        nama_detail: "PAKAN",
        keterangan: "",
        jml_bayar: "42000",
      },
      {
        no: "11",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "17",
        nama_detail: "LISTRIK",
        keterangan: "",
        jml_bayar: "33510.4462",
      },
      {
        no: "12",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "18",
        nama_detail: "BENGKEL",
        keterangan: "",
        jml_bayar: "28598.6020833333",
      },
      {
        no: "13",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "19",
        nama_detail: "BULU",
        keterangan: "",
        jml_bayar: "25000",
      },
      {
        no: "14",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "20",
        nama_detail: "KOPI",
        keterangan: "",
        jml_bayar: "9899.065",
      },
      {
        no: "15",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "21",
        nama_detail: "ASURANSI",
        keterangan: "",
        jml_bayar: "58354.4408333333",
      },
      {
        no: "16",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "22",
        nama_detail: "THR",
        keterangan: "",
        jml_bayar: "140745",
      },
      {
        no: "17",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "23",
        nama_detail: "CICILAN",
        keterangan: "",
        jml_bayar: "140745",
      },
      {
        no: "18",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "24",
        nama_detail: "CICILAN ANDON",
        keterangan: "",
        jml_bayar: "120000",
      },
      {
        no: "19",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "25",
        nama_detail: "THR BROKER",
        keterangan: "",
        jml_bayar: "160000",
      },
    ];
    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#pengeluaran_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun">' +
          det.id_pengeluaran_akun +
          "</span>" +
          det.nama +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun_detail">' +
          det.id_pengeluaran_akun_detail +
          "</span>" +
          det.nama_detail +
          "</td>" +
          "<td>" +
          det.keterangan +
          "</td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });

  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tanggal").val("2022-02-27").trigger("change");
    $("#setor_modal_total").val(81116800).trigger("input");

    const dets = [
      {
        no: "1",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "1",
        nama_detail: "SUPIR",
        keterangan: "",
        jml_bayar: "80000",
      },
      {
        no: "2",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "2",
        nama_detail: "TKBM",
        keterangan: "",
        jml_bayar: "60000",
      },
      {
        no: "3",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "3",
        nama_detail: "UANG MAKAN",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "4",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "4",
        nama_detail: "KAS",
        keterangan: "",
        jml_bayar: "45000",
      },
      {
        no: "5",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "5",
        nama_detail: "BENSIN",
        keterangan: "",
        jml_bayar: "100000",
      },
      {
        no: "6",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "6",
        nama_detail: "PEMELIHARAAN",
        keterangan: "",
        jml_bayar: "195000",
      },
      {
        no: "7",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "7",
        nama_detail: "UPAH KERJA",
        keterangan: "",
        jml_bayar: "280000",
      },
      {
        no: "8",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "8",
        nama_detail: "UANG MAKAN",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "9",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "9",
        nama_detail: "LEMBUR",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "10",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "10",
        nama_detail: "BENSIN",
        keterangan: "",
        jml_bayar: "20000",
      },
      {
        no: "11",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "11",
        nama_detail: "PLASTIK",
        keterangan: "",
        jml_bayar: "70470.4",
      },
      {
        no: "12",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "12",
        nama_detail: "PEMELIHARAAN",
        keterangan: "",
        jml_bayar: "38910.6666666667",
      },
      {
        no: "13",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "13",
        nama_detail: "UPAH KERJA",
        keterangan: "",
        jml_bayar: "80000",
      },
      {
        no: "14",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "14",
        nama_detail: "ATK",
        keterangan: "",
        jml_bayar: "10000",
      },
      {
        no: "15",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "15",
        nama_detail: "GAS LPG",
        keterangan: "",
        jml_bayar: "107492",
      },
      {
        no: "16",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "16",
        nama_detail: "PAKAN",
        keterangan: "",
        jml_bayar: "42000",
      },
      {
        no: "17",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "17",
        nama_detail: "LISTRIK",
        keterangan: "",
        jml_bayar: "43999.648",
      },
      {
        no: "18",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "18",
        nama_detail: "BENGKEL",
        keterangan: "",
        jml_bayar: "37550.3333333333",
      },
      {
        no: "19",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "19",
        nama_detail: "BULU",
        keterangan: "",
        jml_bayar: "25000",
      },
      {
        no: "20",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "20",
        nama_detail: "KOPI",
        keterangan: "",
        jml_bayar: "12997.6",
      },
      {
        no: "21",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "21",
        nama_detail: "ASURANSI",
        keterangan: "",
        jml_bayar: "76620.1333333333",
      },
      {
        no: "22",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "22",
        nama_detail: "THR",
        keterangan: "",
        jml_bayar: "184800",
      },
      {
        no: "23",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "23",
        nama_detail: "CICILAN",
        keterangan: "",
        jml_bayar: "184800",
      },
      {
        no: "24",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "24",
        nama_detail: "CICILAN ANDON",
        keterangan: "",
        jml_bayar: "90000",
      },
      {
        no: "25",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "25",
        nama_detail: "THR BROKER",
        keterangan: "",
        jml_bayar: "125000",
      },
    ];
    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#pengeluaran_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun">' +
          det.id_pengeluaran_akun +
          "</span>" +
          det.nama +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun_detail">' +
          det.id_pengeluaran_akun_detail +
          "</span>" +
          det.nama_detail +
          "</td>" +
          "<td>" +
          det.keterangan +
          "</td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(parseFloat(det.jml_bayar).toFixed(0))
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });
});
