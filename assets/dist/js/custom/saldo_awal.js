$(document).ready(function () {
  $("#saldo_awal").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tanggal").change(function (e) {
    e.preventDefault();
    var tanggal = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("SAL" + tanggal);
  });

  $("#id_pengeluaran_akun").select2({
    theme: "bootstrap4",
    placeholder: "Jenis Pengeluaran",
  });
  $("#id_pengeluaran_akun_detail").select2({
    theme: "bootstrap4",
    placeholder: "Akun Pengeluaran",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });

  function get_pengeluaran_akun_detail() {
    $.ajax({
      type: "POST",
      url: "get_pengeluaran_akun_detail",
      data: { id_pengeluaran_akun: $("#id_pengeluaran_akun").val() },
      success: function (response) {
        var dt_acs = JSON.parse(response);
        var acs = [];
        dt_acs.forEach((element) => {
          var data = {
            id: element.id,
            text: element.nama,
          };
          acs.push(data);
        });

        $("#id_pengeluaran_akun_detail").select2({
          theme: "bootstrap4",
          placeholder: "Akun Pengeluaran",
          data: acs,
        });
      },
    });
  }

  $("#id_pengeluaran_akun").change(function (e) {
    e.preventDefault();
    $("#id_pengeluaran_akun_detail").empty();
    get_pengeluaran_akun_detail();
  });

  $("#tambah_detail").click(function (e) {
    e.preventDefault();

    var no = $("#saldo_awal_detail tbody tr").length + 1;
    var no_trxdetail =
      "SAL" +
      moment($("#tanggal").val()).format("YYYYMMDD") +
      no.toString().padStart(3, "0");
    var id_pengeluaran_akun = $("#id_pengeluaran_akun").val();
    var nama = $("#id_pengeluaran_akun option:selected").text();
    var id_pengeluaran_akun_detail = $("#id_pengeluaran_akun_detail").val();
    var nama_detail = $("#id_pengeluaran_akun_detail option:selected").text();
    var keterangan = $("#keterangan").val();
    var jml_bayar = $("#jml_bayar").val();

    if (
      id_pengeluaran_akun != "" &&
      id_pengeluaran_akun_detail != "" &&
      jml_bayar != ""
    ) {
      $("#saldo_awal_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun">' +
          id_pengeluaran_akun +
          "</span>" +
          nama +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun_detail">' +
          id_pengeluaran_akun_detail +
          "</span>" +
          nama_detail +
          "</td>" +
          "<td>" +
          keterangan +
          "</td>" +
          '<td class="text-right"><span class="d-none jml_bayar">' +
          parseFloat(jml_bayar) +
          "</span>" +
          parseFloat(jml_bayar).toFixed(2) +
          "</td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      reset_form();

      toastr.success("Data berhasil ditambahkan !", "Berhasil", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });

      const ftanggal = $("#tanggal").val();
      const fno_transaksi = $("#no_transaksi").val();
      $("#tanggal").change(function (e) {
        e.preventDefault();
        alert(
          "Mohon tidak mengatur tanggal setelah menginputkan Saldo Awal !\nSilahkan refresh halaman ini untuk mengatur tanggal"
        );
        $("#tanggal").val(ftanggal);
        $("#no_transaksi").val(fno_transaksi);
      });
    } else {
      toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });
    }
  });

  function reset_form() {
    $("#id_pengeluaran_akun").val("").trigger("change");
    $("#id_pengeluaran_akun_detail").val("").trigger("change");
    $("#keterangan").val("");
    $("#jml_bayar").val("");
    $(".select2bs4").select2("open");
  }

  function tambah_saldo_awal(master_saldo_awal, detail_saldo_awal) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah",
      data: master_saldo_awal,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          // insert detail
          var data_detail = { data: detail_saldo_awal };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail",
            data: data_detail,
          });

          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          history.back();
        } else {
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tanggal.invalid-feedback")
            .css("display", "block")
            .html(error.tanggal);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
        }
      },
    });
  }

  $(".simpan_saldo_awal").click(function (e) {
    e.preventDefault();
    var subtotal = 0;
    var detail_saldo_awal = [];
    $("#saldo_awal_detail tr").each(function (row, tr) {
      if ($(tr).find("td:eq(1)").text() == "") {
      } else {
        // MASTER
        subtotal += parseFloat($(tr).find("td:eq(5) .jml_bayar").text());
        // DETAIL
        var sub = {
          no_transaksi: $("#no_transaksi").val(),
          no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
          id_pengeluaran_akun_detail: $(tr)
            .find("td:eq(3) .id_pengeluaran_akun_detail")
            .text(),
          keterangan: $(tr).find("td:eq(4)").text(),
          jml_bayar: parseFloat($(tr).find("td:eq(5) .jml_bayar").text()),
        };
        detail_saldo_awal.push(sub);
      }
    });

    // INSERT saldo_awal
    var master_saldo_awal = {
      no_transaksi: $("#no_transaksi").val(),
      tanggal: $(".tanggal").val(),
      total: subtotal,
    };

    var c = confirm("Apakah anda yakin ?");
    if (c == true) {
      tambah_saldo_awal(master_saldo_awal, detail_saldo_awal);
    }
  });

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tanggal").val("2022-02-26").trigger("change");

    const dets = [
      {
        no: "1",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "1",
        nama_detail: "SUPIR",
        keterangan: "",
        jml_bayar: "66.6666666666667",
      },
      {
        no: "2",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "2",
        nama_detail: "TKBM",
        keterangan: "",
        jml_bayar: "83.3333333333333",
      },
      {
        no: "3",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "3",
        nama_detail: "UANG MAKAN",
        keterangan: "",
        jml_bayar: "25",
      },
      {
        no: "4",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "4",
        nama_detail: "KAS",
        keterangan: "",
        jml_bayar: "54.1666666666667",
      },
      {
        no: "5",
        id_pengeluaran_akun: "1",
        nama: "MOBILISASI",
        id_pengeluaran_akun_detail: "5",
        nama_detail: "BENSIN",
        keterangan: "",
        jml_bayar: "54.1666666666667",
      },
      {
        no: "6",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "11",
        nama_detail: "PLASTIK",
        keterangan: "",
        jml_bayar: "57.2",
      },
      {
        no: "7",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "12",
        nama_detail: "PEMELIHARAAN",
        keterangan: "",
        jml_bayar: "31.5833333333333",
      },
      {
        no: "8",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "15",
        nama_detail: "GAS LPG",
        keterangan: "",
        jml_bayar: "87.25",
      },
      {
        no: "9",
        id_pengeluaran_akun: "2",
        nama: "PRODUKSI",
        id_pengeluaran_akun_detail: "17",
        nama_detail: "LISTRIK",
        keterangan: "",
        jml_bayar: "35.714",
      },
      {
        no: "10",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "18",
        nama_detail: "BENGKEL",
        keterangan: "",
        jml_bayar: "30.4791666666667",
      },
      {
        no: "11",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "20",
        nama_detail: "KOPI",
        keterangan: "",
        jml_bayar: "10.55",
      },
      {
        no: "12",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "21",
        nama_detail: "ASURANSI",
        keterangan: "",
        jml_bayar: "62.1916666666667",
      },
      {
        no: "13",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "22",
        nama_detail: "THR",
        keterangan: "",
        jml_bayar: "150",
      },
      {
        no: "14",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "23",
        nama_detail: "CICILAN",
        keterangan: "",
        jml_bayar: "150",
      },
      {
        no: "15",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "24",
        nama_detail: "CICILAN ANDON",
        keterangan: "",
        jml_bayar: "300",
      },
      {
        no: "16",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "25",
        nama_detail: "THR BROKER",
        keterangan: "",
        jml_bayar: "50",
      },
      {
        no: "17",
        id_pengeluaran_akun: "3",
        nama: "KANTOR",
        id_pengeluaran_akun_detail: "26",
        nama_detail: "FEE",
        keterangan: "",
        jml_bayar: "400",
      },
    ];

    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#saldo_awal_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun">' +
          det.id_pengeluaran_akun +
          "</span>" +
          det.nama +
          "</td>" +
          '<td><span class="d-none id_pengeluaran_akun_detail">' +
          det.id_pengeluaran_akun_detail +
          "</span>" +
          det.nama_detail +
          "</td>" +
          "<td>" +
          det.keterangan +
          "</td>" +
          '<td class="text-right"><span class="d-none jml_bayar">' +
          parseFloat(det.jml_bayar) +
          "</span>" +
          parseFloat(det.jml_bayar).toFixed(2) +
          "</td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );
    });
  });
});
