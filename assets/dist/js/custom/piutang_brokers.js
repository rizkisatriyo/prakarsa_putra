$(document).ready(function () {
  $("#piutang_brokers").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tgl_bayar").change(function (e) {
    e.preventDefault();
    var tgl_bayar = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PJB" + tgl_bayar);
  });

  $("#modal-bayar").on("shown.bs.modal", function (event) {
    $("#jml_bayar").trigger("focus");
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var piutang = button.data("piutang");
    var deposit = button.data("deposit");

    $("#id_broker").val(id).trigger("change");
    $("#piutang_broker").val(piutang.toFixed()).trigger("input");
    $("#deposit_broker").val(deposit.toFixed()).trigger("input");
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
    placeholder: "Pilih broker",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });

  $("#piutang_broker").mask("#.##0", {
    reverse: true,
  });
  $("#deposit_broker").mask("#.##0", {
    reverse: true,
  });
  $("#jml_bayar").mask("#.##0", {
    reverse: true,
  });

  $("#id_broker").on("select2:select", function () {
    var piutang = $("#id_broker option:selected").data("piutang");
    var deposit = $("#id_broker option:selected").data("deposit");
    $("#piutang_broker").val(piutang).trigger("input");
    $("#deposit_broker").val(deposit).trigger("input");
  });

  $("#form_bayar").submit(function (e) {
    e.preventDefault();
    const data = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_bayar: $("#tgl_bayar").val(),
      id_broker: $("#id_broker").val(),
      piutang_broker: $("#piutang_broker").cleanVal(),
      deposit_broker: $("#deposit_broker").cleanVal(),
      jml_bayar: $("#jml_bayar").cleanVal(),
    };

    $.ajax({
      type: "POST",
      url: "piutang_brokers/bayar",
      data: data,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
          location.reload();
        } else {
          console.log(errors);
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_bayar);
          $("#id_broker.invalid-feedback")
            .css("display", "block")
            .html(error.id_broker);
          $("#jml_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.jml_bayar);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          $("#modal-bayar").modal("show");
        }
      },
    });
  });
});
