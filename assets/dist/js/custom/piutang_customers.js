$(document).ready(function () {
  $("#piutang_customers").DataTable({
    responsive: true,
    autoWidth: false,
  });

  $("#tgl_bayar").change(function (e) {
    e.preventDefault();
    var tgl_bayar = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PJP" + tgl_bayar);
  });

  $("#modal-bayar").on("shown.bs.modal", function (event) {
    $("#jml_bayar").trigger("focus");
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var piutang = button.data("piutang");
    var deposit = button.data("deposit");

    $("#id_customer").val(id).trigger("change");
    $("#piutang_customer").val(piutang).trigger("input");
    $("#deposit_customer").val(deposit).trigger("input");
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
    placeholder: "Pilih customer",
  });
  $(document).on("select2:open", () => {
    document.querySelector(".select2-search__field").focus();
  });

  $("#piutang_customer").mask("#.##0", {
    reverse: true,
  });
  $("#deposit_customer").mask("#.##0", {
    reverse: true,
  });
  $("#jml_bayar").mask("#.##0", {
    reverse: true,
  });

  $("#id_customer").on("select2:select", function () {
    var piutang = $("#id_customer option:selected").data("piutang");
    var deposit = $("#id_customer option:selected").data("deposit");
    $("#piutang_customer").val(piutang).trigger("input");
    $("#deposit_customer").val(deposit).trigger("input");
  });

  $("#form_bayar").submit(function (e) {
    e.preventDefault();
    const data = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_bayar: $("#tgl_bayar").val(),
      id_customer: $("#id_customer").val(),
      piutang_customer: $("#piutang_customer").cleanVal(),
      deposit_customer: $("#deposit_customer").cleanVal(),
      jml_bayar: $("#jml_bayar").cleanVal(),
    };

    $.ajax({
      type: "POST",
      url: "piutang_customers/bayar",
      data: data,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
          location.reload();
        } else {
          console.log(errors);
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_bayar);
          $("#id_customer.invalid-feedback")
            .css("display", "block")
            .html(error.id_customer);
          $("#jml_bayar.invalid-feedback")
            .css("display", "block")
            .html(error.jml_bayar);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          $("#modal-bayar").modal("show");
        }
      },
    });
  });
});
