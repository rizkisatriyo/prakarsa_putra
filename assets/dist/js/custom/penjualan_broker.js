$(document).ready(function () {
  $("#penjualan_broker").DataTable({
    responsive: true,
    autoWidth: false,
  });

  get_supps();

  function get_supps() {
    $.ajax({
      type: "POST",
      url: "get_supplier",
      data: { tgl_jual: $("#tgl_jual").val() },
      success: function (response) {
        var dt_supps = JSON.parse(response);
        var supps = [];
        dt_supps.forEach((element) => {
          var data = {
            id: element.id_supplier,
            text: element.nama,
          };
          supps.push(data);
        });

        $("#id_supplier").select2({
          theme: "bootstrap4",
          placeholder: "Pilih Supplier",
          data: supps,
        });
      },
    });
  }

  $("#tgl_jual").change(function (e) {
    e.preventDefault();
    var tgl_jual = moment($(this).val()).format("YYYYMMDD");
    $("#no_transaksi").val("PJB" + tgl_jual);
    get_supps();
  });

  $("#id_broker").select2({
    theme: "bootstrap4",
    placeholder: "Pilih Broker",
  });
  $(document).on("select2:open", "#id_broker", () => {
    $("input.select2-search__field")[1].focus();
  });
  $("#id_broker").on("select2:select", function () {
    var piutang = $("#id_broker option:selected").data("piutang");
    var deposit = $("#id_broker option:selected").data("deposit");
    $(".piutang_broker").removeClass("d-none");
    $("#piutang_broker").val(piutang).trigger("input");
    $(".deposit_broker").removeClass("d-none");
    $("#deposit_broker").val(deposit).trigger("input");
  });

  $("#hrg_ayam").mask("#.##0", { reverse: true });
  $("#jml_bayar").mask("#.##0", { reverse: true });
  $("#piutang_broker").mask("#.##0", { reverse: true });
  $("#deposit_broker").mask("#.##0", { reverse: true });

  $("#kg, #hrg_ayam").keyup(function (e) {
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam").cleanVal();
    var subtotal = 0;

    subtotal = kg * hrg_ayam;
    $("#subtotal")
      .mask("#.##0", { reverse: true })
      .val(subtotal.toFixed())
      .trigger("input");

    let jml_bayar = subtotal - $("#deposit_broker").cleanVal();
    $("#jml_bayar")
      .mask("#.##0", { reverse: true })
      .val(jml_bayar.toFixed())
      .trigger("input");
    if ($("#deposit_broker").val() != 0) {
      $("#jml_bayar").addClass("font-weight-bold text-success");
    }
  });

  $("#tambah_detail").click(function (e) {
    e.preventDefault();

    var no = $("#penjualan_broker_detail tbody tr").length + 1;
    var no_trxdetail =
      "PJB" +
      moment($("#tgl_jual").val()).format("YYYYMMDD") +
      no.toString().padStart(3, "0");
    var id_broker = $("#id_broker").val();
    var piutang = $("#id_broker option:selected").data("piutang");
    var deposit = $("#id_broker option:selected").data("deposit");
    var nama = $("#id_broker option:selected").text();
    var id_supplier = $("#id_supplier").select2("data");
    var suppliers = [];
    for (var i = 0; i <= id_supplier.length - 1; i++) {
      var sub = {
        id_supplier: id_supplier[i].id,
        nama: id_supplier[i].text,
      };
      suppliers.push(sub);
      // console.log(id_supplier[i].text);
    }
    // var ekor = $("#ekor").val();
    var kg = $("#kg").val();
    var hrg_ayam = $("#hrg_ayam");
    var subtotal = $("#subtotal");
    var jml_bayar = $("#jml_bayar");

    if (
      id_broker != "" &&
      suppliers != "" &&
      hrg_ayam.val() != "" &&
      kg != "" &&
      subtotal.val() != ""
      // jml_bayar.val() != ""
    ) {
      $("#penjualan_broker_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_broker">' +
          id_broker +
          "</span>" +
          '<span class="d-none piutang">' +
          piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          deposit +
          "</span>" +
          nama +
          "</td>" +
          '<td><ul class="mb-0"></ul></td>' +
          '<td class="text-center">' +
          kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          hrg_ayam.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(hrg_ayam.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          subtotal.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(subtotal.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          jml_bayar.cleanVal() +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(jml_bayar.cleanVal())
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      for (var i = 0; i <= suppliers.length - 1; i++) {
        $("#penjualan_broker_detail tbody tr:last-child td:eq(3) ul").append(
          "<li><span class='d-none id_s'>" +
            suppliers[i].id_supplier +
            "</span>" +
            suppliers[i].nama +
            "</li>"
        );
      }

      reset_form();

      toastr.success("Data berhasil ditambahkan !", "Berhasil", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });

      const ftgl_jual = $("#tgl_jual").val();
      const fno_transaksi = $("#no_transaksi").val();
      $("#tgl_jual").change(function (e) {
        e.preventDefault();
        alert(
          "Mohon tidak mengatur tanggal setelah menginputkan broker !\nSilahkan refresh halaman ini untuk mengatur tanggal"
        );
        $("#tgl_jual").val(ftgl_jual);
        $("#no_transaksi").val(fno_transaksi);
      });
    } else {
      toastr.error("Mohon isi data dengan lengkap !", "Gagal", {
        progressBar: true,
        positionClass: "toast-top-right",
        timeOut: "5000",
      });
    }
  });

  function reset_form() {
    $("#id_broker").val(null).trigger("change");
    $("#id_supplier").val("").trigger("change");
    $("#kg").val("");
    $("#hrg_ayam").val("");
    $("#subtotal").val("");
    $("#jml_bayar").val("");
    $(".piutang_broker").addClass("d-none");
    $(".deposit_broker").addClass("d-none");
    $("#jml_bayar").removeClass("font-weight-bold text-success");
  }

  function tambah_penjualan_broker(
    master_penjualan_broker,
    detail_penjualan_broker,
    detail_supplier
  ) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah",
      data: master_penjualan_broker,
      success: function (errors) {
        if ($.isEmptyObject(errors)) {
          // insert detail
          var data_detail = { data: detail_penjualan_broker };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail",
            data: data_detail,
          });

          var detail_supp = { data: detail_supplier };
          $.ajax({
            type: "POST",
            url: "aksi_tambah_detail_supplier",
            data: detail_supp,
          });

          toastr.success("Data Berhasil Ditambahkan !", "Berhasil", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });

          history.back();
        } else {
          error = JSON.parse(errors);
          $("#no_transaksi.invalid-feedback")
            .css("display", "block")
            .html(error.no_transaksi);
          $("#tgl_jual.invalid-feedback")
            .css("display", "block")
            .html(error.tgl_jual);

          toastr.error("Data Gagal Ditambahkan !", "Gagal", {
            progressBar: true,
            positionClass: "toast-top-right",
            timeOut: "2500",
          });
        }
      },
    });
  }

  function tambah_pembayaran(master_pembayaran, detail_pembayaran) {
    $.ajax({
      type: "POST",
      url: "aksi_tambah_pembayaran",
      data: master_pembayaran,
      success: function () {
        // insert detail
        var data_detail_pembayaran = { data: detail_pembayaran };
        $.ajax({
          type: "POST",
          url: "aksi_tambah_detail_pembayaran",
          data: data_detail_pembayaran,
        });
      },
    });
  }

  $(".simpan_penjualan_broker").click(function (e) {
    e.preventDefault();
    // INSERT PEnjualAN_broker
    // var total_ekor = 0;
    var total_kg = 0;
    var subtotal = 0;
    var tgl_jual = $(".tgl_jual").val();
    var detail_penjualan_broker = [];
    var detail_supplier = [];
    var total_piutang = 0;
    var total_bayar = 0;
    var total_deposit = 0;
    var detail_pembayaran = [];
    $("#penjualan_broker_detail tr").each(function (row, tr) {
      if ($(tr).find("td:eq(1)").text() == "") {
      } else {
        // PEnjualAN_broker
        // MASTER
        if ($(tr).find("td:eq(3)").text()) {
          // total_ekor += parseInt($(tr).find("td:eq(3)").text());
        }
        if ($(tr).find("td:eq(4)").text()) {
          total_kg += parseFloat($(tr).find("td:eq(4)").text());
        }
        subtotal += parseFloat($(tr).find("td:eq(6) .subtotal").text());
        // DETAIL
        var sub = {
          no_transaksi: $("#no_transaksi").val(),
          no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
          id_broker: $(tr).find("td:eq(2) .id_broker").text(),
          // ekor: $(tr).find("td:eq(3)").text(),
          kg: $(tr).find("td:eq(4)").text(),
          hrg_ayam: $(tr).find("td:eq(5) .hrg_ayam").text(),
          subtotal: $(tr).find("td:eq(6) .subtotal").text(),
        };
        detail_penjualan_broker.push(sub);

        var jml_supp = $(tr).find("td:eq(3) .id_s");

        for (let i = 0; i < jml_supp.length; i++) {
          var sub_supp = {
            no_trxdetail: $(tr).find("td:eq(1)").text().replace("#", ""),
            id_supplier: jml_supp[i].innerText,
          };
          detail_supplier.push(sub_supp);
        }

        // PEMBAYARAN
        // MASTER
        if ($(tr).find("td:eq(2) .piutang").text()) {
          total_piutang +=
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .piutang").text());
        }
        if ($(tr).find("td:eq(7) .jml_bayar").text()) {
          total_bayar += parseFloat($(tr).find("td:eq(7) .jml_bayar").text());
        }
        if ($(tr).find("td:eq(2) .deposit").text()) {
          total_deposit += parseFloat($(tr).find("td:eq(2) .deposit").text());
        }

        // DETAIL
        var sub_byr = {
          tgl_bayar: tgl_jual,
          id_broker: $(tr).find("td:eq(2) .id_broker").text(),
          tagihan: $(tr).find("td:eq(6) .subtotal").text(),
          piutang:
            parseFloat($(tr).find("td:eq(6) .subtotal").text()) +
            parseFloat($(tr).find("td:eq(2) .piutang").text()),
          piutang_broker: $(tr).find("td:eq(2) .piutang").text(),
          jml_bayar: $(tr).find("td:eq(7) .jml_bayar").text(),
          deposit: $(tr).find("td:eq(2) .deposit").text(),
        };
        detail_pembayaran.push(sub_byr);
      }
    });

    // INSERT Penjualan_broker
    var master_penjualan_broker = {
      no_transaksi: $("#no_transaksi").val(),
      tgl_jual: tgl_jual,
      // ekor: total_ekor,
      kg: total_kg,
      harga: subtotal / total_kg,
      total_piutang: subtotal,
    };

    // INSERT PEMBAYARAN
    var master_pembayaran = {
      tgl_bayar: tgl_jual,
      total_piutang: total_piutang,
      total_bayar: total_bayar,
      total_deposit: total_deposit,
    };

    var c = confirm("Apakah anda yakin ?");
    if (c == true) {
      tambah_pembayaran(master_pembayaran, detail_pembayaran);
      tambah_penjualan_broker(
        master_penjualan_broker,
        detail_penjualan_broker,
        detail_supplier
      );
    }
  });

  $("#demo_26").click(function (e) {
    e.preventDefault();

    $("#tgl_jual").val("2022-02-26").trigger("change");

    const dets = [
      {
        no: 1,
        id_broker: 1,
        piutang: 8800,
        deposit: 0,
        nama: "RAHMAT",
        suppliers: [{ id_supplier: 1, nama: "CIOMAS" }],
        kg: 1100,
        hrg_ayam: 18300,
        subtotal: 20130000,
        jml_bayar: 0,
      },
      {
        no: 2,
        id_broker: 2,
        piutang: 0,
        deposit: 0,
        nama: "MARIAH",
        suppliers: [{ id_supplier: 1, nama: "CIOMAS" }],
        kg: 350,
        hrg_ayam: 18200,
        subtotal: 6370000,
        jml_bayar: 6370000,
      },
      {
        no: 3,
        id_broker: 4,
        piutang: 10063699.999601394,
        deposit: 0,
        nama: "KARTA",
        suppliers: [
          { id_supplier: 1, nama: "CIOMAS" },
          { id_supplier: 18, nama: "K A M I / VINCENT" },
        ],
        kg: 850,
        hrg_ayam: 18505.8823529,
        subtotal: 15730000,
        jml_bayar: 0,
      },
      {
        no: 4,
        id_broker: 6,
        piutang: 310000,
        deposit: 0,
        nama: "ELI JUNAERI H",
        suppliers: [{ id_supplier: 1, nama: "CIOMAS" }],
        kg: 200,
        hrg_ayam: 18300,
        subtotal: 3660000,
        jml_bayar: 3970000,
      },
      {
        no: 5,
        id_broker: 7,
        piutang: 0,
        deposit: 0,
        nama: "WARSO",
        suppliers: [{ id_supplier: 18, nama: "K A M I / VINCENT" }],
        kg: 700,
        hrg_ayam: 18800,
        subtotal: 13160000,
        jml_bayar: 13160000,
      },
    ];

    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#penjualan_broker_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_broker">' +
          det.id_broker +
          "</span>" +
          '<span class="d-none piutang">' +
          det.piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td><ul class="mb-0"></ul></td>' +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      const supps = det.suppliers;
      for (var i = 0; i < supps.length; i++) {
        $("#penjualan_broker_detail tbody tr:last-child td:eq(3) ul").append(
          "<li><span class='d-none id_s'>" +
            supps[i].id_supplier +
            "</span>" +
            supps[i].nama +
            "</li>"
        );
      }
    });
  });

  $("#demo_27").click(function (e) {
    e.preventDefault();

    $("#tgl_jual").val("2022-02-27").trigger("change");

    const dets = [
      {
        no: "1",
        id_broker: "1",
        piutang: "8800",
        deposit: "",
        nama: "RAHMAT",
        suppliers: [
          { id_supplier: 1, nama: "CIOMAS" },
        ],
        kg: "550",
        hrg_ayam: "18300",
        subtotal: "10065000",
        jml_bayar: "20130000",
      },
      {
        no: "2",
        id_broker: "3",
        piutang: "1400000",
        deposit: "",
        nama: "IWAN",
        suppliers: [
          { id_supplier: 13, nama: "SIDO AGUNG" },
        ],
        kg: "700",
        hrg_ayam: "18600",
        subtotal: "13020000",
        jml_bayar: "",
      },
      {
        no: "3",
        id_broker: "4",
        piutang: "10063699.999601394",
        deposit: "",
        nama: "KARTA",
        suppliers: [
          { id_supplier: 1, nama: "CIOMAS" },
          { id_supplier: 3, nama: "NEWHOPE 2" },
          { id_supplier: 13, nama: "SIDO AGUNG" },
        ],
        kg: "550",
        hrg_ayam: "18790.9090909",
        subtotal: "10334999.999995",
        jml_bayar: "26200000",
      },
      {
        no: "4",
        id_broker: "7",
        piutang: "0",
        deposit: "",
        nama: "WARSO",
        suppliers: [
          { id_supplier: 10, nama: "KMN / MIFTAHUDIN" },
        ],
        kg: "700",
        hrg_ayam: "19300",
        subtotal: "13510000",
        jml_bayar: "13510000",
      },
    ];
    dets.forEach((det) => {
      var no_trxdetail =
        $("#no_transaksi").val() + det.no.toString().padStart(3, "0");

      $("#penjualan_broker_detail tbody:last-child").append(
        "<tr>" +
          '<td class="text-center" width="50">' +
          det.no +
          "</td>" +
          '<td class="text-center">#' +
          no_trxdetail +
          "</td>" +
          '<td><span class="d-none id_broker">' +
          det.id_broker +
          "</span>" +
          '<span class="d-none piutang">' +
          det.piutang +
          "</span>" +
          '<span class="d-none deposit">' +
          det.deposit +
          "</span>" +
          det.nama +
          "</td>" +
          '<td><ul class="mb-0"></ul></td>' +
          '<td class="text-center">' +
          det.kg +
          "</td>" +
          '<td><span class="d-none hrg_ayam">' +
          det.hrg_ayam +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.hrg_ayam)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none subtotal">' +
          det.subtotal +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.subtotal)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          '<td><span class="d-none jml_bayar">' +
          det.jml_bayar +
          '</span><span class="float-left">Rp.</span><span class="float-right">' +
          new Intl.NumberFormat("en-ID", {
            style: "currency",
            currency: "IDR",
          })
            .format(det.jml_bayar)
            .replace(/[IDR]/gi, "")
            .replace(/(\.+\d{2})/, "")
            .trimLeft() +
          ",-</span></td>" +
          // '<td class="text-center">' +
          // '<div class="btn-group">' +
          // '<button type="button" class="btn btn-sm btn-success" id="edit_detail" data-toggle="modal" data-target="#form_detail"><i class="fas fa-edit"></i></button>' +
          // '<button type="button" class="btn btn-sm btn-danger" onclick="return confirm(' +
          // "Apakah anda yakin?" +
          // ')"><i class="fas fa-trash"></i></button>' +
          // "</div>" +
          // "</td>" +
          "</tr>"
      );

      const supps = det.suppliers;
      for (var i = 0; i < supps.length; i++) {
        $("#penjualan_broker_detail tbody tr:last-child td:eq(3) ul").append(
          "<li><span class='d-none id_s'>" +
            supps[i].id_supplier +
            "</span>" +
            supps[i].nama +
            "</li>"
        );
      }
    });
  });
});
