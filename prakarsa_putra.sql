-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2023 at 09:32 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prakarsa_putra`
--

-- --------------------------------------------------------

--
-- Table structure for table `broker`
--

CREATE TABLE `broker` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` char(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `piutang_broker` double NOT NULL,
  `deposit_broker` double NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `broker`
--

INSERT INTO `broker` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `piutang_broker`, `deposit_broker`, `avatar`) VALUES
(1, 'RAHMAT', '', NULL, NULL, NULL, 8800, 0, ''),
(2, 'MARIAH', '', NULL, NULL, NULL, 0, 0, ''),
(3, 'IWAN', '', NULL, NULL, NULL, 12679999.999959998, 0, ''),
(4, 'KARTA', '', NULL, NULL, NULL, 10063699.999601394, 0, ''),
(5, 'MUANA .S', '', NULL, NULL, NULL, 1327000, 0, ''),
(6, 'ELI JUNAERI H', '', NULL, NULL, NULL, 310000, 0, ''),
(7, 'WARSO', '', NULL, NULL, NULL, 0, 0, ''),
(8, 'ILHAM ADITYA. R', '', NULL, NULL, NULL, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` char(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `piutang_customer` double NOT NULL,
  `deposit_customer` double NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `piutang_customer`, `deposit_customer`, `avatar`) VALUES
(1, 'B. ENTUM', '', NULL, NULL, NULL, 0, 50214.0000126196, ''),
(2, 'P. GEMA', '', NULL, NULL, NULL, 1475390, 0, ''),
(3, 'B. ANDON (B)', '', NULL, NULL, NULL, 44462400, 0, ''),
(4, 'B. ANARTI (K)', '', NULL, NULL, NULL, 0, 0, ''),
(5, 'HAMZAH', '', NULL, NULL, NULL, 4272120, 0, ''),
(6, 'KANTOR', '', NULL, NULL, NULL, 0, 7632490, ''),
(7, 'P. HASAN', '', NULL, NULL, NULL, 2024710, 0, ''),
(8, 'P. CECE', '', NULL, NULL, NULL, 300610, 0, ''),
(9, 'HARDI/BAKAR', '', NULL, NULL, NULL, 203790, 0, ''),
(10, 'P. OYO', '', NULL, NULL, NULL, 3741150, 0, ''),
(11, 'P. ALIF', '', NULL, NULL, NULL, 0, 20170.0000000001, ''),
(12, 'MG. DAWANG', '', NULL, NULL, NULL, 400, 0, ''),
(13, 'P. ASEP', '', NULL, NULL, NULL, 1145290, 0, ''),
(14, 'P. DEDI', '', NULL, NULL, NULL, 0, 1820, ''),
(15, 'B. NENENG', '', NULL, NULL, NULL, 230380, 0, ''),
(16, 'B. KESIH', '', NULL, NULL, NULL, 18540, 0, ''),
(17, 'P. EDI ', '', NULL, NULL, NULL, 1061010, 0, ''),
(18, 'B. RAHMAT', '', NULL, NULL, NULL, 0, 13390, ''),
(19, 'ADE MIQDAD', '', NULL, NULL, NULL, 673140, 0, ''),
(20, 'E ', '', NULL, NULL, NULL, 89730, 0, ''),
(21, 'B. KIKI', '', NULL, NULL, NULL, 130260, 0, ''),
(22, 'B. AGUNG', '', NULL, NULL, NULL, 195310, 0, ''),
(23, 'ELVAS', '', NULL, NULL, NULL, 518000, 0, ''),
(24, 'P. TARWIN', '', NULL, NULL, NULL, 36999.99999999994, 0, ''),
(25, 'w. KARTA', '', NULL, NULL, NULL, 0, 1330, ''),
(26, 'JATISURA', '', NULL, NULL, NULL, 0, 5470, ''),
(27, 'BADOL/ ATENG', '', NULL, NULL, NULL, 40290, 0, ''),
(28, 'CIKEUN ABANG', '', NULL, NULL, NULL, 4008290, 0, ''),
(29, 'A-B', '', NULL, NULL, NULL, 348940, 0, ''),
(30, 'NURBAETI', '', NULL, NULL, NULL, 2109.9999999998836, 0, ''),
(31, 'JATIWANGI', '', NULL, NULL, NULL, 0, 1760, ''),
(32, 'M. UHA', '', NULL, NULL, NULL, 103469.99999999994, 0, ''),
(33, 'B. INDRA/LAGIS/GEPUK', '', NULL, NULL, NULL, 730, 0, ''),
(34, 'KHOLIK', '', NULL, NULL, NULL, 6891090, 0, ''),
(35, 'B. YULI', '', NULL, NULL, NULL, 26930, 0, ''),
(36, 'RAKA', '', NULL, NULL, NULL, 6330, 0, ''),
(37, 'M. UJANG', '', NULL, NULL, NULL, 57840, 0, ''),
(38, 'H.ANDIR', '', NULL, NULL, NULL, 89850, 0, ''),
(39, 'A DEDI', '', NULL, NULL, NULL, 82000, 0, ''),
(40, 'P. TOHA', '', NULL, NULL, NULL, 0, 2070, '');

-- --------------------------------------------------------

--
-- Table structure for table `kandang`
--

CREATE TABLE `kandang` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kandang`
--

INSERT INTO `kandang` (`id`, `nama`, `alamat`, `keterangan`) VALUES
(1, 'Kandang A', '', ''),
(2, 'Kandang B', '', ''),
(3, 'Kandang C', '', ''),
(4, 'Kandang D', '', ''),
(5, 'Kandang E', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tgl_beli` date NOT NULL,
  `status` enum('Tertunda','Selesai') NOT NULL,
  `ekor` int(11) DEFAULT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `total_hutang` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `no_transaksi`, `tgl_beli`, `status`, `ekor`, `kg`, `harga`, `total_hutang`) VALUES
(3, 'PBN20231128', '2023-11-28', 'Selesai', 542, 922.2, 21000, '19366200'),
(4, 'PBN20231129', '2023-11-29', 'Selesai', 467, 830, 15500, '12865000'),
(5, 'PBN20231130', '2023-11-30', 'Selesai', 447, 775, 16000, '12400000');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `hrg_ayam` decimal(10,0) NOT NULL,
  `ekor` int(11) DEFAULT NULL,
  `kg` double NOT NULL,
  `subtotal` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_supplier`, `hrg_ayam`, `ekor`, `kg`, `subtotal`) VALUES
(10, 'PBN20231128', 'PBN20231128001', 9, '21000', 268, 486, '10206000'),
(11, 'PBN20231128', 'PBN20231128002', 9, '21000', 274, 436.2, '9160200'),
(12, 'PBN20231129', 'PBN20231129001', 5, '15500', 467, 830, '12865000'),
(13, 'PBN20231130', 'PBN20231130001', 12, '16000', 447, 775, '12400000');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_pembayaran`
--

CREATE TABLE `pembelian_pembayaran` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `total_hutang` double NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian_pembayaran`
--

INSERT INTO `pembelian_pembayaran` (`id`, `no_transaksi`, `total_hutang`, `tgl_bayar`, `total_bayar`, `total_deposit`) VALUES
(3, 'PBP20231128', 19366200, '2023-11-28', 19366200, 0),
(4, 'PBP20231128', 12450000, '2023-11-28', 37550000, 50000000),
(5, 'PBP20231129', 12865000, '2023-11-29', 62235000, 75100000),
(6, 'PBP20231130', 12400000, '2023-11-30', 8400000, 4000000);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_pembayaran_detail`
--

CREATE TABLE `pembelian_pembayaran_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `hutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian_pembayaran_detail`
--

INSERT INTO `pembelian_pembayaran_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_supplier`, `hutang`, `bayar`, `deposit`) VALUES
(39, 'PBP20231128', 'PBP20231128001', 1, 0, 0, 0),
(40, 'PBP20231128', 'PBP20231128002', 2, 0, 0, 0.00001000240445137),
(41, 'PBP20231128', 'PBP20231128003', 3, 0, 0, 0),
(42, 'PBP20231128', 'PBP20231128004', 4, 0, 0, 0),
(43, 'PBP20231128', 'PBP20231128005', 5, 0, 0, 50000000),
(44, 'PBP20231128', 'PBP20231128006', 6, 0, 0, 0),
(45, 'PBP20231128', 'PBP20231128007', 7, 0, 0, 0.35000000149012),
(46, 'PBP20231128', 'PBP20231128008', 8, 0, 0, 0),
(47, 'PBP20231128', 'PBP20231128009', 9, 9160200, 9160200, 0),
(48, 'PBP20231128', 'PBP20231128010', 10, 0, 0, 0),
(49, 'PBP20231128', 'PBP20231128011', 11, 0, 0, 0),
(50, 'PBP20231128', 'PBP20231128012', 12, 0, 0, 4000000),
(51, 'PBP20231128', 'PBP20231128013', 13, 0, 0, 0),
(52, 'PBP20231128', 'PBP20231128014', 14, 0, 0, 0),
(53, 'PBP20231128', 'PBP20231128015', 15, 0, 0, 0),
(54, 'PBP20231128', 'PBP20231128016', 16, 0, 0, 0),
(55, 'PBP20231128', 'PBP20231128017', 17, 0, 0, 0),
(56, 'PBP20231128', 'PBP20231128018', 18, 0, 0, 0.00011499971151352),
(57, 'PBP20231128', 'PBP20231128019', 19, 0, 0, 0),
(58, 'PBP20231128', 'PBP20231128001', 1, 0, 0, 0),
(59, 'PBP20231128', 'PBP20231128002', 2, 0, 0, 0.00001000240445137),
(60, 'PBP20231128', 'PBP20231128003', 3, 0, 0, 0),
(61, 'PBP20231128', 'PBP20231128004', 4, 0, 0, 0),
(62, 'PBP20231128', 'PBP20231128005', 5, 12450000, 37550000, 50000000),
(63, 'PBP20231128', 'PBP20231128006', 6, 0, 0, 0),
(64, 'PBP20231128', 'PBP20231128007', 7, 0, 0, 0.35000000149012),
(65, 'PBP20231128', 'PBP20231128008', 8, 0, 0, 0),
(66, 'PBP20231128', 'PBP20231128009', 9, 0, 0, 0),
(67, 'PBP20231128', 'PBP20231128010', 10, 0, 0, 0),
(68, 'PBP20231128', 'PBP20231128011', 11, 0, 0, 0),
(69, 'PBP20231128', 'PBP20231128012', 12, 0, 0, 4000000),
(70, 'PBP20231128', 'PBP20231128013', 13, 0, 0, 0),
(71, 'PBP20231128', 'PBP20231128014', 14, 0, 0, 0),
(72, 'PBP20231128', 'PBP20231128015', 15, 0, 0, 0),
(73, 'PBP20231128', 'PBP20231128016', 16, 0, 0, 0),
(74, 'PBP20231128', 'PBP20231128017', 17, 0, 0, 0),
(75, 'PBP20231128', 'PBP20231128018', 18, 0, 0, 0.00011499971151352),
(76, 'PBP20231128', 'PBP20231128019', 19, 0, 0, 0),
(77, 'PBP20231129', 'PBP20231129001', 1, 0, 0, 0),
(78, 'PBP20231129', 'PBP20231129002', 2, 0, 0, 0.00001000240445137),
(79, 'PBP20231129', 'PBP20231129003', 3, 0, 0, 0),
(80, 'PBP20231129', 'PBP20231129004', 4, 0, 0, 0),
(81, 'PBP20231129', 'PBP20231129005', 5, 12865000, 62235000, 75100000),
(82, 'PBP20231129', 'PBP20231129006', 6, 0, 0, 0),
(83, 'PBP20231129', 'PBP20231129007', 7, 0, 0, 0.35000000149012),
(84, 'PBP20231129', 'PBP20231129008', 8, 0, 0, 0),
(85, 'PBP20231129', 'PBP20231129009', 9, 0, 0, 0),
(86, 'PBP20231129', 'PBP20231129010', 10, 0, 0, 0),
(87, 'PBP20231129', 'PBP20231129011', 11, 0, 0, 0),
(88, 'PBP20231129', 'PBP20231129012', 12, 0, 0, 4000000),
(89, 'PBP20231129', 'PBP20231129013', 13, 0, 0, 0),
(90, 'PBP20231129', 'PBP20231129014', 14, 0, 0, 0),
(91, 'PBP20231129', 'PBP20231129015', 15, 0, 0, 0),
(92, 'PBP20231129', 'PBP20231129016', 16, 0, 0, 0),
(93, 'PBP20231129', 'PBP20231129017', 17, 0, 0, 0),
(94, 'PBP20231129', 'PBP20231129018', 18, 0, 0, 0.00011499971151352),
(95, 'PBP20231129', 'PBP20231129019', 19, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_transaksi_pembelian` char(30) NOT NULL,
  `tgl_diterima` date NOT NULL,
  `bongkar_ekor` int(11) DEFAULT NULL,
  `bongkar_kg` double DEFAULT NULL,
  `broker_kg` double NOT NULL,
  `kirim_ekor` int(11) DEFAULT NULL,
  `kirim_kg` double DEFAULT NULL,
  `mati_ekor` int(11) DEFAULT NULL,
  `mati_kg` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penerimaan`
--

INSERT INTO `penerimaan` (`id`, `no_transaksi`, `no_transaksi_pembelian`, `tgl_diterima`, `bongkar_ekor`, `bongkar_kg`, `broker_kg`, `kirim_ekor`, `kirim_kg`, `mati_ekor`, `mati_kg`) VALUES
(1, 'TRM20231128', 'PBN20231128', '2023-11-28', 542, 923.1, 0, 0, 0, 0, 0),
(2, 'TRM20231130', 'PBN20231129', '2023-11-29', 467, 824.5, 0, 0, 0, 0, 0),
(3, 'TRM20231131', 'PBN20231130', '2023-11-30', 190, 525, 0, 0, 0, 10, 13.5);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_checker`
--

CREATE TABLE `penerimaan_checker` (
  `id` int(11) NOT NULL,
  `no_checker` char(30) NOT NULL,
  `no_transaksi_penerimaan` char(30) NOT NULL,
  `total_ekor` int(11) NOT NULL,
  `total_kg` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penerimaan_checker`
--

INSERT INTO `penerimaan_checker` (`id`, `no_checker`, `no_transaksi_penerimaan`, `total_ekor`, `total_kg`) VALUES
(1, 'CKB20231128', 'TRM20231128', 542, 923.1),
(2, 'CKB20231129', 'TRM20231130', 467, 824.5),
(3, 'CKB20231130', 'TRM20231131', 190, 525);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_checker_detail`
--

CREATE TABLE `penerimaan_checker_detail` (
  `id` int(11) NOT NULL,
  `no_checker` char(30) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `ekor` int(11) NOT NULL,
  `kg` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penerimaan_checker_detail`
--

INSERT INTO `penerimaan_checker_detail` (`id`, `no_checker`, `id_kandang`, `ekor`, `kg`) VALUES
(1, 'CKB20231130', 1, 0, 0),
(2, 'CKB20231130', 1, 0, 0),
(3, 'CKB20231130', 2, 0, 0),
(4, 'CKB20231130', 2, 0, 0),
(5, 'CKB20231130', 3, 0, 0),
(6, 'CKB20231130', 3, 0, 0),
(7, 'CKB20231130', 4, 0, 0),
(8, 'CKB20231130', 4, 0, 0),
(9, 'CKB20231130', 5, 0, 0),
(10, 'CKB20231130', 5, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_susut`
--

CREATE TABLE `penerimaan_susut` (
  `id` int(11) NOT NULL,
  `no_transaksi_penerimaan` char(30) NOT NULL,
  `ekor` int(11) NOT NULL,
  `presentase_ekor` double NOT NULL,
  `kg` double NOT NULL,
  `presentase_kg` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penerimaan_susut`
--

INSERT INTO `penerimaan_susut` (`id`, `no_transaksi_penerimaan`, `ekor`, `presentase_ekor`, `kg`, `presentase_kg`) VALUES
(1, 'TRM20231131', 247, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `total` double NOT NULL,
  `tot_setor_bank` double NOT NULL,
  `setoran_rpa` double NOT NULL,
  `setoran_broker` double NOT NULL,
  `setor_tabungan` double NOT NULL,
  `setor_modal_rpa` double NOT NULL,
  `setor_modal_total` double NOT NULL,
  `sisa_setoran` double NOT NULL,
  `output_pokok` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `no_transaksi`, `tanggal`, `total`, `tot_setor_bank`, `setoran_rpa`, `setoran_broker`, `setor_tabungan`, `setor_modal_rpa`, `setor_modal_total`, `sisa_setoran`, `output_pokok`) VALUES
(1, 'PLR20220226', '2022-02-26', 1689025, 54414300, 20194300, 34780000, 1129025, 18505275, 53285200, 75, 560000);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_akun`
--

CREATE TABLE `pengeluaran_akun` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `set_lock` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengeluaran_akun`
--

INSERT INTO `pengeluaran_akun` (`id`, `nama`, `keterangan`, `set_lock`) VALUES
(1, 'MOBILISASI', '', 1),
(2, 'PRODUKSI', '', 1),
(3, 'KANTOR', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_akun_detail`
--

CREATE TABLE `pengeluaran_akun_detail` (
  `id` int(11) NOT NULL,
  `id_pengeluaran_akun` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `biaya_variabel` tinyint(1) NOT NULL,
  `saldo` double NOT NULL,
  `set_lock` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengeluaran_akun_detail`
--

INSERT INTO `pengeluaran_akun_detail` (`id`, `id_pengeluaran_akun`, `nama`, `keterangan`, `biaya_variabel`, `saldo`, `set_lock`) VALUES
(1, 1, 'SUPIR', '', 0, 0, 0),
(2, 1, 'TKBM', '', 0, 0, 0),
(3, 1, 'UANG MAKAN', '', 0, 0, 0),
(4, 1, 'KAS', '', 0, 0, 0),
(5, 1, 'BENSIN', '', 0, 0, 0),
(6, 1, 'PEMELIHARAAN', '', 1, 0, 1),
(7, 2, 'UPAH KERJA', '', 0, 0, 0),
(8, 2, 'UANG MAKAN', '', 0, 0, 0),
(9, 2, 'LEMBUR', '', 0, 0, 0),
(10, 2, 'BENSIN', '', 0, 0, 0),
(11, 2, 'PLASTIK', '', 1, 0, 1),
(12, 2, 'PEMELIHARAAN', '', 1, 0, 1),
(13, 3, 'UPAH KERJA', '', 0, 0, 1),
(14, 3, 'ATK', '', 0, 0, 0),
(15, 3, 'GAS LPG', '', 1, 0, 1),
(16, 3, 'PAKAN', '', 1, 0, 1),
(17, 3, 'LISTRIK', '', 1, 0, 1),
(18, 3, 'BENGKEL', '', 1, 0, 1),
(19, 3, 'BULU', '', 1, 0, 0),
(20, 3, 'KOPI', '', 1, 0, 1),
(21, 3, 'ASURANSI', '', 1, 0, 1),
(22, 3, 'THR', '', 1, 0, 1),
(23, 3, 'CICILAN', '', 1, 0, 1),
(24, 3, 'CICILAN ANDON', '', 1, 0, 0),
(25, 3, 'THR BROKER', '', 1, 0, 1),
(27, 3, 'FEE', '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_detail`
--

CREATE TABLE `pengeluaran_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_pengeluaran_akun_detail` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `bayar` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengeluaran_detail`
--

INSERT INTO `pengeluaran_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_pengeluaran_akun_detail`, `keterangan`, `bayar`) VALUES
(1, 'PLR20220226', 'PLR20220226001', 1, '', 240000),
(2, 'PLR20220226', 'PLR20220226002', 6, '', 195000),
(3, 'PLR20220226', 'PLR20220226003', 7, '', 280000),
(4, 'PLR20220226', 'PLR20220226004', 8, '', 20000),
(5, 'PLR20220226', 'PLR20220226005', 10, '', 20000),
(6, 'PLR20220226', 'PLR20220226006', 11, '', 53670.76),
(7, 'PLR20220226', 'PLR20220226007', 12, '', 29634.6416666667),
(8, 'PLR20220226', 'PLR20220226008', 14, '', 10000),
(9, 'PLR20220226', 'PLR20220226009', 15, '', 81866.675),
(10, 'PLR20220226', 'PLR20220226010', 16, '', 42000),
(11, 'PLR20220226', 'PLR20220226011', 17, '', 33510.4462),
(12, 'PLR20220226', 'PLR20220226012', 18, '', 28598.6020833333),
(13, 'PLR20220226', 'PLR20220226013', 19, '', 25000),
(14, 'PLR20220226', 'PLR20220226014', 20, '', 9899.065),
(15, 'PLR20220226', 'PLR20220226015', 21, '', 58354.4408333333),
(16, 'PLR20220226', 'PLR20220226016', 22, '', 140745),
(17, 'PLR20220226', 'PLR20220226017', 23, '', 140745),
(18, 'PLR20220226', 'PLR20220226018', 24, '', 120000),
(19, 'PLR20220226', 'PLR20220226019', 25, '', 160000);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tgl_jual` date NOT NULL,
  `status` enum('Tertunda','Selesai') NOT NULL,
  `total_ekor` int(11) NOT NULL,
  `total_kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_broker`
--

CREATE TABLE `penjualan_broker` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tgl_jual` date NOT NULL,
  `total_kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_broker_detail`
--

CREATE TABLE `penjualan_broker_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_broker` int(11) NOT NULL,
  `harga` double NOT NULL,
  `kg` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_broker_detail_supplier`
--

CREATE TABLE `penjualan_broker_detail_supplier` (
  `id` int(11) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_broker_pembayaran`
--

CREATE TABLE `penjualan_broker_pembayaran` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_piutang` double NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_broker_pembayaran_detail`
--

CREATE TABLE `penjualan_broker_pembayaran_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_broker` int(11) NOT NULL,
  `piutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_checker`
--

CREATE TABLE `penjualan_checker` (
  `id` int(11) NOT NULL,
  `no_checker` char(30) NOT NULL,
  `no_transaksi_penjualan` char(30) NOT NULL,
  `tgl_check` date DEFAULT NULL,
  `total_ekor` int(11) NOT NULL,
  `total_kg` double NOT NULL,
  `sisa_ekor` int(11) DEFAULT NULL,
  `sisa_kg` double DEFAULT NULL,
  `mati_ekor` int(11) DEFAULT NULL,
  `mati_kg` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_checker_detail`
--

CREATE TABLE `penjualan_checker_detail` (
  `id` int(11) NOT NULL,
  `no_checker` char(30) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `ekor` int(11) NOT NULL,
  `kg` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `harga` double NOT NULL,
  `ekor` int(11) NOT NULL,
  `kg` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_pembayaran`
--

CREATE TABLE `penjualan_pembayaran` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_piutang` double NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_pembayaran_detail`
--

CREATE TABLE `penjualan_pembayaran_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `piutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_susut`
--

CREATE TABLE `penjualan_susut` (
  `id` int(11) NOT NULL,
  `no_checker` char(30) NOT NULL,
  `ekor` int(11) DEFAULT NULL,
  `presentase_ekor` double DEFAULT NULL,
  `kg` double DEFAULT NULL,
  `presentase_kg` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `saldo_awal`
--

CREATE TABLE `saldo_awal` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `total` double NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `saldo_awal`
--

INSERT INTO `saldo_awal` (`id`, `no_transaksi`, `tanggal`, `total`, `keterangan`) VALUES
(1, 'SAL20220226', '2022-02-26', 1648, '');

-- --------------------------------------------------------

--
-- Table structure for table `saldo_awal_detail`
--

CREATE TABLE `saldo_awal_detail` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_pengeluaran_akun_detail` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `bayar` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `saldo_awal_detail`
--

INSERT INTO `saldo_awal_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_pengeluaran_akun_detail`, `keterangan`, `bayar`) VALUES
(1, 'SAL20220226', 'SAL20220226001', 1, '', 66.6666666666667),
(2, 'SAL20220226', 'SAL20220226002', 2, '', 83.3333333333333),
(3, 'SAL20220226', 'SAL20220226003', 3, '', 25),
(4, 'SAL20220226', 'SAL20220226004', 4, '', 54.1666666666667),
(5, 'SAL20220226', 'SAL20220226005', 5, '', 54.1666666666667),
(6, 'SAL20220226', 'SAL20220226006', 11, '', 57.2),
(7, 'SAL20220226', 'SAL20220226007', 12, '', 31.5833333333333),
(8, 'SAL20220226', 'SAL20220226008', 15, '', 87.25),
(9, 'SAL20220226', 'SAL20220226009', 17, '', 35.714),
(10, 'SAL20220226', 'SAL20220226010', 18, '', 30.4791666666667),
(11, 'SAL20220226', 'SAL20220226011', 20, '', 10.55),
(12, 'SAL20220226', 'SAL20220226012', 21, '', 62.1916666666667),
(13, 'SAL20220226', 'SAL20220226013', 22, '', 150),
(14, 'SAL20220226', 'SAL20220226014', 23, '', 150),
(15, 'SAL20220226', 'SAL20220226015', 24, '', 300),
(16, 'SAL20220226', 'SAL20220226016', 25, '', 50),
(17, 'SAL20220226', 'SAL20220226017', 26, '', 400);

-- --------------------------------------------------------

--
-- Table structure for table `sisa_stok`
--

CREATE TABLE `sisa_stok` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_penjualan_checker` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `ekor` int(11) NOT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sisa_stok`
--

INSERT INTO `sisa_stok` (`id`, `no_transaksi`, `no_penjualan_checker`, `tanggal`, `ekor`, `kg`, `harga`, `jumlah`) VALUES
(1, 'STK20220225', 'CKJ20220225', '2022-02-25', 261, 347, 18453, 6403191);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `hutang_supplier` double NOT NULL,
  `deposit_supplier` double NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `hutang_supplier`, `deposit_supplier`, `avatar`) VALUES
(1, 'CIOMAS', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(2, 'NEWHOPE 1', '', NULL, 'a@gmail.com', NULL, 0, 0.0000100024044513702, '6118b2a943acc278631959.png'),
(3, 'NEWHOPE 2', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(4, 'MSP POKPHAND', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(5, 'AWAROO / TONO', '', NULL, 'a@gmail.com', NULL, 0, 124470000, '6118b2a943acc278631959.png'),
(6, 'ASR', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(7, 'ASPM', '', NULL, 'a@gmail.com', NULL, 0, 0.350000001490116, '6118b2a943acc278631959.png'),
(8, 'ASPC', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(9, 'AMANAH', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(10, 'KMN / MIFTAHUDIN', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(11, 'SEMESTA', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(12, 'BINUANG / NONO', '', NULL, 'a@gmail.com', NULL, 0, 4000000, '6118b2a943acc278631959.png'),
(13, 'SIDO AGUNG', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(14, 'MPU IDM 192', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(15, 'MPU PPT 332', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(16, 'MPU CRB 818', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(17, 'BRU MJK 180', '', NULL, 'a@gmail.com', NULL, 0, 0, '6118b2a943acc278631959.png'),
(18, 'K A M I / VINCENT', '', NULL, 'a@gmail.com', NULL, 0, 0.00011499971151352, '6118b2a943acc278631959.png'),
(19, 'tset suplier', '', '08124545', 'a@gmail.com', '-', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tersedia_stok`
--

CREATE TABLE `tersedia_stok` (
  `id` int(11) NOT NULL,
  `no_transaksi` char(30) NOT NULL,
  `no_penerimaan_checker` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `ekor` int(11) NOT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tersedia_stok`
--

INSERT INTO `tersedia_stok` (`id`, `no_transaksi`, `no_penerimaan_checker`, `tanggal`, `ekor`, `kg`, `harga`, `jumlah`) VALUES
(1, 'STM20231130', 'CKB20231130', '2023-11-30', 451, 872, 16758.63725490196, 18803191);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('Operator','Administrator') NOT NULL,
  `avatar` varchar(32) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `address`, `username`, `password`, `role`, `avatar`, `status`, `created_at`, `last_login`) VALUES
(1, 'Administrator', '', 'admin@mail.com', '', 'admin', '$2y$10$hRi1qju2KOeEPcBZ0wYfhu/PN5e9Wl.ddWeDTds8Uokad764X9D1a', 'Administrator', '1.png', 1, '2021-08-14 23:22:33', '2023-11-27 22:51:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `broker`
--
ALTER TABLE `broker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kandang`
--
ALTER TABLE `kandang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_pembayaran`
--
ALTER TABLE `pembelian_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_pembayaran_detail`
--
ALTER TABLE `pembelian_pembayaran_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaan_checker`
--
ALTER TABLE `penerimaan_checker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_transaksi_penerimaan` (`no_transaksi_penerimaan`);

--
-- Indexes for table `penerimaan_checker_detail`
--
ALTER TABLE `penerimaan_checker_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaan_susut`
--
ALTER TABLE `penerimaan_susut`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_transaksi_penerimaan` (`no_transaksi_penerimaan`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_akun`
--
ALTER TABLE `pengeluaran_akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_akun_detail`
--
ALTER TABLE `pengeluaran_akun_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_detail`
--
ALTER TABLE `pengeluaran_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_broker`
--
ALTER TABLE `penjualan_broker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_broker_detail`
--
ALTER TABLE `penjualan_broker_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_broker_detail_supplier`
--
ALTER TABLE `penjualan_broker_detail_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_broker_pembayaran`
--
ALTER TABLE `penjualan_broker_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_broker_pembayaran_detail`
--
ALTER TABLE `penjualan_broker_pembayaran_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_checker`
--
ALTER TABLE `penjualan_checker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_checker_detail`
--
ALTER TABLE `penjualan_checker_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_pembayaran`
--
ALTER TABLE `penjualan_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_pembayaran_detail`
--
ALTER TABLE `penjualan_pembayaran_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_susut`
--
ALTER TABLE `penjualan_susut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saldo_awal`
--
ALTER TABLE `saldo_awal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saldo_awal_detail`
--
ALTER TABLE `saldo_awal_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sisa_stok`
--
ALTER TABLE `sisa_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tersedia_stok`
--
ALTER TABLE `tersedia_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `broker`
--
ALTER TABLE `broker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `kandang`
--
ALTER TABLE `kandang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pembelian_pembayaran`
--
ALTER TABLE `pembelian_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pembelian_pembayaran_detail`
--
ALTER TABLE `pembelian_pembayaran_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penerimaan_checker`
--
ALTER TABLE `penerimaan_checker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penerimaan_checker_detail`
--
ALTER TABLE `penerimaan_checker_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `penerimaan_susut`
--
ALTER TABLE `penerimaan_susut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengeluaran_akun`
--
ALTER TABLE `pengeluaran_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pengeluaran_akun_detail`
--
ALTER TABLE `pengeluaran_akun_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pengeluaran_detail`
--
ALTER TABLE `pengeluaran_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_broker`
--
ALTER TABLE `penjualan_broker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_broker_detail`
--
ALTER TABLE `penjualan_broker_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_broker_detail_supplier`
--
ALTER TABLE `penjualan_broker_detail_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_broker_pembayaran`
--
ALTER TABLE `penjualan_broker_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_broker_pembayaran_detail`
--
ALTER TABLE `penjualan_broker_pembayaran_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_checker`
--
ALTER TABLE `penjualan_checker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_checker_detail`
--
ALTER TABLE `penjualan_checker_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_pembayaran`
--
ALTER TABLE `penjualan_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_pembayaran_detail`
--
ALTER TABLE `penjualan_pembayaran_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_susut`
--
ALTER TABLE `penjualan_susut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `saldo_awal`
--
ALTER TABLE `saldo_awal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `saldo_awal_detail`
--
ALTER TABLE `saldo_awal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sisa_stok`
--
ALTER TABLE `sisa_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tersedia_stok`
--
ALTER TABLE `tersedia_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
