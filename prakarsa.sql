-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for prakarsa_putra
CREATE DATABASE IF NOT EXISTS `prakarsa_putra` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `prakarsa_putra`;

-- Dumping structure for table prakarsa_putra.broker
CREATE TABLE IF NOT EXISTS `broker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` char(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text,
  `piutang_broker` double NOT NULL,
  `deposit_broker` double NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.broker: ~8 rows (approximately)
INSERT INTO `broker` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `piutang_broker`, `deposit_broker`, `avatar`) VALUES
	(1, 'RAHMAT', '', NULL, NULL, NULL, 8800, 0, NULL),
	(2, 'MARIAH', '', NULL, NULL, NULL, 0, 0, NULL),
	(3, 'IWAN', '', NULL, NULL, NULL, 12679999.999959998, 0, NULL),
	(4, 'KARTA', '', NULL, NULL, NULL, 10063699.999601394, 0, NULL),
	(5, 'MUANA .S', '', NULL, NULL, NULL, 1327000, 0, NULL),
	(6, 'ELI JUNAERI H', '', NULL, NULL, NULL, 310000, 0, NULL),
	(7, 'WARSO', '', NULL, NULL, NULL, 0, 0, NULL),
	(8, 'ILHAM ADITYA. R', '', NULL, NULL, NULL, 0, 0, NULL);

-- Dumping structure for table prakarsa_putra.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` char(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text,
  `piutang_customer` double NOT NULL,
  `deposit_customer` double NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.customers: ~40 rows (approximately)
INSERT INTO `customers` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `piutang_customer`, `deposit_customer`, `avatar`) VALUES
	(1, 'B. ENTUM', '', NULL, NULL, NULL, 0, 50214.0000126196, NULL),
	(2, 'P. GEMA', '', NULL, NULL, NULL, 1475390, 0, NULL),
	(3, 'B. ANDON (B)', '', NULL, NULL, NULL, 44462400, 0, NULL),
	(4, 'B. ANARTI (K)', '', NULL, NULL, NULL, 0, 0, NULL),
	(5, 'HAMZAH', '', NULL, NULL, NULL, 4272120, 0, NULL),
	(6, 'KANTOR', '', NULL, NULL, NULL, 0, 7632490, NULL),
	(7, 'P. HASAN', '', NULL, NULL, NULL, 2024710, 0, NULL),
	(8, 'P. CECE', '', NULL, NULL, NULL, 300610, 0, NULL),
	(9, 'HARDI/BAKAR', '', NULL, NULL, NULL, 203790, 0, NULL),
	(10, 'P. OYO', '', NULL, NULL, NULL, 3741150, 0, NULL),
	(11, 'P. ALIF', '', NULL, NULL, NULL, 0, 20170.0000000001, NULL),
	(12, 'MG. DAWANG', '', NULL, NULL, NULL, 400, 0, NULL),
	(13, 'P. ASEP', '', NULL, NULL, NULL, 1145290, 0, NULL),
	(14, 'P. DEDI', '', NULL, NULL, NULL, 0, 1820, NULL),
	(15, 'B. NENENG', '', NULL, NULL, NULL, 230380, 0, NULL),
	(16, 'B. KESIH', '', NULL, NULL, NULL, 18540, 0, NULL),
	(17, 'P. EDI ', '', NULL, NULL, NULL, 1061010, 0, NULL),
	(18, 'B. RAHMAT', '', NULL, NULL, NULL, 0, 0, NULL),
	(19, 'ADE MIQDAD', '', NULL, NULL, NULL, 0, 0, NULL),
	(20, 'E ', '', NULL, NULL, NULL, 89730, 0, NULL),
	(21, 'B. KIKI', '', NULL, NULL, NULL, 130260, 0, NULL),
	(22, 'B. AGUNG', '', NULL, NULL, NULL, 0, 0, NULL),
	(23, 'ELVAS', '', NULL, NULL, NULL, 518000, 0, NULL),
	(24, 'P. TARWIN', '', NULL, NULL, NULL, 37000, 0, NULL),
	(25, 'w. KARTA', '', NULL, NULL, NULL, 0, 1330, NULL),
	(26, 'JATISURA', '', NULL, NULL, NULL, 0, 5470, NULL),
	(27, 'BADOL/ ATENG', '', NULL, NULL, NULL, 40290, 0, NULL),
	(28, 'CIKEUN ABANG', '', NULL, NULL, NULL, 4008290, 0, NULL),
	(29, 'A-B', '', NULL, NULL, NULL, 348940, 0, NULL),
	(30, 'NURBAETI', '', NULL, NULL, NULL, 2109.9999999998836, 0, NULL),
	(31, 'JATIWANGI', '', NULL, NULL, NULL, 0, 1760, NULL),
	(32, 'M. UHA', '', NULL, NULL, NULL, 103469.99999999994, 0, NULL),
	(33, 'B. INDRA/LAGIS/GEPUK', '', NULL, NULL, NULL, 730, 0, NULL),
	(34, 'KHOLIK', '', NULL, NULL, NULL, 6891090, 0, NULL),
	(35, 'B. YULI', '', NULL, NULL, NULL, 26930, 0, NULL),
	(36, 'RAKA', '', NULL, NULL, NULL, 6330, 0, NULL),
	(37, 'M. UJANG', '', NULL, NULL, NULL, 57840, 0, NULL),
	(38, 'H.ANDIR', '', NULL, NULL, NULL, 89850, 0, NULL),
	(39, 'A DEDI', '', NULL, NULL, NULL, 0, 0, NULL),
	(40, 'P. TOHA', '', NULL, NULL, NULL, 0, 0, NULL);

-- Dumping structure for table prakarsa_putra.kandang
CREATE TABLE IF NOT EXISTS `kandang` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` text,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.kandang: ~3 rows (approximately)
INSERT INTO `kandang` (`id`, `nama`, `alamat`, `keterangan`) VALUES
	(1, 'Kandang A', '', ''),
	(2, 'Kandang B', '', ''),
	(3, 'Kandang C', '', '');

-- Dumping structure for table prakarsa_putra.pembelian
CREATE TABLE IF NOT EXISTS `pembelian` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tgl_beli` date NOT NULL,
  `status` enum('Tertunda','Selesai') NOT NULL,
  `ekor` int DEFAULT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `total_hutang` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pembelian: ~4 rows (approximately)
INSERT INTO `pembelian` (`id`, `no_transaksi`, `tgl_beli`, `status`, `ekor`, `kg`, `harga`, `total_hutang`) VALUES
	(1, 'PBN20240205', '2024-02-05', 'Selesai', 120, 280, 20000, 5600000),
	(2, 'PBN20240208', '2024-02-08', 'Selesai', 11, 22, 20000, 440000),
	(3, 'PBN20240327', '2024-03-27', 'Selesai', 700, 700, 22000, 15400000),
	(4, 'PBN20240328', '2024-03-28', 'Selesai', 800, 800, 22000, 17600000),
	(5, 'PBN20240521', '2024-05-21', 'Selesai', 100, 100, 10000, 1000000);

-- Dumping structure for table prakarsa_putra.pembelian_broker
CREATE TABLE IF NOT EXISTS `pembelian_broker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  `tgl_beli` date NOT NULL,
  `status` enum('Tertunda','Selesai') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `total_hutang` decimal(10,0) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table prakarsa_putra.pembelian_broker: ~4 rows (approximately)
INSERT INTO `pembelian_broker` (`id`, `no_transaksi`, `tgl_beli`, `status`, `kg`, `harga`, `total_hutang`, `created_on`) VALUES
	(17, 'PBN202402050002', '2024-02-05', 'Tertunda', 100000, 100000, 9999999999, '2024-02-05 09:51:45'),
	(18, 'PBN202402050018', '2024-02-05', 'Tertunda', 10, 100, 1000, '2024-02-05 09:52:17'),
	(19, 'PBN202402050019', '2024-02-05', 'Tertunda', 10, 23000, 230000, '2024-02-05 09:53:11'),
	(20, 'PBN2024020520', '2024-02-05', 'Tertunda', 1000, 20000, 20000000, '2024-02-05 17:34:23'),
	(21, 'PBN2024020801', '2024-02-08', 'Tertunda', 220, 20545.454545454544, 4520000, '2024-02-08 01:47:48'),
	(22, 'PBN2024032701', '2024-03-27', 'Tertunda', 1200, 22037, 26444400, '2024-03-27 04:54:53');

-- Dumping structure for table prakarsa_putra.pembelian_broker_detail
CREATE TABLE IF NOT EXISTS `pembelian_broker_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `no_trxdetail` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_supplier` int NOT NULL,
  `hrg_ayam` decimal(10,0) DEFAULT NULL,
  `ekor` int DEFAULT NULL,
  `kg` double DEFAULT NULL,
  `subtotal` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table prakarsa_putra.pembelian_broker_detail: ~17 rows (approximately)
INSERT INTO `pembelian_broker_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_supplier`, `hrg_ayam`, `ekor`, `kg`, `subtotal`) VALUES
	(3, 'PBN20240205-01', 'PBN20240205001', 7, 0, NULL, 23000, 0),
	(4, 'PBN20240205-01', 'PBN20240205001', 8, 0, NULL, 25000, 0),
	(5, 'PBN20240205-01', 'PBN20240205001', 9, 0, NULL, 25000, 0),
	(6, 'PBN20240205-01', 'PBN20240205001', 6, 1000, NULL, 100, 100000),
	(7, 'PBN20240205-01', 'PBN20240205001', 5, 21000, NULL, 1000, 21000000),
	(8, 'PBN20240205-01', 'PBN20240205001', 5, 21500, NULL, 1000, 21500000),
	(9, 'PBN202402050001', 'PBN20240205001', 7, 21000, NULL, 100, 2100000),
	(10, 'PBN202402050001', 'PBN20240205001', 9, 24300, NULL, 100, 2430000),
	(11, 'PBN202402050001', 'PBN20240205001', 8, 1000, NULL, 10, 10000),
	(12, 'PBN202402050010', 'PBN20240205001', 6, 1, NULL, 1, 1),
	(13, 'PBN202402050002', 'PBN20240205001', 17, 21000, NULL, 2, 42000),
	(14, 'PBN202402050002', 'PBN20240205001', 6, 1, NULL, 1, 1),
	(15, 'PBN202402050001', 'PBN20240205001', 6, 1, NULL, 1, 1),
	(16, 'PBN202402050002', 'PBN20240205001', 6, 2, NULL, 2, 4),
	(17, 'PBN202402050002', 'PBN20240205001', 6, 100000, NULL, 100000, 9999999999),
	(18, 'PBN202402050018', 'PBN20240205001', 7, 100, NULL, 10, 1000),
	(19, 'PBN202402050019', 'PBN20240205001', 11, 23000, NULL, 10, 230000),
	(20, 'PBN2024020520', 'PBN20240205001', 13, 20000, NULL, 1000, 20000000),
	(21, 'PBN2024020801', 'PBN20240208001', 7, 20000, NULL, 100, 2000000),
	(22, 'PBN2024020801', 'PBN20240208002', 12, 21000, NULL, 120, 2520000),
	(23, 'PBN2024032701', 'PBN20240327001', 9, 22000, NULL, 1000, 22000000),
	(24, 'PBN2024032701', 'PBN20240327002', 5, 22222, NULL, 200, 4444400);

-- Dumping structure for table prakarsa_putra.pembelian_detail
CREATE TABLE IF NOT EXISTS `pembelian_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int NOT NULL,
  `hrg_ayam` decimal(10,0) DEFAULT NULL,
  `ekor` int DEFAULT NULL,
  `kg` double DEFAULT NULL,
  `subtotal` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pembelian_detail: ~2 rows (approximately)
INSERT INTO `pembelian_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_supplier`, `hrg_ayam`, `ekor`, `kg`, `subtotal`) VALUES
	(1, 'PBN20240205', 'PBN20240205001', 2, 20000, 50, 100, 2000000),
	(2, 'PBN20240205', 'PBN20240205002', 12, 20000, 70, 180, 3600000),
	(3, 'PBN20240208', 'PBN20240208001', 11, 20000, 11, 22, 440000),
	(4, 'PBN20240328', 'PBN20240328001', 7, 22000, 800, 800, 17600000),
	(5, 'PBN20240521', 'PBN20240521001', 9, 10000, 100, 100, 1000000);

-- Dumping structure for table prakarsa_putra.pembelian_pembayaran
CREATE TABLE IF NOT EXISTS `pembelian_pembayaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `total_hutang` double NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pembelian_pembayaran: ~7 rows (approximately)
INSERT INTO `pembelian_pembayaran` (`id`, `no_transaksi`, `total_hutang`, `tgl_bayar`, `total_bayar`, `total_deposit`) VALUES
	(1, 'PBP20240205', 5600000, '2024-02-05', 400000, 4000000.0000100024),
	(2, 'PBP20240208', 440000, '2024-02-08', 0, 0),
	(3, 'PBP20240327', 15400000, '2024-03-27', 15400000, 0),
	(4, 'PBP20240327', 17600000, '2024-03-27', 17600000, 0.0000750012695789337),
	(5, 'PBP20240328', 17600000, '2024-03-28', 17600000, 0.350000001490116),
	(6, 'PBP20240521', 1000000, '2024-05-21', 1000000, 0),
	(7, 'PBP20240521', 100, '2024-05-21', 100, 0.350000001490116);

-- Dumping structure for table prakarsa_putra.pembelian_pembayaran_detail
CREATE TABLE IF NOT EXISTS `pembelian_pembayaran_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int NOT NULL,
  `hutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pembelian_pembayaran_detail: ~72 rows (approximately)
INSERT INTO `pembelian_pembayaran_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_supplier`, `hutang`, `bayar`, `deposit`) VALUES
	(1, 'PBP20240208', 'PBP20240208001', 1, 0, 0, 0.00010992586612701),
	(2, 'PBP20240208', 'PBP20240208002', 2, 0, 0, 0.00001000240445137),
	(3, 'PBP20240208', 'PBP20240208003', 3, 0, 0, 0),
	(4, 'PBP20240208', 'PBP20240208004', 4, 0, 0, 0),
	(5, 'PBP20240208', 'PBP20240208005', 5, 0, 0, 50000000),
	(6, 'PBP20240208', 'PBP20240208006', 6, 0, 0, 0),
	(7, 'PBP20240208', 'PBP20240208007', 7, 0, 0, 0.35000000149012),
	(8, 'PBP20240208', 'PBP20240208008', 8, 0, 0, 0),
	(9, 'PBP20240208', 'PBP20240208009', 9, 0, 0, 0),
	(10, 'PBP20240208', 'PBP20240208010', 10, 0, 0, 50000),
	(11, 'PBP20240208', 'PBP20240208011', 11, 440000, 0, 0),
	(12, 'PBP20240208', 'PBP20240208012', 12, 0, 0, 4000000),
	(13, 'PBP20240208', 'PBP20240208013', 13, 0, 0, 0),
	(14, 'PBP20240208', 'PBP20240208014', 14, 0, 0, 0.000075001269578934),
	(15, 'PBP20240208', 'PBP20240208015', 15, 0, 0, 0),
	(16, 'PBP20240208', 'PBP20240208016', 16, 0, 0, 0),
	(17, 'PBP20240208', 'PBP20240208017', 17, 0, 0, 0),
	(18, 'PBP20240208', 'PBP20240208018', 18, 0, 0, 0.00011499971151352),
	(19, 'PBP20240327', 'PBP20240327001', 1, 0, 0, 0.00010992586612701),
	(20, 'PBP20240327', 'PBP20240327002', 2, 0, 0, 0.00001000240445137),
	(21, 'PBP20240327', 'PBP20240327003', 3, 0, 0, 0),
	(22, 'PBP20240327', 'PBP20240327004', 4, 0, 0, 0),
	(23, 'PBP20240327', 'PBP20240327005', 5, 0, 0, 50000000),
	(24, 'PBP20240327', 'PBP20240327006', 6, 15400000, 15400000, 0),
	(25, 'PBP20240327', 'PBP20240327007', 7, 0, 0, 0.35000000149012),
	(26, 'PBP20240327', 'PBP20240327008', 8, 0, 0, 0),
	(27, 'PBP20240327', 'PBP20240327009', 9, 0, 0, 0),
	(28, 'PBP20240327', 'PBP20240327010', 10, 0, 0, 50000),
	(29, 'PBP20240327', 'PBP20240327011', 11, 440000, 0, 0),
	(30, 'PBP20240327', 'PBP20240327012', 12, 0, 0, 4000000),
	(31, 'PBP20240327', 'PBP20240327013', 13, 0, 0, 0),
	(32, 'PBP20240327', 'PBP20240327014', 14, 0, 0, 0.000075001269578934),
	(33, 'PBP20240327', 'PBP20240327015', 15, 0, 0, 0),
	(34, 'PBP20240327', 'PBP20240327016', 16, 0, 0, 0),
	(35, 'PBP20240327', 'PBP20240327017', 17, 0, 0, 0),
	(36, 'PBP20240327', 'PBP20240327018', 18, 0, 0, 0.00011499971151352),
	(37, 'PBP20240327', 'PBP20240327001', 1, 0, 0, 0.00010992586612701),
	(38, 'PBP20240327', 'PBP20240327002', 2, 0, 0, 0.00001000240445137),
	(39, 'PBP20240327', 'PBP20240327003', 3, 0, 0, 0),
	(40, 'PBP20240327', 'PBP20240327004', 4, 0, 0, 0),
	(41, 'PBP20240327', 'PBP20240327005', 5, 0, 0, 50000000),
	(42, 'PBP20240327', 'PBP20240327006', 6, 0, 0, 0),
	(43, 'PBP20240327', 'PBP20240327007', 7, 0, 0, 0.35000000149012),
	(44, 'PBP20240327', 'PBP20240327008', 8, 0, 0, 0),
	(45, 'PBP20240327', 'PBP20240327009', 9, 0, 0, 0),
	(46, 'PBP20240327', 'PBP20240327010', 10, 0, 0, 50000),
	(47, 'PBP20240327', 'PBP20240327011', 11, 440000, 0, 0),
	(48, 'PBP20240327', 'PBP20240327012', 12, 0, 0, 4000000),
	(49, 'PBP20240327', 'PBP20240327013', 13, 0, 0, 0),
	(50, 'PBP20240327', 'PBP20240327014', 14, 17600000, 17600000, 0.000075001269578934),
	(51, 'PBP20240327', 'PBP20240327015', 15, 0, 0, 0),
	(52, 'PBP20240327', 'PBP20240327016', 16, 0, 0, 0),
	(53, 'PBP20240327', 'PBP20240327017', 17, 0, 0, 0),
	(54, 'PBP20240327', 'PBP20240327018', 18, 0, 0, 0.00011499971151352),
	(55, 'PBP20240521', 'PBP20240521001', 1, 0, 0, 0.00010992586612701),
	(56, 'PBP20240521', 'PBP20240521002', 2, 0, 0, 0.00001000240445137),
	(57, 'PBP20240521', 'PBP20240521003', 3, 0, 0, 0),
	(58, 'PBP20240521', 'PBP20240521004', 4, 0, 0, 0),
	(59, 'PBP20240521', 'PBP20240521005', 5, 0, 0, 50000000),
	(60, 'PBP20240521', 'PBP20240521006', 6, 0, 0, 0),
	(61, 'PBP20240521', 'PBP20240521007', 7, 100, 100, 0.35000000149012),
	(62, 'PBP20240521', 'PBP20240521008', 8, 0, 0, 0),
	(63, 'PBP20240521', 'PBP20240521009', 9, 0, 0, 0),
	(64, 'PBP20240521', 'PBP20240521010', 10, 0, 0, 50000),
	(65, 'PBP20240521', 'PBP20240521011', 11, 440000, 0, 0),
	(66, 'PBP20240521', 'PBP20240521012', 12, 0, 0, 4000000),
	(67, 'PBP20240521', 'PBP20240521013', 13, 0, 0, 0),
	(68, 'PBP20240521', 'PBP20240521014', 14, 0, 0, 0.000075001269578934),
	(69, 'PBP20240521', 'PBP20240521015', 15, 0, 0, 0),
	(70, 'PBP20240521', 'PBP20240521016', 16, 0, 0, 0),
	(71, 'PBP20240521', 'PBP20240521017', 17, 0, 0, 0),
	(72, 'PBP20240521', 'PBP20240521018', 18, 0, 0, 0.00011499971151352);

-- Dumping structure for table prakarsa_putra.penerimaan
CREATE TABLE IF NOT EXISTS `penerimaan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_transaksi_pembelian` char(30) NOT NULL,
  `tgl_diterima` date NOT NULL,
  `bongkar_ekor` int DEFAULT NULL,
  `bongkar_kg` double DEFAULT NULL,
  `broker_kg` double NOT NULL,
  `kirim_ekor` int DEFAULT NULL,
  `kirim_kg` double DEFAULT NULL,
  `mati_ekor` int DEFAULT NULL,
  `mati_kg` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penerimaan: ~0 rows (approximately)
INSERT INTO `penerimaan` (`id`, `no_transaksi`, `no_transaksi_pembelian`, `tgl_diterima`, `bongkar_ekor`, `bongkar_kg`, `broker_kg`, `kirim_ekor`, `kirim_kg`, `mati_ekor`, `mati_kg`) VALUES
	(1, 'TRM20240205', 'PBN20240205', '2024-02-05', 70, 180, 0, 0, 0, 0, 0),
	(2, 'TRM20240208', 'PBN20240208', '2024-02-08', 11, 22, 0, 0, 0, 0, 0),
	(3, 'TRM20240327', 'PBN20240327', '2024-03-27', 700, 700, 0, 0, 0, 0, 0),
	(4, 'TRM20240329', 'PBN20240328', '2024-03-28', 800, 770, 0, 0, 0, 0, 0),
	(5, 'TRM20240521', 'PBN20240521', '2024-05-21', 100, 100, 0, 100, 100, 0, 0);

-- Dumping structure for table prakarsa_putra.penerimaan_checker
CREATE TABLE IF NOT EXISTS `penerimaan_checker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_checker` char(30) NOT NULL,
  `no_transaksi_penerimaan` char(30) NOT NULL,
  `total_ekor` int NOT NULL,
  `total_kg` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `no_transaksi_penerimaan` (`no_transaksi_penerimaan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penerimaan_checker: ~0 rows (approximately)
INSERT INTO `penerimaan_checker` (`id`, `no_checker`, `no_transaksi_penerimaan`, `total_ekor`, `total_kg`) VALUES
	(1, 'CKB20240205', 'TRM20240205', 70, 180),
	(2, 'CKB20240208', 'TRM20240208', 11, 22),
	(3, 'CKB20240327', 'TRM20240327', 700, 700),
	(4, 'CKB20240328', 'TRM20240329', 800, 770),
	(5, 'CKB20240521', 'TRM20240521', 100, 100);

-- Dumping structure for table prakarsa_putra.penerimaan_checker_detail
CREATE TABLE IF NOT EXISTS `penerimaan_checker_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_checker` char(30) NOT NULL,
  `id_kandang` int NOT NULL,
  `ekor` int NOT NULL,
  `kg` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penerimaan_checker_detail: ~38 rows (approximately)
INSERT INTO `penerimaan_checker_detail` (`id`, `no_checker`, `id_kandang`, `ekor`, `kg`) VALUES
	(1, 'CKB20240205', 1, 0, 0),
	(2, 'CKB20240205', 1, 0, 0),
	(3, 'CKB20240205', 2, 0, 0),
	(4, 'CKB20240205', 2, 0, 0),
	(5, 'CKB20240205', 3, 0, 0),
	(6, 'CKB20240205', 3, 0, 0),
	(7, 'CKB20240205', 4, 0, 0),
	(8, 'CKB20240205', 4, 0, 0),
	(9, 'CKB20240205', 5, 0, 0),
	(10, 'CKB20240205', 5, 0, 0),
	(11, 'CKB20240208', 1, 0, 0),
	(12, 'CKB20240208', 1, 0, 0),
	(13, 'CKB20240208', 2, 0, 0),
	(14, 'CKB20240208', 2, 0, 0),
	(15, 'CKB20240208', 3, 0, 0),
	(16, 'CKB20240208', 3, 0, 0),
	(17, 'CKB20240208', 4, 0, 0),
	(18, 'CKB20240208', 4, 0, 0),
	(19, 'CKB20240208', 5, 0, 0),
	(20, 'CKB20240208', 5, 0, 0),
	(21, 'CKB20240327', 1, 0, 0),
	(22, 'CKB20240327', 1, 0, 0),
	(23, 'CKB20240327', 2, 0, 0),
	(24, 'CKB20240327', 2, 0, 0),
	(25, 'CKB20240327', 3, 0, 0),
	(26, 'CKB20240327', 3, 0, 0),
	(27, 'CKB20240328', 1, 0, 0),
	(28, 'CKB20240328', 1, 0, 0),
	(29, 'CKB20240328', 2, 0, 0),
	(30, 'CKB20240328', 2, 0, 0),
	(31, 'CKB20240328', 3, 0, 0),
	(32, 'CKB20240328', 3, 0, 0),
	(33, 'CKB20240521', 1, 0, 0),
	(34, 'CKB20240521', 1, 0, 0),
	(35, 'CKB20240521', 2, 0, 0),
	(36, 'CKB20240521', 2, 0, 0),
	(37, 'CKB20240521', 3, 0, 0),
	(38, 'CKB20240521', 3, 0, 0);

-- Dumping structure for table prakarsa_putra.penerimaan_susut
CREATE TABLE IF NOT EXISTS `penerimaan_susut` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi_penerimaan` char(30) NOT NULL,
  `ekor` int NOT NULL,
  `presentase_ekor` double NOT NULL,
  `kg` double NOT NULL,
  `presentase_kg` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `no_transaksi_penerimaan` (`no_transaksi_penerimaan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penerimaan_susut: ~4 rows (approximately)
INSERT INTO `penerimaan_susut` (`id`, `no_transaksi_penerimaan`, `ekor`, `presentase_ekor`, `kg`, `presentase_kg`) VALUES
	(1, 'TRM20240205', 50, 0, 0, 0),
	(2, 'TRM20240208', 0, 0, 0, 0),
	(3, 'TRM20240327', 0, 0, 0, 0),
	(4, 'TRM20240329', 0, 0, 0, 0),
	(5, 'TRM20240521', -100, 0, 0, 0);

-- Dumping structure for table prakarsa_putra.pengeluaran
CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `total` double NOT NULL,
  `tot_setor_bank` double NOT NULL,
  `setoran_rpa` double NOT NULL,
  `setoran_broker` double NOT NULL,
  `setor_tabungan` double NOT NULL,
  `setor_modal_rpa` double NOT NULL,
  `setor_modal_total` double NOT NULL,
  `sisa_setoran` double NOT NULL,
  `output_pokok` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pengeluaran: ~0 rows (approximately)
INSERT INTO `pengeluaran` (`id`, `no_transaksi`, `tanggal`, `total`, `tot_setor_bank`, `setoran_rpa`, `setoran_broker`, `setor_tabungan`, `setor_modal_rpa`, `setor_modal_total`, `sisa_setoran`, `output_pokok`) VALUES
	(1, 'PLR20220226', '2022-02-26', 1689025, 54414300, 20194300, 34780000, 1129025, 18505275, 53285200, 75, 560000);

-- Dumping structure for table prakarsa_putra.pengeluaran_akun
CREATE TABLE IF NOT EXISTS `pengeluaran_akun` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `set_lock` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pengeluaran_akun: ~3 rows (approximately)
INSERT INTO `pengeluaran_akun` (`id`, `nama`, `keterangan`, `set_lock`) VALUES
	(1, 'MOBILISASI', '', 1),
	(2, 'PRODUKSI', '', 1),
	(3, 'KANTOR', '', 1);

-- Dumping structure for table prakarsa_putra.pengeluaran_akun_detail
CREATE TABLE IF NOT EXISTS `pengeluaran_akun_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_pengeluaran_akun` int NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `biaya_variabel` tinyint(1) NOT NULL,
  `saldo` double NOT NULL,
  `set_lock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pengeluaran_akun_detail: ~26 rows (approximately)
INSERT INTO `pengeluaran_akun_detail` (`id`, `id_pengeluaran_akun`, `nama`, `keterangan`, `biaya_variabel`, `saldo`, `set_lock`) VALUES
	(1, 1, 'SUPIR', '', 0, 0, 0),
	(2, 1, 'TKBM', '', 0, 0, 0),
	(3, 1, 'UANG MAKAN', '', 0, 0, 0),
	(4, 1, 'KAS', '', 0, 0, 0),
	(5, 1, 'BENSIN', '', 0, 0, 0),
	(6, 1, 'PEMELIHARAAN', '', 1, 0, 1),
	(7, 2, 'UPAH KERJA', '', 0, 0, 0),
	(8, 2, 'UANG MAKAN', '', 0, 0, 0),
	(9, 2, 'LEMBUR', '', 0, 0, 0),
	(10, 2, 'BENSIN', '', 0, 0, 0),
	(11, 2, 'PLASTIK', '', 1, 0, 1),
	(12, 2, 'PEMELIHARAAN', '', 1, 0, 1),
	(13, 3, 'UPAH KERJA', '', 0, 0, 1),
	(14, 3, 'ATK', '', 0, 0, 0),
	(15, 3, 'GAS LPG', '', 1, 0, 1),
	(16, 3, 'PAKAN', '', 1, 0, 1),
	(17, 3, 'LISTRIK', '', 1, 0, 1),
	(18, 3, 'BENGKEL', '', 1, 0, 1),
	(19, 3, 'BULU', '', 1, 0, 0),
	(20, 3, 'KOPI', '', 1, 0, 1),
	(21, 3, 'ASURANSI', '', 1, 0, 1),
	(22, 3, 'THR', '', 1, 0, 1),
	(23, 3, 'CICILAN', '', 1, 0, 1),
	(24, 3, 'CICILAN ANDON', '', 1, 0, 0),
	(25, 3, 'THR BROKER', '', 1, 0, 1),
	(27, 3, 'FEE', '', 0, 0, 1);

-- Dumping structure for table prakarsa_putra.pengeluaran_detail
CREATE TABLE IF NOT EXISTS `pengeluaran_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_pengeluaran_akun_detail` int NOT NULL,
  `keterangan` text NOT NULL,
  `bayar` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.pengeluaran_detail: ~19 rows (approximately)
INSERT INTO `pengeluaran_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_pengeluaran_akun_detail`, `keterangan`, `bayar`) VALUES
	(1, 'PLR20220226', 'PLR20220226001', 1, '', 240000),
	(2, 'PLR20220226', 'PLR20220226002', 6, '', 195000),
	(3, 'PLR20220226', 'PLR20220226003', 7, '', 280000),
	(4, 'PLR20220226', 'PLR20220226004', 8, '', 20000),
	(5, 'PLR20220226', 'PLR20220226005', 10, '', 20000),
	(6, 'PLR20220226', 'PLR20220226006', 11, '', 53670.76),
	(7, 'PLR20220226', 'PLR20220226007', 12, '', 29634.6416666667),
	(8, 'PLR20220226', 'PLR20220226008', 14, '', 10000),
	(9, 'PLR20220226', 'PLR20220226009', 15, '', 81866.675),
	(10, 'PLR20220226', 'PLR20220226010', 16, '', 42000),
	(11, 'PLR20220226', 'PLR20220226011', 17, '', 33510.4462),
	(12, 'PLR20220226', 'PLR20220226012', 18, '', 28598.6020833333),
	(13, 'PLR20220226', 'PLR20220226013', 19, '', 25000),
	(14, 'PLR20220226', 'PLR20220226014', 20, '', 9899.065),
	(15, 'PLR20220226', 'PLR20220226015', 21, '', 58354.4408333333),
	(16, 'PLR20220226', 'PLR20220226016', 22, '', 140745),
	(17, 'PLR20220226', 'PLR20220226017', 23, '', 140745),
	(18, 'PLR20220226', 'PLR20220226018', 24, '', 120000),
	(19, 'PLR20220226', 'PLR20220226019', 25, '', 160000);

-- Dumping structure for table prakarsa_putra.penjualan
CREATE TABLE IF NOT EXISTS `penjualan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tgl_jual` date NOT NULL,
  `status` enum('Tertunda','Selesai') NOT NULL,
  `total_ekor` int NOT NULL,
  `total_kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan: ~0 rows (approximately)
INSERT INTO `penjualan` (`id`, `no_transaksi`, `tgl_jual`, `status`, `total_ekor`, `total_kg`, `harga`, `jumlah`) VALUES
	(8, 'PJN20240422', '2024-04-22', 'Selesai', 10, 10, 10, 100);

-- Dumping structure for table prakarsa_putra.penjualan_broker
CREATE TABLE IF NOT EXISTS `penjualan_broker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tgl_jual` date NOT NULL,
  `total_kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_broker: ~0 rows (approximately)

-- Dumping structure for table prakarsa_putra.penjualan_broker_detail
CREATE TABLE IF NOT EXISTS `penjualan_broker_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_broker` int NOT NULL,
  `harga` double NOT NULL,
  `kg` double NOT NULL,
  `jumlah` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_broker_detail: ~0 rows (approximately)

-- Dumping structure for table prakarsa_putra.penjualan_broker_detail_supplier
CREATE TABLE IF NOT EXISTS `penjualan_broker_detail_supplier` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_trxdetail` char(30) NOT NULL,
  `id_supplier` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_broker_detail_supplier: ~0 rows (approximately)

-- Dumping structure for table prakarsa_putra.penjualan_broker_pembayaran
CREATE TABLE IF NOT EXISTS `penjualan_broker_pembayaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_piutang` double NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_broker_pembayaran: ~0 rows (approximately)

-- Dumping structure for table prakarsa_putra.penjualan_broker_pembayaran_detail
CREATE TABLE IF NOT EXISTS `penjualan_broker_pembayaran_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_broker` int NOT NULL,
  `piutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_broker_pembayaran_detail: ~0 rows (approximately)

-- Dumping structure for table prakarsa_putra.penjualan_checker
CREATE TABLE IF NOT EXISTS `penjualan_checker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_checker` char(30) NOT NULL,
  `no_transaksi_penjualan` char(30) NOT NULL,
  `tgl_check` date DEFAULT NULL,
  `total_ekor` int NOT NULL,
  `total_kg` double NOT NULL,
  `sisa_ekor` int DEFAULT NULL,
  `sisa_kg` double DEFAULT NULL,
  `mati_ekor` int DEFAULT NULL,
  `mati_kg` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_checker: ~0 rows (approximately)
INSERT INTO `penjualan_checker` (`id`, `no_checker`, `no_transaksi_penjualan`, `tgl_check`, `total_ekor`, `total_kg`, `sisa_ekor`, `sisa_kg`, `mati_ekor`, `mati_kg`) VALUES
	(1, 'CKJ20240205', 'PJN20240205', '2024-02-05', 100, 200, 0, 0, 0, 0),
	(2, 'CKJ20240327', 'PJN20240327', '2024-03-27', 30, 30, 0, 0, 0, 0),
	(3, 'CKJ20240521', 'PJN20240422', '2024-05-21', 10, 10, 0, 0, 0, 0);

-- Dumping structure for table prakarsa_putra.penjualan_checker_detail
CREATE TABLE IF NOT EXISTS `penjualan_checker_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_checker` char(30) NOT NULL,
  `id_kandang` int NOT NULL,
  `id_customer` int NOT NULL,
  `ekor` int NOT NULL,
  `kg` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_checker_detail: ~4 rows (approximately)
INSERT INTO `penjualan_checker_detail` (`id`, `no_checker`, `id_kandang`, `id_customer`, `ekor`, `kg`) VALUES
	(1, 'CKJ20240205', 1, 19, 100, 200),
	(2, 'CKJ20240327', 1, 19, 10, 10),
	(3, 'CKJ20240327', 1, 24, 10, 10),
	(4, 'CKJ20240327', 1, 40, 10, 10),
	(5, 'CKJ20240521', 1, 19, 10, 10);

-- Dumping structure for table prakarsa_putra.penjualan_detail
CREATE TABLE IF NOT EXISTS `penjualan_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_customer` int NOT NULL,
  `harga` double NOT NULL,
  `ekor` int NOT NULL,
  `kg` double NOT NULL,
  `jumlah` double NOT NULL,
  `category` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_detail: ~0 rows (approximately)
INSERT INTO `penjualan_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_customer`, `harga`, `ekor`, `kg`, `jumlah`, `category`) VALUES
	(7, 'PJN20240422', 'PJN20240422001', 19, 10, 10, 10, 100, 'brangkas');

-- Dumping structure for table prakarsa_putra.penjualan_pembayaran
CREATE TABLE IF NOT EXISTS `penjualan_pembayaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_piutang` double NOT NULL,
  `total_bayar` double NOT NULL,
  `total_deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_pembayaran: ~11 rows (approximately)
INSERT INTO `penjualan_pembayaran` (`id`, `no_transaksi`, `tgl_bayar`, `total_piutang`, `total_bayar`, `total_deposit`) VALUES
	(1, 'PJP20240205', '2024-02-05', 5673140, 5000000, 0),
	(2, 'PJP20240208', '2024-02-08', 82000, 82000, 0),
	(3, 'PJP20240208', '2024-02-08', 46000, 32610, 13390),
	(4, 'PJP20240327', '2024-03-27', 17148940, 16800000, 0),
	(5, 'PJP20240327', '2024-03-27', 4346280, 3000000, 0),
	(6, 'PJP20240328', '2024-03-28', 2500000, 2500000, 0),
	(7, 'PJP20240329', '2024-03-29', 845310, 599786, 50214),
	(8, 'PJP20240327', '2024-03-27', 1419024, 706814, 2070),
	(9, 'PJP20240422', '2024-04-22', 0, 0, 0),
	(10, 'PJP20240422', '2024-04-22', 0, 0, 0),
	(11, 'PJP20240422', '2024-04-22', 0, 0, 0),
	(12, 'PJP20240422', '2024-04-22', 0, 0, 0),
	(13, 'PJP20240422', '2024-04-22', 0, 0, 0);

-- Dumping structure for table prakarsa_putra.penjualan_pembayaran_detail
CREATE TABLE IF NOT EXISTS `penjualan_pembayaran_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_customer` int NOT NULL,
  `piutang` double NOT NULL,
  `bayar` double NOT NULL,
  `deposit` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_pembayaran_detail: ~361 rows (approximately)
INSERT INTO `penjualan_pembayaran_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_customer`, `piutang`, `bayar`, `deposit`) VALUES
	(1, 'PJP20240205', 'PJP20240205001', 1, 0, 0, 50214.00001262),
	(2, 'PJP20240205', 'PJP20240205002', 2, 1475390, 0, 0),
	(3, 'PJP20240205', 'PJP20240205003', 3, 44462400, 0, 0),
	(4, 'PJP20240205', 'PJP20240205004', 4, 0, 0, 0),
	(5, 'PJP20240205', 'PJP20240205005', 5, 4272120, 0, 0),
	(6, 'PJP20240205', 'PJP20240205006', 6, 0, 0, 7632490),
	(7, 'PJP20240205', 'PJP20240205007', 7, 2024710, 0, 0),
	(8, 'PJP20240205', 'PJP20240205008', 8, 300610, 0, 0),
	(9, 'PJP20240205', 'PJP20240205009', 9, 203790, 0, 0),
	(10, 'PJP20240205', 'PJP20240205010', 10, 3741150, 0, 0),
	(11, 'PJP20240205', 'PJP20240205011', 11, 0, 0, 20170),
	(12, 'PJP20240205', 'PJP20240205012', 12, 400, 0, 0),
	(13, 'PJP20240205', 'PJP20240205013', 13, 1145290, 0, 0),
	(14, 'PJP20240205', 'PJP20240205014', 14, 0, 0, 1820),
	(15, 'PJP20240205', 'PJP20240205015', 15, 230380, 0, 0),
	(16, 'PJP20240205', 'PJP20240205016', 16, 18540, 0, 0),
	(17, 'PJP20240205', 'PJP20240205017', 17, 1061010, 0, 0),
	(18, 'PJP20240205', 'PJP20240205018', 18, 0, 0, 13390),
	(19, 'PJP20240205', 'PJP20240205019', 19, 5673140, 5000000, 0),
	(20, 'PJP20240205', 'PJP20240205020', 20, 89730, 0, 0),
	(21, 'PJP20240205', 'PJP20240205021', 21, 130260, 0, 0),
	(22, 'PJP20240205', 'PJP20240205022', 22, 195310, 0, 0),
	(23, 'PJP20240205', 'PJP20240205023', 23, 518000, 0, 0),
	(24, 'PJP20240205', 'PJP20240205024', 24, 37000, 0, 0),
	(25, 'PJP20240205', 'PJP20240205025', 25, 0, 0, 1330),
	(26, 'PJP20240205', 'PJP20240205026', 26, 0, 0, 5470),
	(27, 'PJP20240205', 'PJP20240205027', 27, 40290, 0, 0),
	(28, 'PJP20240205', 'PJP20240205028', 28, 4008290, 0, 0),
	(29, 'PJP20240205', 'PJP20240205029', 29, 348940, 0, 0),
	(30, 'PJP20240205', 'PJP20240205030', 30, 2109.9999999999, 0, 0),
	(31, 'PJP20240205', 'PJP20240205031', 31, 0, 0, 1760),
	(32, 'PJP20240205', 'PJP20240205032', 32, 103470, 0, 0),
	(33, 'PJP20240205', 'PJP20240205033', 33, 730, 0, 0),
	(34, 'PJP20240205', 'PJP20240205034', 34, 6891090, 0, 0),
	(35, 'PJP20240205', 'PJP20240205035', 35, 26930, 0, 0),
	(36, 'PJP20240205', 'PJP20240205036', 36, 6330, 0, 0),
	(37, 'PJP20240205', 'PJP20240205037', 37, 57840, 0, 0),
	(38, 'PJP20240205', 'PJP20240205038', 38, 89850, 0, 0),
	(39, 'PJP20240205', 'PJP20240205039', 39, 82000, 0, 0),
	(40, 'PJP20240205', 'PJP20240205040', 40, 0, 0, 2070),
	(41, 'PJP20240208', 'PJP20240208001', 39, 82000, 82000, 0),
	(42, 'PJP20240208', 'PJP20240208001', 1, 0, 0, 50214.00001262),
	(43, 'PJP20240208', 'PJP20240208002', 2, 1475390, 0, 0),
	(44, 'PJP20240208', 'PJP20240208003', 3, 44462400, 0, 0),
	(45, 'PJP20240208', 'PJP20240208004', 4, 0, 0, 0),
	(46, 'PJP20240208', 'PJP20240208005', 5, 4272120, 0, 0),
	(47, 'PJP20240208', 'PJP20240208006', 6, 0, 0, 7632490),
	(48, 'PJP20240208', 'PJP20240208007', 7, 2024710, 0, 0),
	(49, 'PJP20240208', 'PJP20240208008', 8, 300610, 0, 0),
	(50, 'PJP20240208', 'PJP20240208009', 9, 203790, 0, 0),
	(51, 'PJP20240208', 'PJP20240208010', 10, 3741150, 0, 0),
	(52, 'PJP20240208', 'PJP20240208011', 11, 0, 0, 20170),
	(53, 'PJP20240208', 'PJP20240208012', 12, 400, 0, 0),
	(54, 'PJP20240208', 'PJP20240208013', 13, 1145290, 0, 0),
	(55, 'PJP20240208', 'PJP20240208014', 14, 0, 0, 1820),
	(56, 'PJP20240208', 'PJP20240208015', 15, 230380, 0, 0),
	(57, 'PJP20240208', 'PJP20240208016', 16, 18540, 0, 0),
	(58, 'PJP20240208', 'PJP20240208017', 17, 1061010, 0, 0),
	(59, 'PJP20240208', 'PJP20240208018', 18, 46000, 32610, 13390),
	(60, 'PJP20240208', 'PJP20240208019', 19, 673140, 0, 0),
	(61, 'PJP20240208', 'PJP20240208020', 20, 89730, 0, 0),
	(62, 'PJP20240208', 'PJP20240208021', 21, 130260, 0, 0),
	(63, 'PJP20240208', 'PJP20240208022', 22, 195310, 0, 0),
	(64, 'PJP20240208', 'PJP20240208023', 23, 518000, 0, 0),
	(65, 'PJP20240208', 'PJP20240208024', 24, 37000, 0, 0),
	(66, 'PJP20240208', 'PJP20240208025', 25, 0, 0, 1330),
	(67, 'PJP20240208', 'PJP20240208026', 26, 0, 0, 5470),
	(68, 'PJP20240208', 'PJP20240208027', 27, 40290, 0, 0),
	(69, 'PJP20240208', 'PJP20240208028', 28, 4008290, 0, 0),
	(70, 'PJP20240208', 'PJP20240208029', 29, 348940, 0, 0),
	(71, 'PJP20240208', 'PJP20240208030', 30, 2109.9999999999, 0, 0),
	(72, 'PJP20240208', 'PJP20240208031', 31, 0, 0, 1760),
	(73, 'PJP20240208', 'PJP20240208032', 32, 103470, 0, 0),
	(74, 'PJP20240208', 'PJP20240208033', 33, 730, 0, 0),
	(75, 'PJP20240208', 'PJP20240208034', 34, 6891090, 0, 0),
	(76, 'PJP20240208', 'PJP20240208035', 35, 26930, 0, 0),
	(77, 'PJP20240208', 'PJP20240208036', 36, 6330, 0, 0),
	(78, 'PJP20240208', 'PJP20240208037', 37, 57840, 0, 0),
	(79, 'PJP20240208', 'PJP20240208038', 38, 89850, 0, 0),
	(80, 'PJP20240208', 'PJP20240208039', 39, 0, 0, 0),
	(81, 'PJP20240208', 'PJP20240208040', 40, 0, 0, 2070),
	(82, 'PJP20240327', 'PJP20240327001', 1, 0, 0, 50214.00001262),
	(83, 'PJP20240327', 'PJP20240327002', 2, 1475390, 0, 0),
	(84, 'PJP20240327', 'PJP20240327003', 3, 44462400, 0, 0),
	(85, 'PJP20240327', 'PJP20240327004', 4, 0, 0, 0),
	(86, 'PJP20240327', 'PJP20240327005', 5, 4272120, 0, 0),
	(87, 'PJP20240327', 'PJP20240327006', 6, 0, 0, 7632490),
	(88, 'PJP20240327', 'PJP20240327007', 7, 2024710, 0, 0),
	(89, 'PJP20240327', 'PJP20240327008', 8, 300610, 0, 0),
	(90, 'PJP20240327', 'PJP20240327009', 9, 203790, 0, 0),
	(91, 'PJP20240327', 'PJP20240327010', 10, 3741150, 0, 0),
	(92, 'PJP20240327', 'PJP20240327011', 11, 0, 0, 20170),
	(93, 'PJP20240327', 'PJP20240327012', 12, 400, 0, 0),
	(94, 'PJP20240327', 'PJP20240327013', 13, 1145290, 0, 0),
	(95, 'PJP20240327', 'PJP20240327014', 14, 0, 0, 1820),
	(96, 'PJP20240327', 'PJP20240327015', 15, 230380, 0, 0),
	(97, 'PJP20240327', 'PJP20240327016', 16, 18540, 0, 0),
	(98, 'PJP20240327', 'PJP20240327017', 17, 1061010, 0, 0),
	(99, 'PJP20240327', 'PJP20240327018', 18, 0, 0, 0),
	(100, 'PJP20240327', 'PJP20240327019', 19, 673140, 0, 0),
	(101, 'PJP20240327', 'PJP20240327020', 20, 89730, 0, 0),
	(102, 'PJP20240327', 'PJP20240327021', 21, 130260, 0, 0),
	(103, 'PJP20240327', 'PJP20240327022', 22, 195310, 0, 0),
	(104, 'PJP20240327', 'PJP20240327023', 23, 518000, 0, 0),
	(105, 'PJP20240327', 'PJP20240327024', 24, 37000, 0, 0),
	(106, 'PJP20240327', 'PJP20240327025', 25, 0, 0, 1330),
	(107, 'PJP20240327', 'PJP20240327026', 26, 0, 0, 5470),
	(108, 'PJP20240327', 'PJP20240327027', 27, 40290, 0, 0),
	(109, 'PJP20240327', 'PJP20240327028', 28, 4008290, 0, 0),
	(110, 'PJP20240327', 'PJP20240327029', 29, 17148940, 16800000, 0),
	(111, 'PJP20240327', 'PJP20240327030', 30, 2109.9999999999, 0, 0),
	(112, 'PJP20240327', 'PJP20240327031', 31, 0, 0, 1760),
	(113, 'PJP20240327', 'PJP20240327032', 32, 103470, 0, 0),
	(114, 'PJP20240327', 'PJP20240327033', 33, 730, 0, 0),
	(115, 'PJP20240327', 'PJP20240327034', 34, 6891090, 0, 0),
	(116, 'PJP20240327', 'PJP20240327035', 35, 26930, 0, 0),
	(117, 'PJP20240327', 'PJP20240327036', 36, 6330, 0, 0),
	(118, 'PJP20240327', 'PJP20240327037', 37, 57840, 0, 0),
	(119, 'PJP20240327', 'PJP20240327038', 38, 89850, 0, 0),
	(120, 'PJP20240327', 'PJP20240327039', 39, 0, 0, 0),
	(121, 'PJP20240327', 'PJP20240327040', 40, 0, 0, 2070),
	(122, 'PJP20240327', 'PJP20240327001', 1, 0, 0, 50214.00001262),
	(123, 'PJP20240327', 'PJP20240327002', 2, 1475390, 0, 0),
	(124, 'PJP20240327', 'PJP20240327003', 3, 44462400, 0, 0),
	(125, 'PJP20240327', 'PJP20240327004', 4, 0, 0, 0),
	(126, 'PJP20240327', 'PJP20240327005', 5, 4272120, 0, 0),
	(127, 'PJP20240327', 'PJP20240327006', 6, 0, 0, 7632490),
	(128, 'PJP20240327', 'PJP20240327007', 7, 2024710, 0, 0),
	(129, 'PJP20240327', 'PJP20240327008', 8, 300610, 0, 0),
	(130, 'PJP20240327', 'PJP20240327009', 9, 203790, 0, 0),
	(131, 'PJP20240327', 'PJP20240327010', 10, 3741150, 0, 0),
	(132, 'PJP20240327', 'PJP20240327011', 11, 0, 0, 20170),
	(133, 'PJP20240327', 'PJP20240327012', 12, 400, 0, 0),
	(134, 'PJP20240327', 'PJP20240327013', 13, 1145290, 0, 0),
	(135, 'PJP20240327', 'PJP20240327014', 14, 0, 0, 1820),
	(136, 'PJP20240327', 'PJP20240327015', 15, 230380, 0, 0),
	(137, 'PJP20240327', 'PJP20240327016', 16, 18540, 0, 0),
	(138, 'PJP20240327', 'PJP20240327017', 17, 1061010, 0, 0),
	(139, 'PJP20240327', 'PJP20240327018', 18, 0, 0, 0),
	(140, 'PJP20240327', 'PJP20240327019', 19, 1173140, 500000, 0),
	(141, 'PJP20240327', 'PJP20240327020', 20, 89730, 0, 0),
	(142, 'PJP20240327', 'PJP20240327021', 21, 130260, 0, 0),
	(143, 'PJP20240327', 'PJP20240327022', 22, 195310, 0, 0),
	(144, 'PJP20240327', 'PJP20240327023', 23, 518000, 0, 0),
	(145, 'PJP20240327', 'PJP20240327024', 24, 37000, 0, 0),
	(146, 'PJP20240327', 'PJP20240327025', 25, 0, 0, 1330),
	(147, 'PJP20240327', 'PJP20240327026', 26, 0, 0, 5470),
	(148, 'PJP20240327', 'PJP20240327027', 27, 40290, 0, 0),
	(149, 'PJP20240327', 'PJP20240327028', 28, 4008290, 0, 0),
	(150, 'PJP20240327', 'PJP20240327029', 29, 348940, 0, 0),
	(151, 'PJP20240327', 'PJP20240327030', 30, 2109.9999999999, 0, 0),
	(152, 'PJP20240327', 'PJP20240327031', 31, 0, 0, 1760),
	(153, 'PJP20240327', 'PJP20240327032', 32, 103470, 0, 0),
	(154, 'PJP20240327', 'PJP20240327033', 33, 730, 0, 0),
	(155, 'PJP20240327', 'PJP20240327034', 34, 6891090, 0, 0),
	(156, 'PJP20240327', 'PJP20240327035', 35, 26930, 0, 0),
	(157, 'PJP20240327', 'PJP20240327036', 36, 6330, 0, 0),
	(158, 'PJP20240327', 'PJP20240327037', 37, 57840, 0, 0),
	(159, 'PJP20240327', 'PJP20240327038', 38, 89850, 0, 0),
	(160, 'PJP20240327', 'PJP20240327039', 39, 0, 0, 0),
	(161, 'PJP20240327', 'PJP20240327040', 40, 0, 0, 2070),
	(162, 'PJP20240327', 'PJP20240327001', 1, 0, 0, 50214.00001262),
	(163, 'PJP20240327', 'PJP20240327002', 2, 1475390, 0, 0),
	(164, 'PJP20240327', 'PJP20240327003', 3, 44462400, 0, 0),
	(165, 'PJP20240327', 'PJP20240327004', 4, 0, 0, 0),
	(166, 'PJP20240327', 'PJP20240327005', 5, 4272120, 0, 0),
	(167, 'PJP20240327', 'PJP20240327006', 6, 0, 0, 7632490),
	(168, 'PJP20240327', 'PJP20240327007', 7, 2024710, 0, 0),
	(169, 'PJP20240327', 'PJP20240327008', 8, 300610, 0, 0),
	(170, 'PJP20240327', 'PJP20240327009', 9, 203790, 0, 0),
	(171, 'PJP20240327', 'PJP20240327010', 10, 3741150, 0, 0),
	(172, 'PJP20240327', 'PJP20240327011', 11, 0, 0, 20170),
	(173, 'PJP20240327', 'PJP20240327012', 12, 400, 0, 0),
	(174, 'PJP20240327', 'PJP20240327013', 13, 1145290, 0, 0),
	(175, 'PJP20240327', 'PJP20240327014', 14, 0, 0, 1820),
	(176, 'PJP20240327', 'PJP20240327015', 15, 230380, 0, 0),
	(177, 'PJP20240327', 'PJP20240327016', 16, 18540, 0, 0),
	(178, 'PJP20240327', 'PJP20240327017', 17, 1061010, 0, 0),
	(179, 'PJP20240327', 'PJP20240327018', 18, 0, 0, 0),
	(180, 'PJP20240327', 'PJP20240327019', 19, 893140, 220000, 0),
	(181, 'PJP20240327', 'PJP20240327020', 20, 89730, 0, 0),
	(182, 'PJP20240327', 'PJP20240327021', 21, 130260, 0, 0),
	(183, 'PJP20240327', 'PJP20240327022', 22, 195310, 0, 0),
	(184, 'PJP20240327', 'PJP20240327023', 23, 518000, 0, 0),
	(185, 'PJP20240327', 'PJP20240327024', 24, 259220, 222220, 0),
	(186, 'PJP20240327', 'PJP20240327025', 25, 0, 0, 1330),
	(187, 'PJP20240327', 'PJP20240327026', 26, 0, 0, 5470),
	(188, 'PJP20240327', 'PJP20240327027', 27, 40290, 0, 0),
	(189, 'PJP20240327', 'PJP20240327028', 28, 4008290, 0, 0),
	(190, 'PJP20240327', 'PJP20240327029', 29, 348940, 0, 0),
	(191, 'PJP20240327', 'PJP20240327030', 30, 2109.9999999999, 0, 0),
	(192, 'PJP20240327', 'PJP20240327031', 31, 0, 0, 1760),
	(193, 'PJP20240327', 'PJP20240327032', 32, 103470, 0, 0),
	(194, 'PJP20240327', 'PJP20240327033', 33, 730, 0, 0),
	(195, 'PJP20240327', 'PJP20240327034', 34, 6891090, 0, 0),
	(196, 'PJP20240327', 'PJP20240327035', 35, 26930, 0, 0),
	(197, 'PJP20240327', 'PJP20240327036', 36, 6330, 0, 0),
	(198, 'PJP20240327', 'PJP20240327037', 37, 57840, 0, 0),
	(199, 'PJP20240327', 'PJP20240327038', 38, 89850, 0, 0),
	(200, 'PJP20240327', 'PJP20240327039', 39, 0, 0, 0),
	(201, 'PJP20240327', 'PJP20240327040', 40, 266664, 264594, 2070),
	(202, 'PJP20240422', 'PJP20240422001', 1, 0, 0, 50214.00001262),
	(203, 'PJP20240422', 'PJP20240422002', 2, 1475390, 0, 0),
	(204, 'PJP20240422', 'PJP20240422003', 3, 44462400, 0, 0),
	(205, 'PJP20240422', 'PJP20240422004', 4, 0, 0, 0),
	(206, 'PJP20240422', 'PJP20240422005', 5, 4272120, 0, 0),
	(207, 'PJP20240422', 'PJP20240422006', 6, 0, 0, 7632490),
	(208, 'PJP20240422', 'PJP20240422007', 7, 2024710, 0, 0),
	(209, 'PJP20240422', 'PJP20240422008', 8, 300610, 0, 0),
	(210, 'PJP20240422', 'PJP20240422009', 9, 203790, 0, 0),
	(211, 'PJP20240422', 'PJP20240422010', 10, 3741150, 0, 0),
	(212, 'PJP20240422', 'PJP20240422011', 11, 0, 0, 20170),
	(213, 'PJP20240422', 'PJP20240422012', 12, 400, 0, 0),
	(214, 'PJP20240422', 'PJP20240422013', 13, 1145290, 0, 0),
	(215, 'PJP20240422', 'PJP20240422014', 14, 0, 0, 1820),
	(216, 'PJP20240422', 'PJP20240422015', 15, 230380, 0, 0),
	(217, 'PJP20240422', 'PJP20240422016', 16, 18540, 0, 0),
	(218, 'PJP20240422', 'PJP20240422017', 17, 1061010, 0, 0),
	(219, 'PJP20240422', 'PJP20240422018', 18, 0, 0, 0),
	(220, 'PJP20240422', 'PJP20240422019', 19, 0, 0, 0),
	(221, 'PJP20240422', 'PJP20240422020', 20, 89730, 0, 0),
	(222, 'PJP20240422', 'PJP20240422021', 21, 130260, 0, 0),
	(223, 'PJP20240422', 'PJP20240422022', 22, 195310, 0, 0),
	(224, 'PJP20240422', 'PJP20240422023', 23, 518000, 0, 0),
	(225, 'PJP20240422', 'PJP20240422024', 24, 37000, 0, 0),
	(226, 'PJP20240422', 'PJP20240422025', 25, 0, 0, 1330),
	(227, 'PJP20240422', 'PJP20240422026', 26, 0, 0, 5470),
	(228, 'PJP20240422', 'PJP20240422027', 27, 40290, 0, 0),
	(229, 'PJP20240422', 'PJP20240422028', 28, 4008290, 0, 0),
	(230, 'PJP20240422', 'PJP20240422029', 29, 348940, 0, 0),
	(231, 'PJP20240422', 'PJP20240422030', 30, 2109.9999999999, 0, 0),
	(232, 'PJP20240422', 'PJP20240422031', 31, 0, 0, 1760),
	(233, 'PJP20240422', 'PJP20240422032', 32, 103470, 0, 0),
	(234, 'PJP20240422', 'PJP20240422033', 33, 730, 0, 0),
	(235, 'PJP20240422', 'PJP20240422034', 34, 6891090, 0, 0),
	(236, 'PJP20240422', 'PJP20240422035', 35, 26930, 0, 0),
	(237, 'PJP20240422', 'PJP20240422036', 36, 6330, 0, 0),
	(238, 'PJP20240422', 'PJP20240422037', 37, 57840, 0, 0),
	(239, 'PJP20240422', 'PJP20240422038', 38, 89850, 0, 0),
	(240, 'PJP20240422', 'PJP20240422039', 39, 0, 0, 0),
	(241, 'PJP20240422', 'PJP20240422040', 40, 0, 0, 0),
	(242, 'PJP20240422', 'PJP20240422001', 1, 0, 0, 50214.00001262),
	(243, 'PJP20240422', 'PJP20240422002', 2, 1475390, 0, 0),
	(244, 'PJP20240422', 'PJP20240422003', 3, 44462400, 0, 0),
	(245, 'PJP20240422', 'PJP20240422004', 4, 0, 0, 0),
	(246, 'PJP20240422', 'PJP20240422005', 5, 4272120, 0, 0),
	(247, 'PJP20240422', 'PJP20240422006', 6, 0, 0, 7632490),
	(248, 'PJP20240422', 'PJP20240422007', 7, 2024710, 0, 0),
	(249, 'PJP20240422', 'PJP20240422008', 8, 300610, 0, 0),
	(250, 'PJP20240422', 'PJP20240422009', 9, 203790, 0, 0),
	(251, 'PJP20240422', 'PJP20240422010', 10, 3741150, 0, 0),
	(252, 'PJP20240422', 'PJP20240422011', 11, 0, 0, 20170),
	(253, 'PJP20240422', 'PJP20240422012', 12, 400, 0, 0),
	(254, 'PJP20240422', 'PJP20240422013', 13, 1145290, 0, 0),
	(255, 'PJP20240422', 'PJP20240422014', 14, 0, 0, 1820),
	(256, 'PJP20240422', 'PJP20240422015', 15, 230380, 0, 0),
	(257, 'PJP20240422', 'PJP20240422016', 16, 18540, 0, 0),
	(258, 'PJP20240422', 'PJP20240422017', 17, 1061010, 0, 0),
	(259, 'PJP20240422', 'PJP20240422018', 18, 0, 0, 0),
	(260, 'PJP20240422', 'PJP20240422019', 19, 0, 0, 0),
	(261, 'PJP20240422', 'PJP20240422020', 20, 89730, 0, 0),
	(262, 'PJP20240422', 'PJP20240422021', 21, 130260, 0, 0),
	(263, 'PJP20240422', 'PJP20240422022', 22, 195310, 0, 0),
	(264, 'PJP20240422', 'PJP20240422023', 23, 518000, 0, 0),
	(265, 'PJP20240422', 'PJP20240422024', 24, 37000, 0, 0),
	(266, 'PJP20240422', 'PJP20240422025', 25, 0, 0, 1330),
	(267, 'PJP20240422', 'PJP20240422026', 26, 0, 0, 5470),
	(268, 'PJP20240422', 'PJP20240422027', 27, 40290, 0, 0),
	(269, 'PJP20240422', 'PJP20240422028', 28, 4008290, 0, 0),
	(270, 'PJP20240422', 'PJP20240422029', 29, 348940, 0, 0),
	(271, 'PJP20240422', 'PJP20240422030', 30, 2109.9999999999, 0, 0),
	(272, 'PJP20240422', 'PJP20240422031', 31, 0, 0, 1760),
	(273, 'PJP20240422', 'PJP20240422032', 32, 103470, 0, 0),
	(274, 'PJP20240422', 'PJP20240422033', 33, 730, 0, 0),
	(275, 'PJP20240422', 'PJP20240422034', 34, 6891090, 0, 0),
	(276, 'PJP20240422', 'PJP20240422035', 35, 26930, 0, 0),
	(277, 'PJP20240422', 'PJP20240422036', 36, 6330, 0, 0),
	(278, 'PJP20240422', 'PJP20240422037', 37, 57840, 0, 0),
	(279, 'PJP20240422', 'PJP20240422038', 38, 89850, 0, 0),
	(280, 'PJP20240422', 'PJP20240422039', 39, 0, 0, 0),
	(281, 'PJP20240422', 'PJP20240422040', 40, 0, 0, 0),
	(282, 'PJP20240422', 'PJP20240422001', 1, 0, 0, 50214.00001262),
	(283, 'PJP20240422', 'PJP20240422002', 2, 1475390, 0, 0),
	(284, 'PJP20240422', 'PJP20240422003', 3, 44462400, 0, 0),
	(285, 'PJP20240422', 'PJP20240422004', 4, 0, 0, 0),
	(286, 'PJP20240422', 'PJP20240422005', 5, 4272120, 0, 0),
	(287, 'PJP20240422', 'PJP20240422006', 6, 0, 0, 7632490),
	(288, 'PJP20240422', 'PJP20240422007', 7, 2024710, 0, 0),
	(289, 'PJP20240422', 'PJP20240422008', 8, 300610, 0, 0),
	(290, 'PJP20240422', 'PJP20240422009', 9, 203790, 0, 0),
	(291, 'PJP20240422', 'PJP20240422010', 10, 3741150, 0, 0),
	(292, 'PJP20240422', 'PJP20240422011', 11, 0, 0, 20170),
	(293, 'PJP20240422', 'PJP20240422012', 12, 400, 0, 0),
	(294, 'PJP20240422', 'PJP20240422013', 13, 1145290, 0, 0),
	(295, 'PJP20240422', 'PJP20240422014', 14, 0, 0, 1820),
	(296, 'PJP20240422', 'PJP20240422015', 15, 230380, 0, 0),
	(297, 'PJP20240422', 'PJP20240422016', 16, 18540, 0, 0),
	(298, 'PJP20240422', 'PJP20240422017', 17, 1061010, 0, 0),
	(299, 'PJP20240422', 'PJP20240422018', 18, 0, 0, 0),
	(300, 'PJP20240422', 'PJP20240422019', 19, 0, 0, 0),
	(301, 'PJP20240422', 'PJP20240422020', 20, 89730, 0, 0),
	(302, 'PJP20240422', 'PJP20240422021', 21, 130260, 0, 0),
	(303, 'PJP20240422', 'PJP20240422022', 22, 0, 0, 0),
	(304, 'PJP20240422', 'PJP20240422023', 23, 518000, 0, 0),
	(305, 'PJP20240422', 'PJP20240422024', 24, 37000, 0, 0),
	(306, 'PJP20240422', 'PJP20240422025', 25, 0, 0, 1330),
	(307, 'PJP20240422', 'PJP20240422026', 26, 0, 0, 5470),
	(308, 'PJP20240422', 'PJP20240422027', 27, 40290, 0, 0),
	(309, 'PJP20240422', 'PJP20240422028', 28, 4008290, 0, 0),
	(310, 'PJP20240422', 'PJP20240422029', 29, 348940, 0, 0),
	(311, 'PJP20240422', 'PJP20240422030', 30, 2109.9999999999, 0, 0),
	(312, 'PJP20240422', 'PJP20240422031', 31, 0, 0, 1760),
	(313, 'PJP20240422', 'PJP20240422032', 32, 103470, 0, 0),
	(314, 'PJP20240422', 'PJP20240422033', 33, 730, 0, 0),
	(315, 'PJP20240422', 'PJP20240422034', 34, 6891090, 0, 0),
	(316, 'PJP20240422', 'PJP20240422035', 35, 26930, 0, 0),
	(317, 'PJP20240422', 'PJP20240422036', 36, 6330, 0, 0),
	(318, 'PJP20240422', 'PJP20240422037', 37, 57840, 0, 0),
	(319, 'PJP20240422', 'PJP20240422038', 38, 89850, 0, 0),
	(320, 'PJP20240422', 'PJP20240422039', 39, 0, 0, 0),
	(321, 'PJP20240422', 'PJP20240422040', 40, 0, 0, 0),
	(322, 'PJP20240422', 'PJP20240422001', 1, 0, 0, 50214.00001262),
	(323, 'PJP20240422', 'PJP20240422002', 2, 1475390, 0, 0),
	(324, 'PJP20240422', 'PJP20240422003', 3, 44462400, 0, 0),
	(325, 'PJP20240422', 'PJP20240422004', 4, 0, 0, 0),
	(326, 'PJP20240422', 'PJP20240422005', 5, 4272120, 0, 0),
	(327, 'PJP20240422', 'PJP20240422006', 6, 0, 0, 7632490),
	(328, 'PJP20240422', 'PJP20240422007', 7, 2024710, 0, 0),
	(329, 'PJP20240422', 'PJP20240422008', 8, 300610, 0, 0),
	(330, 'PJP20240422', 'PJP20240422009', 9, 203790, 0, 0),
	(331, 'PJP20240422', 'PJP20240422010', 10, 3741150, 0, 0),
	(332, 'PJP20240422', 'PJP20240422011', 11, 0, 0, 20170),
	(333, 'PJP20240422', 'PJP20240422012', 12, 400, 0, 0),
	(334, 'PJP20240422', 'PJP20240422013', 13, 1145290, 0, 0),
	(335, 'PJP20240422', 'PJP20240422014', 14, 0, 0, 1820),
	(336, 'PJP20240422', 'PJP20240422015', 15, 230380, 0, 0),
	(337, 'PJP20240422', 'PJP20240422016', 16, 18540, 0, 0),
	(338, 'PJP20240422', 'PJP20240422017', 17, 1061010, 0, 0),
	(339, 'PJP20240422', 'PJP20240422018', 18, 0, 0, 0),
	(340, 'PJP20240422', 'PJP20240422019', 19, 0, 0, 0),
	(341, 'PJP20240422', 'PJP20240422020', 20, 89730, 0, 0),
	(342, 'PJP20240422', 'PJP20240422021', 21, 130260, 0, 0),
	(343, 'PJP20240422', 'PJP20240422022', 22, 0, 0, 0),
	(344, 'PJP20240422', 'PJP20240422023', 23, 518000, 0, 0),
	(345, 'PJP20240422', 'PJP20240422024', 24, 37000, 0, 0),
	(346, 'PJP20240422', 'PJP20240422025', 25, 0, 0, 1330),
	(347, 'PJP20240422', 'PJP20240422026', 26, 0, 0, 5470),
	(348, 'PJP20240422', 'PJP20240422027', 27, 40290, 0, 0),
	(349, 'PJP20240422', 'PJP20240422028', 28, 4008290, 0, 0),
	(350, 'PJP20240422', 'PJP20240422029', 29, 348940, 0, 0),
	(351, 'PJP20240422', 'PJP20240422030', 30, 2109.9999999999, 0, 0),
	(352, 'PJP20240422', 'PJP20240422031', 31, 0, 0, 1760),
	(353, 'PJP20240422', 'PJP20240422032', 32, 103470, 0, 0),
	(354, 'PJP20240422', 'PJP20240422033', 33, 730, 0, 0),
	(355, 'PJP20240422', 'PJP20240422034', 34, 6891090, 0, 0),
	(356, 'PJP20240422', 'PJP20240422035', 35, 26930, 0, 0),
	(357, 'PJP20240422', 'PJP20240422036', 36, 6330, 0, 0),
	(358, 'PJP20240422', 'PJP20240422037', 37, 57840, 0, 0),
	(359, 'PJP20240422', 'PJP20240422038', 38, 89850, 0, 0),
	(360, 'PJP20240422', 'PJP20240422039', 39, 0, 0, 0),
	(361, 'PJP20240422', 'PJP20240422040', 40, 0, 0, 0);

-- Dumping structure for table prakarsa_putra.penjualan_susut
CREATE TABLE IF NOT EXISTS `penjualan_susut` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_checker` char(30) NOT NULL,
  `ekor` int DEFAULT NULL,
  `presentase_ekor` double DEFAULT NULL,
  `kg` double DEFAULT NULL,
  `presentase_kg` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.penjualan_susut: ~0 rows (approximately)
INSERT INTO `penjualan_susut` (`id`, `no_checker`, `ekor`, `presentase_ekor`, `kg`, `presentase_kg`) VALUES
	(1, 'CKJ20240205', 0, NULL, 0, 0),
	(2, 'CKJ20240327', 0, NULL, 0, 0),
	(3, 'CKJ20240521', 0, NULL, 0, 0);

-- Dumping structure for table prakarsa_putra.saldo_awal
CREATE TABLE IF NOT EXISTS `saldo_awal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `total` double NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.saldo_awal: ~0 rows (approximately)
INSERT INTO `saldo_awal` (`id`, `no_transaksi`, `tanggal`, `total`, `keterangan`) VALUES
	(1, 'SAL20220226', '2022-02-26', 1648, '');

-- Dumping structure for table prakarsa_putra.saldo_awal_detail
CREATE TABLE IF NOT EXISTS `saldo_awal_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_trxdetail` char(30) NOT NULL,
  `id_pengeluaran_akun_detail` int NOT NULL,
  `keterangan` text NOT NULL,
  `bayar` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.saldo_awal_detail: ~17 rows (approximately)
INSERT INTO `saldo_awal_detail` (`id`, `no_transaksi`, `no_trxdetail`, `id_pengeluaran_akun_detail`, `keterangan`, `bayar`) VALUES
	(1, 'SAL20220226', 'SAL20220226001', 1, '', 66.6666666666667),
	(2, 'SAL20220226', 'SAL20220226002', 2, '', 83.3333333333333),
	(3, 'SAL20220226', 'SAL20220226003', 3, '', 25),
	(4, 'SAL20220226', 'SAL20220226004', 4, '', 54.1666666666667),
	(5, 'SAL20220226', 'SAL20220226005', 5, '', 54.1666666666667),
	(6, 'SAL20220226', 'SAL20220226006', 11, '', 57.2),
	(7, 'SAL20220226', 'SAL20220226007', 12, '', 31.5833333333333),
	(8, 'SAL20220226', 'SAL20220226008', 15, '', 87.25),
	(9, 'SAL20220226', 'SAL20220226009', 17, '', 35.714),
	(10, 'SAL20220226', 'SAL20220226010', 18, '', 30.4791666666667),
	(11, 'SAL20220226', 'SAL20220226011', 20, '', 10.55),
	(12, 'SAL20220226', 'SAL20220226012', 21, '', 62.1916666666667),
	(13, 'SAL20220226', 'SAL20220226013', 22, '', 150),
	(14, 'SAL20220226', 'SAL20220226014', 23, '', 150),
	(15, 'SAL20220226', 'SAL20220226015', 24, '', 300),
	(16, 'SAL20220226', 'SAL20220226016', 25, '', 50),
	(17, 'SAL20220226', 'SAL20220226017', 26, '', 400);

-- Dumping structure for table prakarsa_putra.sisa_stok
CREATE TABLE IF NOT EXISTS `sisa_stok` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_penjualan_checker` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `ekor` int NOT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.sisa_stok: ~0 rows (approximately)
INSERT INTO `sisa_stok` (`id`, `no_transaksi`, `no_penjualan_checker`, `tanggal`, `ekor`, `kg`, `harga`, `jumlah`) VALUES
	(1, 'STK20220225', 'CKJ20220225', '2022-02-25', 261, 347, 18453, 6403191);

-- Dumping structure for table prakarsa_putra.stok
CREATE TABLE IF NOT EXISTS `stok` (
  `id` int NOT NULL,
  `tanggal` date DEFAULT NULL,
  `ekor` int NOT NULL,
  `kg` int NOT NULL,
  `harga` int DEFAULT NULL,
  `jumlah` int DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.stok: ~0 rows (approximately)
INSERT INTO `stok` (`id`, `tanggal`, `ekor`, `kg`, `harga`, `jumlah`, `updated_on`) VALUES
	(0, '2024-02-05', 100, 200, 100000, 10000, '2024-02-05 13:27:27');

-- Dumping structure for table prakarsa_putra.suppliers
CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `singkatan` varchar(10) NOT NULL,
  `telefon` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text,
  `hutang_supplier` double NOT NULL,
  `deposit_supplier` double NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.suppliers: ~18 rows (approximately)
INSERT INTO `suppliers` (`id`, `nama`, `singkatan`, `telefon`, `email`, `alamat`, `hutang_supplier`, `deposit_supplier`, `avatar`) VALUES
	(1, 'CIOMAS', '', NULL, NULL, NULL, 0, 0.000109925866127014, NULL),
	(2, 'NEWHOPE 1', '', NULL, NULL, NULL, 0, 0.0000100024044513702, NULL),
	(3, 'NEWHOPE 2', '', NULL, NULL, NULL, 0, 0, NULL),
	(4, 'MSP POKPHAND', '', NULL, NULL, NULL, 0, 0, NULL),
	(5, 'AWAROO / TONO', '', NULL, NULL, NULL, 0, 50000000, NULL),
	(6, 'ASR', '', NULL, NULL, NULL, 0, 0, NULL),
	(7, 'ASPM', '', NULL, NULL, NULL, 0, 0.35000000149012, NULL),
	(8, 'ASPC', '', NULL, NULL, NULL, 0, 0, NULL),
	(9, 'AMANAH', '', NULL, NULL, NULL, 0, 0, NULL),
	(10, 'KMN / MIFTAHUDIN', '', NULL, NULL, NULL, 0, 50000, NULL),
	(11, 'SEMESTA', '', NULL, NULL, NULL, 440000, 0, NULL),
	(12, 'BINUANG / NONO', '', NULL, NULL, NULL, 0, 4000000, NULL),
	(13, 'SIDO AGUNG', '', NULL, NULL, NULL, 0, 0, NULL),
	(14, 'MPU IDM 192', '', NULL, NULL, NULL, 0, 0.000075001269578934, NULL),
	(15, 'MPU PPT 332', '', NULL, NULL, NULL, 0, 0, NULL),
	(16, 'MPU CRB 818', '', NULL, NULL, NULL, 0, 0, NULL),
	(17, 'BRU MJK 180', '', NULL, NULL, NULL, 0, 0, NULL),
	(18, 'K A M I / VINCENT', '', NULL, NULL, NULL, 0, 0.000114999711513519, NULL);

-- Dumping structure for table prakarsa_putra.tersedia_stok
CREATE TABLE IF NOT EXISTS `tersedia_stok` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_transaksi` char(30) NOT NULL,
  `no_penerimaan_checker` char(30) NOT NULL,
  `tanggal` date NOT NULL,
  `ekor` int NOT NULL,
  `kg` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table prakarsa_putra.tersedia_stok: ~0 rows (approximately)
INSERT INTO `tersedia_stok` (`id`, `no_transaksi`, `no_penerimaan_checker`, `tanggal`, `ekor`, `kg`, `harga`, `jumlah`) VALUES
	(1, 'STM20240205', 'CKB20240205', '2024-02-05', 170, 380, 11687.5, 5610000),
	(2, 'STM20240208', 'CKB20240208', '2024-02-08', 111, 222, 2027.027027027027, 450000),
	(3, 'STM20240327', 'CKB20240327', '2024-03-27', 800, 900, 17122.222222222223, 15410000),
	(4, 'STM20240521', 'CKB20240521', '2024-05-21', 200, 300, 3366.6666666666665, 1010000);

-- Dumping structure for table prakarsa_putra.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('Operator','Administrator') NOT NULL,
  `avatar` varchar(32) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table prakarsa_putra.user: ~0 rows (approximately)
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `address`, `username`, `password`, `role`, `avatar`, `status`, `created_at`, `last_login`) VALUES
	(1, 'Administrator', '', 'admin@mail.com', '', 'admin', '$2y$10$hRi1qju2KOeEPcBZ0wYfhu/PN5e9Wl.ddWeDTds8Uokad764X9D1a', 'Administrator', '1.png', 1, '2021-08-14 23:22:33', '2024-05-21 01:38:18');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
